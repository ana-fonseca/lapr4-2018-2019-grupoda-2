package isep.eapli.georef.BMS;

public class GeocodeRequest {

    private String culture;
    private String query;
    private String addressLine = "-";
    private String countryRegion = "-";
    private String district = "-";
    private String locality = "-";
    private String postalCode = "-";
    private String LatLng = "-";
    private String type = "-";
    private String radius = "-";
    private String travelMode = "-";

    private String bingMapsKey;

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getCulture() {
        return culture;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }
    
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setCountryRegion(String countryRegion) {
        this.countryRegion = countryRegion;
    }

    public String getCountryRegion() {
        return countryRegion;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return district;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLocality() {
        return locality;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setLatLng(String LatLng) {
        this.LatLng = LatLng;
    }

    public String getLatLng() {
        return LatLng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public void setKey(String bingMapsKey) {
        this.bingMapsKey = bingMapsKey;
    }

    public String getKey() {
        return bingMapsKey;
    }
}
