package isep.eapli.georef.BMS;

public enum TypeIdentifiers {
    EatDrink("EatDrink"),
    SeeDo("SeeDo"),
    Shop("Shop"),
    BanksAndCreditUnions("BanksAndCreditUnions"),
    Hospitals("Hospitals"),
    HotelsAndMotels("HotelsAndMotels"),
    Parking("Parking");

    TypeIdentifiers(final String placeType) {
        this.placeType = placeType;
    }

    private final String placeType;

    @Override
    public String toString() {
        return placeType;
    }
}
