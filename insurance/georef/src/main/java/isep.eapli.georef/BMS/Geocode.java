package isep.eapli.georef.BMS;

import com.google.maps.model.LatLng;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Geocode {

    private final String RestUrlBase = "http://dev.virtualearth.net/REST/v1/Locations";//?description={0}&input={1}&output={2}&key={3}";
    private final String RestUrlBaseCircularRegion = "https://dev.virtualearth.net/REST/v1/LocalSearch/";//?type={type}&userCircularMapView={lat,lon,radius}&key={BingMapsAPIKey}";
    private final String RestUrlBaseElevation = "https://dev.virtualearth.net/REST/v1/Elevation/SeaLevel";
    private final String RestUrlDistanceMatrix = "https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix";

    private final String urlSearch = "http://dev.virtualearth.net/REST/v1/Locations";

    /**
     * Returns the latitude and longitude of a place request using its postal
     * address using the Microsoft Bing Services generating the link to the xml
     * of the API
     *
     * @param reqP = the GeocodeRequest with the information about the request
     * place
     * @return a String with the latitude and longitude of the place
     * (latitude,longitude)
     * @throws java.lang.Exception in case the XML link is not generated
     * correctly
     *
     * Tiago Ribeiro (1170426)
     */
    public String GeocodeLocationWithAdress(GeocodeRequest reqP) throws Exception {
        String returnFinal = "";
        URL requestUrl = generateLinkXML(reqP);
        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        String inputLine;
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        while ((inputLine = br.readLine()) != null) {
            returnFinal = new ArrayList<>(getElementsFromXMLMBS(inputLine).keySet()).get(0);
        }
        br.close();
        inStream.close();

        return returnFinal;
    }

    /**
     * Returns the complete postal address of the chosen place under the format
     * of a list of Strings
     *
     * @param reqP = the GeocodeRequest with the information about the request
     * place
     * @return a list of String with the complete postal address, using the
     * following format: 1st line: Address line 2nd line: Postal code 3rd line:
     * Locality/City 4th line: District 5th line: Country
     * @throws java.lang.Exception in case the XML link is not generated
     * correctly
     *
     * Tiago Ribeiro (1170426)
     */
    public List<String> completePostalAddress(GeocodeRequest reqP) throws Exception {
        List<String> addressParts = new ArrayList<>();
        URL requestUrl = generateLinkXML(reqP);
        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        String inputLine;
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        while ((inputLine = br.readLine()) != null) {
            addressParts = fullPostalAddress(inputLine);
        }
        br.close();
        inStream.close();

        List<String> fullAddressParts = new ArrayList<>();
        fullAddressParts.add(reqP.getAddressLine());
        fullAddressParts.add(reqP.getPostalCode());
        fullAddressParts.add(reqP.getLocality());
        fullAddressParts.addAll(addressParts);

        return fullAddressParts;
    }

    /**
     * Reads the xml link formed previously, received as a String and returns
     * the complete postal address of a chosen location under the format of a
     * list of strings
     *
     * @param inputLine = the xml file under the format of a single String
     * @return a list of String with the complete postal address, using the
     * following format: 1st line: Address line 2nd line: Postal code 3rd line:
     * Locality/City 4th line: District 5th line: Country
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     *
     * Tiago Ribeiro (1170426)
     */
    private List<String> fullPostalAddress(String inputLine) throws ParserConfigurationException, SAXException, IOException {
        List<String> output = new ArrayList<>();
        inputLine = inputLine.substring(3, inputLine.length());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(inputLine));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("Resource");

        if (nodes.getLength() == 0) {
            nodes = doc.getElementsByTagName("Resources");
        }

        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList name = element.getElementsByTagName("Name");
            String nameAux = name.item(0).getTextContent();

            if (element.getElementsByTagName("AdminDistrict").getLength() > 0) {
                String district = element.getElementsByTagName("AdminDistrict").item(0).getTextContent();
                output.add(district);
            }

            if (element.getElementsByTagName("CountryRegion").getLength() > 0) {
                String country = element.getElementsByTagName("CountryRegion").item(0).getTextContent();
                output.add(country);
            }
        }
        return output;
    }

    /**
     * Generates the xml link of the MBS API of the chosen place
     *
     * @param reqP = the GeocodeRequest with the information of the previously
     * chosen place
     * @return a URL redirecting to the link of the xml link of the MBS API of
     * the chosen place
     * @throws MalformedURLException
     *
     * Tiago Ribeiro (1170426)
     */
    private URL generateLinkXML(GeocodeRequest reqP) throws MalformedURLException {
        String url = urlSearch;

        url += "?q=";
        url += reqP.getAddressLine().replace(" ", "%20");

        url += ",%20";
        url += reqP.getLocality().replace(" ", "%20");

        url += ",%20";
        url += reqP.getPostalCode().replace(" ", "%20");

        url += "&output=xml";

        url += "&key=";
        url += reqP.getKey();

        return new URL(url);
    }

    /**
     * Geocode
     *
     * @param request
     * @return
     * @throws Exception
     */
    public Map<String, String> GeocodeLocationStructuredQueryCircularRegion(GeocodeRequest request) throws Exception {
        // Build the URL
        URL requestUrl;
        String url = RestUrlBaseCircularRegion;
        //type
        url += "?type=";
        url += request.getType();
        //ciruclar map
        url += "&userCircularMapView=";
        url += request.getLatLng();
        url += "," + request.getRadius();

        url += "&key=" + request.getKey();

        url += "&o=xml";
        requestUrl = new URL(url);

        // Make the web request
        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        String inputLine;
        Map<String, String> output = new HashMap<>();

        while ((inputLine = br.readLine()) != null) {
            output.putAll(getElementsFromXMLMBS(inputLine));
        }
        br.close();
        inStream.close();
        return output;
    }

    public static Map<String, String> getElementsFromXMLMBS(String inputLine) throws SAXException, IOException, ParserConfigurationException {
        Map<String, String> output = new HashMap<>();
        inputLine = inputLine.substring(3, inputLine.length());
        DocumentBuilderFactory dbf
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(inputLine));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("Resource");

        if (nodes.getLength() == 0) {
            nodes = doc.getElementsByTagName("Resources");
        }

        // iterate the employees
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList name = element.getElementsByTagName("Name");
            String nameAux = name.item(0).getTextContent();

            if (element.getElementsByTagName("Latitude").getLength() > 0) {
                NodeList latitude = element.getElementsByTagName("Latitude");
                String lat = latitude.item(0).getTextContent();
                if (element.getElementsByTagName("Longitude").getLength() > 0) {
                    NodeList longitude = element.getElementsByTagName("Longitude");
                    String lng = longitude.item(0).getTextContent();

                    output.put(lat + "," + lng, nameAux);
                }
            }
        }
        return output;
    }

    public static float getSeaLevelElevationFromXMLMBS(String inputLine) throws SAXException, IOException, ParserConfigurationException {
        inputLine = inputLine.substring(3, inputLine.length());
        DocumentBuilderFactory dbf
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(inputLine));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("Resource");

        if (nodes.getLength() == 0) {
            nodes = doc.getElementsByTagName("Resources");
        }

        float output = 0;

        // iterate the employees
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            if (element.getElementsByTagName("int").getLength() > 0) {
                NodeList elevation = element.getElementsByTagName("int");
                output = Float.parseFloat(elevation.item(0).getTextContent());
            }
        }
        return output;
    }

    /**
     * Geocode an address using the Structured Query (only Canada support)
     *
     * @param request
     * @throws Exception
     */
    public String GeocodeLocationStructuredQuery(GeocodeRequest request) throws Exception {
        // Build the URL
        URL requestUrl;
        String url = RestUrlBase;
        url += "/" + request.getCountryRegion(); // Country
        url += "/" + request.getDistrict(); // admin district (province)

        url += "/" + request.getPostalCode(); // postal code
        url += "/" + request.getLocality(); // city
        url += "/" + request.getAddressLine().replace(" ", "%20"); // address
        //url += "/userLocation=" + request.getLatLng();
        url += "?key=" + request.getKey();

        url += "&o=xml";
        requestUrl = new URL(url);
        // Make the web request
        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        String inputLine, output = "";
        while ((inputLine = br.readLine()) != null) {
            output += inputLine;
        }
        br.close();
        inStream.close();
        return output;
    }

    public float GeocodeElevation(GeocodeRequest request) throws Exception {
        // Build the URL
        URL requestUrl;
        String url = RestUrlBaseElevation;
        url += "?points=" + request.getLatLng();
        url += "&key=" + request.getKey();
        url += "&o=xml";
        requestUrl = new URL(url);

        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        String inputLine;
        float output = 0;
        while ((inputLine = br.readLine()) != null) {
            output = getSeaLevelElevationFromXMLMBS(inputLine);
        }
        br.close();
        inStream.close();
        return output;

    }

    public float[] GeocodeTravellingTimeDistance(GeocodeRequest origin, GeocodeRequest destination) throws Exception {
        float[] result = new float[2];

        URL requestUrl;
        String url = RestUrlDistanceMatrix;
        url += "?origins=" + origin.getLatLng();
        url += "&destinations=" + destination.getLatLng();
        url += "&travelMode=" + destination.getTravelMode();
        url += "&key=" + origin.getKey();
        url += "&o=xml";
        requestUrl = new URL(url);

        URLConnection conn = requestUrl.openConnection();
        InputStream inStream = conn.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));

        String inputLine;
        while ((inputLine = br.readLine()) != null) {
            result = getTravellingTimeDistanceFromXMLMBS(inputLine);
        }
        
        br.close();
        inStream.close();    
        
        return result;
    }
    
    public static float[] getTravellingTimeDistanceFromXMLMBS(String inputLine) throws SAXException, IOException, ParserConfigurationException {
        inputLine = inputLine.substring(3, inputLine.length());
        DocumentBuilderFactory dbf
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(inputLine));

        Document doc = db.parse(is);
        
        NodeList nodes = doc.getElementsByTagName("Results");

        float[] output = new float[2];

        // iterate the employees
       for (int i = 0; i < nodes.getLength(); i++) {
           
            Element element = (Element) nodes.item(i);

            if (element.getElementsByTagName("TravelDistance").getLength() > 0) {
                NodeList distanceNode = element.getElementsByTagName("TravelDistance");
                String distance = distanceNode.item(0).getTextContent();
                if (element.getElementsByTagName("TravelDuration").getLength() > 0) {
                    NodeList durationNode = element.getElementsByTagName("TravelDuration");
                    String duration = durationNode.item(0).getTextContent();
                    
                    output[0] = Float.parseFloat(distance);
                    output[1] = Float.parseFloat(duration);
                }
            }
        }
        return output;
    }
}
