package isep.eapli.georef.BMS;

import com.google.maps.model.LatLng;
import isep.eapli.georef.SearchService.SearchService;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BMSRequest implements SearchService {

    /**
     * Returns the latitude and longitude of a place request using its postal
     * address
     *
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr.
     * Bernardino)
     * @return a String with the latitude and longitude of the place
     * (latitude,longitude)
     *
     * Tiago Ribeiro (1170426)
     */
    @Override
    public String getAddressSearch(String locality, String postalCode, String adressLine) {
        try {
            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesMBS.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            String apiMBSKey = apiMBS.getProperty("apiKey");

            GeocodeRequest geoCodeRequest = new GeocodeRequest();

            //
            geoCodeRequest.setLocality(locality);
            geoCodeRequest.setPostalCode(postalCode);
            geoCodeRequest.setAddressLine(adressLine);
            geoCodeRequest.setKey(apiMBSKey);

            Geocode g = new Geocode();

            return g.GeocodeLocationWithAdress(geoCodeRequest);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Method that override the interface SearchService, receives a latitude, a
     * longitude and surroudingType & will return the surroudings near by the
     * location of the type received
     *
     * @param latitude double between ]-90,90[
     * @param longitude double between ]-180,180[
     * @param surroundingType ex:Hospital
     * @return a map with key coordinates(latitude+","+longitude) & value the
     * name of the surrouding
     */
    @Override
    public Map<String, String> getSurroundingsNearBy(String latitude, String longitude, String surroundingType) {
        try {
            Map<String, String> result;

            FileReader in;

            in = new FileReader("../apiPlacesMBS.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            String apiMBSKey = apiMBS.getProperty("apiKey");
            String radius = apiMBS.getProperty("radius");

            GeocodeRequest r = new GeocodeRequest();
            //select the surrouding type
            TypeIdentifiers ti = null;
            for (TypeIdentifiers tiAux : TypeIdentifiers.values()) {
                if (tiAux.toString().equalsIgnoreCase(surroundingType)) {
                    ti = tiAux;
                }
            }
            if (ti == null) {
                throw new Exception("The surrounding type doesn't exit.");
            }

            r.setLatLng(latitude + "," + longitude);
            r.setRadius(latitude);
            r.setKey(apiMBSKey);
            r.setType(ti.toString());
            r.setRadius(radius);
            Geocode g = new Geocode();

            result = g.GeocodeLocationStructuredQueryCircularRegion(r);

            return result;
        } catch (Exception ex) {
            return new HashMap<>();
        }

    }

    /**
     * Returns the complete postal address of the chosen place under the format
     * of a list of Strings
     *
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param addressLine = the address line of the place (e.g. Rua Dr.
     * Bernardino)
     * @return a list of String with the complete postal address, using the
     * following format: 1st line: Address line 2nd line: Postal code 3rd line:
     * Locality/City 4th line: District 5th line: Country
     *
     * Tiago Ribeiro (1170426)
     */
    @Override
    public List<String> fullPostalAddress(String locality, String postalCode, String addressLine) {
        try {
            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesMBS.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            String apiMBSKey = apiMBS.getProperty("apiKey");

            GeocodeRequest geoCodeRequest = new GeocodeRequest();

            geoCodeRequest.setLocality(locality);
            geoCodeRequest.setPostalCode(postalCode);
            geoCodeRequest.setAddressLine(addressLine);
            geoCodeRequest.setKey(apiMBSKey);

            Geocode g = new Geocode();

            return g.completePostalAddress(geoCodeRequest);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public float elevationSearch(String latitude, String longitude) {
        try {
            FileReader in;

            in = new FileReader("../apiPlacesMBS.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            String apiMBSKey = apiMBS.getProperty("apiKey");

            GeocodeRequest geoCodeRequest = new GeocodeRequest();

            String latLng = latitude + "," + longitude;

            geoCodeRequest.setLatLng(latLng);
            geoCodeRequest.setKey(apiMBSKey);

            Geocode g = new Geocode();
            return g.GeocodeElevation(geoCodeRequest);
        } catch (Exception ex) {
            return 0;
        }

    }

    public float[] travellingTimeDistanceBetweenLocations(String latO, String longO, String latD, String longD, String travelMode) throws FileNotFoundException, IOException {

        try {
            float[] result = new float[2];

            if (travelMode == null) {
                travelMode = "driving";
            }

            String origin = latO + "," + longO;
            String destination = latD + "," + longD;

            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesMBS.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            String apiMBSKey = apiMBS.getProperty("apiKey");

            GeocodeRequest originRequest = new GeocodeRequest();
            GeocodeRequest destinationRequest = new GeocodeRequest();

            originRequest.setKey(apiMBSKey);
            originRequest.setLatLng(origin);
            destinationRequest.setLatLng(destination);
            if (!(travelMode.equalsIgnoreCase("driving")) && travelMode.equalsIgnoreCase("bicycling") && (!travelMode.equalsIgnoreCase("walking"))) {
                travelMode = "driving";
            }
            destinationRequest.setTravelMode(travelMode);
            result = new Geocode().GeocodeTravellingTimeDistance(originRequest, destinationRequest);
            return result;
        } catch (Exception ex) {
            Logger.getLogger(BMSRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new float[2];
    }

    /**
     * This method calculates the elevation difference between two locations
     * 
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return difference
     * 
     * @author Maria Junqueira (1170842)
     */
    @Override
    public float elevationDifference(String latitude1, String longitude1, String latitude2, String longitude2) {
        float elevation1 = elevationSearch(latitude1, longitude1);
        float elevation2 = elevationSearch(latitude2, longitude2);
        return (elevation1 - elevation2);
    }

}
