package isep.eapli.georef.SearchService;

import com.google.maps.model.LatLng;
import java.util.List;
import java.util.Map;

public interface SearchService {

    /**
     * Method that will be overridden by different search services. Receives a
     * latitude, a longitude and surroudingType & will return the surroudings
     * near by the location of the type received
     *
     * @param latitude double between ]-90,90[
     * @param longitude double between ]-180,180[
     * @param surroundingType ex:Hospital
     * @return a map with key coordinates(latitude+","+longitude) & value the
     * name of the surrouding
     */
    public Map<String, String> getSurroundingsNearBy(String latitude, String longitude, String surroundingType);
    
    public String getAddressSearch (String locality , String postalCode, String adressLine);
    
    public float elevationSearch(String latitude, String longitude);

    public float elevationDifference(String latitude1, String longitude1, String latitude2, String longitude2);
    
    public List<String> fullPostalAddress(String locality , String postalCode, String addressLine);
    
    

}
