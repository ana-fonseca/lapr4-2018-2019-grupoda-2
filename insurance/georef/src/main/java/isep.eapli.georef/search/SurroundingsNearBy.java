package isep.eapli.georef.search;

import isep.eapli.georef.BMS.BMSRequest;
import isep.eapli.georef.GMS.GMSRequest;
import java.util.HashMap;
import java.util.Map;

public class SurroundingsNearBy {

    /**
     * Receives a latitude, a latitude & surrouding type. If any is null, return
     * an empty map, otherwise will run the bing request & google request and
     * will join non repeated ellements
     *
     * @param latitude double between ]-90,90[
     * @param longitude double between ]-180,180[
     * @param surroundingType ex:Hospital
     * @return a map with key coordinates(latitude+","+longitude) & value the
     * name of the surrouding
     */
    public static Map<String, String> getSurroundings(String latitude, String longitude, String surroundingType) {
        if (latitude == null || longitude == null || surroundingType == null) {
            return new HashMap<>();
        }
        BMSRequest bmsr = new BMSRequest();
        Map<String, String> result = bmsr.getSurroundingsNearBy(latitude, longitude, surroundingType);
        if (result == null) {
            result = new HashMap<>();
        }

        GMSRequest gmsr = new GMSRequest();
        Map<String, String> resultBMS = gmsr.getSurroundingsNearBy(latitude, longitude, surroundingType);

        if (resultBMS != null) {
            result.putAll(resultBMS);
        }

        return result;
    }
}
