package isep.eapli.georef.search;

public class SurroundingGetter {

    /**
     * Public empty constructor
     */
    public SurroundingGetter() {
        //Empty constructor
    }

    /**
     * Method that returns a list of surroundings given an address
     *
     * @param latitude  latitude of the place
     * @param longitude longitude of the place
     * @return list of surroundings
     */
    public String surroundingsByAddress(double latitude, double longitude) {
        String su1 = "R1;River;Distance,low;Time,medium";
        String su2 = "FFS1;Fire Fighter Station;Distance,medium;Time,medium";
        String su3 = "R2;River;Distance,low;Time,high";
        String su4 = "FFS2;Fire Fighter Station;Distance,high;Time,low";
        String su5 = "R3;River;Distance,high;Time,medium";
        String su6 = "FFS3;Fire Fighter Station;Distance,low;Time,low";
        String answer = "Failure";

        if (latitude == 12.2 && longitude == 13.2) {
            //Location 1
            answer = su1;
        } else if (latitude == 24.3 && longitude == 43.4) {
            //Location 2
            answer = su2 + "|" + su3;
        } else if (latitude == 48.2 && longitude == 21.3) {
            //Location 3
            answer = su1 + "|" + su4;
        } else if (latitude == 64.2 && longitude == 28.9) {
            //Location 4
            answer = su2 + "|" + su5;
        } else if (latitude == 53.6 && longitude == 51.7) {
            //Location 5
            answer = su3 + "|" + su4;
        } else if (latitude == 32.7 && longitude == 43.8) {
            //Location 6
            answer = su6 + "|" + su5;
        }

        return answer;

        /*
        LinkedList<Surroundings> locationSurroundings = new LinkedList<>();
        String information = "";
        //Already defined Addresses to be used in the system
        //IMPORTANT
        Address loc1 = new Address(12.2, 13.2);
        Address loc2 = new Address(24.3, 43.4);
        Address loc3 = new Address(48.2, 21.3);
        Address loc4 = new Address(64.2, 28.9);
        Address loc5 = new Address(53.6, 51.7);
        Address loc6 = new Address(32.7, 43.8);
        Map<Metric, String> firstList = new HashMap<>();
        firstList.put(new Metric("Distance"), "low");
        firstList.put(new Metric("Time"), "medium");
        Surroundings so1 = new Surroundings("R1", new SurroundingType("River"), new HashMap<>(firstList));
        firstList.clear();
        firstList.put(new Metric("Distance"), "medium");
        firstList.put(new Metric("Time"), "medium");
        Surroundings so2 = new Surroundings("FFS1", new SurroundingType("Fire Fighter Station"), new HashMap<>(firstList));
        firstList.clear();
        firstList.put(new Metric("Distance"), "low");
        firstList.put(new Metric("Time"), "high");
        Surroundings so3 = new Surroundings("R2", new SurroundingType("River"), new HashMap<>(firstList));
        firstList.clear();
        firstList.put(new Metric("Distance"), "high");
        firstList.put(new Metric("Time"), "low");
        Surroundings so4 = new Surroundings("FFS2", new SurroundingType("Fire Fighter Station"), new HashMap<>(firstList));
        firstList.clear();
        firstList.put(new Metric("Distance"), "high");
        firstList.put(new Metric("Time"), "medium");
        Surroundings so5 = new Surroundings("R3", new SurroundingType("River"), new HashMap<>(firstList));
        firstList.clear();
        firstList.put(new Metric("Distance"), "low");
        firstList.put(new Metric("Time"), "low");
        Surroundings so6 = new Surroundings("FFS", new SurroundingType("Fire Fighter Station"), new HashMap<>(firstList));

        if (location.equals(loc1)) {
            locationSurroundings.add(so1);
        } else if (location.equals(loc2)) {
            locationSurroundings.add(so2);
            locationSurroundings.add(so3);
        } else if (location.equals(loc3)) {
            locationSurroundings.add(so1);
            locationSurroundings.add(so5);
        } else if (location.equals(loc4)) {
            locationSurroundings.add(so2);
            locationSurroundings.add(so6);
        } else if (location.equals(loc5)) {
            locationSurroundings.add(so3);
            locationSurroundings.add(so4);
        } else if (location.equals(loc6)) {
            locationSurroundings.add(so2);
            locationSurroundings.add(so4);
        }

        return locationSurroundings;
        */
    }
}
