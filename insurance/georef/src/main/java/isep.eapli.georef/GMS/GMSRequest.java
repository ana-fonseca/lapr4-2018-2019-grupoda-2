package isep.eapli.georef.GMS;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.ElevationApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.NearbySearchRequest;
import com.google.maps.PendingResult;
import com.google.maps.errors.ApiException;
import com.google.maps.model.ComponentFilter;
import com.google.maps.model.ElevationResult;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlaceType;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;
import isep.eapli.georef.SearchService.SearchService;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.TravelMode;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GMSRequest implements SearchService {

    /**
     * Method that override the interface SearchService, receives a latitude, a
     * longitude and surroudingType & will return the surroudings near by the
     * location of the type received
     *
     * @param latitude double between ]-90,90[
     * @param longitude double between ]-180,180[
     * @param surroundingType ex:Hospital
     * @return a map with key coordinates(latitude+","+longitude) & value the
     * name of the surrouding
     */
    @Override
    public Map<String, String> getSurroundingsNearBy(String latitude, String longitude, String surroundingType) {
        try {
            Double lat = Double.parseDouble(latitude);
            Double lng = Double.parseDouble(longitude);
            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesGMS.properties");
            Properties apiPlaces = new Properties();
            apiPlaces.load(in);
            apiKey = apiPlaces.getProperty("apiKey");
            int distanceMax = Integer.parseInt(apiPlaces.getProperty("distanceMax"));

            Map<String, String> result = new HashMap<>();

            GeoApiContext geo = new GeoApiContext.Builder()
                    .apiKey(apiKey)
                    .channel("https://maps.googleapis.com")
                    .build();

            LatLng local = new LatLng(lat, lng);

            NearbySearchRequest nsr = new NearbySearchRequest(geo);
            nsr.radius(distanceMax);
            nsr.location(local);
            PlaceType placeType = null;
            for (PlaceType pt : PlaceType.values()) {
                if (pt.toString().equalsIgnoreCase(surroundingType)) {
                    placeType = pt;
                }
            }
            if (placeType == null) {
                throw new Exception("Surrouding type invalid");
            }
            nsr.type(placeType);
            nsr.channel("https://maps.googleapis.com");

            PlacesSearchResponse psr = nsr.await();
            for (PlacesSearchResult psResult : Arrays.asList(psr.results)) {
                result.put(psResult.geometry.location.lat + "," + psResult.geometry.location.lng, psResult.name);
            }

            return result;
        } catch (Exception ex) {
            return new HashMap<>();
        }
    }

    /**
     * Returns the latitude and longitude of a place request using its postal
     * address
     *
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr.
     * Bernardino)
     * @return a String with the latitude and longitude of the place
     * (latitude,longitude)
     *
     * Tiago Ribeiro (1170426)
     */
    @Override //Locality = city // AdressLine = street
    public String getAddressSearch(String locality, String postalCode, String adressLine) {
        try {
            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesGMS.properties");

            Properties apiPlaces = new Properties();
            apiPlaces.load(in);
            apiKey = apiPlaces.getProperty("geoCodingAPI");

            GeocodingApiRequest geo = GeocodingApi.newRequest(new GeoApiContext.Builder()
                    .apiKey(apiKey)
                    .channel("https://maps.googleapis.com")
                    .build());

            ComponentFilter address = new ComponentFilter("address", adressLine);
            ComponentFilter postalCodeFilter = new ComponentFilter("postal_code", postalCode);
            ComponentFilter localityFilter = new ComponentFilter("locality", locality);

            geo = geo.components(address, postalCodeFilter, localityFilter);

            geo.channel("https://maps.googleapis.com");

            GeocodingResult[] geoResults = geo.await();
            for (GeocodingResult result : geoResults) {
                return result.geometry.location.toString();
            }

        } catch (Exception ex) {
            return "";
        }
        return "";
    }

    @Override
    public float elevationSearch(String latitude, String longitude) {
        try {
            Double lat = Double.parseDouble(latitude);
            Double lng = Double.parseDouble(longitude);
            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesGMS.properties");
            Properties apiPlaces = new Properties();
            apiPlaces.load(in);
            apiKey = apiPlaces.getProperty("apiKeyElevation");

            GeoApiContext geo = new GeoApiContext.Builder()
                    .apiKey(apiKey)
                    .channel("https://maps.googleapis.com")
                    .build();

            LatLng local = new LatLng(lat, lng);

            PendingResult<ElevationResult[]> pr = ElevationApi.getByPoints(geo, local);

            ElevationResult[] er = pr.await();

            return (float) er[0].elevation;

        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     * Returns the complete postal address of the chosen place under the format
     * of a list of Strings
     *
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param addressLine = the address line of the place (e.g. Rua Dr.
     * Bernardino)
     * @return a list of String with the complete postal address, using the
     * following format: 1st line: Address line 2nd line: Postal code 3rd line:
     * Locality/City 4th line: District 5th line: Country
     *
     * Tiago Ribeiro (1170426)
     */
    @Override
    public List<String> fullPostalAddress(String locality, String postalCode, String addressLine) {
        try {
            List<String> output = new ArrayList<>();

            output.add(addressLine);
            output.add(postalCode);
            output.add(locality);

            FileReader in;
            String apiKey;

            in = new FileReader("../apiPlacesGMS.properties");

            Properties apiPlaces = new Properties();
            apiPlaces.load(in);
            apiKey = apiPlaces.getProperty("geoCodingAPI");

            GeocodingApiRequest geo = GeocodingApi.newRequest(new GeoApiContext.Builder()
                    .apiKey(apiKey)
                    .channel("https://maps.googleapis.com")
                    .build());

            ComponentFilter address = new ComponentFilter("address", addressLine);
            ComponentFilter postalCodeFilter = new ComponentFilter("postal_code", postalCode);
            ComponentFilter localityFilter = new ComponentFilter("locality", locality);

            geo = geo.components(address, postalCodeFilter, localityFilter);

            geo.channel("https://maps.googleapis.com");

            GeocodingResult[] geoResults = geo.await();
            for (GeocodingResult result : geoResults) {
                for (AddressComponent component : result.addressComponents) {
                    List<AddressComponentType> componentType = Arrays.asList(component.types);
                    if (componentType.contains(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {
                        output.add(3, component.longName);
                    }
                    if (componentType.contains(AddressComponentType.COUNTRY)) {
                        output.add(4, component.longName);
                    }
                }
            }

            return output;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * This method, receiving 2 locations and a travel Mode (bicycling, walking, driving), will return an array of floats which contains
     * travelling time in minutes and distance in kilometers.
     * @param latO origin
     * @param longO origin
     * @param latD destination
     * @param longD destination
     * @param travelMode if null, defaults to driving, but can be: driving, walking, bicycling.
     * @return array with the distance in first pos and time in the second pos.
     * This method was created by Ana Fonseca (1170656)
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ApiException
     * @throws InterruptedException 
     */
    public float[] travellingTimeDistanceBetweenLocations(String latO, String longO, String latD, String longD, String travelMode) throws FileNotFoundException, IOException, ApiException, InterruptedException {
        float[] result = new float[2];

        if (travelMode == null) {
            travelMode = "driving";
        }

        Double latitudeO = Double.parseDouble(latO);
        Double longitudeO = Double.parseDouble(longO);
        Double latitudeD = Double.parseDouble(latD);
        Double longitudeD = Double.parseDouble(longD);

        LatLng origin = new LatLng(latitudeO, longitudeO);
        LatLng destination = new LatLng(latitudeD, longitudeD);

        FileReader in;
        String apiKey;

        in = new FileReader("../apiPlacesGMS.properties");

        Properties apiPlaces = new Properties();
        apiPlaces.load(in);
        apiKey = apiPlaces.getProperty("geoCodingAPI");

        DistanceMatrixApiRequest dmRequest = DistanceMatrixApi.newRequest(new GeoApiContext.Builder()
                .apiKey(apiKey)
                .channel("https://maps.googleapis.com")
                .build());
        
        dmRequest.origins(origin);
        dmRequest.destinations(destination);
        
        if(travelMode.equalsIgnoreCase("walking")){
            dmRequest.mode(TravelMode.WALKING);
        }else if(travelMode.equalsIgnoreCase("bicycling")){
            dmRequest.mode(TravelMode.BICYCLING);
        }else{
            dmRequest.mode(TravelMode.DRIVING);
        }
        
        DistanceMatrix dM = dmRequest.await();
        DistanceMatrixRow[] dmRows = dM.rows;
        
        float distance = (dmRows[0].elements[0].distance.inMeters)/1000;
        float time = (dmRows[0].elements[0].duration.inSeconds)/60;
        
        result[0] = distance;
        result[1] = time;
        
        return result;
    }
    
    /**
     * This method calculates the elevation difference between two locations
     * 
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return difference
     * 
     * @author Maria Junqueira (1170842)
     */
    @Override
    public float elevationDifference(String latitude1, String longitude1, String latitude2, String longitude2) {
        float elevation1 = elevationSearch(latitude1, longitude1);
        float elevation2 = elevationSearch(latitude2, longitude2);
        return (elevation1 - elevation2);
    }

}
