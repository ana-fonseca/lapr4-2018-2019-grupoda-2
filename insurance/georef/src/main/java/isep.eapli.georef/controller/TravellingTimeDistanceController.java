package isep.eapli.georef.controller;

import com.google.maps.errors.ApiException;
import isep.eapli.georef.BMS.BMSRequest;
import isep.eapli.georef.GMS.GMSRequest;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TravellingTimeDistanceController {

    /**
     * Returns distance and time using Google Maps
     * @param latitudeO
     * @param longitudeO
     * @param latitudeD
     * @param longitudeD
     * @param travelMode
     * @returns array with the two values
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ApiException
     * @throws InterruptedException 
     */
    public static float[] travelCalculationGMS(String latitudeO, String longitudeO, String latitudeD, String longitudeD, String travelMode) throws IOException, FileNotFoundException, ApiException, InterruptedException {
        GMSRequest gms = new GMSRequest();
        return gms.travellingTimeDistanceBetweenLocations(latitudeO, longitudeO, latitudeO, longitudeD, travelMode);
    }

    /**
     * Returns distance and time using Microsoft
     * @param latitudeO
     * @param longitudeO
     * @param latitudeD
     * @param longitudeD
     * @param travelMode
     * @return array with the two values
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ApiException
     * @throws InterruptedException 
     */
    public static float[] travelCalculationBMS(String latitudeO, String longitudeO, String latitudeD, String longitudeD, String travelMode) throws IOException, FileNotFoundException, ApiException, InterruptedException {
        BMSRequest bmsr = new BMSRequest();
        return bmsr.travellingTimeDistanceBetweenLocations(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
    }

    /**
     * Returns distance and time using Microsoft and Google
     * @param latitudeO
     * @param longitudeO
     * @param latitudeD
     * @param longitudeD
     * @param travelMode
     * @returns average of both APIs
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ApiException
     * @throws InterruptedException 
     */
    public static float[] travelCalculationAVG(String latitudeO, String longitudeO, String latitudeD, String longitudeD, String travelMode) throws IOException, FileNotFoundException, ApiException, InterruptedException {
        GMSRequest gms = new GMSRequest();
        float[] google = gms.travellingTimeDistanceBetweenLocations(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
        BMSRequest bmsr = new BMSRequest();
        float[] bing = bmsr.travellingTimeDistanceBetweenLocations(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
        
        float[] result = new float[2];
        result[0] = (google[0] + bing[0])/2;
        result[1] = (google[1] + bing[1])/2;
        
        return result;
    }
}
