/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.georef.controller;

import isep.eapli.georef.GMS.GMSRequest;
import isep.eapli.georef.BMS.BMSRequest;

/**
 *
 * @author becas
 */
public class ElevationController {
    
    public static float elevationGMS (String latitude, String longitude){
        GMSRequest gms = new GMSRequest();
        return gms.elevationSearch(latitude, longitude);
    }
    public static float elevationBMS (String latitude, String longitude){
        BMSRequest bmsr = new BMSRequest();
        return bmsr.elevationSearch(latitude, longitude);
    }
    
    public static float elevationAVG(String latitude, String longitude){
        GMSRequest gms = new GMSRequest();
        float google = gms.elevationSearch(latitude, longitude);
        BMSRequest bmsr = new BMSRequest();
        float bing = bmsr.elevationSearch(latitude, longitude);
        return (google + bing) / 2;
    }
}
