package isep.eapli.georef.controller;

public interface GeoReferenceService {

    /**
     * Method that returns a list of Surroundings, given an location
     *
     * @param latitude  latitude of the place
     * @param longitude longitude of the place
     * @return list with surroundings of the given location
     */
    String getSurroundings(double latitude, double longitude);

}
