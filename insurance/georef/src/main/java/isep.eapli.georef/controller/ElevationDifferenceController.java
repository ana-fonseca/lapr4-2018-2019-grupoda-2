package isep.eapli.georef.controller;

import isep.eapli.georef.GMS.GMSRequest;
import isep.eapli.georef.BMS.BMSRequest;
/**
 *
 * @author Maria Junqueira
 */
public class ElevationDifferenceController {
    
    public static float elevationDifferenceGMS(String latitude1, String longitude1, String latitude2, String longitude2){
        GMSRequest gms = new GMSRequest();
        return gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
    }
    
    public static float elevationDifferenceBMS(String latitude1, String longitude1, String latitude2, String longitude2){
        BMSRequest bms = new BMSRequest();
        return bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
    }
    
    public static float elevationDifferenceAVG(String latitude1, String longitude1, String latitude2, String longitude2){
        GMSRequest gms = new GMSRequest();
        float google = gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        BMSRequest bms = new BMSRequest();
        float bing = bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        return (google + bing) / 2;
    }
}
