package isep.eapli.georef.controller;

import isep.eapli.georef.search.SurroundingGetter;

public class ObtainSurroundingsController implements GeoReferenceService {

    private SurroundingGetter sg;

    /**
     * Public constructor
     */
    public ObtainSurroundingsController() {
        sg = new SurroundingGetter();
    }

    /**
     * Method that returns a list of Surroundings, given an location
     *
     * @param latitude  latitude of the place
     * @param longitude longitude of the place
     * @return surroundings of the given location
     */
    @Override
    public String getSurroundings(double latitude, double longitude) {
        return sg.surroundingsByAddress(latitude, longitude);
    }

}
