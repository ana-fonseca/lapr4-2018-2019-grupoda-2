package isep.eapli.georef.controller;

import isep.eapli.georef.BMS.BMSRequest;
import isep.eapli.georef.GMS.GMSRequest;
import isep.eapli.georef.search.SurroundingsNearBy;
import java.util.List;
import java.util.Map;

public class AdressSearchController {
    
    /**
     * Returns the latitude and longitude of a place request using its postal address using the GMS
     * 
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr. Bernardino)
     * @return a String with the latitude and longitude of the place (latitude,longitude)
     * 
     * Tiago Ribeiro (1170426)
     */
    public String addressSearch(String locality , String postalCode, String adressLine) {
        GMSRequest gms = new GMSRequest();
        return gms.getAddressSearch(locality, postalCode, adressLine);
    }
    
    /**
     * Returns the latitude and longitude of a place request using its postal address using the MBS
     * 
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr. Bernardino)
     * @return a String with the latitude and longitude of the place (latitude,longitude)
     * 
     * Tiago Ribeiro (1170426)
     */
    public String addressSearchBMS(String locality , String postalCode, String adressLine) {
        BMSRequest bmsr = new BMSRequest();
        return bmsr.getAddressSearch(locality, postalCode, adressLine);
    }
    
    /**
     * Returns the complete postal address of the chosen place under the format of a list of Strings using the GMS
     * 
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr. Bernardino)
     * @return a list of String with the complete postal address, using the following format:
     * 1st line: Address line
     * 2nd line: Postal code
     * 3rd line: Locality/City
     * 4th line: District
     * 5th line: Country
     * 
     * Tiago Ribeiro (1170426)
     */
    public List<String> completePostalAddress(String locality , String postalCode, String adressLine) {
        GMSRequest gms = new GMSRequest();
        return gms.fullPostalAddress(locality, postalCode, adressLine);
    }
    
    /**
     * Returns the complete postal address of the chosen place under the format of a list of Strings using the MBS
     * 
     * @param locality = the locality of the place (e.g. Porto)
     * @param postalCode = the postal code of the place (e.g. 4420-542)
     * @param adressLine = the address line of the place (e.g. Rua Dr. Bernardino)
     * @return a list of String with the complete postal address, using the following format:
     * 1st line: Address line
     * 2nd line: Postal code
     * 3rd line: Locality/City
     * 4th line: District
     * 5th line: Country
     * 
     * Tiago Ribeiro (1170426)
     */
    public List<String> completePostalAddressBMS(String locality , String postalCode, String adressLine) {
        BMSRequest bmsr = new BMSRequest();
        return bmsr.fullPostalAddress(locality, postalCode, adressLine);
    }

}
