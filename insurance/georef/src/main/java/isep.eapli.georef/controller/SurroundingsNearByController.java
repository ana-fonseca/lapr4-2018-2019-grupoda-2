package isep.eapli.georef.controller;

import isep.eapli.georef.search.SurroundingsNearBy;
import java.util.Map;

public class SurroundingsNearByController {

    public static Map<String, String> getSurroundings(String latitude, String longitude, String surroundingType) {
        return SurroundingsNearBy.getSurroundings(latitude, longitude, surroundingType);
    }

}
