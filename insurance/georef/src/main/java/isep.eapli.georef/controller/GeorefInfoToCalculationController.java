/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.georef.controller;

import com.google.maps.errors.ApiException;
import isep.eapli.georef.Utils.GeorefInfoToCalculation;
import isep.eapli.georef.search.SurroundingsNearBy;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;

/**
 *
 * @author Luís Silva
 */
public class GeorefInfoToCalculationController {
    
    public String getInfoToCalculationDistance(String surronding, int valueA, int valueB, String latitudeO, String longitudeO) throws IOException, FileNotFoundException, ApiException, InterruptedException{
        DecimalFormat df = new DecimalFormat("00.00");
        Map<String,String> map = SurroundingsNearByController.getSurroundings(surronding, latitudeO, longitudeO);
        float min = Float.MAX_VALUE;
        for (String value : map.values()) {
            String[] coordenadas = value.split(",");
            float [] distanceTime = TravellingTimeDistanceController.travelCalculationGMS(latitudeO, longitudeO, coordenadas[0], coordenadas[1], null);
            if (distanceTime[0] < min) {
                min = distanceTime[0];
            }
        }
        String result = new GeorefInfoToCalculation().transformValueToMatrixInfo(valueA, valueB, min);
        return "Distance: " + df.format(min) + "km\n Result to scale: " + result;
    }

    public String getInfoToCalculationTime(String surronding, int valueA, int valueB, String latitudeO, String longitudeO) throws IOException, FileNotFoundException, ApiException, InterruptedException {
        DecimalFormat df = new DecimalFormat("00.00");
        Map<String, String> map = SurroundingsNearBy.getSurroundings(surronding, latitudeO, longitudeO);
        float min = Float.MAX_VALUE;
        for (String value : map.values()) {
            String[] coordenadas = value.split(",");
            float[] distanceTime = TravellingTimeDistanceController.travelCalculationGMS(latitudeO, longitudeO, coordenadas[0], coordenadas[1], null);
            if (distanceTime[1] < min) {
                min = distanceTime[1];
            }
        }
        String result = new GeorefInfoToCalculation().transformValueToMatrixInfo(valueA, valueB, min);
        return "Time: " + df.format(min) + "min\n Result to scale: " + result;
    }
}
