package isep.eapli.georef.search;

import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SurroundingsNearByTest {

    /**
     * Test of getSurroundings method, of class SurroundingsNearBy.
     */
    public void testGetSurroundingsBMS() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "hospitals";
        Map<String, String> result = SurroundingsNearBy.getSurroundings(latitude, longitude, surroundingType);

        Map<String, String> expResult = new HashMap<>();
        expResult.put("47.667209625244141" + "," + "-122.37998962402344", "Swedish Medical Center Ballard Campus");
        expResult.put("47.668670654296875" + "," + "-122.38017272949219", "North Seattle Women's Specialists");
        expResult.put("47.667209625244141" + "," + "-122.37977600097656", "Swedish Medical Center-Ballard-ER");

        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));
        assertTrue(expResult.values().containsAll(result.values()));
    }

    /**
     * Test of getSurroundings method, of class SurroundingsNearBy.
     */
    @Test
    public void testGetSurroundingsGMS() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "hospital";
        Map<String, String> result = SurroundingsNearBy.getSurroundings(latitude, longitude, surroundingType);

        Map<String, String> expResult = new HashMap<>();
        expResult.put("47.6847803" + "," + "-122.3765106", "Ballard Pediatric Clinic");
        expResult.put("47.6681531" + "," + "-122.3800277", "The Polyclinic Ballard");
        expResult.put("47.68699489999999" + "," + "-122.3879501", "Washington FEAST");
        expResult.put("47.6682633" + "," + "-122.3802533", "Swedish Vascular Surgery and Laboratory - Ballard");
        expResult.put("47.668368" + "," + "-122.380854", "Swedish Hospital in Ballard");
        expResult.put("47.6676638" + "," + "-122.3800472", "Cardio Pulmonary");
        expResult.put("47.6680739" + "," + "-122.3793388", "Kate's hospital");
        expResult.put("47.6674625" + "," + "-122.3795306", "Swedish Anticoagulation Clinic - Ballard");

        //compare key set
        //assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
    }

    /**
     * Test of getSurroundings method, of class SurroundingsNearBy.
     */
    @Test
    public void testGetSurroundingsNullParam() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "hospital";
        Map<String, String> result;
        Map<String, String> expResult = new HashMap<>();

        //latitude null
        result = SurroundingsNearBy.getSurroundings(null, longitude, surroundingType);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));

        //longitude null
        result = SurroundingsNearBy.getSurroundings(latitude, null, surroundingType);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));

        //surroundingType null
        result = SurroundingsNearBy.getSurroundings(latitude, longitude, null);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));
    }

    /**
     * Test of getSurroundings method, of class SurroundingsNearBy.
     */
    @Test
    public void testGetSurroundingsNotValidSurroudingType() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "DontExit";
        Map<String, String> result = SurroundingsNearBy.getSurroundings(null, longitude, surroundingType);;
        Map<String, String> expResult = new HashMap<>();
    }

}
