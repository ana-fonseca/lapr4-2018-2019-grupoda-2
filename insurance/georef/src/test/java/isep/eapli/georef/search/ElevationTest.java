package isep.eapli.georef.search;

import isep.eapli.georef.BMS.BMSRequest;
import isep.eapli.georef.GMS.GMSRequest;
import java.text.DecimalFormat;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ElevationTest {

    @Test
    public void ElevationRequestGMSTest() {
        String latitude = "48.2";
        String longitude = "21.3";
        GMSRequest gms = new GMSRequest();
        float result = gms.elevationSearch(latitude, longitude);
        DecimalFormat df = new DecimalFormat("00.000");
        assertEquals(df.format(203.364), df.format(result));
    }

    @Test
    public void ElevationRequestBMSTest() {
        String latitude = "48.2";
        String longitude = "21.3";
        BMSRequest bms = new BMSRequest();
        float result = bms.elevationSearch(latitude, longitude);
        assertEquals(40, result, 0.0001);
    }
}
