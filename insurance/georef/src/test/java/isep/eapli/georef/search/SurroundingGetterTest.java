package isep.eapli.georef.search;

import org.junit.Test;

import static org.junit.Assert.*;

public class SurroundingGetterTest {

    SurroundingGetter sg;

    public SurroundingGetterTest() {
        sg = new SurroundingGetter();
    }

    @Test
    public void surroundingsByAddressLoc1() {
        String expected = "R1;River;Distance,low;Time,medium";
        String result = sg.surroundingsByAddress(12.2, 13.2);
        assertEquals(expected, result);
    }

    @Test
    public void surroundingsByAddressLoc2() {
        String expected = "FFS1;Fire Fighter Station;Distance,medium;Time,medium|R2;River;Distance,low;Time,high";
        String result = sg.surroundingsByAddress(24.3, 43.4);
        assertEquals(expected, result);
    }

    @Test
    public void surroundingsByAddressLoc3() {
        String expected = "R1;River;Distance,low;Time,medium|FFS2;Fire Fighter Station;Distance,high;Time,low";
        String result = sg.surroundingsByAddress(48.2, 21.3);
        assertEquals(expected, result);
    }

    @Test
    public void surroundingsByAddressLoc4() {
        String expected = "FFS1;Fire Fighter Station;Distance,medium;Time,medium|R3;River;Distance,high;Time,medium";
        String result = sg.surroundingsByAddress(64.2, 28.9);
        assertEquals(expected, result);
    }

    @Test
    public void surroundingsByAddressLoc5() {
        String expected = "R2;River;Distance,low;Time,high|FFS2;Fire Fighter Station;Distance,high;Time,low";
        String result = sg.surroundingsByAddress(53.6, 51.7);
        assertEquals(expected, result);
    }

    @Test
    public void surroundingsByAddressLoc6() {
        String expected = "FFS3;Fire Fighter Station;Distance,low;Time,low|R3;River;Distance,high;Time,medium";
        String result = sg.surroundingsByAddress(32.7, 43.8);
        assertEquals(expected, result);
    }

    @Test
    public void invalidAddress(){
        String expected = "Failure";
        String result = sg.surroundingsByAddress(12.2, 90.0);
        assertEquals(expected, result);
    }
}