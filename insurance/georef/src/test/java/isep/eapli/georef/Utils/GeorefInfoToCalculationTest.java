/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.georef.Utils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luís Silva
 */
public class GeorefInfoToCalculationTest {
    
    public GeorefInfoToCalculationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of transformValueToMatrixInfo method, of class GeorefInfoToCalculation.
     */
    @Test
    public void testTransformValueToMatrixInfo() {
        System.out.println("transformValueToMatrixInfo");
        int valueA = 0;
        int valueB = 5;
        float min = 3.0F;
        GeorefInfoToCalculation instance = new GeorefInfoToCalculation();
        String expResult = "Média";
        String result = instance.transformValueToMatrixInfo(valueA, valueB, min);
        assertEquals(expResult, result);
        valueA = 4;
        expResult = "Baixa";
        result = instance.transformValueToMatrixInfo(valueA, valueB, min);
        assertEquals(expResult, result);
        min = 7;
        expResult = "Alta";
        result = instance.transformValueToMatrixInfo(valueA, valueB, min);
        assertEquals(expResult, result);
    }
    
}
