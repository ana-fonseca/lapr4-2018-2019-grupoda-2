package isep.eapli.georef.controller;

import org.junit.Test;

import static org.junit.Assert.*;

public class ObtainSurroundingsControllerTest {

    @Test
    public void getSurroundingsValidAddress() {
        ObtainSurroundingsController osc = new ObtainSurroundingsController();
        String expected = "R1;River;Distance,low;Time,medium";
        String result = osc.getSurroundings(12.2, 13.2);
        assertEquals(expected, result);
    }

    @Test
    public void getSurroundingsInvalidAddress() {
        ObtainSurroundingsController osc = new ObtainSurroundingsController();
        String expected = "Failure";
        String result = osc.getSurroundings(12.5, 89.2);
        assertEquals(expected, result);
    }
}