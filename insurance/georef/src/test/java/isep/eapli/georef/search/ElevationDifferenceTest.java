package isep.eapli.georef.search;

import isep.eapli.georef.BMS.BMSRequest;
import isep.eapli.georef.GMS.GMSRequest;
import java.text.DecimalFormat;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Maria Junqueira
 */
public class ElevationDifferenceTest {
    
    @Test
    public void ElevationDifferenceGMSTest() {
        String latitude1 = "48.2";
        String longitude1 = "21.3";
        String latitude2 = "49.5";
        String longitude2 = "21.9";
        GMSRequest gms = new GMSRequest();
        float result = gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        DecimalFormat df = new DecimalFormat("00.000");
        assertEquals(df.format(-250.550), df.format(result));
    }

    @Test
    public void ElevationDifferenceBMSTest() {
        String latitude1 = "48.2";
        String longitude1 = "21.3";
        String latitude2 = "49.5";
        String longitude2 = "21.9";
        BMSRequest bms = new BMSRequest();
        float result = bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        assertEquals(3, result, 0.0001);
    }
}
