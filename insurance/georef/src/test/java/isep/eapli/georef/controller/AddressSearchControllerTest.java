package isep.eapli.georef.controller;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;

public class AddressSearchControllerTest {

    @Test
    public void addressSearchBMSTestComplete() {
        String result = new AdressSearchController().addressSearchBMS("Valbom", "4420-542", "Passeio Quinta do Sol");
        String expected = "41.12784,-8.56206";

        assertEquals(expected, result);
    }

    @Test
    public void addressSearchBMSTestCityNull() {
        String result = new AdressSearchController().addressSearchBMS(null, "4420-542", "Passeio Quinta do Sol");
        
        assertNull(result);
    }

    @Test
    public void addressSearchBMSTestPostalCodeNull() {
        String result = new AdressSearchController().addressSearchBMS(null, null, "Passeio Quinta do Sol");
        String expected = "41.12784,-8.56206";
        
        assertNull(result);
    }

    @Test
    public void addressSearchGMSTestComplete() {
        String result = new AdressSearchController().addressSearch("Valbom", "4420-542", "Passeio Quinta do Sol, 19");
        String expected = "41.12785210,-8.56214360";

        assertEquals(expected, result);
    }

    @Test
    public void addressSearchGMSTestCityNull() {
        String result = new AdressSearchController().addressSearch(null, "4420-542", "Passeio Quinta do Sol");

        assertEquals("", result);
    }

    @Test
    public void addressSearchGMSTestPostalCodeNull() {
        String result = new AdressSearchController().addressSearch(null, null, "Passeio Quinta do Sol");

        assertEquals("", result);
    }
    
    @Test
    public void completePostalAddressBMS() {
        List<String> result = new AdressSearchController().completePostalAddressBMS("Valbom", "4420-542", "Passeio Quinta do Sol");
        List<String> expected = new ArrayList<>();
        
        expected.add("Passeio Quinta do Sol");
        expected.add("4420-542");
        expected.add("Valbom");
        expected.add("Porto");
        expected.add("Portugal");
        
        assertEquals(expected, result);
    }
    
    @Test
    public void completePostalAddressGMS() {
        List<String> result = new AdressSearchController().completePostalAddress("Valbom", "4420-542", "Passeio Quinta do Sol, 19");
        List<String> expected = new ArrayList<>();
        
        expected.add("Passeio Quinta do Sol, 19");
        expected.add("4420-542");
        expected.add("Valbom");
        expected.add("Porto");
        expected.add("Portugal");
        
        assertEquals(expected, result);
    }
    
}
