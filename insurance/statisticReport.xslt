<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
        <xsl:template match = "/report">
            <html>
            <body>
            <table border  = '1'>
                <tr>
                    <th>Number of Consultations</th>
                    <td><xsl:value-of select = "numberOfConsultations" /></td>
                </tr>
                    <th>Number of Requests</th>
                    <td><xsl:value-of select = "numberOfRequests"/></td>
                <xsl:apply-templates select="time"/>
                
                
            </table>
            </body>
            </html>
        </xsl:template>

        <xsl:template match = "time">
            <tr>
                <th>Initial Time</th>
                <td><xsl:value-of select="executionTime"/></td>
            </tr>
            <tr>
                <th>Execution Time</th>
                <td><xsl:value-of select="timePassedSinceExecuting"/></td>
            </tr>
        </xsl:template>

</xsl:stylesheet>