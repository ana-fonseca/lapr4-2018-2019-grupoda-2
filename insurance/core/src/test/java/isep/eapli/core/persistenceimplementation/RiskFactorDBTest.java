package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RiskFactorDBTest {
    
    @After
    public void clearDB() {
        RiskFactorDB cdb = new RiskFactorDB("DomainDB");
        for (RiskFactor cov : cdb.findAll()) {
            cdb.remove(cov);
        }
    }

    @Test
    public void checkIfAlreadyExists() throws IntegrityViolationException, ConcurrencyException {
        RiskFactorDB rfdb = new RiskFactorDB("DomainDB");
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        RiskFactor tmp = new RiskFactor(new SurroundingType("type1"), new Metric("metric1"));
        tmp = rfdb.save(tmp);
        assertTrue(rfdb.checkIfAlreadyExists(tmp));
        RiskFactor tmp2 = new RiskFactor(new SurroundingType("type2"), new Metric("metric1"));
        assertFalse(rfdb.checkIfAlreadyExists(tmp2));
        rfdb.delete(tmp);
        stdb.delete(new SurroundingType("type1"));
        assertFalse(rfdb.checkIfAlreadyExists(tmp));
    }
}