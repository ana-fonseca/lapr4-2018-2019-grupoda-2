package isep.eapli.core.domain;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ResultTest {

    @Test
    public void equalsAndHashcode() throws Exception {
        Result result = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<Explanation>(), 10);
        Result result2 = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<Explanation>(), 10);
        assertEquals(result, result2);
        result2 = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new District("Porto"))), new Coverage("Test1")), new ArrayList<Explanation>(), 10);
        assertNotEquals(result, result2);

    }

    @Test
    public void obtainIccAndIndex() throws Exception {
        Result result = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<Explanation>(), 10);
        assertEquals(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), result.obtainIcc());
        assertEquals(10, result.obtainIndex());
    }
}