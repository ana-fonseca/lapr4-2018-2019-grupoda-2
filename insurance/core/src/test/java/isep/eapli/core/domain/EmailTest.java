/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author becas
 */
public class EmailTest {
    
    public EmailTest(){
        
    }
    
    private final String mail1 = "ola1@gmail.com";
    private final String mail2 = "ola";
    private final String mail3 = "ola@gmailcom";
    private final String mail4 = "olagmail.com";
    
    @Test
    public void emailValid() {
        try {
            Email mail = new Email(mail1);
            assertTrue(true);
            
        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
        @Test
    public void emailMissingAtAndDot() {
        try {
            Email mail = new Email(mail2);
            
        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }
        
    }
    
            @Test
    public void emailMissingDot() {
        try {
            Email mail = new Email(mail3);
            
        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }
        
    }
    
            @Test
    public void emailMissingAt() {
        try {
            Email mail = new Email(mail4);
            
        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }
        
    }
}
