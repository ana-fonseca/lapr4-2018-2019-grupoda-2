package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ContributionTest {

    private Contribution cont;

    public ContributionTest(){
        cont = new Contribution("positivo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNecessityError(){
        Contribution cont2 = new Contribution("error");
    }

    @Test
    public void obtainContributionTest() {
        assertEquals(1, cont.obtainContribution());
    }

    @Test
    public void obtainContributionNegativeTest() {
        Contribution cont2 = new Contribution("negative");
        assertEquals(0, cont2.obtainContribution());
    }

    @Test
    public void hashCodeTest() {
        assertEquals(123457, cont.hashCode());
    }

    @Test
    public void equalsTest() {
        Contribution cont2 = new Contribution("positive");
        assertEquals(cont, cont);
        assertNotEquals(null, cont);
        assertNotEquals(cont, new Weight(3));
        assertEquals(cont, cont2);
    }

    @Test
    public void toStringTest() {
        assertEquals("POSITIVO (POSITIVE)", cont.toString());
    }

    @Test
    public void toStringNegativeTest() {
        Contribution cont2 = new Contribution("negative");
        assertEquals("NEGATIVO (NEGATIVE)", cont2.toString());
    }
}