package isep.eapli.core.persistenceimplementation;

import isep.eapli.core.domain.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Test;

public class RequestDBTest {

    EntityManager em;

    @Test
    public void checkIfAlreadyExists() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Request c = new Request(insuranceCoverageConnectionList, "Testing Check if already exists", State.PENDING, new Version(1));
        cdb.save(c);
        cdb.update(c);
        
        boolean exists = cdb.checkIfAlreadyExists(c);
        clearDB();
        codb.remove(cov);
        assertTrue(exists);
    }

    @Test
    public void obtainCaseByVersion() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Version v1 = new Version(1);
        Request requestExpected = new Request(insuranceCoverageConnectionList, "Testing obtain case by version", State.PENDING, v1);
        cdb.save(requestExpected);
        Request requestResult = cdb.obtainCaseByVersion(v1);
        clearDB();
        codb.remove(cov);
        assertEquals(requestExpected, requestResult);
    }

    @Test
    public void findPendingUnassignedRequests() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        rp.getRiskAnalystDB().save(ra1);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Version v1 = new Version(1);
        Version v2 = new Version(2);
        Version v3 = new Version(3);
        Request request1 = new Request(insuranceCoverageConnectionList, "1", State.PENDING, v1);
        Request request2 = new Request(insuranceCoverageConnectionList, "2", State.PENDING, v2);
        Request request3 = new Request(insuranceCoverageConnectionList, "3", State.PENDING, v3);
        Request request4 = new Request(insuranceCoverageConnectionList, "4", State.PENDING, new Version(4));
        request2.assignRiskAnalyst(ra1);
        request3.assignRiskAnalyst(ra1);
        request3.setState(State.VALIDATED);

        List<Request> expected = new ArrayList<>();
        expected.add(request1);
        expected.add(request4);

        cdb.save(request1);
        cdb.save(request2);
        cdb.save(request3);
        cdb.save(request4);

        List<Request> result = cdb.findPendingUnassignedRequests();

        clearDB();

        assertEquals(expected, result);

        codb.remove(cov);
    }

    @Test
    public void findPendingAssignedRequests() throws Exception {

        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        rp.getRiskAnalystDB().save(ra1);
        ra1 = rp.getRiskAnalystDB().update(ra1);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Version v1 = new Version(1);
        Version v2 = new Version(2);
        Version v3 = new Version(3);
        Request request1 = new Request(insuranceCoverageConnectionList, "1", State.PENDING, v1);
        Request request2 = new Request(insuranceCoverageConnectionList, "2", State.PENDING, v2);
        Request request3 = new Request(insuranceCoverageConnectionList, "3", State.PENDING, v3);
        request2.assignRiskAnalyst(ra1);
        request3.assignRiskAnalyst(ra1);
        request3.setState(State.VALIDATED);

        List<Request> expected = new ArrayList<>();
        expected.add(request2);

        cdb.save(request1);
        cdb.save(request2);
        cdb.save(request3);

        List<Request> result = cdb.findPendingAssignedRequests(ra1);

        clearDB();

        assertEquals(expected, result);

        codb.remove(cov);
    }
    
    @Test
    public void testCountProcessingRequests() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        rp.getRiskAnalystDB().save(ra1);
        ra1 = rp.getRiskAnalystDB().update(ra1);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Version v1 = new Version(1);
        Version v2 = new Version(2);
        Version v3 = new Version(3);
        Request request1 = new Request(insuranceCoverageConnectionList, "1", State.PROCESSING, v1);
        Request request2 = new Request(insuranceCoverageConnectionList, "2", State.PENDING, v2);
        Request request3 = new Request(insuranceCoverageConnectionList, "3", State.VALIDATED, v3);
        request2.assignRiskAnalyst(ra1);
        request3.assignRiskAnalyst(ra1);
        
        cdb.save(request1);
        cdb.save(request2);
        cdb.save(request3);
        
        assertEquals(2, cdb.countCurrentRequests());
        
        codb.remove(cov);
    }

    @After
    public void clearDB() {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        for (Request request : cdb.findAll()) {
            cdb.remove(request);
        }
    }
}
