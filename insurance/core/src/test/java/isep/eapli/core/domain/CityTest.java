package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class CityTest {

    @Test
    public void testEqualsWorking () {
        City city1 = new City("Porto");
        City city2 = new City("Porto");
        assertEquals(city1, city2);
    }

    @Test
    public void testEqualsNotWorking () {
        City city1 = new City("Porto");
        City city2 = new City("Lisboa");
        assertNotEquals(city1, city2);
    }

}
