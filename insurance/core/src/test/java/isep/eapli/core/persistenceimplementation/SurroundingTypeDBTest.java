/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.SurroundingType;
import org.junit.After;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author catar
 */
public class SurroundingTypeDBTest {
    
    public SurroundingTypeDBTest() {
    }
    
    @After
    public void clearDB() {
        SurroundingTypeDB cdb = new SurroundingTypeDB("DomainDB");
        for (SurroundingType cov : cdb.findAll()) {
            cdb.remove(cov);
        }
    }
    
    @Test
    public void checkIfAlreadyExists() throws IntegrityViolationException, ConcurrencyException {
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        SurroundingType tmp = new SurroundingType("typeTest1");
        tmp = stdb.save(tmp);
        assertTrue(stdb.checkIfAlreadyExists(tmp));
        assertFalse(stdb.checkIfAlreadyExists(new SurroundingType("typeTest2")));
        stdb.delete(tmp);
        assertFalse(stdb.checkIfAlreadyExists(new SurroundingType("typeTest1")));
    }
    
}
