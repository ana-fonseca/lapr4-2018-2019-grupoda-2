package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StreetTest {

    @Test
    public void testEqualsWorking () {
        Street street1 = new Street("Rua Dr. Bernardino", "453");
        Street street2 = new Street("Rua Dr. Bernardino", "453");
        assertEquals(street1, street2);
    }

    @Test
    public void testEqualsWorkingNull () {
        Street street1 = new Street("Rua Dr. Bernardino");
        Street street2 = new Street("Rua Dr. Bernardino");
        assertEquals(street1, street2);
    }

    @Test
    public void testEqualsNotWorkingStreet () {
        Street street1 = new Street("Rua Dr. Bernardino");
        Street street2 = new Street("Avenida da Boavista", "93");
        assertNotEquals(street1, street2);
    }

    @Test
    public void testEqualsNotWorkingDoorNumber () {
        Street street1 = new Street("Rua Dr. Bernardino", "93");
        Street street2 = new Street("Rua Dr. Bernardino", "453");
        assertNotEquals(street1, street2);
    }

    @Test
    public void testEqualsNotWorkingNullThis () {
        Street street1 = new Street("Rua Dr. Bernardino");
        Street street2 = new Street("Rua Dr. Bernardino", "453");
        assertNotEquals(street1, street2);
    }

    @Test
    public void testEqualsNotWorkingNullOther () {
        Street street2 = new Street("Rua Dr. Bernardino");
        Street street1 = new Street("Rua Dr. Bernardino", "453");
        assertNotEquals(street1, street2);
    }

}
