package isep.eapli.core.domain;

import isep.eapli.core.persistenceimplementation.*;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.Ignore;

public class RiskMatrixTest {

    @After
    public void deleteDB(){
        RepositoryFactory rf = new RepositoryFactory();
        CoverageDB cdb = rf.getCoverageDB();
        SurroundingTypeDB stdb = rf.getSurroundingTypeDB();
        RiskFactorDB rdb = rf.getRiskFactorDB();
        RiskMatrixDB rmdb = rf.getRiskMatrixDB();
        for (RiskMatrix rm: rmdb.findAll()){
            rmdb.remove(rm);
        }
        for (RiskFactor rfa: rdb.findAll()){
            rdb.remove(rfa);
        }
        for (SurroundingType st: stdb.findAll()){
            stdb.remove(st);
        }
        for (Coverage c: cdb.findAll()){
            cdb.remove(c);
        }
    }


    @Test
    public void addAndListTest(){
        CoverageRiskFactorCell crfc = new CoverageRiskFactorCell(new Coverage("Storm"), new RiskFactor
                (new SurroundingType("Police"), new Metric("distance")));
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(crfc);
        RiskMatrix rm = new RiskMatrix(list, "Not Published",1);
        CharacterizedCell cc = new CharacterizedCell(crfc, new Contribution("Positive"), new Weight(3)
                , new Necessity("Required"));
        rm.addNewCharacterizedCell(cc);
        assertEquals(1, rm.getListCharacterizedCell().size());
        assertEquals(cc, rm.getListCharacterizedCell().get(0));
        assertEquals(crfc, rm.getListCoverageRiskFactorCell().get(0));
        deleteDB();
    }

    /**
     * Test for the equals method when matrixes are equal.
     */
    @Test
    public void equalsTestTrue() {
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published",1);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertTrue(rm.equals(rm));
        deleteDB();
    }

    /**
     * Test for the equals method when the matrixes are different.
     */
    @Test
    public void equalsTestFalse(){
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        RiskMatrixDB riskMatrixDB = repositoryAccess.getRiskMatrixDB();

        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        SurroundingType st2 = new SurroundingType("polícia");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st2,m1);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcList2 = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList2.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        riskMatrixDB.save(rm);

        RiskMatrix rm2 = new RiskMatrix(crfcList, "Not Published", 1);
        rm2.addNewCharacterizedCell(cl);
        rm2.addNewDetailedCell(dc);

        riskMatrixDB.save(rm2);
        assertFalse(rm.equals(rm2));
        deleteDB();
    }

    /**
     * Test for the equals method for when the classes are different.
     */
    @Test
    public void equalsTestOtherClass(){
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertFalse(rm.equals("A"));
        deleteDB();
    }

    /**
     * Test for the equals method when the object is null.
     */
    @Test
    public void equalsTestNull(){
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertFalse(rm.equals(null));
        deleteDB();
    }

    /**
     * Test for the toString method when the strings are equal.
     */
    @Test
    public void toStringTest() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        RiskMatrixDB riskMatrixDB = repositoryAccess.getRiskMatrixDB();

        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(crfc1);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(list, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertEquals(rm.toString(),rm.toString());
        deleteDB();
    }

    /**
     * Test for the equals method when the matrixes are different.
     */
    @Test
    public void equalsToStringFalse(){
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        RiskMatrixDB riskMatrixDB = repositoryAccess.getRiskMatrixDB();

        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        SurroundingType st2 = new SurroundingType("polícia");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st2,m1);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcList2 = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList2.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);

        riskMatrixDB.save(rm);
        RiskMatrix rm2 = new RiskMatrix(crfcList2, "Not Published", 0);
        rm2.addNewCharacterizedCell(cl);
        rm2.addNewDetailedCell(dc);

        riskMatrixDB.save(rm2);
        assertFalse(rm.toString().equals(rm2.toString()));
        deleteDB();
    }

    /**
     * Test for the hashCode method when they're equal.
     */
    @Test
    public void hashCodeTest() {
        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(crfc1);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(list, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertEquals(975324383,rm.hashCode());
        deleteDB();
    }

    /**
     * Test for the hashCode method when they're equal.
     */
    @Test
    public void hashCodeTestFalse() {
        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(crfc1);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(list, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertNotEquals(0,rm.hashCode());
        deleteDB();
    }
    @Test
    public void addNewDetailedCell() {
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewDetailedCell(dc);
        RiskMatrix rm2 = new RiskMatrix(crfcList, "Not Published", 0);
        rm2.addNewDetailedCell(dc);
        assertTrue(rm.equals(rm2));
        List<DetailedCell> dcl = new ArrayList<>();
        dcl.add(dc);
        assertEquals(rm.getListDetailedCell(), dcl);
        deleteDB();
    }

    /**
     * Test for the equals without version method when matrixes are equal.
     */
    @Test
    public void equalsWithoutVersionTestTrue() {
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        RiskMatrix rm2 = new RiskMatrix(crfcList, "Not Published", 0);
        rm2.addNewCharacterizedCell(cl);
        rm2.addNewDetailedCell(dc);
        assertTrue(rm.equalsWithoutVersion(rm2));
        assertTrue(rm.equalsWithoutVersion(rm));
        deleteDB();
    }

    /**
     * Test for the equals without version method when the matrixes are different.
     */
    @Test
    public void equalsWithoutVersionTestFalse(){
        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        SurroundingType st2 = new SurroundingType("polícia");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st2,m1);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcList2 = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList2.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        RiskMatrix rm2 = new RiskMatrix(crfcList2, "Not Published", 0);
        rm2.addNewCharacterizedCell(cl);
        rm2.addNewDetailedCell(dc);
        assertFalse(rm.equalsWithoutVersion(rm2));
        deleteDB();
    }

    /**
     * Test for the equals without version method for when the classes are different.
     */
    @Test
    public void equalsWithoutVersionTestOtherClass(){
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertFalse(rm.equalsWithoutVersion("A"));
        deleteDB();
    }

    /**
     * Test for the equals without version method when the object is null.
     */
    @Test
    public void equalsWithoutVersionTestNull(){
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        assertFalse(rm.equalsWithoutVersion(null));
        deleteDB();
    }

    /**
     * Test for the toString method when the strings are equal.
     */
    @Test
    public void riskMatrixPresentationTest() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        RiskMatrixDB riskMatrixDB = repositoryAccess.getRiskMatrixDB();

        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(crfc1);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(list, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);
        riskMatrixDB.save(rm);
        assertEquals(rm.riskMatrixPresentation(),rm.riskMatrixPresentation());
        deleteDB();
    }

    /**
     * Test for the equals method when the matrixes are different.
     */
    @Test
    public void equalsRiskMatrixPresentationFalse(){
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        RiskMatrixDB riskMatrixDB = repositoryAccess.getRiskMatrixDB();

        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        SurroundingType st2 = new SurroundingType("polícia");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st2,m1);
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcList2 = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList2.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        DetailedCell dc = new DetailedCell(cl, new Scale(1,2,3));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewDetailedCell(dc);

        riskMatrixDB.save(rm);
        RiskMatrix rm2 = new RiskMatrix(crfcList2, "Published", 0);
        rm2.addNewCharacterizedCell(cl);
        rm2.addNewDetailedCell(dc);
        riskMatrixDB.save(rm2);
        System.out.println(rm.riskMatrixPresentation());
        System.out.println(rm2.riskMatrixPresentation());
        assertFalse(rm.riskMatrixPresentation().equals(rm2.riskMatrixPresentation()));
        deleteDB();
    }

    @Ignore
    public void calculationWithoutDetails() throws Exception {
        Metric m1 = new Metric("Time");
        SurroundingType st = new SurroundingType("River");
        SurroundingType st2 = new SurroundingType("Fire Fighter Station");
        RiskFactor rf1 = new RiskFactor(st, m1);
        RiskFactor rf2 = new RiskFactor(st2, m1);
        Coverage cov = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(cov, rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(cov, rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<CoverageRiskFactorCell>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        CharacterizedCell c2 = new CharacterizedCell(crfc2, new Contribution("negative"), new Weight(4), new Necessity("Required"));
        DetailedCell dc1 = new DetailedCell(cl, new Scale(1, 2, 3));
        DetailedCell dc2 = new DetailedCell(c2, new Scale(2, 4, 5));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewCharacterizedCell(c2);
        rm.addNewDetailedCell(dc1);
        rm.addNewDetailedCell(dc2);
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        int actualIndex = rm.calculationWithoutDetails(cov, ad);
        int expectedIndex = 50;
        assertEquals(expectedIndex, actualIndex);
    }

    @Ignore
    public void calculationWithDetails() throws Exception{
        Metric m1 = new Metric("Time");
        SurroundingType st = new SurroundingType("River");
        SurroundingType st2 = new SurroundingType("Fire Fighter Station");
        RiskFactor rf1 = new RiskFactor(st, m1);
        RiskFactor rf2 = new RiskFactor(st2, m1);
        Coverage cov = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(cov, rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(cov, rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<CoverageRiskFactorCell>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        CharacterizedCell cl = new CharacterizedCell(crfc1, new Contribution("POSITIVE"), new Weight(2), new Necessity("Required"));
        CharacterizedCell c2 = new CharacterizedCell(crfc2, new Contribution("negative"), new Weight(4), new Necessity("Required"));
        DetailedCell dc1 = new DetailedCell(cl, new Scale(1, 2, 3));
        DetailedCell dc2 = new DetailedCell(c2, new Scale(2, 4, 5));
        RiskMatrix rm = new RiskMatrix(crfcList, "Not Published", 0);
        rm.addNewCharacterizedCell(cl);
        rm.addNewCharacterizedCell(c2);
        rm.addNewDetailedCell(dc1);
        rm.addNewDetailedCell(dc2);
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        HashMap<Integer,List<Explanation>> actualMap = rm.calculationWithDetails(cov, ad);
        HashMap<Integer,List<Explanation>> expectedMap = new HashMap<Integer, List<Explanation>>();
        List<Explanation> exList = new ArrayList<Explanation>();
        exList.add(new Explanation("~~~~~~ Cálculo do valor máximo ~~~~~~\nFatores de risco considerados:\n" +
                "RiskFactor-> Surrounding type: River; Metric: Time\n - Este fator de risco tem um peso de 2 e uma classificação máxima de 3.\n"+
                "RiskFactor-> Surrounding type: Fire Fighter Station; Metric: Time\n - Este fator de risco tem um peso de 4 e uma classificação máxima de 5.\n"+
                "Score máximo final: 26.\n\n"));
        exList.add(new Explanation("~~~~~~ Cálculo do valor obtido ~~~~~~\nFatores de risco considerados:\n"+
                "RiskFactor-> Surrounding type: River; Metric: Time\n" +
                " - Este fator de risco tem um peso de 2 e uma classificação medium. Assim, o valor usado da escala foi 2.\n"+
                "RiskFactor-> Surrounding type: Fire Fighter Station; Metric: Time\n"+
                "- Este fator de risco não tem foi reconhecido pelo serviço de georreferência, mas visto que é obrigatório é considerado a situacão mais gravosa. Assim foi usado o peso 4 e o valor usado da escala foi 5.\n"+
                "Score obtido final: 24.\n\n"));
        exList.add(new Explanation("- O índice de risco é 50."));
        expectedMap.put(50,exList);
        assertEquals(expectedMap, actualMap);
    }


}