package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.SurroundingType;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;


public class RepositoryFactoryTest {

    public RepositoryFactory rp;

    public RepositoryFactoryTest(){
        rp = new RepositoryFactory();
    }
    
    @After
    public void clearDB() {
        CoverageDB cdb = new CoverageDB("DomainDB");
        for (Coverage cov : cdb.findAll()) {
            cdb.remove(cov);
        }
        
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        for (SurroundingType st : stdb.findAll()) {
            stdb.remove(st);
        }
    }


    @Test
    public void getCoverageDB() throws IntegrityViolationException, ConcurrencyException {
        CoverageDB cdb = rp.getCoverageDB();
        assertNotNull(cdb);
        Coverage cov = new Coverage("coverage1");
        assertFalse(cdb.checkIfAlreadyExists(cov));
        cov = cdb.save(cov);
        assertTrue(cdb.checkIfAlreadyExists(new Coverage("coverage1")));
        cdb.delete(cov);
        assertFalse(cdb.checkIfAlreadyExists(new Coverage("coverage1")));
    }

    @Test
    public void testGetSurroundingDB() throws IntegrityViolationException, ConcurrencyException {
        System.out.println("getSurroundingDB");
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        assertNotNull(stdb);
        SurroundingType st = new SurroundingType("st1");
        assertFalse(stdb.checkIfAlreadyExists(st));
        st = stdb.save(new SurroundingType("st1"));
        assertTrue(stdb.checkIfAlreadyExists(new SurroundingType("st1")));
        stdb.delete(st);
        assertFalse(stdb.checkIfAlreadyExists(new SurroundingType("st1")));
    }
}