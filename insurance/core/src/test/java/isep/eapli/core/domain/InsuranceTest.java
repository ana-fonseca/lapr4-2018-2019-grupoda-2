package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class InsuranceTest {

    @Test
    public void getAddress() throws Exception {
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        assertTrue(i.getAddress().equals(ad));
    }

    @Test
    public void equals1() throws Exception {
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        Insurance i2 = new Insurance(ad);
        assertEquals(i, i2);
    }

    @Test
    public void hashCode1() throws Exception {
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        Insurance i2 = new Insurance(ad);
        assertEquals(i.hashCode(), i2.hashCode());
    }
}