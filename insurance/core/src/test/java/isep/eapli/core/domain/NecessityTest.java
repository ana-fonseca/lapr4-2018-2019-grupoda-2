package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class NecessityTest {

    private Necessity nec;
    public NecessityTest(){
        nec=new Necessity("facultativo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNecessityError(){
        Necessity nec2 = new Necessity("error");
    }

    @Test
    public void obtainNecessityTest() {
        assertEquals(0, nec.obtainNecessity());
    }

    @Test
    public void obtainNecessityRequiredTest() {
        Necessity nec2=new Necessity("obrigatorio");
        assertEquals(1, nec2.obtainNecessity());
    }

    @Test
    public void hashCodeTest() {
        assertEquals(12345+nec.obtainNecessity(), nec.hashCode());
    }

    @Test
    public void equalsTest() {
        Necessity nec2=new Necessity("facultativo");
        assertTrue(nec.equals(nec));
        assertFalse(nec.equals(null));
        assertFalse(nec.equals(new Weight(3)));
        assertTrue(nec.equals(nec2));
    }

    @Test
    public void toStringTest() {
        assertEquals("FACULTATIVO (OPTIONAL)", nec.toString());
    }

    @Test
    public void toStringRequiredTest() {
        Necessity nec2 = new Necessity("required");
        assertEquals("OBRIGATORIO (REQUIRED)", nec2.toString());
    }
}