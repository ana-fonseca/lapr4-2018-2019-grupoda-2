package isep.eapli.core.domain;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class SurroundingsTest {

    private Surroundings surroundings;

    public SurroundingsTest(){
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        surroundings = new Surroundings(description, surroundingType, metricStringMap);
    }

    @Test
    public void allGetsTest() {
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        assertEquals(description, surroundings.getDescription());
        assertEquals(surroundingType, surroundings.getType());
        assertEquals(metricStringMap, surroundings.getMetricScaleMap());
    }

    @Test
    public void equalsHashcodeToString() {
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        Surroundings obj = null;
        assertNotEquals(surroundings, obj);
        obj = new Surroundings("failure", surroundingType, metricStringMap);
        assertNotEquals(surroundings, obj);
        obj = new Surroundings(description, new SurroundingType("Failure"), metricStringMap);
        assertNotEquals(surroundings, obj);
        obj = new Surroundings(description, surroundingType, new HashMap<>());
        assertNotEquals(surroundings, obj);
        obj = new Surroundings(description, surroundingType, metricStringMap);
        assertEquals(surroundings, obj);
        assertEquals(surroundings.hashCode(), obj.hashCode());
        StringBuilder finalValue = new StringBuilder("Description: " + description + " " + surroundings.getType().toString() + "\n");
        for (Metric m : obj.getMetricScaleMap().keySet()) {
            finalValue.append(m).append(", ").append(obj.getMetricScaleMap().get(m)).append("\n");
        }
        System.out.println(surroundings);
        assertEquals(finalValue.toString(), surroundings.toString());
    }
}