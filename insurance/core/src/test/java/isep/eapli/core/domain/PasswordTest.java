/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author becas
 */
public class PasswordTest {

    public PasswordTest() {

    }

    private final String pass1 = "Ola123123#";
    private final String pass2 = "ola123123#";
    private final String pass3 = "Ola123123";
    private final String pass4 = "Olaolaola#";
    private final String pass5 = "Ola1#";

    @Test
    public void passValid() {
        try {
            Password pass = new Password(pass1);
            assertTrue(true);

        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void passMissingCapsLock() {
        try {
            Password pass = new Password(pass2);

        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }

    }

    @Test
    public void passMissingSpecialChar() {
        try {
            Password pass = new Password(pass3);

        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }

    }

    @Test
    public void passMissingNumber() {
        try {
            Password pass = new Password(pass4);

        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }

    }

    @Test
    public void passMissingNumberOfChars() {
        try {
            Password pass = new Password(pass5);

        } catch (Exception ex) {
            Logger.getLogger(EmailTest.class.getName()).log(Level.SEVERE, null, ex);
            assertFalse(false);
        }

    }
    
    @Test
    public void passHashCode(){
        try {
            Password pass = new Password(pass1);
            String hashPass = pass.password();
            assertEquals(hashPass, new Password(pass1).password());
        } catch (Exception ex) {
            Logger.getLogger(PasswordTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
