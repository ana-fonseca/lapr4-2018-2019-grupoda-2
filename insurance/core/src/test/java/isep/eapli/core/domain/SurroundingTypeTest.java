/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1171138
 */
public class SurroundingTypeTest {
    
    public SurroundingTypeTest() {
    }
    

    /**
     * Test of obtainDesignation method, of class SurroundingType.
     */
    @Test
    public void testObtainDescription() {
       System.out.println("obtainType");
        SurroundingType instance = new SurroundingType("Forest");
        String expResult = "Forest";
        String result = instance.obtainDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class SurroundingType.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        SurroundingType instance = new SurroundingType("River");
        int expResult = "River".hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class SurroundingType.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        SurroundingType instance = new SurroundingType("Hospital");
        assertEquals(instance, instance);
        Object nullObject = null;
        assertNotEquals(instance, nullObject);
        SurroundingType nova = new SurroundingType("Fire Fighter Station");
        assertNotEquals(instance, nova);
        nova = new SurroundingType("Hospital");
        assertEquals(instance, nova);
    }

    /**
     * Test of toString method, of class SurroundingType.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SurroundingType instance = new SurroundingType("Police");
        String expResult = "Surrounding type: Police";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
