package isep.eapli.core.domain;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Metric class test implementation
 *
 */
public class MetricTest {

    public MetricTest() {
    }

    /**
     * Test of equals method, of class Metric.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Metric instance = new Metric("Distance");
        Metric instance2 = new Metric("Distance");
        boolean expResult = true;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Metric.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Metric instance = new Metric("Distance");
        String expResult = "Metric: Distance";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
