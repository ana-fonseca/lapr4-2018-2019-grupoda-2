package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CountryTest {

    @Test
    public void testEqualsWorking () {
        Country country1 = new Country("Portugal");
        Country country2 = new Country("Portugal");
        assertEquals(country1, country2);
    }

    @Test
    public void testEqualsNotWorking () {
        Country country1 = new Country("Portugal");
        Country country2 = new Country("France");
        assertNotEquals(country1, country2);
    }

}
