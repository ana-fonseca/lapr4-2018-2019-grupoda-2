package isep.eapli.core.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rikar
 */
public class DetailedCellTest {
    
    private final DetailedCell detailedCell;
    
    public DetailedCellTest() {
        CoverageRiskFactorCell cove = new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CharacterizedCell chara = new CharacterizedCell(cove, new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
        Scale scale = new Scale(1,2,3);
        this.detailedCell = new DetailedCell(chara,scale);
    }

    /**
     * Test of hashCode method, of class DetailedCell.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 1408078881;
        int result = detailedCell.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class DetailedCell.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        CoverageRiskFactorCell cove = new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CharacterizedCell chara = new CharacterizedCell(cove, new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
        Scale scale = new Scale(1,2,3);
        DetailedCell instance = new DetailedCell(chara,scale);
        boolean expResult = true;
        boolean result = detailedCell.equals(instance);

        assertEquals(expResult, result);
    }
    
    
    @Test
    public void testNotEqualsScale() {
        System.out.println("notEqualsScale");
         CoverageRiskFactorCell cove = new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CharacterizedCell chara = new CharacterizedCell(cove, new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
        Scale scale = new Scale(1,2,4);
        DetailedCell instance = new DetailedCell(chara,scale);
        boolean expResult = false;
        boolean result = detailedCell.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsCharacterizedCell() {
        System.out.println("notEqualsCharacterizedCell");
         CoverageRiskFactorCell cove = new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CharacterizedCell chara = new CharacterizedCell(cove, new Contribution("Negativo"), new Weight(3), new Necessity("Obrigatorio"));
        Scale scale = new Scale(1,2,3);
        DetailedCell instance = new DetailedCell(chara,scale);
        boolean expResult = false;
        boolean result = detailedCell.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsCoverageRiskFactorCell() {
        System.out.println("notEqualsCoverageRiskFactorCell");
        CoverageRiskFactorCell cove = new CoverageRiskFactorCell(new Coverage("Sismo"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CharacterizedCell chara = new CharacterizedCell(cove, new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
        Scale scale = new Scale(1,2,3);
        DetailedCell instance = new DetailedCell(chara,scale);
        boolean expResult = false;
        boolean result = instance.equals(detailedCell);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsClass() {
        System.out.println("notEqualsClass");
        Necessity instance = new Necessity();
        boolean expResult = false;
        boolean result = detailedCell.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsNull() {
        System.out.println("notEqualsNull");
        DetailedCell instance = null;
        boolean expResult = false;
        boolean result = detailedCell.equals(instance);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class DetailedCell.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "DetailedCell: Characterized Cell = \n" +
                "Coverage Risk Factor Cell with Coverage Incendio and RiskFactor-> Surrounding type: Bombeiros; Metric: Distancia\n" +
                "Contribution: POSITIVO (POSITIVE)\n" +
                "PESO (WEIGHT) :3\n" +
                "Necessity: OBRIGATORIO (REQUIRED)Scale:\n\n" +
                "Low scale: 1\n" +
                "Medium scale: 2\n" +
                "High scale: 3";
        String result = detailedCell.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testToStringFalse() {
        System.out.println("toStringFalse");
        String expResult = "DetailedCell: Characterized Cell = \n" +
                "Coverage Risk Factor Cell with Coverage Incendio and RiskFactor-> Surrounding type: Bombeiros; Metric: Distancia\n" +
                "Contribution: POSITIVO (POSITIVE)\n" +
                "PESO (WEIGHT) :3\n" +
                "Necessity: OBRIGATORIO (REQUIRED)Scale:\n\n" +
                "Low scale: 1\n" +
                "Medium scale: 2\n" +
                "High scale: 4";
        String result = detailedCell.toString();
        assertNotEquals(expResult, result);
    }
    
}
