package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class WeightTest {

    private Weight w;
    public WeightTest(){
        w = new Weight(3);
    }
    @Test
    public void obtainWeightTest() {
        assertEquals(3, w.obtainWeight());
    }

    @Test
    public void hashCodeTest() {
        assertEquals(1234570, w.hashCode());
    }

    @Test
    public void equalsTest() {
        Weight w2 = new Weight(3);
        assertTrue(w.equals(w));
        assertFalse(w.equals(null));
        assertFalse(w.equals(new Contribution("negativo")));
        assertTrue(w.equals(w2));
    }

    @Test
    public void toStringTest() {
        assertEquals("PESO (WEIGHT) :3", w.toString());
    }
}