package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class InsuranceCoverageConnectionTest {

    @Test
    public void obtainCoverage() throws Exception {
        Coverage c = new Coverage("a");
        Insurance i = new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto")));
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i, c);
        assertTrue(icc.obtainCoverage().equals(c));
    }

    @Test
    public void obtainInsurance() throws Exception {
        Coverage c = new Coverage("a");
        Insurance i = new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto")));
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i, c);
        assertTrue(icc.obtainInsurance().equals(i));
    }

    @Test
    public void equalsHashcode() throws Exception {
        Coverage c = new Coverage("a");
        Insurance i = new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto")));
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i, c);
        InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(i, c);
        assertEquals(icc, icc2);
        assertEquals(icc.hashCode(), icc2.hashCode());
    }
}