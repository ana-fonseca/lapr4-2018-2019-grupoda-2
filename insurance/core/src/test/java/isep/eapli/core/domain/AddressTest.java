package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class AddressTest {

    Street streetComplete = new Street("Rua Dr. Bernardino", "453");
    City city = new City("Porto");
    PostalCode postalCode;
    District district = new District("Porto");
    Country country = new Country("Portugal");
    Address complete;

    public AddressTest () throws Exception {
        postalCode = new PostalCode("4100-150");
        complete = new Address(streetComplete, city, postalCode, country, district);
    }

    @Test
    public void equalsCompleteTrue () {
        Address complete2 = new Address(streetComplete, city, postalCode, country, district);

        assertEquals(complete, complete2);
    }

    @Test
    public void equalsTrueCountryNull () {
        Address complete1 = new Address(streetComplete, city, postalCode, district);
        Address complete2 = new Address(streetComplete, city, postalCode, district);

        assertEquals(complete1, complete2);
    }

    @Test
    public void equalsTrueDistrictNull () {
        Address complete1 = new Address(streetComplete, city, postalCode, country);
        Address complete2 = new Address(streetComplete, city, postalCode, country);

        assertEquals(complete1, complete2);
    }

    @Test
    public void equalsTrueCountryDistrictNull () {
        Address complete1 = new Address(streetComplete, city, postalCode);
        Address complete2 = new Address(streetComplete, city, postalCode);

        assertEquals(complete1, complete2);
    }

    @Test
    public void equalsCompleteFalseStreet () {
        Address complete2 = new Address(new Street("Avenida da Boavista"), city, postalCode, country, district);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseCity () {
        Address complete2 = new Address(streetComplete, new City("Braga"), postalCode, country, district);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalsePostalCode () throws Exception {
        Address complete2 = new Address(streetComplete, city, new PostalCode("4100-140"), country, district);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseCountry () {
        Address complete2 = new Address(streetComplete, city, postalCode, new Country("France"), district);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseDistrict () {
        Address complete2 = new Address(streetComplete, city, postalCode, country, new District("Braga"));

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseDistrictNull () {
        Address complete2 = new Address(streetComplete, city, postalCode, country);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseCountryNull () {
        Address complete2 = new Address(streetComplete, city, postalCode, district);

        assertNotEquals(complete, complete2);
    }

    @Test
    public void equalsCompleteFalseDistrictCountryNull () {
        Address complete2 = new Address(streetComplete, city, postalCode);

        assertNotEquals(complete, complete2);
    }

}