package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class CoverageRiskFactorCellTest {

    @Test
    public void toStringTestTrue() {
        Metric m1 = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        String expected = "Coverage Risk Factor Cell with Coverage tempestades and RiskFactor-> Surrounding type: bombeiros; Metric: distância";
        String actual = crfc1.toString();
        assertEquals(expected,actual);
    }

    @Test
    public void toStringTestFalse(){
        Metric m1 = new Metric("tempo");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        String expected = "Coverage Risk Factor Cell with Coverage tempestades and RiskFactor-> Surrounding type: bombeiros; Metric: distância";
        String actual = crfc1.toString();
        assertNotEquals(expected,actual);
    }

    @Test
    public void hashCodeTestTrue() {
        Metric m = new Metric("distância");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        int expected = 730831874;
        int actual = crfc1.hashCode();
        assertEquals(expected,actual);
    }

    @Test
    public void hashCodeTestFalse() {
        Metric m1 = new Metric("lala");
        SurroundingType st = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        int expected = 730831874;
        int actual = crfc1.hashCode();
        assertNotEquals(expected,actual);
    }

    @Test
    public void equalsTestTrue(){
        Metric m1 = new Metric("distância");
        SurroundingType st1 = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st1,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        Metric m2 = new Metric("distância");
        SurroundingType st2 = new SurroundingType("bombeiros");
        RiskFactor rf2 = new RiskFactor(st2,m2);
        Coverage c2 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        assertTrue(crfc1.equals(crfc2));
    }

    @Test
    public void equalsTestFalse(){
        Metric m1 = new Metric("tempo");
        SurroundingType st1 = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st1,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        Metric m2 = new Metric("distância");
        SurroundingType st2 = new SurroundingType("bombeiros");
        RiskFactor rf2 = new RiskFactor(st2,m2);
        Coverage c2 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        assertFalse(crfc1.equals(crfc2));
    }

    @Test
    public void equalsTestNull(){
        Metric m1 = new Metric("distância");
        SurroundingType st1 = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st1,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        assertFalse(crfc1.equals(null));
    }

    @Test
    public void equalsTestOtherClass(){
        Metric m1 = new Metric("distância");
        SurroundingType st1 = new SurroundingType("bombeiros");
        RiskFactor rf1 = new RiskFactor(st1,m1);
        Coverage c1 = new Coverage("tempestades");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        assertFalse(crfc1.equals("aa"));
    }
}