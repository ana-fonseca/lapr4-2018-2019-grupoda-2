package isep.eapli.core.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rikar
 */
public class ScaleTest {
    
    private final Scale scale;
    public ScaleTest() {
        scale = new Scale(1,2,3);
    }


    /**
     * Test of hashCode method, of class Scale.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 179772;
        int result = scale.hashCode();
        assertEquals(expResult, result);   
    }
    
    @Test
    public void testHashCode2() {
        System.out.println("hashCode");
        Scale instance = new Scale(1,2,4);
        int expResult = 179773;
        int result = instance.hashCode();
        assertEquals(expResult, result);   
    }

    /**
     * Test of equals method, of class Scale.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Scale instance = new Scale(1,2,3);
        boolean expResult = true;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsLowValue() {
        System.out.println("equals");
        Scale instance = new Scale(2,2,3);
        boolean expResult = false;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsMediumValue() {
        System.out.println("equals");
        Scale instance = new Scale(1,3,3);
        boolean expResult = false;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsHighValue() {
        System.out.println("equals");
        Scale instance = new Scale(1,2,4);
        boolean expResult = false;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsClass() {
        System.out.println("equals");
        Necessity instance = new Necessity();
        boolean expResult = false;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNotEqualsNull() {
        System.out.println("equals");
        Scale instance = null;
        boolean expResult = false;
        boolean result = scale.equals(instance);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Scale.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "\nLow scale: 1\n" +
                "Medium scale: 2\n" +
                "High scale: 3";
        String result = scale.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testToStringFalse() {
        System.out.println("toStringFalse");
        String expResult = "Low scale: 2\n" +
                "Medium scale: 2\n" +
                "High scale: 3";
        String result = scale.toString();
        assertNotEquals(expResult, result);
    }
    
}
