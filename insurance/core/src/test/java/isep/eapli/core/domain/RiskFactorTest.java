package isep.eapli.core.domain;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * RiskFactor class test implementation
 */
public class RiskFactorTest {

    public RiskFactorTest() {
    }

    /**
     * Test of equals method, of class RiskFactor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        RiskFactor instance = new RiskFactor(new SurroundingType("Forest"), new Metric("Distance"));
        RiskFactor instance2 = new RiskFactor(new SurroundingType("Forest"), new Metric("Distance"));
        boolean expResult = true;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class RiskFactor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        RiskFactor instance = new RiskFactor(new SurroundingType("Forest"), new Metric("Distance"));
        String expResult = "RiskFactor-> Surrounding type: Forest; Metric: Distance";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
