package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class VersionTest {

    @Test
    public void equalsTest() {
        Version v1 = new Version(5);
        Version v2 = new Version(5);
        assertEquals(v1,v2);
    }

    @Test
    public void hashCodeTest() {
        Version v1 = new Version(6);
        Version v2 = new Version(6);
        assertEquals(v1.hashCode(),v2.hashCode());
    }

    @Test
    public void toStringTest() {
        Version v1 = new Version(5);
        Version v2 = new Version(5);
        assertEquals(v2.toString(),v1.toString());
    }
}