package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExplanationTest {

    @Test
    public void equalsAndHashcode() {
        Explanation a = new Explanation("Bandido");
        Explanation b = new Explanation("Bandido");
        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
        b = new Explanation("NaoBandido");
        assertNotEquals(a, b);
        assertNotEquals(a, null);
    }

    @Test
    public void toString1() {
        Explanation a = new Explanation("Bandido");
        assertEquals("Bandido", a.toString());
    }
}