

package isep.eapli.core.persistenceimplementation;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.domain.Surroundings;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;


public class SurroundingDBTest {

    @After
    public void clearDB(){
        SurroundingDB db = new RepositoryFactory().getSurroundingDB();
        for (NearBySurrounding s : db.findAll()) {
            db.remove(s);
        }
        SurroundingTypeDB stDB = new RepositoryFactory().getSurroundingTypeDB();
        for (SurroundingType s : stDB.findAll()) {
            stDB.remove(s);
        }
    }
    
    @Test
    public void testPersistenceNearbySurrounding() throws Exception{
        //needed objets instance
        clearDB();
        Metric m = new Metric("MetricTest");
        String m1 = "1" ;
        SurroundingType st = new SurroundingType("SurroundingTypeTest");
        SurroundingTypeDB stDB = new RepositoryFactory().getSurroundingTypeDB();
        st = stDB.save(st);
        Street street = new Street("Av boavista");
        City city = new City("porto");
        PostalCode postalCode = new PostalCode("4050-112");
       
        
        Address add = new Address(street, city, postalCode);
        Map<Metric,String> map1 = new HashMap<>();
        map1.put(m, m1);
        
        //create NearBySurrounding object
        NearBySurrounding beforeSave = new NearBySurrounding("RefinariaTest", st, map1, add);
        //db instance
        SurroundingDB sDB = new RepositoryFactory().getSurroundingDB();
        //save nearbySurrounding
        beforeSave = sDB.save(beforeSave);
        
        //get saved nearbySurrounding
        List<NearBySurrounding> listTest = sDB.getAll();
        //get the saved element
        NearBySurrounding afterSave = listTest.get(0);
        
        assertEquals(beforeSave, afterSave);
    }
}
