package isep.eapli.core.domain;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PostalCodeTest {

    @Test
    public void testConstructorWorking () {
        try {
            PostalCode postalCode1 = new PostalCode("4100-150");
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConstructorNotWorking () {
        try {
            PostalCode postalCode1 = new PostalCode("f-150");
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testEqualsWorking () {
        try {
            PostalCode postalCode1 = new PostalCode("4100-150");
            PostalCode postalCode2 = new PostalCode("4100 - 150");
            assertEquals(postalCode1, postalCode2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEqualsNotWorkingWrongFirstDigits () {
        try {
            PostalCode postalCode1 = new PostalCode("4100-150");
            PostalCode postalCode2 = new PostalCode("4150-100");
            assertNotEquals(postalCode1, postalCode2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEqualsNotWorkingWrongLastDigits () {
        try {
            PostalCode postalCode1 = new PostalCode("4100-150");
            PostalCode postalCode2 = new PostalCode("4100-100");
            assertNotEquals(postalCode1, postalCode2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEqualsNotWorkingWrong () {
        try {
            PostalCode postalCode1 = new PostalCode("4100-150");
            PostalCode postalCode2 = new PostalCode("4150-100");
            assertNotEquals(postalCode1, postalCode2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
