package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class CharacterizedCellTest {

    private CharacterizedCell cm;

    public CharacterizedCellTest(){
        cm = new CharacterizedCell(new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia"))), new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
    }

    @Test
    public void hashCodeTest() {
        assertEquals(-1340057407, cm.hashCode());
    }

    @Test
    public void obtainCoverageRisk() {
        CoverageRiskFactorCell crfc = new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia")));
        CoverageRiskFactorCell c = cm.obtainCoverageRisk();
        assertEquals(c,crfc);
    }
    @Test
    public void equalsTest() {
        assertFalse(cm.equals(null));
        assertFalse(cm.equals(new Weight(3)));
        assertTrue(cm.equals(cm));
        CharacterizedCell cm2 = new CharacterizedCell(new CoverageRiskFactorCell(new Coverage("Incendio"), new RiskFactor(new SurroundingType("Bombeiros"),
                new Metric("Distancia"))), new Contribution("Positivo"), new Weight(3), new Necessity("Obrigatorio"));
        assertTrue(cm.equals(cm2));
    }

    @Test
    public void toStringTest() {
        String exp="\nCoverage Risk Factor Cell with Coverage Incendio and RiskFactor-> Surrounding type: Bombeiros; Metric: Distancia"+
                "\nContribution: POSITIVO (POSITIVE)"+
                "\nPESO (WEIGHT) :3"+
                "\nNecessity: OBRIGATORIO (REQUIRED)";
        assertEquals(exp, cm.toString());
    }
}