package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Coverage;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoverageDBTest {
    
    @After
    public void clearDB() {
        CoverageDB cdb = new CoverageDB("DomainDB");
        for (Coverage cov : cdb.findAll()) {
            cdb.remove(cov);
        }
    }

    @Test
    public void checkIfAlreadyExists() throws IntegrityViolationException, ConcurrencyException {
        CoverageDB cdb = new CoverageDB("DomainDB");
        Coverage tmp1 = new Coverage("coveragetest1");
        tmp1 = cdb.save(tmp1);
        assertTrue(cdb.checkIfAlreadyExists(tmp1));
        Coverage tmp = new Coverage("cOverageTest1");
        tmp = cdb.save(tmp);
        assertTrue(cdb.checkIfAlreadyExists(tmp));
        tmp = new Coverage("coveragetest2");
        assertFalse(cdb.checkIfAlreadyExists(tmp));
        cdb.delete(tmp1);
        assertFalse(cdb.checkIfAlreadyExists(new Coverage("coveragetest1")));
    }
}