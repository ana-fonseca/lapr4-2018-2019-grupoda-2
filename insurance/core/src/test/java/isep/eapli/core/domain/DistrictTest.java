package isep.eapli.core.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DistrictTest {

    @Test
    public void testEqualsWorking () {
        District district1 = new District("Porto");
        District district2 = new District("Porto");
        assertEquals(district1, district2);
    }

    @Test
    public void testEqualsNotWorking () {
        District district1 = new District("Porto");
        District district2 = new District("Braga");
        assertNotEquals(district1, district2);
    }

}
