/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1170982
 */
public class CoverageTest {
    
    public CoverageTest() {
    }

    /**
     * Test of obtainDesignation method, of class Coverage.
     */
    @Test
    public void testObtainDesignation() {
        System.out.println("obtainDesignation");
        Coverage instance = new Coverage("Fire");
        String expResult = "Fire";
        String result = instance.obtainDesignation();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Coverage.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Coverage instance = new Coverage("Water");
        int expResult = "Water".hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Coverage.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Coverage instance = new Coverage("Earth");
        assertEquals(instance, instance);
        assertNotEquals(instance, null);
        Coverage nova = new Coverage("Mud");
        assertNotEquals(instance, nova);
        nova = new Coverage("Earth");
        assertEquals(instance, nova);
    }

    /**
     * Test of toString method, of class Coverage.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Coverage instance = new Coverage("Wind");
        String expResult = "Coverage related to Wind";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
