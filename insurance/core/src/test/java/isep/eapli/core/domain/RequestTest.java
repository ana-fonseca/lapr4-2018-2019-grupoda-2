package isep.eapli.core.domain;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RequestTest {

    @Test
    public void getMatrixVersion() throws Exception {
        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request cas = new Request(iccList,"a",State.PENDING,v);
        assertTrue(cas.getMatrixVersion().equals(v));
    }

    @Test
    public void getInsuranceCoverageConnectionList() throws Exception {
        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request cas = new Request(iccList,"a",State.PENDING,v);
        assertTrue(cas.getInsuranceCoverageConnectionList().equals(iccList));
    }

    @Test
    public void equals1() throws Exception {
        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request cas1 = new Request(iccList,"a",State.PENDING,v);
        Request cas2 = new Request(iccList,"a",State.PENDING,v);
        assertTrue(cas1.equals(cas2));
    }

    @Test
    public void hashCode1() throws Exception {
        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request cas1 = new Request(iccList,"a",State.PENDING,v);
        Request cas2 = new Request(iccList,"a",State.PENDING,v);
        assertEquals(cas2.hashCode(), cas1.hashCode());
    }

    @Test
    public void addResult() throws Exception {
        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request cas1 = new Request(iccList,"a",State.PENDING,v);
        cas1.addResult(new Result(icc, new ArrayList<Explanation>(), 10));
        Result r = new Result(icc, new ArrayList<Explanation>(), 10);
        assertEquals(1, cas1.getResultList().size());
        assertTrue(cas1.getResultList().contains(r));
    }

    @Test
    public void assignRiskAnalystWorking() throws Exception {
        RiskAnalyst riskAnalyst = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");

        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request request = new Request(iccList,"a",State.PENDING,v);
        assertTrue(request.obtainRequestedDate().format(DateTimeFormatter.BASIC_ISO_DATE).equals(LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE)));

        assertTrue(request.assignRiskAnalyst(riskAnalyst));
        assertTrue(request.obtainAssignedDate().format(DateTimeFormatter.BASIC_ISO_DATE).equals(LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE)));

        assertEquals(riskAnalyst, request.obtainRiskAnalyst());
    }

    @Test
    public void assignRiskAnalystNotWorking() throws Exception {
        RiskAnalyst riskAnalyst = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        RiskAnalyst riskAnalyst2 = new RiskAnalyst("lapr4da2.1@isep.ipp.pt", "lapr4DA#2");

        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request request = new Request(iccList,"a",State.PENDING,v);
        request.assignRiskAnalyst(riskAnalyst);

        assertFalse(request.assignRiskAnalyst(riskAnalyst2));
    }
}