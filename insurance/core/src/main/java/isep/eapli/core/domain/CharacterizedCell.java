package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

/**
 * Characterized Cell class object
 */
@Embeddable
public class CharacterizedCell implements Serializable {

    /**
     * Combination of the risk factor and coverage
     */
    @Embedded
    private CoverageRiskFactorCell coverageRiskFactorCell;

    /**
     * Contribution of the risk factor to the coverage
     */
    @Embedded
    private Contribution contribution;

    /**
     * Weight of the risk factor to the coverage
     */
    @Embedded
    private Weight weight;

    /**
     * Necessity of the risk factor to the coverage
     */
    @Embedded
    private Necessity necessity;

    /**
     * Empty constructor
     */
    protected CharacterizedCell(){
    }

    /**
     * Complete constructor
     * @param crfcell Coverage and Risk Factor Pair
     * @param cont Contribution
     * @param w Weight
     * @param n Necessity
     */
    public CharacterizedCell(CoverageRiskFactorCell crfcell, Contribution cont, Weight w, Necessity n){
        coverageRiskFactorCell=crfcell;
        contribution=cont;
        weight=w;
        necessity=n;
    }

    public CoverageRiskFactorCell obtainCoverageRisk() {
        return coverageRiskFactorCell;
    }

    /**
     * Get weight
     *
     * @return weight
     */
    public Weight getWeight(){ return weight; }

    /**
     * Get necessity
     *
     * @return necessity
     */
    public Necessity getNecessity(){ return necessity; }

    /**
     * Hash Code of the Characterized Cell
     * @return HashCode
     */
    @Override
    public int hashCode() {
        return coverageRiskFactorCell.hashCode();
    }

    /**
     * Equals method of the Characterized Cell
     * @param obj Cell to compare to
     * @return true if equals, false if not
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            return false;
        }
        CharacterizedCell cm2 = (CharacterizedCell) obj;
        return ((this.coverageRiskFactorCell.equals(cm2.coverageRiskFactorCell)) && (this.necessity.equals(cm2.necessity))
                && (this.weight.equals(cm2.weight)) && (this.contribution.equals(cm2.contribution)));
    }

    /**
     * To String method of the Characterized Cell
     * @return String description of the Characterized Cell
     */
    @Override
    public String toString() {
        return "\n"+coverageRiskFactorCell.toString()+
                "\nContribution: "+contribution.toString()+
                "\n"+weight.toString()+
                "\nNecessity: "+necessity.toString();
    }
}
