package isep.eapli.core.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author 1171138
 */
@Entity
public class SurroundingType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    /**
     * Surrounding description
     */
    private String description;

    //valores para a conversão para a escala da matriz valueA minimo, valueB maximo
    private int distanceValueA;
    private int distanceValueB;
    private int timeValueA;
    private int timeValueB;
    
    /**
     * Protected constructor, to be used with DB only
     */
    protected SurroundingType() {
        //Left empty on porpouse, not for use unless for DB 
    }

    /**
     * Complete constructor of surroundingType class
     *
     * @param description description of surrounding
     */
    public SurroundingType(String description) {
        if (!description.isEmpty()) {
            this.description = description;
        } else {
            throw new IllegalArgumentException("Description cannot be empty");
        }
        distanceValueA = 15;
        distanceValueB = 30;
        timeValueA = 123;
        timeValueB = 12423;
    }

    public int getDistanceValueA() {
        return distanceValueA;
    }

    public int getDistanceValueB() {
        return distanceValueB;
    }

    public int getTimeValueA() {
        return timeValueA;
    }

    public int getTimeValueB() {
        return timeValueB;
    }

    /**
     * Method that returns the description of the SurroundingType object
     *
     * @return designation of the coverage
     */
    public String obtainDescription() {
        return description;
    }

    /**
     * Hahcode method
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return description.hashCode();
    }


    /**
     * Method that checks if one object is equal to another
     *
     * @param obj Object to be compared
     * @return result of the comparation
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SurroundingType other = (SurroundingType) obj;
        return(this.description.equals(other.description));
    }

    /**
     * toString of surrounding Type class
     *
     * @return string
     */
    @Override
    public String toString() {
        return "Surrounding type: " + description;
    }


}
