/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import javax.persistence.Embeddable;

/**
 *
 * @author DoriaTheGod
 */
@Embeddable
public class Email {
    
    private static final String REGEXP = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$)";
    private String email;
    
    public Email(){
    }
    
    public Email(String email) throws Exception{
        if (!email.matches(REGEXP)) {
            throw new Exception("Invalid Email");
        }
        this.email = email;
    }
    
    public String Email(){
        return email;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Email)) {
            return false;
        }
        Email other = (Email) o;
        return other.email.equals(email);
    }
    
}
