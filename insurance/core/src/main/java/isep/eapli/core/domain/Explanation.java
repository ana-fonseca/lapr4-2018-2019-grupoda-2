package isep.eapli.core.domain;

import java.io.Serializable;
import java.util.Objects;

public class Explanation implements Serializable {

    /**
     * Attribute details
     */
    private String details;

    /**
     * Complete constructor
     *
     * @param details
     */
    public Explanation(String details) {
        this.details = details;
    }

    /**
     * Equals method of the class
     *
     * @param o object to compare
     * @return result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Explanation that = (Explanation) o;
        return details.equals(that.details);
    }

    /**
     * Hash code of the object
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(details);
    }

    /**
     * ToString methodd of the class
     *
     * @return String with object explanation
     */
    @Override
    public String toString() {
        return details;
    }
}
