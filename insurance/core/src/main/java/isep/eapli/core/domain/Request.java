package isep.eapli.core.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
public class Request implements Serializable {

    /**
     * Attribute list with insurance and coverage connections.
     */
    @ElementCollection
    private List<InsuranceCoverageConnection> iccList;

    /**
     * Attribute list with insurance and coverage connections.
     */
    @ElementCollection
    private List<Result> resultList;

    /**
     * Attribute type
     */
    private String type;
    
    private String observation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REQUEST_ID")
    private long rID;

    /**
     * Attribute state of the case
     */
    @Enumerated(EnumType.STRING)
    private State state;

    @ManyToOne
    private RiskAnalyst riskAnalyst;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime requestedDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime assignedDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime decisionDate;
    /**
     * Atribute version
     */
    @Embedded
    private Version version;

    /**
     * Complete constructor
     *
     * @param iccList
     * @param type
     * @param state
     * @param version
     */
    public Request(List<InsuranceCoverageConnection> iccList, String type, State state, Version version) {
        this.iccList = iccList;
        this.type = type;
        this.state = state;
        this.version = version;
        this.resultList = new LinkedList<>();
        this.requestedDate = LocalDateTime.now();
        this.observation = new String();
    }

    /**
     * Constructor for database purposes
     */
    protected Request() {
    }
    
    /**
     * Get matrix version
     *
     * @return matrix version
     */
    public Version getMatrixVersion() {
        return version;
    }

    
     /**
     * Get case type
     *
     * @return type
     */
    public String obtainType() {
        return type;
    }

    public long obtainID() {
        return rID;
    }

     /**
     * Get case state
     *
     * @return state
     */
    public State obtainState() {
        return state;
    }

    public RiskAnalyst obtainRiskAnalyst() { return riskAnalyst; }

    public LocalDateTime obtainRequestedDate() { return requestedDate; }

    public LocalDateTime obtainAssignedDate() { return assignedDate; }
    
    public LocalDateTime obtainDecisionDate() { return decisionDate; }

    /**
     * Set matrix version
     *
     * @param version - new version
     */
    public void setMatrixVersion(Version version) {
        this.version = version;
    }

    public void changeDecisionDate() {
        this.decisionDate = LocalDateTime.now();
    }

    
    /**
     * Method that defines the state
     *
     * @param state state to be defined
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * Assign a risk analyst to this request if there are none assigned to it
     *
     * @param riskAnalyst
     * @return
     */
    public boolean assignRiskAnalyst(RiskAnalyst riskAnalyst) {
        if (this.riskAnalyst == null) {
            this.riskAnalyst = riskAnalyst;
            this.assignedDate = LocalDateTime.now();
            return true;
        }
        return false;
    }

    /**
     * Get insurance and coverage connections çist
     *
     * @return
     */
    public List<InsuranceCoverageConnection> getInsuranceCoverageConnectionList() {
        return iccList;
    }

    /**
     * Get result list of the case
     *
     * @return list of all the results
     */
    public List<Result> getResultList() {
        return resultList;
    }

    /**
     * Method that adds a result to the case list
     *
     * @param result result to add
     */
    public boolean addResult(Result result) {
        if (result != null) {
            return this.resultList.add(result);
        }
        return false;
    }

    public void introduceObs(String observation) {
        this.observation = observation;
    }
    
    public void decide(){
        decisionDate = LocalDateTime.now();
    }

    /**
     * Equals method for the case class.
     *
     * @param o
     * @return true if equal, false if different
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request cas = (Request) o;
        return iccList.equals(cas.iccList) && type.equals(cas.type)
                && state.equals(cas.state) && version.equals(cas.version) && resultList.equals(cas.resultList);
    }

    /**
     * HashCode method for the version class.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(iccList, type, state, version, resultList);
    }

}
