package isep.eapli.core.domain;

import java.util.Map;
import java.util.Objects;

public class Surroundings {

    /**
     * Surrounding description
     */
    private String description;

    /**
     * SurroundingType registered in the Surrounding
     */
    private SurroundingType type;

    public void setType(SurroundingType type) {
        this.type = type;
    }

    /**
     * Map connecting an Metric to a String value, representing the evalution
     * (low, medium, high)
     */
    private Map<Metric, String> metricScaleMap;

    public Surroundings() {
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Public class constructor
     *
     * @param description description of the surrounding
     * @param type surrounding type
     * @param metricScaleMap map defining the connection between a Metric and
     * its evaluation
     */
    public Surroundings(String description, SurroundingType type, Map<Metric, String> metricScaleMap) {
        this.description = description;
        this.type = type;
        this.metricScaleMap = metricScaleMap;
    }

    /**
     * Method that obtains the description
     *
     * @return description of the surrounding
     */
    public String getDescription() {
        return description;
    }

    /**
     * Obtain SurroundingType of the Surrounding
     *
     * @return surrounding type
     */
    public SurroundingType getType() {
        return type;
    }

    /**
     * Obtain Map of connection between a Metric and a String
     *
     * @return map
     */
    public Map<Metric, String> getMetricScaleMap() {
        return metricScaleMap;
    }

    /**
     * Equals method
     *
     * @param o object to compare to
     * @return true if equals, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Surroundings that = (Surroundings) o;
        return description.equalsIgnoreCase(((Surroundings) o).description) && type.equals(that.type)
                && metricScaleMap.equals(that.metricScaleMap);
    }

    /**
     * Method that returns the hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(type, metricScaleMap, description);
    }

    /**
     * Method that gives the information about the object
     *
     * @return Object information
     */
    @Override
    public String toString() {
        StringBuilder finalValue = new StringBuilder("Description: " + description + " " + type.toString() + "\n");
        for (Metric m : getMetricScaleMap().keySet()) {
            finalValue.append(m).append(", ").append(metricScaleMap.get(m)).append("\n");
        }
        return finalValue.toString();
    }
}
