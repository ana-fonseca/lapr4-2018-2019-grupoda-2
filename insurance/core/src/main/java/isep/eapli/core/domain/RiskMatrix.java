package isep.eapli.core.domain;

import isep.eapli.georef.controller.AdressSearchController;
import isep.eapli.georef.controller.ObtainSurroundingsController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class RiskMatrix implements Serializable {

    /**
     * Designation of the current version.
     */
    @EmbeddedId
    private Version currentVersion;

    /**
     * List of Coverage Risk Factor cells.
     */
    @ElementCollection
    private List<CoverageRiskFactorCell> crfcList;

    /**
     * List of Characterized cells.
     */
    @ElementCollection
    private List<CharacterizedCell> ccList;

    /**
     * List of Detailed cells.
     */
    @ElementCollection
    private List<DetailedCell> dcList;

    @Column(unique = true)
    private int idRiskMatriz;

    /**
     * String that defines the non publication version's name of this risk
     * matrix. Can represent a version's name or "Not published".
     */
    private String nonPublishedVersionName;

    private static int numMatrix = 0;

    /**
     * Complete constructor.
     *
     * @param crfcList list of coverage risk factor cells
     * @param nonPublishedVersionName string containing this risk matrix's non
     * publicated version's name
     */
    public RiskMatrix(List<CoverageRiskFactorCell> crfcList, String nonPublishedVersionName, int num) {
        this.crfcList = new ArrayList<>(crfcList);
        this.ccList = new ArrayList<>();
        this.dcList = new ArrayList<>();
        this.currentVersion = new Version(num);
        this.nonPublishedVersionName = nonPublishedVersionName;
        idRiskMatriz = numMatrix;
        numMatrix++;
    }

    /**
     * Constructor for database purposes.
     */
    protected RiskMatrix() {
    }

    /**
     * Add a new characterized cell to the risk matrix
     *
     * @param chcell Characterized Cell
     */
    public void addNewCharacterizedCell(CharacterizedCell chcell) {
        ccList.add(chcell);
    }

    public static void startIDat(int start) {
        numMatrix = start;
    }

    /**
     * Add a new detailed cell to the risk matrix
     *
     * @param detailedCell Detailed Cell
     */
    public void addNewDetailedCell(DetailedCell detailedCell) {
        dcList.add(detailedCell);
    }

    /**
     * Return all the CoverageRiskFactorCells in the matrix
     *
     * @return List of CoverageRiskFactorCells
     */
    public List<CoverageRiskFactorCell> getListCoverageRiskFactorCell() {
        return new ArrayList<>(crfcList);
    }

    /**
     * Return all the Characterized Cells in the matrix
     *
     * @return List of Characterized Cells
     */
    public List<CharacterizedCell> getListCharacterizedCell() {
        return new ArrayList<>(ccList);
    }

    /**
     * Return all the Detailed Cells in the matrix
     *
     * @return List of Detailed Cells
     */
    public List<DetailedCell> getListDetailedCell() {
        return new ArrayList<>(dcList);
    }

    /**
     * Method that saves this risk matrix's non publicated version's name.
     *
     * @param version version of this risk matrix
     */
    public void saveNonPublishedVersionName(Version version) {
        this.nonPublishedVersionName = version.getVersionName();
    }

    /**
     * Returns this risk matrix's non publicated version's name.
     *
     * @return non publicated version's name
     */
    public String obtainNonPublishedVersionName() {
        return this.nonPublishedVersionName;
    }

    /**
     * Returns this risk matrix's current version.
     *
     * @return risk matrix's current version.
     */
    public Version obtainVersion() {
        return this.currentVersion;
    }

    /**
     * Equals method.
     *
     * @param o object to compare
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RiskMatrix rm = (RiskMatrix) o;
        return crfcList.equals(rm.crfcList) && ccList.equals(rm.ccList) && dcList.equals(rm.dcList) && currentVersion.equals(rm.currentVersion);
    }

    /**
     * Equals method for risk matrixes with different versions and different
     * publication states.
     *
     * @param o object to compare
     * @return true if equal, false if not
     */
    public boolean equalsWithoutVersion(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RiskMatrix rm = (RiskMatrix) o;
        return crfcList.equals(rm.crfcList) && ccList.equals(rm.ccList) && dcList.equals(rm.dcList);
    }

    /**
     * ToString method.
     *
     * @return string with the matrix information
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Matriz de risco:\n");
        for (CoverageRiskFactorCell cell : crfcList) {
            sb.append(cell.toString() + "\n");
        }
        for (CharacterizedCell cell : ccList) {
            sb.append(cell.toString() + "\n");
        }
        for (DetailedCell cell : dcList) {
            sb.append(cell.toString() + "\n");
        }
        sb.append("and " + currentVersion.toString());
        if (nonPublishedVersionName.equals("Not Published")) {
            sb.append("Publication state: Not Published\n");
        } else {
            sb.append("Publication state: Published\n");
        }
        return sb.toString().trim();
    }

    public int getIdRiskMatriz() {
        return idRiskMatriz;
    }

    /**
     * Returns a string with essential information to show to the user
     *
     * @return string with risk matrix information
     */
    public String riskMatrixPresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(currentVersion);
        if (nonPublishedVersionName.equals("Not Published")) {
            sb.append("Publication state: Not Published\n");
        } else {
            sb.append("Publication state: Published\n");
        }
        String information = sb.toString();
        return information;
    }

    /**
     * Method to obtain hash code of the class
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return crfcList.hashCode() * ccList.hashCode() * dcList.hashCode();
    }

    /**
     * Calculates the risk index without details.
     *
     * @param cov
     * @param ad
     * @return risk index
     */
    public int calculationWithoutDetails(Coverage cov, Address ad) {
        List<Surroundings> adList = getSurroundingsFromgeoref(ad);
        List<DetailedCell> withCov = getCellsWithCoverage(cov);
        int max = calculateMaximScore(withCov, new ArrayList<Explanation>());
        int obt = calculateObtainedScore(withCov, adList, new ArrayList<Explanation>());
        return max + obt;
    }

    /**
     * Calculates the risk index with details.
     *
     * @param cov
     * @param ad
     * @return map with risk index and explanations
     */
    public HashMap<Integer, List<Explanation>> calculationWithDetails(Coverage cov, Address ad) {
        List<Explanation> exList = new ArrayList<Explanation>();
        List<Surroundings> adList = getSurroundingsFromgeoref(ad);
        List<DetailedCell> withCov = getCellsWithCoverage(cov);
        int index = calculateMaximScore(withCov, exList) + calculateObtainedScore(withCov, adList, exList);
        exList.add(new Explanation("- O índice de risco é " + index + "."));
        HashMap<Integer, List<Explanation>> map = new HashMap<Integer, List<Explanation>>();
        map.put(index, exList);
        return map;
    }

    /**
     * Method that calculates the maximum score
     *
     * @param withCovList
     * @param explanations
     * @return maximScore
     */
    private int calculateMaximScore(List<DetailedCell> withCovList, List<Explanation> explanations) {
        StringBuilder sb = new StringBuilder();
        int maxScore = 0;
        sb.append("~~~~~~ Cálculo do valor máximo ~~~~~~\n");
        sb.append("Fatores de risco considerados:\n");
        for (DetailedCell dc : withCovList) {
            RiskFactor rf = dc.getCharacterizedCell().obtainCoverageRisk().obtainRiskFactor();
            int weight = dc.getCharacterizedCell().getWeight().obtainWeight();
            int maxScale = dc.getScale().obtainHighValue();
            maxScore += weight * maxScale;
            sb.append(rf.toString() + "\n");
            sb.append(" - Este fator de risco tem um peso de " + weight + " e uma classificação máxima de " + maxScale + ".\n");
        }
        sb.append("Score máximo final: " + maxScore + ".\n\n");
        explanations.add(new Explanation(sb.toString()));
        return maxScore;
    }

    /**
     * Method that calculates the obtained score
     *
     * @param withCovList
     * @param sList
     * @param explanations
     * @return obtainedScore
     */
    private int calculateObtainedScore(List<DetailedCell> withCovList, List<Surroundings> sList, List<Explanation> explanations) {
        StringBuilder sb = new StringBuilder();
        int obtainedScore = 0;
        sb.append("~~~~~~ Cálculo do valor obtido ~~~~~~\n");
        sb.append("Fatores de risco considerados:\n");
        for (DetailedCell dc : withCovList) {
            RiskFactor rf = dc.getCharacterizedCell().obtainCoverageRisk().obtainRiskFactor();
            sb.append(rf.toString() + "\n");
            int weight = dc.getCharacterizedCell().getWeight().obtainWeight();
            String classification = getRiskFactorClassification(sList, rf);
            if (classification != null) {
                int value = 0;
                if (classification.equalsIgnoreCase("low")) {
                    value = dc.getScale().obtainLowValue();
                    obtainedScore += weight * value;
                }
                if (classification.equalsIgnoreCase("medium")) {
                    value = dc.getScale().obtainMediumValue();
                    obtainedScore += weight * value;
                }
                if (classification.equalsIgnoreCase("high")) {
                    value = dc.getScale().obtainHighValue();
                    obtainedScore += weight * value;
                }
                sb.append(" - Este fator de risco tem um peso de " + weight + " e uma classificação " + classification + ". Assim, o valor usado da escala foi " + value + ".\n");
            } else if (dc.getCharacterizedCell().getNecessity().obtainNecessity() == 1) {
                int value = dc.getScale().obtainHighValue();
                obtainedScore += weight * value;
                sb.append("- Este fator de risco não tem foi reconhecido pelo serviço de georreferência, mas visto que é obrigatório "
                        + "é considerado a situacão mais gravosa. Assim foi usado o peso " + weight + " e o valor usado da escala foi " + value + ".\n");
            }
        }
        sb.append("Score obtido final: " + obtainedScore + ".\n\n");
        explanations.add(new Explanation(sb.toString()));
        return obtainedScore;
    }

    /**
     * Method that returns a string with the classification of the risk factor
     *
     * @param list
     * @param rf
     * @return classification if exists, empty if doesn't
     */
    private String getRiskFactorClassification(List<Surroundings> list, RiskFactor rf) {
        for (Surroundings s : list) {
            if (s.getType().equals(rf.getSurroundingType()) && s.getMetricScaleMap().containsKey(rf.getMetric())) {
                return s.getMetricScaleMap().get(rf.getMetric());
            }
        }
        return null;
    }

    /**
     * Creates a list of detailed cells with a certain coverage
     *
     * @param cov
     * @return list with detailed cells
     */
    private List<DetailedCell> getCellsWithCoverage(Coverage cov) {
        List<DetailedCell> newList = new ArrayList<DetailedCell>();
        for (DetailedCell dc : dcList) {
            if (dc.getCharacterizedCell().obtainCoverageRisk().obtainCoverage().equals(cov)) {
                newList.add(dc);
            }
        }
        return newList;
    }

    /**
     * Method that transforms string in objects
     *
     * @param ad
     * @return list of surroundings
     */
    private List<Surroundings> getSurroundingsFromgeoref(Address ad) {
        String[] latLng = new AdressSearchController().addressSearch(ad.getCity().city(), ad.postalCode().postalCode(), ad.street().toString()).split(",");
        String surroundingsString = new ObtainSurroundingsController().getSurroundings(Float.parseFloat(latLng[0]), Float.parseFloat(latLng[1]));
        List<Surroundings> list = new LinkedList<Surroundings>();
        String[] firstDegree = surroundingsString.split("\\|");
        for (String s : firstDegree) {
            String[] secondDegree = s.split(";");
            HashMap<Metric, String> metricStringHashMap = new HashMap<Metric, String>();
            for (int j = 2; j < secondDegree.length; j++) {
                String[] threeDegree = secondDegree[j].split(",");
                metricStringHashMap.put(new Metric(threeDegree[0]), threeDegree[1]);
            }
            Surroundings surroundings = new Surroundings(secondDegree[0], new SurroundingType(secondDegree[1]), metricStringHashMap);
            list.add(surroundings);
        }
        return list;
    }

}
