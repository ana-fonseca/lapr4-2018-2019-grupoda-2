package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class InsuranceCoverageConnection implements Serializable {

    /**
     * Insurance attribute
     */
    private Insurance insurance;

    /**
     * Coverage attribute
     */
    private Coverage coverage;

    /**
     * Complete constructor
     *
     * @param insurance
     * @param coverage
     */
    public InsuranceCoverageConnection(Insurance insurance, Coverage coverage) {
        this.coverage = coverage;
        this.insurance = insurance;
    }

    /**
     * Constructor for database purposes
     */
    public InsuranceCoverageConnection() {
    }

    /**
     * Get coverage
     *
     * @return coverage
     */
    public Coverage obtainCoverage() {
        return coverage;
    }

    /**
     * Get insurance
     *
     * @return insurance
     */
    public Insurance obtainInsurance() {
        return insurance;
    }

    /**
     * Equals method of the class
     *
     * @param o Object to be compared
     * @return result of the comparision
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsuranceCoverageConnection that = (InsuranceCoverageConnection) o;
        return Objects.equals(insurance, that.insurance) &&
                Objects.equals(coverage, that.coverage);
    }

    /**
     * Hashcode of the object
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(insurance, coverage);
    }

    /**
     * toString method of the class
     *
     * @return information about the object
     */
    @Override
    public String toString() {
        return insurance.toString() + ". Coverage to Analyse: " + coverage;
    }
}
