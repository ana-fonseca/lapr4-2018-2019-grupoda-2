package isep.eapli.core.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Coverage class implementation
 *
 * @author 1170982
 */
@Entity
public class Coverage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    /**
     * Coverage designation
     */
    private String designation;

    /**
     * Protected constructor, to be used with DB only
     */
    protected Coverage() {
        //Left empty on purpose, not for use unless for DB
    }

    /**
     * Complete constructor
     *
     * @param designation designation of the Coverage
     */
    public Coverage(String designation) {
        if (!designation.isEmpty()) {
            this.designation = designation;
        } else {
            throw new IllegalArgumentException("Designation cannot be empty");
        }
    }

    /**
     * Method that returns the designation of the Coverage object
     *
     * @return designation of the coverage
     */
    public String obtainDesignation() {
        return designation;
    }

    /**
     * Method to obtain hash code of the class
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return designation.hashCode();
    }

    /**
     * Method that checks if one object is equal to another
     *
     * @param obj Object to be compared
     * @return result of the comparision
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coverage other = (Coverage) obj;
        return this.obtainDesignation().equals(other.designation);
    }

    /**
     * toString class method. Returns a String with information about the object
     *
     * @return information about the object
     */
    @Override
    public String toString() {
        return "Coverage related to " + designation;
    }

}
