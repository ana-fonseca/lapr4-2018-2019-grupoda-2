package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.RiskFactor;

/**
 * Class that controls the access to the RiskFactor Database
 */
public class RiskFactorDB extends JpaTransactionalRepository<RiskFactor, String> implements Repository<RiskFactor, String> {


    /**
     * Class constructor
     *
     * @param persistenceName name of the file that controls the persistence
     */
    public RiskFactorDB(String persistenceName) {
        super(persistenceName);
    }

    /**
     * Method that checks if a certain risk factor is already in the database.
     * If the risk factor to verify exists in the DB, it returns true. If not, it returns false.
     *
     * @param riskFactor - risk factor to be tested
     * @return result of the search
     */
    public boolean checkIfAlreadyExists(RiskFactor riskFactor) {
        for (RiskFactor riskFactorInDB : findAll()) {
            if (riskFactorInDB.equals(riskFactor)) {
                return true;
            }
        }
        return false;
    }
}