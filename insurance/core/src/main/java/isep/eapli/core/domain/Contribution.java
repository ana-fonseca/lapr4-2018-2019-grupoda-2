package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Contribution class implementation
 */
@Embeddable
public class Contribution implements Serializable {

    /**
     * Enum containing all the contributions accepted by the program
     */
    private enum possibilities{
        POSITIVO, NEGATIVO, POSITIVE, NEGATIVE
    }

    /**
     * Contribution value
     */
    private int caseContribution;

    /**
     * Protected empty constructor (database related)
     */
    protected Contribution(){
    }

    //NEGATIVO, NEGATIVE --> 0
    //POSITIVO, POSITIVE --> 1

    /**
     * Complete constructor
     *
     * @param nec contribution inputted
     */
    public Contribution(String nec){
        int flag=0;
        List<possibilities> arr= Arrays.asList(possibilities.values());
        for(int i=1; i<arr.size()+1; i++){
            if(arr.get(i-1).name().equalsIgnoreCase(nec))
                flag=i;
        }
        if(flag==0)
            throw new IllegalArgumentException();
        if(flag==1 || flag == 3)
            this.caseContribution=1;
        if(flag==2 || flag == 4)
            this.caseContribution=0;
    }

    /**
     * Method to obtain the contribution of a given risk factor
     *
     * @return Integer referring to the contribution
     */
    public int obtainContribution(){
        return this.caseContribution;
    }

    /**
     * Hash Code method
     *
     * @return Hash Code of the object
     */
    @Override
    public int hashCode() {
        int i=123456;
        return caseContribution+i;
    }

    /**
     * Equals method
     *
     * @param obj object to compare to
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.caseContribution == ((Contribution) obj).obtainContribution();
    }

    /**
     * To String method
     *
     * @return String description of the object
     */
    @Override
    public String toString() {
        if(caseContribution==0)
            return "NEGATIVO (NEGATIVE)";
        return "POSITIVO (POSITIVE)";
    }
}
