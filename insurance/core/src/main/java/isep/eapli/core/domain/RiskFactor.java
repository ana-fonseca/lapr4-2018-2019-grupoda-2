package isep.eapli.core.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Risk Factor class implementation
 */
@Entity
public class RiskFactor implements Serializable {

    @Id
    private String id;

    /**
     * RiskFactor surroundingType
     */
    @ManyToOne
    private SurroundingType surroundingType;

    /**
     * RiskFactor metric
     */
    @Embedded
    private Metric metric;

    /**
     * Protected constructor, to be used with DB only
     */
    protected RiskFactor() {
    }

    /**
     * Complete constructor
     *
     * @param surroundingType - risk factor surroundingType
     * @param metric          - risk factor metric
     */
    public RiskFactor(SurroundingType surroundingType, Metric metric) {
        this.surroundingType = surroundingType;
        this.metric = metric;
        this.id = surroundingType.toString() + metric.toString();
    }

    public SurroundingType getSurroundingType(){
        return surroundingType;
    }

    public Metric getMetric(){
        return metric;
    }
    /**
     * Method to obtain hash code of the class
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.surroundingType);
        hash = 41 * hash + Objects.hashCode(this.metric);
        return hash;
    }

    /**
     * Method that checks if one object is equal to another
     *
     * @param obj Object to be compared
     * @return result of the comparation
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RiskFactor other = (RiskFactor) obj;
        if (!Objects.equals(this.surroundingType, other.surroundingType)) {
            return false;
        }
        if (!Objects.equals(this.metric, other.metric)) {
            return false;
        }
        return true;
    }

    /**
     * toString class method. Returns a String with information about the object
     *
     * @return information about the object
     */
    @Override
    public String toString() {
        return "RiskFactor-> " + surroundingType + "; " + metric;
    }

}
