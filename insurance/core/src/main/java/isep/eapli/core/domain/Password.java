/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.Embeddable;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author DoriaTheGod
 */
@Embeddable
public class Password {

    private static final String REGEXP = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}$";
    private static final String HASH_ALGORITHM = "MD5";

    private String password;
    
    protected Password(){
    }

    public Password(String password) throws Exception {
        if (!password.matches(REGEXP)) {
            throw new Exception("Invalid password");
        }
        this.password = hashPassword(password);
    }

    private String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(HASH_ALGORITHM);
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }
    
    public String password () {
        return password;
    }

    @Override
    public int hashCode() {
        return password.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Password other = (Password) obj;
        return this.password.equals(other.password);
    }

}
