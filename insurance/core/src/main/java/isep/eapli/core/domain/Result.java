package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Embeddable
public class Result implements Serializable {

    /**
     * Attribute insurance coverage connection
     */
    private InsuranceCoverageConnection icc;

    /**
     * Attribute explanation
     */
    private List<Explanation> explanation;

    /**
     * Attribute index
     */
    private int index;

    /**
     * Complete constructor
     *
     * @param icc
     * @param explanation
     * @param index
     */
    public Result(InsuranceCoverageConnection icc, List<Explanation> explanation, int index) {
        this.explanation = explanation;
        this.icc = icc;
        this.index = index;
    }

    /**
     * Constructor for database purposes
     */
    public Result() {
        //DB purpose
    }

    /**
     * Method to obtain InsuranceCoverageConnection object
     *
     * @return ICC
     */
    public InsuranceCoverageConnection obtainIcc() {
        return icc;
    }

    /**
     * Method to obtain index
     * @return index
     */
    public int obtainIndex() {
        return index;
    }
    
      /**
     * Method to obtain List<Explanation>
     * @return List<Explanation>
     */
    public List<Explanation> obtainExplanationList() {
        return explanation;
    }

    /**
     * Equals method of the class
     *
     * @param o object to compare
     * @return result of the comparision
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return index == result.index &&
                Objects.equals(icc, result.icc) &&
                Objects.equals(explanation, result.explanation);
    }

    /**
     * Hash code of the object
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(icc, explanation, index);
    }


    @Override
    public String toString() {
        return "========= RESULT =========\n" + icc.toString() + "\nScore obtained: " + index;
    }

    public void assignIndex(Integer index) {
        this.index = index;
    }
}
