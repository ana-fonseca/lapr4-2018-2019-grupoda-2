package isep.eapli.core.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

/**
 * Detailed Cell class
 */
@Embeddable
public class DetailedCell implements Serializable {

    /**
     * Characterized Cell attribute
     */
    @Embedded
    private CharacterizedCell characterizedCell;

    /**
     * Scale attribute;
     */
    @Embedded
    private Scale scale;

    /**
     * Complete Constructor of the class
     *
     * @param characterizedCell characterized cell
     * @param scale             scale object
     */
    public DetailedCell(CharacterizedCell characterizedCell, Scale scale) {
        this.characterizedCell = characterizedCell;
        this.scale = scale;
    }

    /**
     * Constructor for database purposes.
     */
    public DetailedCell() {
    }

    /**
     * Get scale
     *
     * @return scale
     */
    public Scale getScale(){ return scale; }

    /**
     * Get Characterized Cell
     *
     * @return characterizedCell
     */
    public CharacterizedCell getCharacterizedCell(){ return characterizedCell; }

    /**
     * Hashcode method
     *
     * @return hashcode.
     */
    @Override
    public int hashCode() {
        int result = 6;
        result = 31 * result + characterizedCell.hashCode();
        result = 31 * result + scale.hashCode();
        return result;
    }

    /**
     * Method that compares DetailedCell objects
     *
     * @param obj DetailedCell object
     * @return true if the objects are equal, false if not.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetailedCell other = (DetailedCell) obj;
        if (!this.characterizedCell.equals(other.characterizedCell)) {
            return false;
        }
        if (!this.scale.equals(other.scale)) {
            return false;
        }
        return true;
    }
    /**
     * toString class method. Returns a String with information about the object of this class
     * @return information about the object of this class
     */
    @Override
    public String toString() {
        return "DetailedCell: " +
                "Characterized Cell = " + characterizedCell +
                "Scale:\n" + scale;
    }
}
