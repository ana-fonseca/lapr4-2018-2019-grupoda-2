package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.NearBySurrounding;
import java.util.ArrayList;
import java.util.List;

public class SurroundingDB extends JpaTransactionalRepository<NearBySurrounding, Long> implements Repository<NearBySurrounding, Long> {

    public SurroundingDB(String persistenceName) {
        super(persistenceName);
    }

    public List<NearBySurrounding> getAll() {
        List<NearBySurrounding> lnbs = new ArrayList<>();
        if (size() == 0) {
            return lnbs;
        }
        for (NearBySurrounding nbs : findAll()) {
            lnbs.add(nbs);
        }
        return lnbs;
    }

}
