package isep.eapli.core.domain;

import java.io.Serializable;

public class Street implements Serializable {
    
    private String street;
    private String doorNumber;
    
    public Street(String street, String doorNumber) {
        this.street = street;
        this.doorNumber = doorNumber;
    }
    
    public Street(String street) {
        this.street = street;
    }

    public String streetName () {
        return street;
    }

    public String doorNumber() {
        return doorNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Street)) {
            return false;
        }
        Street other = (Street) o;
        if (!street.equals(other.street)) {
            return false;
        }
        if ((doorNumber == null && other.doorNumber != null) || (doorNumber != null && doorNumber == null)) {
            return false;
        }
        if (doorNumber == null && other.doorNumber == null) {
            return true;
        }
        return doorNumber.equals(other.doorNumber);
    }

    @Override
    public String toString() {
        return doorNumber == null ? street : street + ", " + doorNumber;
    }
    
}
