package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Weight Class Implementation
 */
@Embeddable
public class Weight implements Serializable {

    /**
     * Weight value
     */
    private int weightValue;

    /**
     * Empty constructor (database related)
     */
    protected Weight(){

    }

    /**
     * Complete constructor
     *
     * @param value Weight inputted
     */
    public Weight(int value){
        this.weightValue = value;
    }

    /**
     * Method that returns the weight value
     *
     * @return weight value
     */
    public int obtainWeight(){
        return this.weightValue;
    }

    /**
     * Hash Code of the object
     *
     * @return Hash Code of the object
     */
    @Override
    public int hashCode() {
        int i=1234567;
        return weightValue+i;
    }

    /**
     * Equals method
     *
     * @param obj object to compare to
     * @return true if equals, false if not
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.weightValue == ((Weight) obj).obtainWeight();
    }

    /**
     * To String Method
     *
     * @return String describing the object
     */
    @Override
    public String toString() {
        return "PESO (WEIGHT) :"+this.weightValue;
    }
}
