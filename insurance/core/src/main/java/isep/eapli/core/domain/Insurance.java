package isep.eapli.core.domain;

import java.io.Serializable;
import java.util.Objects;

public class Insurance implements Serializable {

    /**
     * Address attribute
     */
    private Address address;

    /**
     * Complete constructor
     *
     * @param address
     */
    public Insurance(Address address) {
        this.address = address;
    }

    /**
     * Get address
     *
     * @return address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Equals method
     *
     * @param o object to compare
     * @return result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Insurance insurance = (Insurance) o;
        return address.equals(insurance.address);
    }

    /**
     * Hashcode of the class
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(address);
    }

    @Override
    public String toString() {
        return "Insurance\n" + address.toString();
    }
}
