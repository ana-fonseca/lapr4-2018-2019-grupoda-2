package isep.eapli.core.domain;

import java.io.Serializable;

public class Country implements Serializable {
    
    private String country;
    
    public Country (String country) {
        this.country = country;
    }

    public String country() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Country)) {
            return false;
        }
        Country other = (Country) o;
        return country.equals(other.country);
    }

    @Override
    public String toString() {
        return country;
    }
    
}
