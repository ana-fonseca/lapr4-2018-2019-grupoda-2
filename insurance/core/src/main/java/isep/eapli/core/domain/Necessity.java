package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Necessity class implementation
 */
@Embeddable
public class Necessity implements Serializable {

    /**
     * Enum containing the necessities accepted by the program
     */
    private enum possibilities{
        FACULTATIVO, OBRIGATORIO, OPTIONAL, REQUIRED
    }

    /**
     * Necessity value
     */
    private int caseNecessity;

    /**
     * Empty constructor (database related)
     */
    protected Necessity(){
    }

    //FACULTATIVO, OPTIONAL --> 0
    //OBRIGATORIO, REQUIRED --> 1

    /**
     * Complete constructor
     *
     * @param nec necessity inputted
     */
    public Necessity(String nec){
        int flag=0;
        List<possibilities> arr=Arrays.asList(possibilities.values());
        for(int i=1; i<arr.size()+1; i++){
            if(arr.get(i-1).name().equalsIgnoreCase(nec))
                flag=i;
        }
        if(flag==0)
            throw new IllegalArgumentException();
        if(flag==1 || flag == 3)
            this.caseNecessity=0;
        if(flag==2 || flag == 4)
            this.caseNecessity=1;
    }

    /**
     * Method that returns the necessity value
     *
     * @return necessity value
     */
    public int obtainNecessity(){
        return this.caseNecessity;
    }

    /**
     * Hash code of the object
     *
     * @return Hash Code of the object
     */
    @Override
    public int hashCode() {
        int i=12345;
        return caseNecessity+i;
    }

    /**
     * Equals method
     *
     * @param obj object to compare to
     * @return true if equals, false if not
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.caseNecessity == ((Necessity) obj).obtainNecessity();
    }

    /**
     * To String method
     *
     * @return String describing the object
     */
    @Override
    public String toString() {
        if(caseNecessity==0)
            return "FACULTATIVO (OPTIONAL)";
        return "OBRIGATORIO (REQUIRED)";
    }
}
