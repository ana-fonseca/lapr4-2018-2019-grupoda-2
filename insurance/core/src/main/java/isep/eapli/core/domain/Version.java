package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Embeddable
public class Version implements Serializable {

    /**
     * Creation date.
     */
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    /**
     * Version name.
     */
    private String versionName;

    /**
     * Complete constructor.
     *
     */
    public Version(int num) {
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH)+1;
        int year = cal.get(Calendar.YEAR);
        this.creationDate = cal.getTime();
        this.versionName = year+"/"+month+"/"+day+"_"+num;
    }

    public String getVersionName(){
        return versionName;
    }

    /**
     * Constructor for database purposes.
     */
    public Version() {
    }

    /**
     * Equals method for the version class.
     *
     * @param o
     * @return true if equal, false if different
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Version version = (Version) o;
        return creationDate.equals(version.creationDate) &&
                Objects.equals(versionName, version.versionName);
    }

    /**
     * HashCode method for the version class.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(creationDate, versionName);
    }

    /**
     * ToString for the version class.
     *
     * @return string that describes the version
     */
    @Override
    public String toString() {
        return "Version: creation date " + creationDate +
                ", version name " + versionName + '\n';
    }
}
