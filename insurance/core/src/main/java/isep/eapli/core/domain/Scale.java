package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Scale class
 */
@Embeddable
public class Scale implements Serializable {
    /**
     * Lowest value of the scale.
     */
    private int lowValue;
    
    /**
     * Medium value of the scale.
     */
    private int mediumValue;
    
    /**
     * Highest value of the scale.
     */
    private int highValue;

    /**
     * No-arg constructor, for exclusive use of the DB
     */
    protected Scale(){
        //For DB use
    }
    /**
     * Constructor for the class
     * @param lowValue lowest value of the scale
     * @param mediumValue medium value of the scale
     * @param highValue highest value of the scale
     */
    public Scale(int lowValue, int mediumValue, int highValue){
        if(lowValue>0 && mediumValue>0 && highValue>0){
            this.lowValue = lowValue;
            this.mediumValue = mediumValue;
            this.highValue = highValue;
        }else{
        throw new IllegalArgumentException();
        }
    }

    /**
     * Get the lowest value in the scale
     * @return lowest value
     */
    public int obtainLowValue() {
        return lowValue;
    }

    /**
     * Get the medium value in the scale
     * @return medium value
     */
    public int obtainMediumValue() {
        return mediumValue;
    }

    /**
     * Get the highest value in the scale
     * @return highest value
     */
    public int obtainHighValue() {
        return highValue;
    }

    /**
     * Hashcode method
     * @return hashcode.
     */
    @Override
    public int hashCode() {
        int result = 6;
        result = 31 * result + lowValue;
        result = 31 * result + mediumValue;
        result = 31 * result + highValue;
        return result;
    }
    /**
     * Method that compares Scale objects
     * @param obj Scale instance
     * @return true if the objects are equal, false if not.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Scale other = (Scale) obj;
        if (this.lowValue != other.lowValue) {
            return false;
        }
        if (this.mediumValue != other.mediumValue) {
            return false;
        }
        if (this.highValue != other.highValue) {
            return false;
        }
        return true;
    }

    /**
     * toString method, that returns a string with information about this object
     * @return information about the object of this class
     */
    @Override
    public String toString(){
        return "\nLow scale: "+lowValue+"\nMedium scale: "+mediumValue+"\nHigh scale: "+highValue;
    }
    
}
