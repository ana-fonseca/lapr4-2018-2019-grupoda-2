/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.RiskAnalyst;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author becas
 */
public class RiskAnalystDB extends JpaTransactionalRepository<RiskAnalyst, String> implements Repository<RiskAnalyst, String>{
    
    public RiskAnalystDB(String persistenceUnitName) {
        super(persistenceUnitName);
    }
    
     public boolean checkIfAlreadyExists(RiskAnalyst riskAnalyst) {
        for (RiskAnalyst riskAnalystInDB : findAll()) {
            if (riskAnalystInDB.email().equals(riskAnalyst.email())) {
                return true;
            }
        }
        return false;
    }
     
    public boolean checkIfAlreadyExistsFull(RiskAnalyst riskAnalyst) {
        for (RiskAnalyst riskAnalystInDB : findAll()) {
            if (riskAnalystInDB.equals(riskAnalyst)) {
                return true;
            }
        }
        return false;
    }
    
    public RiskAnalyst findByEmail (String email) {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM RiskAnalyst WHERE Email = ?", RiskAnalyst.class);
        query.setParameter(1, email);
        return (RiskAnalyst) query.getSingleResult();
    }
    
}
