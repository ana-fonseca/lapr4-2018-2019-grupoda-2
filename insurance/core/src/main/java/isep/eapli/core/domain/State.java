package isep.eapli.core.domain;

import java.io.Serializable;

public enum State implements Serializable {
    AUTOMATIC, // Does not need an RA while is being processed automatically
    PENDING, //May or may not have a RA but the request was not opened
    PROCESSING, //It is being calculated 
    VALIDATED; //Finished
}
