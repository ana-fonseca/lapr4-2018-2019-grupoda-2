package isep.eapli.core.domain;

import java.io.Serializable;
import java.util.Objects;

public class Address implements Serializable {

    private Street street;
    private City city;
    private PostalCode postalCode;
    private Country country;
    private District district;

    public Address(Street street, City city, PostalCode postalCode, Country country, District district) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.district = district;
    }

    public Address(Street street, City city, PostalCode postalCode, Country country) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
    }

    public Address(Street street, City city, PostalCode postalCode, District district) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.district = district;
    }

    public Address(Street street, City city, PostalCode postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    public Street street() {
        return street;
    }

    public City getCity() {
        return city;
    }

    public PostalCode postalCode() {
        return postalCode;
    }

    public Country country() {
        return country;
    }

    public District district() {
        return district;
    }
    
    public boolean hasDistrict() {
        return district != null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        Address other = (Address) o;
        if ((country == null && other.country != null) || (district == null && other.district != null) || (country != null && other.country == null) || (district != null && other.district == null)) {
            return false;
        }
        if (country == null) {
            return district == null ? street.equals(other.street) && city.equals(other.city) && postalCode.equals(other.postalCode) : street.equals(other.street) && city.equals(other.city) && postalCode.equals(other.postalCode) && district.equals(other.district);
        }
        if (district == null) {
            return street.equals(other.street) && city.equals(other.city) && postalCode.equals(other.postalCode) && country.equals(other.country);
        }
        return street.equals(other.street) && city.equals(other.city) && postalCode.equals(other.postalCode) && district.equals(other.district) && country.equals(other.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, city, postalCode);
    }

    /**
     * Method that gives the information about the object
     *
     * @return Object information
     */
    @Override
    public String toString() {
        String toString = "Postal Address:\nStreet: " + street + "\nCity: " + city + "\nPostal Code: " + postalCode;
        if (district == null) {
            toString += country != null ? "\nCountry: " + country : "";
        } else {
            toString += country != null ? "\nDistrict: " + district + "\nCountry: " + country : "\nDistrict: " + district;
        }
        return toString;
    }
}
