package isep.eapli.core.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Coverage Risk Factor cell implementation.
 */
@Embeddable
public class CoverageRiskFactorCell implements Serializable {

    /**
     * Coverage attribute.
     */
    private Coverage coverage;

    /**
     * RiskFactor attribute.
     */
    private RiskFactor riskFactor;

    /**
     * Complete constructor.
     * @param coverage
     * @param riskFactor
     */
    public CoverageRiskFactorCell(Coverage coverage, RiskFactor riskFactor){
        this.coverage = coverage;
        this.riskFactor = riskFactor;
    }

    /**
     * Constructor for database purposes.
     */
    protected CoverageRiskFactorCell() {
    }

    /**
     * Obtain risk factor
     * @return risk factor
     */
    public RiskFactor obtainRiskFactor(){
        return this.riskFactor;
    }

    /**
     * Get coverage
     *
     * @return coverage
     */
    public Coverage obtainCoverage(){ return coverage;}

    /**
     * ToString of the class.
     */
    @Override
    public String toString(){
        return "Coverage Risk Factor Cell with Coverage "+coverage.obtainDesignation()+" and "+riskFactor.toString();
    }

    /**
     * Method to obtain hash code of the class
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return coverage.hashCode() + riskFactor.hashCode();
    }

    /**
     * Method that checks if one object is equal to another
     *
     * @param obj Object to be compared
     * @return result of the comparasion
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CoverageRiskFactorCell c = (CoverageRiskFactorCell) obj;
        return c.coverage.equals(coverage) && c.riskFactor.equals(riskFactor);
    }


}
