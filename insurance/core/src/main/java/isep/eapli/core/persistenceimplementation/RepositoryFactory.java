package isep.eapli.core.persistenceimplementation;

/**
 * Class that serves as connection to objects that connects to the various
 * databases
 */
public class RepositoryFactory {

    /**
     * Class constructor
     */
    public RepositoryFactory() {
        //Empty Constructor
    }

    /**
     * Method that returns a new instance of a DB related to the Coverage
     * object.
     *
     * @return new connection to the Coverage DB
     */
    public CoverageDB getCoverageDB() {
        return new CoverageDB("DomainDB");
    }

    /**
     * Method that returns a new instance of a DB related to the RiskFactor
     * object.
     *
     * @return new connection to the RiskFactor DB
     */
    public RiskFactorDB getRiskFactorDB() {
        return new RiskFactorDB("DomainDB");
    }

    /**
     * Method that returns a new instance of a DB related to the SurroundingType
     * object.
     *
     * @return new connection to the SurroundingType DB
     */
    public SurroundingTypeDB getSurroundingTypeDB() {
        return new SurroundingTypeDB("DomainDB");
    }

    /**
     * Method that returns a new instance of a BD related to the RiskMatrix
     * object.
     */
    public RiskMatrixDB getRiskMatrixDB() {
        return new RiskMatrixDB("DomainDB");
    }

    /**
     * Method that returns a new instance of a BD related to the RequestDB
     * object
     *
     * @return new connection to the Request DB
     */
    public RequestDB getRequestDB() {
        return new RequestDB("DomainDB");
    }

    /**
     * Method that returns a new instance of a BD related to the SurroundingDB
     * object
     *
     * @return new connection to the SurroundingDB DB
     */
    public SurroundingDB getSurroundingDB() {
        return new SurroundingDB("DomainDB");
    }

    public RiskAnalystDB getRiskAnalystDB() {
        return new RiskAnalystDB("DomainDB");
    }
}
