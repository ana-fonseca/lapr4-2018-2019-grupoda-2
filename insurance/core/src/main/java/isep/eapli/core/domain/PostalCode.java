package isep.eapli.core.domain;

import java.io.Serializable;

public class PostalCode implements Serializable {

    private long firstDigits;
    private long lastDigits;
    
    public PostalCode (String postalCode) throws Exception {
        try {
            firstDigits = Long.parseLong(postalCode.replaceAll(" ", "").split("-")[0]);
            lastDigits = Long.parseLong(postalCode.replaceAll(" ", "").split("-")[1]);
        } catch (Exception e) {
            throw new Exception("Invalid postal code!");
        }
    }

    public long firstDigits() { return firstDigits; }

    public long lastDigits() { return lastDigits; }

    public String postalCode() { return firstDigits + "-" + lastDigits; }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof PostalCode)) {
            return false;
        }
        PostalCode other = (PostalCode) o;
        return firstDigits == other.firstDigits && lastDigits == other.lastDigits;
    }

    @Override
    public String toString() {
        return firstDigits + "-" + lastDigits;
    }
    
}
