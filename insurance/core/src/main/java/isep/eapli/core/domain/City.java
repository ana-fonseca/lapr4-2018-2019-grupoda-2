package isep.eapli.core.domain;

import java.io.Serializable;

public class City implements Serializable {
    
    private String city;
    
    public City (String city) {
        this.city = city;
    }

    public String city() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof City)) {
            return false;
        }
        City other = (City) o;
        return city.equals(other.city);
    }

    @Override
    public String toString() {
        return city;
    }
    
}
