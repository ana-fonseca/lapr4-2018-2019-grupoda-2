/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author DoriaTheGod
 */
@Entity
public class RiskAnalyst implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long raID;
    
    @Embedded
    private Email email;
    @Embedded
    private Password password;

    /**
     * Protected constructor, to be used with DB only
     */
    protected RiskAnalyst() {
        //Left empty on purpose, not for use unless for DB
    }

    public RiskAnalyst(String email, String password) throws Exception {
        this.email = new Email(email);
        this.password = new Password(password);
    }
    
    public long raID (){
        return raID;
    }
    
    public Email email () {
        return email;
    }
    
    public Password password () {
        return password;
    }
    @Override
    public boolean equals(Object obj){
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RiskAnalyst)) {
            return false;
        }
        RiskAnalyst other = (RiskAnalyst) obj;
        return password.equals(other.password) && email.equals(other.email);
    }
}
