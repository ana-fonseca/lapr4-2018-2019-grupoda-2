package isep.eapli.core.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * Metric class implementation
 */
@Embeddable
public class Metric implements Serializable {

    /**
     * Metric designation
     */
    private String designation;

    /**
     * Complete constructor
     *
     * @param designation - metric designation
     */
    public Metric(String designation) {
        this.designation = designation;
    }

    /**
     * Protected constructor, to be used with DB only
     */
    protected Metric() {
    }

    /**
     * Get the metric designation
     */
    public String obtainDesignation(){
        return designation;
    }

    /**
     * Method to obtain hash code of the class
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.designation);
        return hash;
    }

    /**
     * Method that checks if one object is equal to another
     *
     * @param obj Object to be compared
     * @return result of the comparasion
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Metric other = (Metric) obj;
        if (!Objects.equals(this.designation, other.designation)) {
            return false;
        }
        return true;
    }

    /**
     * toString class method. Returns a String with information about the object
     *
     * @return information about the object
     */
    @Override
    public String toString() {
        return "Metric: " + designation;
    }

}
