package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.core.domain.State;
import isep.eapli.core.domain.Version;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Class that controls the access to the Request Database
 *
 * @author 1170982
 */
public class RequestDB extends JpaTransactionalRepository<Request, Long> implements Repository<Request, Long> {
    /**
     * Class constructor
     *
     * @param persistenceUnitName name of the file that controls the persistence
     */
    public RequestDB(String persistenceUnitName) {
        super(persistenceUnitName);
    }

    /**
     * Method that checks if a certain case is already in the database.
     *
     * @param requestA Coverage to be tested
     * @return result of the search
     */
    public boolean checkIfAlreadyExists(Request requestA) {
        for (Request requestInDB : findAll()) {
            if (requestInDB.equals(requestA)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method that checks if a certain case with a given version is already in
     * the database
     *
     * @param version - version to verify
     * @return
     */
    public Request obtainCaseByVersion(Version version) {
        for (Request requestInDB : findAll()) {
            if (requestInDB.getMatrixVersion().equals(version)) {
                return requestInDB;
            }
        }
        return null;
    }

    /**
     * Searches for the Risk Assessment Requests that have the State PENDING and have no Risk Analyst assigned to them.
     * 
     * @return the list of the pending unassigned risk assessment requests
     * 
     * Tiago Ribeiro (1170426)
     */    
    public List<Request> findPendingUnassignedRequests() {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'PENDING' AND RISKANALYST_RAID IS NULL ORDER BY REQUESTEDDATE ASC", Request.class);
        List<Request> requestList = query.getResultList();
        if (requestList.isEmpty()) {
            return new ArrayList<>();
        }
        return requestList;
    }

    public List<Request> findConcludedRequests() {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'VALIDATED' ORDER BY REQUESTEDDATE ASC", Request.class);
        List<Request> requestList = query.getResultList();
        if (requestList.isEmpty()) {
            return new ArrayList<>();
        }
        return requestList;
    }

    /**
     * Returns Risk Analyst's pending requests, from oldest to the most recent
     *
     * @param riskAnalyst logged in risk analyst
     * @return List of Requests
     */
    public List<Request> findPendingAssignedRequests(RiskAnalyst riskAnalyst) {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'PENDING' AND RISKANALYST_RAID = ? ORDER BY REQUESTEDDATE ASC", Request.class);
        query.setParameter(1, riskAnalyst.raID());
        List<Request> requestList = query.getResultList();
        if (requestList.isEmpty()) {
            return new ArrayList<>();
        }
        return requestList;
    }

    /**
     * Returns Risk Analyst's validated requests, from oldest to the most recent
     *
     * @param riskAnalyst logged in risk analyst
     * @return List of Request
     */
    public List<Request> findValidatedAssignedRequests(RiskAnalyst riskAnalyst) {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'VALIDATED' AND RISKANALYST_RAID = ? ORDER BY REQUESTEDDATE ASC", Request.class);
        query.setParameter(1, riskAnalyst.raID());
        List<Request> requestList = query.getResultList();
        if (requestList.isEmpty()) {
            return new ArrayList<>();
        }
        return requestList;
    }
    
    /**
     * Returns the number of requests being processed at the moment
     * 
     * @return the number of requests
     * 
     * @author Tiago Ribeiro (1170426)
     */
    public long countCurrentRequests() {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT COUNT(*) FROM Request WHERE State = 'PROCESSING' OR State = 'PENDING'");
        
        Object result = query.getSingleResult();
        
        return result instanceof Long ? (Long) result : 0;
    }
    
    /**
     * Returns the list of requests pending or processing
     * 
     * @return the list of requests pending or processing
     * 
     * @author Tiago Ribeiro (1170426)
     */
    public List<Request> allCurrentRequests() {
        EntityManager em = this.entityManager();
        Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'PROCESSING' OR State = 'PENDING'", Request.class);
        
        return query.getResultList();
    }

    /**
     * Method that checks if a certain case with a given id is already in
     * the database
     *
     * @param id
     * @return
     */
    public Request obtainRequestByID(long id) {
        for (Request requestInDB : findAll()) {
            if (requestInDB.obtainID() == id) {
                //if the request exist in DB
                return requestInDB;
            }
        }
        //if the request does not exist with the ID
        return null;
    }

}
