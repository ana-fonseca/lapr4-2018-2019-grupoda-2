package isep.eapli.core.domain;

import java.io.Serializable;

public class District implements Serializable {
    
    private String district;
    
    public District (String district) {
        this.district = district;
    }

    public String district() {
        return district;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof District)) {
            return false;
        }
        District other = (District) o;
        return district.equals(other.district);
    }

    @Override
    public String toString() {
        return district;
    }
    
}
