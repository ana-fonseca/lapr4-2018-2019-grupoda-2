/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.SurroundingType;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author 1171138
 */
public class SurroundingTypeDB extends JpaTransactionalRepository<SurroundingType, String> implements Repository<SurroundingType, String> {

    /**
     * Constructor
     *
     * @param persistenceFileName name of the file that controls the persistence
     */
    public SurroundingTypeDB(String persistenceFileName) {
        super(persistenceFileName);
    }

    public SurroundingType findByType(String type) {
        Query query = this.entityManager().createNativeQuery("SELECT * FROM SurroundingType WHERE description = ?", SurroundingType.class);
        query.setParameter(1, type);
        List<SurroundingType> list = query.getResultList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * Method that checks if a certain surroudingType is already in the
     * database. This method uses the description of each surroundingtype (from
     * database and one received by parameter) to see if they are the same (case
     * insensitive). If they are, the surroundind types are the same, therefore
     * it already exists and the method returns true
     *
     * @param sType surrounding type to check
     * @return result of the search
     */
    public boolean checkIfAlreadyExists(SurroundingType sType) {
        for (SurroundingType sTypeExisting : findAll()) {
            if (sTypeExisting.equals(sType)) {
                return true;
            }
        }
        return false;
    }
}
