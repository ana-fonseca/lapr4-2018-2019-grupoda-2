package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.core.domain.Version;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Class that controls the access to the RiskMatrix Database
 */
public class RiskMatrixDB extends JpaTransactionalRepository<RiskMatrix, Version> implements Repository<RiskMatrix, Version> {

    /**
     * Class Constructor
     *
     * @param persistenceUnitName
     */
    public RiskMatrixDB(String persistenceUnitName) {
        super(persistenceUnitName);
    }

    public RiskMatrix findMatrixById(int id) {
        for (RiskMatrix riskMatrixInDB : findAll()) {
            if (riskMatrixInDB.getIdRiskMatriz() == id) {
                return riskMatrixInDB;
            }
        }
        return null;
    }

    /**
     * Method that checks if a certain risk matrix is already in the database.
     * If the risk factor to verify exists in the DB, it returns true. If not,
     * it returns false.
     *
     * @param riskMatrix - risk factor to be tested
     * @return result of the search
     */
    public boolean checkIfAlreadyExists(RiskMatrix riskMatrix) {
        for (RiskMatrix riskMatrixInDB : findAll()) {
            if (riskMatrixInDB.equalsWithoutVersion(riskMatrix)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkIfBaseAlreadyExists(RiskMatrix riskMatrix) {
        for (RiskMatrix riskMatrixInDB : findAll()) {
            if (riskMatrixInDB.getListCoverageRiskFactorCell().equals(riskMatrix.getListCoverageRiskFactorCell()) && riskMatrixInDB.obtainVersion().equals(riskMatrix.obtainVersion())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method that returns how manny matrixes exist in the database.
     */
    public int getNumMatrixes() {
        return new ArrayList<RiskMatrix>((Collection<? extends RiskMatrix>) findAll()).size();
    }

    @Override
    public void delete(RiskMatrix entity) {
        for (RiskMatrix rm : findAll()) {
            if (entity.equalsWithoutVersion(rm)) {
                super.delete(entity);
            }
        }
    }
}
