package isep.eapli.core.persistenceimplementation;

import eapli.framework.domain.repositories.Repository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaTransactionalRepository;
import isep.eapli.core.domain.Coverage;

/**
 * Class that controls the access to the Coverage Database
 *
 * @author 1170982
 */
public class CoverageDB extends JpaTransactionalRepository<Coverage, String> implements Repository<Coverage, String> {

    /**
     * Class constructor
     *
     * @param persistenceName name of the file that controls the persistence
     */
    public CoverageDB(String persistenceName) {
        super(persistenceName);
    }

    /**
     * Method that checks if a certain coverage is already in the database.
     * This method uses the designation of each coverage (from database and one received by parameter) to see if they are the same (case insensitive).
     * If they are, the coverages are the same, therefore it already exists and the method returns true. If not, it returns false
     *
     * @param coverage Coverage to be tested
     * @return result of the search
     */
    public boolean checkIfAlreadyExists(Coverage coverage) {
        for (Coverage coverageInDB : findAll()) {
            if (coverageInDB.equals(coverage)) {
                return true;
            }
        }
        return false;
    }
    
    public Coverage findByDesignation(String coverage){
        for (Coverage coverageInDB : findAll()) {
            if (coverageInDB.obtainDesignation().equals(coverage)) {
                return coverageInDB;
            }
        }
        return null;
    }
}
