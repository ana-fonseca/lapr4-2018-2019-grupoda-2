package isep.eapli.core.domain;

import java.io.Serializable;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class NearBySurrounding implements Serializable {
    //anotação 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Address address;
    private String description;
    @ManyToOne
    private SurroundingType type;
    @ElementCollection
    private Map<Metric, String> metricScaleMap;

    //latitude e longitude
    @Column(name = "localization")
    private String localization;

    public NearBySurrounding(String description, SurroundingType type, Map<Metric, String> metricScaleMap, Address add) {
        this.description = description;
        this.type = type;
        this.metricScaleMap = metricScaleMap;
        this.address = add;
    }

    public NearBySurrounding() {

    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(SurroundingType type) {
        this.type = type;
    }

    /**
     * Obtain Map of connection between a Metric and a String
     *
     * @return map
     */
    public Map<Metric, String> getMetricScaleMap() {
        return metricScaleMap;
    }

    public String getDescription() {
        return description;
    }

    public Address getAdd() {
        return address;
    }

    public void setAdd(Address add) {
        this.address = add;
    }

    /**
     * Obtain SurroundingType of the Surrounding
     *
     * @return surrounding type
     */
    public SurroundingType getType() {
        return type;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof NearBySurrounding)) {
            return false;
        }
        NearBySurrounding other = (NearBySurrounding) o;
        return other.address.equals(address) && other.description.equals(description) && other.type.equals(type) && other.metricScaleMap.equals(metricScaleMap);
    }

}
