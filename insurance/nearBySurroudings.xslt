<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/ListNearBySurrounding">
		<html>
			<head>
				<title>Near By Surroudings</title>
			</head>
			<body>
				<table id="nearBySurroudings">
					<thead>
						<tr>
							<th>Address</th>
							<th>Description</th>
							<th>SurroundingType</th>
						</tr>
					</thead>
					<tbody>
						<xsl:apply-templates select="NearBySurrounding"/>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
		
	<xsl:template match="NearBySurrounding">
		<tr>
			<td><xsl:value-of select="Address"/></td>
			<td><xsl:value-of select="Description"/></td>
			<td><xsl:value-of select="SurroundingType"/></td>
		</tr>
	</xsl:template>
	
</xsl:stylesheet>
