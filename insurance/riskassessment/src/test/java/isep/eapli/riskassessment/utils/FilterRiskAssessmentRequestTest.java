/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.State;
import isep.eapli.core.domain.Version;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 *
 * @author Luís Silva
 */
public class FilterRiskAssessmentRequestTest {
    
    public FilterRiskAssessmentRequestTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of filter method, of class FilterRiskAssessmentRequest.
     * @throws java.lang.Exception
     */
    @Test
    public void testFilter() throws Exception {
        System.out.println("filter");
        String response = "{\n"
                + "    \"DataInicio\": \"2019-06-02 14:12\",\n"
                + "    \"DataFim\": \"2019-06-02 15:12\",\n"
                + "    \"pair\": [\n"
                + "        {\n"
                + "            \"Cidade1\":\"Desconhecida\"\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        FilterRiskAssessmentRequest instance = new FilterRiskAssessmentRequest();
        String expResult = "";
        String result = instance.filter(response);
        assertEquals(expResult, result);
    }

    /**
     * Test of filterByDate method, of class FilterRiskAssessmentRequest.
     */
    @Test
    public void testFilterByDate() {
        System.out.println("filterByDate");
        InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("incendio"));
        InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("tempestade"));
        InsuranceCoverageConnection icc3 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("furacao"));
        InsuranceCoverageConnection icc4 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("inundacoes"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        iccList.add(icc1);
        iccList.add(icc2);
        iccList.add(icc3);
        iccList.add(icc4);
        Request r =  new Request(iccList, "teste", State.PENDING, new Version());
        List<Request> rList = new ArrayList<>();
        rList.add(r);
        
        LocalDateTime inicio = LocalDateTime.MIN;
        LocalDateTime fim = LocalDateTime.MIN.plusHours(5);
        List<Request> expResult = new ArrayList<>();
        List<Request> result = FilterRiskAssessmentRequest.filterByDate(rList, inicio, fim);
        assertEquals(expResult, result);
        
        inicio = LocalDateTime.MIN;
        fim = LocalDateTime.MAX;
        expResult = new ArrayList<>();
        expResult.add(r);
        result = FilterRiskAssessmentRequest.filterByDate(rList, inicio, fim);
        assertEquals(expResult, result);
    }

    /**
     * Test of filterByLocation method, of class FilterRiskAssessmentRequest.
     */
    @Test
    public void testFilterByLocation() {
        System.out.println("filterByLocation");
        InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Porto"), null)), new Coverage("incendio"));
        InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Aveiro"), null)), new Coverage("tempestade"));
        InsuranceCoverageConnection icc3 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Lisboa"), null)), new Coverage("furacao"));
        InsuranceCoverageConnection icc4 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Coimbra"), null)), new Coverage("inundacoes"));
        List<InsuranceCoverageConnection> iccList1 = new ArrayList<>();
        List<InsuranceCoverageConnection> iccList2 = new ArrayList<>();
        iccList1.add(icc1);
        iccList1.add(icc2);
        iccList2.add(icc3);
        iccList2.add(icc4);
        Request r =  new Request(iccList1, "teste", State.PENDING, new Version());
        Request r1 =  new Request(iccList2, "teste", State.PENDING, new Version());
        List<Request> rList = new ArrayList<>();
        rList.add(r);
        rList.add(r1);
        
        List<City> locations = new ArrayList<>();
        List<Request> expResult = new ArrayList<>();
        List<Request> result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
        assertEquals(expResult, result);
        
        locations.add(new City("Porto"));
        expResult.add(r);
        result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
        assertEquals(expResult, result);
        
        locations.add(new City("Coimbra"));
        expResult.add(r1);
        result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
        assertEquals(expResult, result);
    }
    
}
