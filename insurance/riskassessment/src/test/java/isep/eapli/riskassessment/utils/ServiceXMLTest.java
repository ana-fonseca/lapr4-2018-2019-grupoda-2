package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.core.domain.State;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.Version;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ServiceXMLTest {

    @After
    public void clearDB() {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        for (Request request : cdb.findAll()) {
            cdb.delete(request);
        }
    }

    /**
     * Test of writeXMLRiskAssessmentComparasion method, of class ServiceXML.
     */
    @Test
    public void testWriteJSONRiskAssessmentComparasion() throws Exception {
        Result resultInst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<>(), 10);
        Result result2Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino2", "4532"), new City("Porto2"), new PostalCode("4100-150"), new Country("Portugal2"), new District("Porto2"))), new Coverage("Test2")), new ArrayList<>(), 20);
        Result result3Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino3", "4532"), new City("Porto3"), new PostalCode("4100-150"), new Country("Portugal3"), new District("Porto3"))), new Coverage("Test3")), new ArrayList<>(), 30);

        List<String> info = new ArrayList<>();
        //Cases with the same result:
        info.add(resultInst.toString());
        info.add(result2Inst.toString());
        //Different results:
        Map<String, String> infoDifferent = new HashMap<>();
        infoDifferent.put(result3Inst.toString(), "35,50");
        RiskAssessmentComparasionDTO expInfo = new RiskAssessmentComparasionDTO(info, infoDifferent);
        String expResult = "<results>\n"
                + " <same-results>\n"
                + "     <result>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino, 453\n"
                + "City: Porto\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto\n"
                + "Country: Portugal. Coverage to Analyse: Coverage related to Test1\n"
                + "Score obtained: 10</result>\n"
                + "        <result>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino2, 4532\n"
                + "City: Porto2\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto2\n"
                + "Country: Portugal2. Coverage to Analyse: Coverage related to Test2\n"
                + "Score obtained: 20</result>\n"
                + "    </same-results>\n"
                + "    <differente-results>\n"
                + "        <result>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino3, 4532\n"
                + "City: Porto3\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto3\n"
                + "Country: Portugal3. Coverage to Analyse: Coverage related to Test3\n"
                + "Score obtained: 30<rc1>35</rc1>\n"
                + "            <rc2>50</rc2>\n"
                + "        </result>\n"
                + "    </differente-results>\n"
                + "</results>";
        String result = ServiceXML.writeXMLRiskAssessmentComparasion(expInfo);
        result = result.replaceAll("\\s", "");
        expResult = expResult.replaceAll("\\s", "");

        assertTrue(result.equalsIgnoreCase(expResult));
        assertEquals(expResult, result);
    }

    @Test
    public void testWriteCurrentRequestAvailabilityXMLAvailable() throws Exception {
        clearDB();
        
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 10; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String xml = ServiceXML.writeCurrentRequestAvailabilityXML(requestList, currentLoad, 15);
        
        clearDB();
        codb.remove(cov);
        
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<Requests>\n" +
"   <CurrentLoad>10</CurrentLoad>\n" +
"   <MaxLoad>15</MaxLoad>\n" +
"   <Availability>true</Availability>\n";
        
        for (Request request : requestList) {
            expected += "   <Request id=\"" + request.obtainID() + "\">\n" +
                    (request.obtainRiskAnalyst() == null ? 
"       <RiskAnalyst/>\n" : "       <RiskAnalyst>" + request.obtainRiskAnalyst().email().Email() + "</RiskAnalyst>\n") +
"       <State>" + request.obtainState().name() + "</State>\n" +
"   </Request>\n";
        }
        
        expected += "</Requests>";
        
        assertEquals(expected.replaceAll("\\s", ""), xml.replaceAll("\\s", ""));
    }
    
    @Test
    public void testWriteCurrentRequestAvailabilityXMLNotAvailable() throws Exception {
        clearDB();
        
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 15; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String xml = ServiceXML.writeCurrentRequestAvailabilityXML(requestList, currentLoad, 15);
        
        clearDB();
        codb.remove(cov);
        
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<Requests>\n" +
"   <CurrentLoad>15</CurrentLoad>\n" +
"   <MaxLoad>15</MaxLoad>\n" +
"   <Availability>false</Availability>\n";
        
        for (Request request : requestList) {
            expected += "   <Request id=\"" + request.obtainID() + "\">\n" +
                    (request.obtainRiskAnalyst() == null ? 
"       <RiskAnalyst/>\n" : "       <RiskAnalyst>" + request.obtainRiskAnalyst().email().Email() + "</RiskAnalyst>\n") +
"       <State>" + request.obtainState().name() + "</State>\n" +
"   </Request>\n";
        }
        
        expected += "</Requests>";
        
        assertEquals(expected.replaceAll("\\s", ""), xml.replaceAll("\\s", ""));
    }

}
