package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.SurroundingType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author Luis Moreira
 */
public class ServiceXHTMLTest {

    Street streetComplete = new Street("Rua Dr. Bernardino", "453");
    Street street2 = new Street("Rua Ascensão Guimarães");
    City city = new City("Porto");
    City city2 = new City("Faro");
    PostalCode postalCode;
    PostalCode postalCode2;
    District district = new District("Porto");
    District district2 = new District("Braga");
    District district3 = new District("Faro");
    Country country = new Country("Portugal");

    /**
     * Test of writeListSurroundings method, of class ServiceXHTML.
     */
    @Test
    public void testWriteListSurroundings() {
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));
        ServiceXHTML.writeListSurroundings(lnbs);
    }

}
