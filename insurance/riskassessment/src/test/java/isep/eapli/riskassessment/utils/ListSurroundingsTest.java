package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.SurroundingType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Luis Moreira
 */
public class ListSurroundingsTest {

    Street streetComplete = new Street("Rua Dr. Bernardino", "453");
    Street street2 = new Street("Rua Ascensão Guimarães");
    City city = new City("Porto");
    City city2 = new City("Faro");
    PostalCode postalCode;
    PostalCode postalCode2;
    District district = new District("Porto");
    District district2 = new District("Braga");
    District district3 = new District("Faro");
    Country country = new Country("Portugal");

    /**
     * Test of filterByDistrict method, of class ListSurroundings.
     */
    @Test
    public void testFilterByDistrict() throws Exception {
        String districtTest = "Porto";
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        postalCode = new PostalCode("4100-150");
        postalCode2 = new PostalCode("4100-140");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));

        //result
        List<NearBySurrounding> result = listSurr.filterByDistrict(districtTest);
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }

    /**
     * Test of filterByDistrict method, of class ListSurroundings.
     */
    @Test
    public void testFilterByDistrictNull() throws Exception {
        String districtTest = "Porto";
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        postalCode = new PostalCode("4100-150");
        postalCode2 = new PostalCode("4100-140");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        //result
        List<NearBySurrounding> result = listSurr.filterByDistrict(null);
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }

    /**
     * Test of filterByDefaultArea method, of class ListSurroundings.
     */
    @Test
    public void testFilterByDefaultArea() throws Exception {
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        SurroundingType surroundingType2 = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        postalCode = new PostalCode("4100-150");
        postalCode2 = new PostalCode("4100-140");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);
        Address complete3 = new Address(street2, city2, postalCode2, country, district3);

        NearBySurrounding nbs1 = new NearBySurrounding(description, surroundingType, metricStringMap, complete);
        nbs1.setLocalization("41.124480926269115,-8.443161629638666");
        NearBySurrounding nbs2 = new NearBySurrounding(description, surroundingType, metricStringMap, complete2);
        nbs2.setLocalization("41.53597325445661,-8.433498243408166");
        NearBySurrounding nbs3 = new NearBySurrounding(description, surroundingType2, metricStringMap, complete3);
        nbs3.setLocalization("37.1536076569702,-8.066788338623041");

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(nbs1);
        lnbs.add(nbs2);
        lnbs.add(nbs3);

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        //result
        List<NearBySurrounding> result = listSurr.filterByDefaultArea();
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }

    /**
     * Test of saveListSurroundings method, of class ListSurroundings.
     */
    @Test
    public void testSaveListSurroundings() throws Exception {
        String description = "SurroundingTest";
        SurroundingType surroundingType1 = new SurroundingType("SurroundingTypeTest1");
        SurroundingType surroundingType2 = new SurroundingType("SurroundingTypeTest2");
        SurroundingType surroundingType3 = new SurroundingType("SurroundingTypeTest3");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        postalCode = new PostalCode("4100-150");
        postalCode2 = new PostalCode("4100-140");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);
        Address complete3 = new Address(street2, city2, postalCode2, country, district3);

        NearBySurrounding nbs1 = new NearBySurrounding(description, surroundingType2, metricStringMap, complete);
        nbs1.setLocalization("41.124480926269115,-8.443161629638666");
        NearBySurrounding nbs2 = new NearBySurrounding(description, surroundingType1, metricStringMap, complete2);
        nbs2.setLocalization("41.53597325445661,-8.433498243408166");
        NearBySurrounding nbs3 = new NearBySurrounding(description, surroundingType3, metricStringMap, complete3);
        nbs3.setLocalization("37.1536076569702,-8.066788338623041");

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(nbs1);
        lnbs.add(nbs2);
        lnbs.add(nbs3);

        ListSurroundings instance = new ListSurroundings(lnbs);
        instance.saveListSurroundings();
    }

}
