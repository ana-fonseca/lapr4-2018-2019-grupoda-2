/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class ServiceJSONTest {
    
    @After
    public void clearDB() {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        for (Request request : cdb.findAll()) {
            cdb.delete(request);
        }
    }

    /**
     * Test of readJSON method, of class ServiceJSON.
     */
    @Test
    public void testReadJSON() throws Exception {        
        System.out.println("readJSON");
        String[] givenPath = new String[1];
        givenPath[0] = "caseTESTE.json";

        String type = "teste1";
        Version v = new Version(3);

        List<InsuranceCoverageConnection> iccList = new ArrayList<>();

        Coverage c1 = new Coverage("teste4");
        Address a1 = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i1 = new Insurance(a1);
        InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(i1, c1);
        iccList.add(icc1);

        Coverage c2 = new Coverage("teste5");
        Address a2 = new Address(new Street("Avenida da Boavista"), new City("Porto"), new PostalCode("4150-150"), new Country("Portugal"), new District("Porto"));
        Insurance i2 = new Insurance(a2);
        InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(i2, c2);
        iccList.add(icc2);

        Request expResult = new Request(iccList, type, State.VALIDATED, v);
        Request result = ServiceJSON.readJSON(givenPath[0]);        
        assertEquals(result, result);
    }

    @Test
    public void testWriteJSON() throws Exception {
        System.out.println("witeJSON");

        String type = "teste1";
        Version v = new Version(3);

        List<InsuranceCoverageConnection> iccList = new ArrayList<>();

        Coverage c1 = new Coverage("teste4");
        Address a1 = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance i1 = new Insurance(a1);
        InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(i1, c1);
        iccList.add(icc1);

        Coverage c2 = new Coverage("teste5");
        Address a2 = new Address(new Street("Avenida da Boavista"), new City("Porto"), new PostalCode("4150-150"), new Country("Portugal"));
        Insurance i2 = new Insurance(a2);
        InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(i2, c2);
        iccList.add(icc2);

        Request expResult = new Request(iccList, type, State.VALIDATED, v);
        ServiceJSON.writeJSON(expResult);
    }

    /**
     * Test of writeJSONRiskAssessmentComparasion method, of class ServiceJSON.
     */
    @Test
    public void testWriteJSONRiskAssessmentComparasion() throws Exception {
        Result resultInst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<>(), 10);
        Result result2Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino2", "4532"), new City("Porto2"), new PostalCode("4100-150"), new Country("Portugal2"), new District("Porto2"))), new Coverage("Test2")), new ArrayList<>(), 20);
        Result result3Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino3", "4532"), new City("Porto3"), new PostalCode("4100-150"), new Country("Portugal3"), new District("Porto3"))), new Coverage("Test3")), new ArrayList<>(), 30);

        List<String> info = new ArrayList<>();
        //Cases with the same result:
        info.add(resultInst.toString());
        info.add(result2Inst.toString());
        //Different results:
        Map<String, String> infoDifferent = new HashMap<>();
        infoDifferent.put(result3Inst.toString(), "35,50");
        RiskAssessmentComparasionDTO expInfo = new RiskAssessmentComparasionDTO(info, infoDifferent);
        String expResult = "{\"same-results\":[{\"result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino, 453\\nCity: Porto\\nPostal Code: 4100-150\\nDistrict: Porto\\nCountry: Portugal. Coverage to Analyse: Coverage related to Test1\\nScore obtained: 10\"},{\"result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino2, 4532\\nCity: Porto2\\nPostal Code: 4100-150\\nDistrict: Porto2\\nCountry: Portugal2. Coverage to Analyse: Coverage related to Test2\\nScore obtained: 20\"}],\"different-result\":[{\"result\":{\"rcs\":{\"rc2\":\"50\",\"rc1\":\"35\"},\"info-result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino3, 4532\\nCity: Porto3\\nPostal Code: 4100-150\\nDistrict: Porto3\\nCountry: Portugal3. Coverage to Analyse: Coverage related to Test3\\nScore obtained: 30\"}}]}";
        String result = ServiceJSON.writeJSONRiskAssessmentComparasion(expInfo);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testWriteCurrentRequestAvailabilityJSONAvailable() throws Exception {
        clearDB();
        
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 10; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String json = ServiceJSON.writeCurrentRequestAvailabilityJSON(requestList, currentLoad, 15);
        
        String expected = "{\"maxLoad\":15,\"availability\":true,\"requests\":[";
        
        int i = 0;
        for (Request request : requestList) {
            if (request.obtainRiskAnalyst() != null) {
                expected += "{\"riskAnalyst\":\"" + request.obtainRiskAnalyst().email().Email() + "\",";
            } else {
                expected += "{";
            }
            i++;
            expected += "\"id\":" + request.obtainID() + ",\"state\":\"" + request.obtainState().name() + "\"}" + (i < requestList.size() ? "," : "]");
        }
        
        expected += ",\"currentLoad\":10}";
        
        clearDB();
        codb.remove(cov);
        
        assertEquals(json, expected);
    }
    
    @Ignore
    public void testWriteCurrentRequestAvailabilityJSONNotAvailable() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 15; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String json = ServiceJSON.writeCurrentRequestAvailabilityJSON(requestList, currentLoad, 15);
        
        String expected = "{\"maxLoad\":15,\"availability\":false,\"requests\":[";
        
        int i = 0;
        for (Request request : requestList) {
            if (request.obtainRiskAnalyst() != null) {
                expected += "{\"riskAnalyst\":\"" + request.obtainRiskAnalyst().email().Email() + "\",";
            } else {
                expected += "{";
            }
            i++;
            expected += "\"id\":" + request.obtainID() + ",\"state\":\"" + request.obtainState().name() + "\"}" + (i < requestList.size() ? "," : "]");
        }
        
        expected += ",\"currentLoad\":15}";
        
        clearDB();
        codb.remove(cov);
        
        assertEquals(json, expected);
    }
}
