package isep.eapli.riskassessment.controller;

import isep.eapli.config.bootstrapers.CoverageBootstrapper;
import isep.eapli.config.bootstrapers.RiskFactorBootstrapper;
import isep.eapli.config.bootstrapers.RiskMatrixBootstrapper;
import isep.eapli.config.bootstrapers.SurroundingTypeBootstrapper;
import isep.eapli.config.controller.PublishRiskMatrixController;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RiskAssessmentComparisonControllerTest {

    @Test
    public void getPublishedRiskMatrices() {
        RepositoryFactory rp = new RepositoryFactory();
        RiskMatrixDB rmdb = rp.getRiskMatrixDB();
        CoverageDB cdb = rp.getCoverageDB();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        RiskMatrixBootstrapper rfb = new RiskMatrixBootstrapper();
        new CoverageBootstrapper().execute();
        new SurroundingTypeBootstrapper().execute();
        new RiskFactorBootstrapper().execute();
        rfb.execute();
        Metric m1 = new Metric("Distance");
        Metric m2 = new Metric("Time");
        SurroundingType st2 = new SurroundingType("Fire Fighter Station");
        SurroundingType st1 = new SurroundingType("River");
        SurroundingType st3 = new SurroundingType("Hospital");
        SurroundingType st4 = new SurroundingType("Police");
        RiskFactor rf1 = new RiskFactor(st1, m1);
        RiskFactor rf2 = new RiskFactor(st1, m2);
        RiskFactor rf3 = new RiskFactor(st2, m1);
        RiskFactor rf4 = new RiskFactor(st2, m2);
        Coverage c1 = new Coverage("Hurricane");
        Coverage c2 = new Coverage("Earthquake");
        Coverage c3 = new Coverage("Fire");
        Coverage c4 = new Coverage("Storm");
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(new CoverageRiskFactorCell(c4, rf1));
        list.add(new CoverageRiskFactorCell(c4, rf2));
        list.add(new CoverageRiskFactorCell(c4, rf4));
        list.add(new CoverageRiskFactorCell(c3, rf1));
        list.add(new CoverageRiskFactorCell(c3, rf3));
        list.add(new CoverageRiskFactorCell(c3, rf4));
        list.add(new CoverageRiskFactorCell(c1, rf2));
        list.add(new CoverageRiskFactorCell(c1, rf3));
        list.add(new CoverageRiskFactorCell(c1, rf4));
        list.add(new CoverageRiskFactorCell(c2, rf1));
        list.add(new CoverageRiskFactorCell(c2, rf2));
        list.add(new CoverageRiskFactorCell(c2, rf3));
        list.add(new CoverageRiskFactorCell(c2, rf4));
        RiskMatrix riskMatrix = new RiskMatrix(list, "Not Published", 0);
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(0), new Contribution("Positive"), new Weight(9), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(1), new Contribution("Positive"), new Weight(3), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(2), new Contribution("Positive"), new Weight(9), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(3), new Contribution("Positive"), new Weight(4), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(4), new Contribution("Positive"), new Weight(4), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(5), new Contribution("Negative"), new Weight(4), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(6), new Contribution("Negative"), new Weight(6), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(7), new Contribution("Negative"), new Weight(6), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(8), new Contribution("Positive"), new Weight(6), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(9), new Contribution("Negative"), new Weight(2), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(10), new Contribution("Negative"), new Weight(9), new Necessity("Required")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(11), new Contribution("Positive"), new Weight(2), new Necessity("Optional")));
        riskMatrix.addNewCharacterizedCell(new CharacterizedCell(list.get(12), new Contribution("Negative"), new Weight(4), new Necessity("Required")));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(0), new Scale(1, 2, 5)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(1), new Scale(1, 2, 6)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(2), new Scale(1, 2, 7)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(3), new Scale(1, 2, 8)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(4), new Scale(1, 2, 9)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(5), new Scale(1, 2, 10)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(6), new Scale(1, 2, 11)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(7), new Scale(1, 2, 12)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(8), new Scale(1, 2, 13)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(9), new Scale(1, 2, 14)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(10), new Scale(1, 2, 15)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(11), new Scale(1, 2, 16)));
        riskMatrix.addNewDetailedCell(new DetailedCell(riskMatrix.getListCharacterizedCell().get(12), new Scale(1, 2, 17)));
        PublishRiskMatrixController prmc = new PublishRiskMatrixController();
        prmc.publishRiskMatrix(riskMatrix);
        RiskAssessmentComparisonController racc = new RiskAssessmentComparisonController();
        List<RiskMatrix> rmList = (List<RiskMatrix>) racc.getPublishedRiskMatrices();
        assertEquals(1, rmList.size());
        for (RiskMatrix rm : rmdb.findAll()) {
            rmdb.remove(rm);
        }
        rfdb.delete(rf1);
        rfdb.delete(rf2);
        rfdb.delete(rf3);
        rfdb.delete(rf4);
        stdb.delete(st2);
        stdb.delete(st1);
        stdb.delete(st3);
        stdb.delete(st4);
        cdb.delete(c1);
        cdb.delete(c2);
        cdb.delete(c3);
        cdb.delete(c4);
    }

    @Test
    public void compareRiskAssessment() {
    }
}