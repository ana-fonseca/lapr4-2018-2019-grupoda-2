package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.riskassessment.controller.InsuranceCalculationController;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RiskAssessmentComparasionCalc {

    /**
     * Access to calculation
     */
    private InsuranceCalculationController icc = new InsuranceCalculationController();

    /**
     * Receives two request & local and calculate a risk assessment to those
     * requests and the specific local
     *
     * Luís Moreira
     *
     * @param request1 request associated to the selected risk matrix
     * @param request2 request associated to the selected risk matrix
     * @param local specific local (city)
     * @return
     */
    public RiskAssessmentComparasionDTO riskAssessmentComparasion(Request request1, Request request2, String local) {
        List<String> sameResult = new ArrayList<>();
        Map<String, String> differentResult = new HashMap<>();

//        icc.InsuranceCalculationWithoutDetails(request1);
//        icc.InsuranceCalculationWithoutDetails(request2);
        List<Result> case1Result = request1.getResultList();
        List<Result> case2Result = request2.getResultList();
        List<InsuranceCoverageConnection> equalResults = new LinkedList<>();
        for (Result r : case1Result) {
            if (case2Result.contains(r) && r.obtainIcc().obtainInsurance().getAddress().getCity().city().equalsIgnoreCase(local)) {
                sameResult.add("Coverage:" + r.obtainIcc().obtainCoverage().obtainDesignation() + "\nCity:" + r.obtainIcc().obtainInsurance().getAddress().getCity() + "\nIndex:" + r.obtainIndex() + "\n");
                equalResults.add(r.obtainIcc());
            }
        }
        for (Result r : case1Result) {
            if (!equalResults.contains(r.obtainIcc()) && r.obtainIcc().obtainInsurance().getAddress().getCity().city().equalsIgnoreCase(local)) {
                int rc1 = 0;
                for (Result ra : case1Result) {
                    if (r.obtainIcc().equals(ra.obtainIcc())) {
                        rc1 = ra.obtainIndex();
                    }
                }
                int rc2 = 0;
                for (Result ra : case2Result) {
                    if (r.obtainIcc().equals(ra.obtainIcc())) {
                        rc2 = ra.obtainIndex();
                    }
                }
                differentResult.put("Coverage:" + r.obtainIcc().obtainCoverage().obtainDesignation() + "\nCity:" + r.obtainIcc().obtainInsurance().getAddress().getCity() + "\n", rc1 + "," + rc2);
            }
        }
        return new RiskAssessmentComparasionDTO(sameResult, differentResult);
    }
}
