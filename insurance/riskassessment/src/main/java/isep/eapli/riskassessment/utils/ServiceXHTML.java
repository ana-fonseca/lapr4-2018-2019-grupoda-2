package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.domain.State;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Maria Junqueira
 */
public class ServiceXHTML {

    private static final String XSLT_RISK_ASSESSMENT_COMP = "../riskAssessmentComp.xsl";

    private static final String XSLT_FILE_RESULTS = "../getAssessmentResult.xsl";

    private static final String XSLT_FILE_STATE = "../getAssessmentState.xsl";

    private static final String XSLT_CURRENT_AVAILABILITY = "../currentAvailability.xslt";

    private static final String XSLT_REPORT = "../statisticReport.xslt";

    private static final String XSLT_NEAR_BY_SURROUNDINGS = "../nearBySurroudings.xslt";

    private static final String XSLT_SUBMITTED_REQUESTS = "../submittedRequests.xslt";

    private static final String XSD_OUTPUT_XML_RESULTS = "../getAssessmentResult_Results.xsd";

    private static final String XSD_OUTPUT_XML_STATE = "../getAssessmentResult_State.xsd";
    
    private static final String XSLT_VALIDATED_REQUESTS = "../validatedRequests.xslt";

    private static final Logger LOGGER = Logger.getLogger(ServiceXHTML.class.getName());

    public boolean validateXML(Document doc, File schemaFile) {
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = schemaFactory.newSchema(schemaFile);

            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(doc));

            return true;
        } catch (SAXException e) {

            e.printStackTrace();
            LOGGER.severe("The XML file is not valid");
        } catch (IOException e) {

            e.printStackTrace();
            LOGGER.severe("File doesn't exist");
        }

        return false;
    }

    /**
     * Gets the results or the state of a case on String formatted in xml style
     *
     * @param Request request
     * @return results/state
     */
    public String getContentInXml(Request request) {
        Document doc;
        String xmlString = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            doc = docBuilder.newDocument();

            if (request.obtainState() != State.VALIDATED) {
                Element root = doc.createElement("request");
                doc.appendChild(root);

                Element requestID = doc.createElement("id");
                requestID.setTextContent(String.valueOf(request.obtainID()));
                root.appendChild(requestID);

                Element requestState = doc.createElement("state");
                requestState.setTextContent(request.obtainState().toString());
                root.appendChild(requestState);

                Element requestDate = doc.createElement("requestDate");
                requestDate.setTextContent(request.obtainRequestedDate().toString());
                root.appendChild(requestDate);

                Element assignedDate = doc.createElement("assignedDate");
                assignedDate.setTextContent(request.obtainAssignedDate().toString());
                root.appendChild(assignedDate);

                List<Result> resultList = request.getResultList();
                for (Result r : resultList) {
                    Element resultElem = doc.createElement("result");
                    root.appendChild(resultElem);

                    Element country = doc.createElement("country");
                    country.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().country()));
                    resultElem.appendChild(country);

                    Element district = doc.createElement("district");
                    district.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().district()));
                    resultElem.appendChild(district);

                    Element postalCode = doc.createElement("postalCode");
                    postalCode.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().postalCode()));
                    resultElem.appendChild(postalCode);

                    Element street = doc.createElement("street");
                    street.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().street()));
                    resultElem.appendChild(street);

                    Element coverage = doc.createElement("coverage");
                    coverage.setTextContent(r.obtainIcc().obtainCoverage().obtainDesignation());
                    resultElem.appendChild(coverage);

                    Element riskIndex = doc.createElement("index");
                    riskIndex.setTextContent(String.valueOf(r.obtainIndex()));
                    resultElem.appendChild(riskIndex);
                }
            } else {
                Element root = doc.createElement("results");
                doc.appendChild(root);

                List<Result> resultList = request.getResultList();
                for (Result r : resultList) {
                    Element resultElem = doc.createElement("result");
                    root.appendChild(resultElem);

                    Element country = doc.createElement("country");
                    country.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().country()));
                    resultElem.appendChild(country);

                    Element district = doc.createElement("district");
                    district.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().district()));
                    resultElem.appendChild(district);

                    Element postalCode = doc.createElement("postalCode");
                    postalCode.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().postalCode()));
                    resultElem.appendChild(postalCode);

                    Element street = doc.createElement("street");
                    street.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().street()));
                    resultElem.appendChild(street);

                    Element coverage = doc.createElement("coverage");
                    coverage.setTextContent(r.obtainIcc().obtainCoverage().obtainDesignation());
                    resultElem.appendChild(coverage);

                    Element riskIndex = doc.createElement("index");
                    riskIndex.setTextContent(String.valueOf(r.obtainIndex()));
                    resultElem.appendChild(riskIndex);
                }
            }

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                StringWriter writer = new StringWriter();
                tr.transform(new DOMSource(doc), new StreamResult(writer));

//                if (request.obtainState() == State.VALIDATED) {
//                    if (!validateXML(doc, new File(XSD_OUTPUT_XML_RESULTS))) {
//                        return null;
//                    }
//                } else {
//                    if (!validateXML(doc, new File(XSD_OUTPUT_XML_STATE))) {
//                        return null;
//                    }
//                }
                xmlString = writer.getBuffer().toString();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return xmlString;
    }

    /**
     * Gets the results of the state of a case on String formatted in xhtml
     * style
     *
     * @param Request request
     * @return results/state
     */
    public String getContentInXhtml(Request request) {
        String xmlOutput = getContentInXml(request);
        String xhtmlOutput = "";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document doc = null;

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlOutput)));
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        if (doc == null) {
            return "";
        }

        try {
            Transformer tr;
            if (request.obtainState() == State.VALIDATED) {
                tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_FILE_RESULTS));
            } else {
                tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_FILE_STATE));
            }

            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(doc), new StreamResult(writer));

            xhtmlOutput = writer.getBuffer().toString();

        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return xhtmlOutput;
    }

    /**
     * Receives a RiskAssessmentComparasionDTO and returns the important info in
     * a xhtml file
     *
     * Luís Moreira
     *
     * @param info - RiskAssessmentComparasionDTO
     * @return important info in xhtml
     * @throws java.io.IOException
     */
    public static String getRiskAssessmentComparasionInXhtml(RiskAssessmentComparasionDTO info) throws IOException {
        String xmlOutput = ServiceXML.writeXMLRiskAssessmentComparasion(info);
        String xhtmlOutput;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document doc;

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlOutput)));
        } catch (ParserConfigurationException | IOException | SAXException e) {
            return "";
        }

        if (doc == null) {
            return "";
        }

        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_RISK_ASSESSMENT_COMP));

            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(doc), new StreamResult(writer));

            xhtmlOutput = writer.getBuffer().toString();
            return xhtmlOutput;
        } catch (TransformerException e) {
            return "";
        }

    }

    /**
     * Receives a List of Near By Surrounding and write info in a xhtml file
     * named "ListNearBySurroundings.html"
     *
     * @param lnbs - list of NearBySurrounding
     * @author Luis Moreira
     */
    public static void writeListSurroundings(List<NearBySurrounding> lnbs) {
        try {
            String xmlOutput = ServiceXML.writeXMLNearBySurrouding(lnbs);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            Document doc;

            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

            if (doc != null) {

                Transformer tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_NEAR_BY_SURROUNDINGS));

                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "html");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                FileWriter writer = new FileWriter("../ListNearBySurroundings.html");
                tr.transform(new DOMSource(doc), new StreamResult(writer));
            }
        } catch (Exception e) {
            System.out.println("e = " + e.getStackTrace());
        }
    }

    /**
     * Returns a string which is a XHTML file with the current availability
     * information. It uses the method "writeCurrentRequestAvailabilityXML"
     * developed in the ServiceXML class, creating a XML to then transform using
     * the developed XSLT.
     *
     * @param requestList the current requests
     * @param currentLoad the current number of requests
     * @param maxLoad the maximum limit of concurrent requests
     * @return Returns a XHTML String that has the main information
     *
     * @author Tiago Ribeiro (1170426)
     */
    public static String getCurrentAvailabilityXHTML(List<Request> requestList, long currentLoad, int maxLoad) {
        try {
            String xmlOutput = ServiceXML.writeCurrentRequestAvailabilityXML(requestList, currentLoad, maxLoad);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

            if (doc == null) {
                return "";
            }

            StreamSource strSource = new StreamSource(XSLT_CURRENT_AVAILABILITY);
            Transformer tr = TransformerFactory.newInstance().newTransformer(strSource);

            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter writer = new StringWriter();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(writer);
            tr.transform(source, result);

            String xhtmlOutput = writer.getBuffer().toString();
            return xhtmlOutput;
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * This method returns information in XHTML
     *
     * @param numConsultations
     * @param numRequests
     * @param initTime
     * @return
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws SAXException
     * @throws IOException This was created by Ana Fonseca (1170656)
     */
    public static String writeXHTMLStatisticReport(Integer numConsultations, Integer numRequests, LocalDateTime initTime) throws ParserConfigurationException, TransformerException, SAXException, IOException {

        String xmlOutput = ServiceXML.writeXMLStatisticReport(numConsultations, numRequests, initTime);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

        StreamSource strSource = new StreamSource(XSLT_REPORT);
        Transformer tr = TransformerFactory.newInstance().newTransformer(strSource);

        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "html");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        StringWriter writer = new StringWriter();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(writer);
        tr.transform(source, result);

        String xhtmlOutput = writer.getBuffer().toString();
        return xhtmlOutput;
    }

    public static String writeXHTMLSubmitRequests(int numMax, int counter) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        String xmlOutput = ServiceXML.writeRequestsSubmissionMessageXML(numMax, counter);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

        StreamSource strSource = new StreamSource(XSLT_SUBMITTED_REQUESTS);
        Transformer tr = TransformerFactory.newInstance().newTransformer(strSource);

        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "html");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        StringWriter writer = new StringWriter();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(writer);
        tr.transform(source, result);

        String xhtmlOutput = writer.getBuffer().toString();
        return xhtmlOutput;
    }
    
    /**
     * Returns a string which is a XHTML file with the current availability
     * information. It uses the method "ValidatedRequestsXML"
     * developed in the ServiceXML class, creating a XML to then transform using
     * the developed XSLT.
     *
     * @param requestList the validated requests
     * @param sum the summary with the quantity and the average time period
     * @return Returns a XHTML String that has the main information
     *
     * @author Maria Junqueira (1170842)
     */
    public String getValidatedRequestsXHTML(List<Request> requestList, String sum) {
        try {
            String xmlOutput = ServiceXML.ValidatedRequestsXML(requestList, sum);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

            if (doc == null) {
                return "";
            }

            StreamSource strSource = new StreamSource(XSLT_VALIDATED_REQUESTS);
            Transformer tr = TransformerFactory.newInstance().newTransformer(strSource);

            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter writer = new StringWriter();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(writer);
            tr.transform(source, result);

            String xhtmlOutput = writer.getBuffer().toString();
            return xhtmlOutput;
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            e.printStackTrace();
            return "";
        }
    }
}
