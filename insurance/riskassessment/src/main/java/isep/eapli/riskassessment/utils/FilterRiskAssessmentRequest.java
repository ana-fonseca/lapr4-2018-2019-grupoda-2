/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Request;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class FilterRiskAssessmentRequest {
    private RepositoryFactory rp;
    private RequestDB rdb;

    public FilterRiskAssessmentRequest() {
        this.rp = new RepositoryFactory();
        this.rdb = rp.getRequestDB();
    }
    
    public String filter(String response) throws ParseException, IOException{
        String filteredInfo = new String();
        List<Request> requestFiltered = new ArrayList<>();
        List<Request> r = rdb.findConcludedRequests();
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(response);
        JSONObject caseObject = (JSONObject) obj;
        LocalDateTime datas[] = parseJsonToDate(caseObject);
        List<City> locations = parseJsonToLocation(caseObject);

        if (datas[0] != null) {
            requestFiltered = filterByDate(r, datas[0], datas[1]);
            if (!locations.isEmpty()) {
                requestFiltered = filterByLocation(requestFiltered, locations);
            }
        } else if (!locations.isEmpty()) {
            requestFiltered = filterByLocation(r, locations);
        } else requestFiltered = r;

        for (Request request : requestFiltered) {
            filteredInfo += ServiceJSON.writeJSON(request) + "\n";
        }
        return filteredInfo;
    }
    
    public static List<Request> filterByDate(List<Request> r, LocalDateTime inicio, LocalDateTime fim){
        List<Request> requestFiltered = new ArrayList<>(); 
        for (Request request : r) {
            if (request.obtainRequestedDate().isAfter(inicio) && request.obtainRequestedDate().isBefore(fim)) {
                requestFiltered.add(request);
            }
        }
        return requestFiltered;
    }
    
    public static List<Request> filterByLocation(List<Request> r, List<City> locations){
        List<Request> requestFiltered = new ArrayList<>();
        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getInsuranceCoverageConnectionList().size(); j++) {
                if (locations.contains(r.get(i).getInsuranceCoverageConnectionList().get(j).obtainInsurance().getAddress().getCity())) {
                    requestFiltered.add(r.get(i));
                }
            }
        }
        return requestFiltered;
    }

    private static LocalDateTime[] parseJsonToDate(JSONObject caseJson) {
        LocalDateTime datas[] = new LocalDateTime[2];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); 
        
        //Get dataIni
        String dataIni = (String) caseJson.get("DataInicio");
        if (dataIni != null) {
            LocalDateTime ini = LocalDateTime.parse(dataIni, formatter);    
            datas[0] = ini;
        }
        
        //Get dataFim
        String dataFim = (String) caseJson.get("DataFim");
        if (dataFim != null) {
            LocalDateTime fim = LocalDateTime.parse(dataFim, formatter);
            datas[1] = fim;
        }

        return datas; 
    }

    private static List<City> parseJsonToLocation(JSONObject caseJson) {
        List<City> locations = new ArrayList<>();
 
        //Get case pair list, locations
        JSONArray pairList = (JSONArray) caseJson.get("pair");
        int i = 0;
        if (pairList != null) {
            for (Object pairObj : pairList) {
                JSONObject obj = (JSONObject) pairObj;
                String city = (String) obj.get("Cidade" + (i + 1));
                City cidade = new City(city);
                locations.add(cidade);
                i++;
            }
        }
        return locations;
    }
    
     
}
