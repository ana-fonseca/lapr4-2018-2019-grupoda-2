package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.*;
import java.util.*;

public class SeveralInsurancesCalculationController {

    private InsuranceCalculationController icc;

    public SeveralInsurancesCalculationController(){
        icc = new InsuranceCalculationController();
    }

    public void SeveralInsurancesCalculationWithoutDetails(List<Request> requests){
        Thread[] thrs = new Thread[requests.size()];
        for(int i = 0; i< requests.size(); i++){
            Request cas = requests.get(i);
            thrs[i] = new Thread(() -> icc.InsuranceCalculationWithoutDetails(cas));
            thrs[i].start();
        }
        for(int i = 0; i< requests.size(); i++){
            try {
                thrs[i].join();
            } catch (InterruptedException e) {
                System.out.println("ERROR IN THREAD "+i+"\n");
                e.printStackTrace();
            }
        }
    }

    public void SeveralInsurancesCalculationWithDetails(List<Request> requests){
        Thread[] thrs = new Thread[requests.size()];
        for(int i = 0; i< requests.size(); i++){
            Request cas = requests.get(i);
            thrs[i] = new Thread(() -> icc.InsuranceCalculationWithDetails(cas));
            thrs[i].start();
        }
        for(int i = 0; i< requests.size(); i++){
            try {
                thrs[i].join();
            } catch (InterruptedException e) {
                System.out.println("ERROR IN THREAD "+i+"\n");
                e.printStackTrace();
            }
        }
    }
}
