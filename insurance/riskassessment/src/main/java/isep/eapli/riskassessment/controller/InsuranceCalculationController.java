package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InsuranceCalculationController {

    /**
     * Attribute risk matrix DB
     */
    private final RiskMatrixDB rmDB;

    /**
     * Attribute case DB
     */
    private final RequestDB requestDB;

    /**
     * Constructor
     */
    public InsuranceCalculationController() {
        rmDB = new RepositoryFactory().getRiskMatrixDB();
        requestDB = new RepositoryFactory().getRequestDB();
    }

    /**
     * Method that calculates risk index without details
     *
     * @param cas
     */
    public void InsuranceCalculationWithoutDetails(Request cas) {
        RiskMatrix rm = null;
        for (RiskMatrix rmInDB : rmDB.findAll()) {
            if (cas.getMatrixVersion().equals(rmInDB.obtainVersion())) {
                rm = rmInDB;
                break;
            }
        }
        if (rm != null) {
            cas.setState(State.PROCESSING);
            for (InsuranceCoverageConnection icc : cas.getInsuranceCoverageConnectionList()) {
                int index = rm.calculationWithoutDetails(icc.obtainCoverage(), icc.obtainInsurance().getAddress());
                Result r = new Result(icc, new ArrayList<Explanation>(), index);
                cas.addResult(r);
            }
            cas.setState(State.VALIDATED);
            synchronized (requestDB) {
                requestDB.save(cas);
            }
        }
    }

    /**
     * Method that calculates risk index with details
     *
     * @param cas
     */
    public void InsuranceCalculationWithDetails(Request cas) {
        RiskMatrix rm = null;
        for (RiskMatrix rmInDB : rmDB.findAll()) {
            if (cas.getMatrixVersion().equals(rmInDB.obtainVersion()) && !rmInDB.obtainNonPublishedVersionName().equalsIgnoreCase("Not Published")) {
                rm = rmInDB;
            }
        }
        if (rm != null) {
            cas.setState(State.PROCESSING);
            for (InsuranceCoverageConnection icc : cas.getInsuranceCoverageConnectionList()) {
                HashMap<Integer, List<Explanation>> map = rm.calculationWithDetails(icc.obtainCoverage(), icc.obtainInsurance().getAddress());
                int index = 0;
                List<Explanation> exps = new ArrayList<Explanation>();
                for (Integer i : map.keySet()) {
                    index = i;
                    exps = map.get(i);
                }
                Result r = new Result(icc, exps, index);
                cas.addResult(r);
            }
            cas.setState(State.VALIDATED);
            synchronized (requestDB) {
                requestDB.save(cas);
            }
        }
    }
}
