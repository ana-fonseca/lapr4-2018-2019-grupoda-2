package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RiskAssessmentComparisonController {

    /**
     * RequestDB object
     */
    private RequestDB cdb;

    /**
     * RiskMatrixDB object
     */
    private RiskMatrixDB rmdb;

    /**
     * Access to calculation
     */
    private InsuranceCalculationController icc = new InsuranceCalculationController();

    /**
     * Public constructor
     */
    public RiskAssessmentComparisonController() {
        RepositoryFactory rp = new RepositoryFactory();
        cdb = rp.getRequestDB();
        rmdb = rp.getRiskMatrixDB();
    }

    /**
     * Method that gets the published risk matrixes in the system
     *
     * @return list with risk matrixes published
     */
    public Iterable<RiskMatrix> getPublishedRiskMatrices() {
        List<RiskMatrix> riskMatrixList = new ArrayList<>();
        for (RiskMatrix rm : rmdb.findAll()) {
            if (!rm.obtainNonPublishedVersionName().equalsIgnoreCase("Not Published")) {
                riskMatrixList.add(rm);
            }
        }
        return riskMatrixList;
    }

    /**
     * Method that compares and prints the information about the comparasion
     *
     * @param v1 version 1 to compare
     * @param v2 version 2 to compare
     */
    public void compareRiskAssessment(Version v1, Version v2) {
        Request request1 = cdb.obtainCaseByVersion(v1);
        Request request2 = cdb.obtainCaseByVersion(v1);
        request2.setMatrixVersion(v2);
        icc.InsuranceCalculationWithoutDetails(request1);
        icc.InsuranceCalculationWithoutDetails(request2);
        List<Result> case1Result = request1.getResultList();
        List<Result> case2Result = request2.getResultList();
        List<InsuranceCoverageConnection> equalResults = new LinkedList<>();
        System.out.println("Cases with the same result:");
        for (Result r : case1Result) {
            if (case2Result.contains(r)) {
                System.out.println(r);
                equalResults.add(r.obtainIcc());
            }
        }
        System.out.println("Different results:");
        for (Result r : case1Result) {
            if (!equalResults.contains(r.obtainIcc())) {
                System.out.println(icc);
                int rc1 = 0;
                for (Result ra : case1Result) {
                    if (r.obtainIcc().equals(ra.obtainIcc())) {
                        rc1 = ra.obtainIndex();
                    }
                }
                int rc2 = 0;
                for (Result ra : case2Result) {
                    if (r.obtainIcc().equals(ra.obtainIcc())) {
                        rc2 = ra.obtainIndex();
                    }
                }
                System.out.println("Score on Version 1: " + rc1);
                System.out.println("Score on Version 2: " + rc2 + "\n====================\n");
            }
        }
    }

}
