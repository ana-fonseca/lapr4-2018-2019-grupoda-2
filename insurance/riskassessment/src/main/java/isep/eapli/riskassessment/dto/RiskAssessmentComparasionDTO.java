package isep.eapli.riskassessment.dto;

import java.util.List;
import java.util.Map;

public class RiskAssessmentComparasionDTO {

    private List<String> listSameResult;
    private Map<String, String> listDifferentResult;//String with Score on Version 1 + "," + Score on Version 2

    public RiskAssessmentComparasionDTO(List<String> listSameResult, Map<String, String> listDifferentResult) {
        this.listSameResult = listSameResult;
        this.listDifferentResult = listDifferentResult;
    }

    public List<String> getListSameResult() {
        return listSameResult;
    }

    public Map<String, String> getListDifferentResult() {
        return listDifferentResult;
    }

}
