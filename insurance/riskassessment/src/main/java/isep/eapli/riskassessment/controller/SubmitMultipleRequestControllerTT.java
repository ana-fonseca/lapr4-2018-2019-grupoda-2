/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.Request;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.utils.ServiceXML;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

/**
 *
 * @author becas
 */
public class SubmitMultipleRequestControllerTT {

    public boolean submitOneRequest(Request r) throws ParseException, FileNotFoundException, IOException, SAXException, Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB rdb = rp.getRequestDB();
            FileReader in = new FileReader("../requestLimit.properties");
            Properties requestLimitProperties = new Properties();
            requestLimitProperties.load(in);
            int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));

            if (!rdb.checkIfAlreadyExists(r)) {
                if (rdb.countCurrentRequests() < requestLimit) {
                    rdb.save(r);
                    return true;
                }
            }
        return false;
    }

}
