package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Version;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class RiskAssessmentComparasionRequest {

    private RepositoryFactory repositoryAccess;
    private RequestDB requestDB;
    private RiskMatrixDB riskMatrixDB;

    public RiskAssessmentComparasionRequest() {
        repositoryAccess = new RepositoryFactory();
        riskMatrixDB = repositoryAccess.getRiskMatrixDB();
        requestDB = repositoryAccess.getRequestDB();
    }

    /**
     * Receives a request to risk assessment comparasion which can be in xml or
     * json format & can return in different formats also
     *
     * Luís Moreira
     *
     * @param response
     * @return the risk assessment comparasion in different formats
     */
    public String riskAssessmentComparasion(String response) {
        try {
            if (response.isEmpty()) {
                return "";
            }
            String[] info = null;
            response = response.trim();
            if (response.startsWith("<?xml")) {
                info = ServiceXML.readXMLRiskAssessmentComparasion(response);
            } else {
                JSONParser jsonParser = new JSONParser();
                Object obj = jsonParser.parse(response);
                JSONObject caseObject = (JSONObject) obj;
                info = parseJsonToInfo(caseObject);
            }

            String id1 = info[0];
            String id2 = info[1];
            String local = info[2];
            if (info[3] == null) {
                info[3] = "json";
            }

            if (id1 == null || id2 == null || local == null || local.isEmpty() || id1.equalsIgnoreCase(id2)) {
                return "";
            }

            Version v1 = riskMatrixDB.findMatrixById(Integer.parseInt(id1)).obtainVersion();
            Version v2 = riskMatrixDB.findMatrixById(Integer.parseInt(id2)).obtainVersion();

            Request request1 = requestDB.obtainCaseByVersion(v1);
            Request request2 = requestDB.obtainCaseByVersion(v2);
            RiskAssessmentComparasionCalc racc = new RiskAssessmentComparasionCalc();

            RiskAssessmentComparasionDTO result = racc.riskAssessmentComparasion(request1, request2, local);

            if (info[3].equalsIgnoreCase("xml")) {
                return info[3] + "," + ServiceXML.writeXMLRiskAssessmentComparasion(result);
            } else if (info[3].equalsIgnoreCase("xhtml")) {
                return info[3] + "," + ServiceXHTML.getRiskAssessmentComparasionInXhtml(result);
            } else {
                return info[3] + "," + ServiceJSON.writeJSONRiskAssessmentComparasion(result);
            }

        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * With the json info returns the important information in a array of string
     *
     * Luís Moreira
     *
     * @param caseJson
     * @return important information in an array of String
     */
    private static String[] parseJsonToInfo(JSONObject caseJson) {
        String[] info = new String[4];
        //Get id1
        String id1 = (String) caseJson.get("id1");
        if (id1 != null) {
            info[0] = id1;
        }

        //Get id2
        String id2 = (String) caseJson.get("id2");
        if (id2 != null) {
            info[1] = id2;
        }

        String local = (String) caseJson.get("local");
        if (local != null) {
            info[2] = local;
        }

        String typeOutput = (String) caseJson.get("typeOutput");
        if (typeOutput != null) {
            info[3] = typeOutput;
        }

        return info;
    }

}
