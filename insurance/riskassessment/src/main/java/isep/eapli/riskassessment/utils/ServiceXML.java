package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.State;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.Version;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ServiceXML {

    /**
     * From a string with the info of a xml return the info needed for the
     * comparasion
     *
     * Luís Moreira
     *
     * @param infoXML
     *
     * @return
     */
    public static String[] readXMLRiskAssessmentComparasion(String infoXML) {
        try {
            String[] output = new String[4];
            //infoXML = infoXML.substring(3, infoXML.length());
            DocumentBuilderFactory dbf
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(infoXML));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("Resource");

            if (nodes.getLength() == 0) {
                nodes = doc.getElementsByTagName("Resources");
            }
            //only has one element, so index = 0
            Element element = (Element) nodes.item(0);

            NodeList name = element.getElementsByTagName("id1");
            output[0] = name.item(0).getTextContent();
            name = element.getElementsByTagName("id2");
            output[1] = name.item(0).getTextContent();
            name = element.getElementsByTagName("local");
            output[2] = name.item(0).getTextContent();
            NodeList typeFile = element.getElementsByTagName("typeFile");
            if (typeFile == null) {
                output[3] = "json";
            } else {
                output[3] = name.item(0).getTextContent();
            }

            return output;
        } catch (Exception ex) {
            return new String[0];
        }
    }

    public static List<Request> readSubmitRequestsXML(String infoXML) throws ParserConfigurationException, SAXException, Exception {
        String vec[];
        vec = infoXML.split("<nsl:Request>");
        List<InsuranceCoverageConnection> listI = new ArrayList<>();
        List<Request> listR = new ArrayList<>();
        for (int i = 1; i < vec.length; i++) {
            vec[i] = "<nsl:Request>" + vec[i];
            if (vec[i].contains("</nsl:Requests>")) {
                vec[i] = vec[i].replaceAll("</nsl:Requests>", "");
            }
            try {
                //infoXML = infoXML.substring(3, infoXML.length());
                DocumentBuilderFactory dbf
                        = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(vec[i]));

                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("nsl:Request");

                //only has one element, so index = 0
                Element element = (Element) nodes.item(0);

                NodeList name = element.getElementsByTagName("nsl:state");
                String state = name.item(0).getTextContent();
                State stateObj = null;
                switch (state) {
                    case "AUTOMATIC":
                        stateObj = State.AUTOMATIC;
                        break;
                    case "PENDING":
                        stateObj = State.PENDING;
                        break;
                    case "VALIDATED":
                        stateObj = State.VALIDATED;
                        break;
                }
                name = element.getElementsByTagName("nsl:type");
                String type = name.item(0).getTextContent();
                name = element.getElementsByTagName("nsl:version");
                String version = name.item(0).getTextContent();

                NodeList pairs = element.getElementsByTagName("nsl:pairs");

                for (int c = 0; c < pairs.getLength(); c++) {
                    Element iccObj = (Element) pairs.item(c);

                    String address_street = iccObj.getElementsByTagName("nsl:address-street").item(0).getTextContent();
                    String address_city = iccObj.getElementsByTagName("nsl:address-city").item(0).getTextContent();
                    String address_postalCode = iccObj.getElementsByTagName("nsl:address-postalCode").item(0).getTextContent();
                    String address_district = "";
                    String address_country = "";
                    if (iccObj.getElementsByTagName("nsl:address-district").item(0) != null) {
                        address_district = iccObj.getElementsByTagName("nsl:address-district").item(0).getTextContent();
                    }
                    if (iccObj.getElementsByTagName("nsl:address-country").item(0) != null) {
                        address_country = iccObj.getElementsByTagName("nsl:address-country").item(0).getTextContent();
                    }
                    String street[] = address_street.split(",");
                    String covDesignation = iccObj.getElementsByTagName("nsl:coverage-designation").item(0).getTextContent();

                    Address ad = null;
                    if (address_country.isEmpty()) {
                        ad = address_district.isEmpty() ? new Address(new Street(street[0], street[1]), new City(address_city), new PostalCode(address_postalCode)) : new Address(new Street(street[0], street[1]), new City(address_city), new PostalCode(address_postalCode), new District(address_district));
                    } else {
                        ad = address_district.isEmpty() ? new Address(new Street(street[0], street[1]), new City(address_city), new PostalCode(address_postalCode), new Country(address_country)) : new Address(new Street(street[0], street[1]), new City(address_city), new PostalCode(address_postalCode), new Country(address_country), new District(address_district));
                    }

                    InsuranceCoverageConnection icc = new InsuranceCoverageConnection(new Insurance(ad), new Coverage(covDesignation));
                    listI.add(icc);

                    Request r = new Request(listI, type, stateObj, new Version(Integer.parseInt(version)));
                    listR.add(r);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            /*
             String vec2[];
             vec2 = vec[i].split("<nsl:insuranceCoverageConnection>");
             for (int j = 1; j < vec2.length; j++) {
             vec2[j] = "<nsl:insuranceCoverageConnection>" + vec2[j];
             try {
             //infoXML = infoXML.substring(3, infoXML.length());
             DocumentBuilderFactory dbf2
             = DocumentBuilderFactory.newInstance();
             DocumentBuilder db2 = dbf2.newDocumentBuilder();
             InputSource is2 = new InputSource();
             is.setCharacterStream(new StringReader(vec2[j]));

             Document doc2 = db2.parse(is2);
             NodeList nodes2 = doc2.getElementsByTagName("nsl:insuranceCoverageConnection");
             Element aux = (Element) nodes2.item(0);
             NodeList pairs = aux.getElementsByTagName("nsl:address-street");
             String address_street = pairs.item(0).getTextContent();
             pairs = aux.getElementsByTagName("nsl:address-city");
             String address_city = pairs.item(0).getTextContent();
             String street[] = address_street.split(",");
             pairs = aux.getElementsByTagName("nsl:address-postalCode");
             String address_postalCode = pairs.item(0).getTextContent();
             pairs = aux.getElementsByTagName("nsl:address-country");
             String address_country = pairs.item(0).getTextContent();
             pairs = aux.getElementsByTagName("nsl:address-district");
             String address_district = pairs.item(0).getTextContent();
             pairs = aux.getElementsByTagName("nsl:coverage-designation");
             String coverage_designation = pairs.item(0).getTextContent();
             InsuranceCoverageConnection icc = new InsuranceCoverageConnection(new Insurance(new Address(new Street(street[0],street[1]), new City(address_city), new PostalCode(address_postalCode), new Country (address_country),new District (address_district))),new Coverage(coverage_designation));
             listI.add(icc);
             } catch (Exception e) {
             return null;
             }
             Request r = new Request(listI,type, stateObj, new Version(Integer.parseInt(version)));
             listR.add(r);
             }
             } catch (Exception ex) {
             return null;
             }
             }
             */
        }
        return listR;
    }

    /**
     * With the help of the RiskAssessmentComparasionDTO converts the needed
     * information in a xml file
     *
     * Luís Moreira
     *
     * @param racdto
     *
     * @return
     * @throws IOException
     */
    public static String writeXMLRiskAssessmentComparasion(RiskAssessmentComparasionDTO racdto) throws IOException {

        Document dom;
        Element e, eAux, eAux2;

        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            // use factory to get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            // create the root element
            Element rootEle = (Element) dom.createElement("results");
            Element sameResultsEle = (Element) dom.createElement("same-results");

            List<String> sameResult = racdto.getListSameResult();
            for (int i = 0; i < sameResult.size(); i++) {
                // create data elements and place them under root
                e = dom.createElement("result");
                e.appendChild(dom.createTextNode(sameResult.get(i)));
                sameResultsEle.appendChild(e);
            }
            rootEle.appendChild(sameResultsEle);

            Element differentResultsEle = (Element) dom.createElement("differente-results");
            Map<String, String> differentResult = racdto.getListDifferentResult();
            List<String> valuesDifferent = new ArrayList<>(racdto.getListDifferentResult().values());
            int c = 0;
            for (String difResult : differentResult.keySet()) {
                // create data elements and place them under root
                e = dom.createElement("result");
                e.appendChild(dom.createTextNode(difResult));
                eAux = dom.createElement("rc1");
                eAux.appendChild(dom.createTextNode(valuesDifferent.get(c).split(",")[0]));
                eAux2 = dom.createElement("rc2");
                eAux2.appendChild(dom.createTextNode(valuesDifferent.get(c).split(",")[1]));
                e.appendChild(eAux);
                e.appendChild(eAux2);
                differentResultsEle.appendChild(e);
                c++;
            }
            rootEle.appendChild(differentResultsEle);

            dom.appendChild(rootEle);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            // send DOM to file
            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(dom), new StreamResult(writer));

            return writer.getBuffer().toString();
        } catch (Exception exce) {
            return "";
        }
    }

    /**
     * Receives a list of Near By Surroundings and save the information in a
     * XHTML file
     *
     * Luís Moreira
     *
     * @param lnbs - A list of Near By Surroudings
     *
     * @return the information of the list a xml file
     * @throws IOException
     * @author Luis Moreira
     */
    public static String writeXMLNearBySurrouding(List<NearBySurrounding> lnbs) throws IOException {

        Document dom;
        Element e, eAux, eAux2;

        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            // use factory to get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            // create the root element
            Element rootEle = (Element) dom.createElement("ListNearBySurrounding");
            for (int i = 0; i < lnbs.size(); i++) {
                // create data elements and place them under root
                e = dom.createElement("NearBySurrounding");
                Element aux = (Element) dom.createElement("Description");
                aux.appendChild(dom.createTextNode(lnbs.get(i).getDescription()));
                e.appendChild(aux);
                aux = (Element) dom.createElement("Address");
                aux.appendChild(dom.createTextNode(lnbs.get(i).getAdd().toString()));
                e.appendChild(aux);
                aux = (Element) dom.createElement("SurroundingType");
                aux.appendChild(dom.createTextNode(lnbs.get(i).getType().obtainDescription()));
                e.appendChild(aux);
                rootEle.appendChild(e);
            }
            dom.appendChild(rootEle);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            // send DOM to file
            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(dom), new StreamResult(writer));

            return writer.getBuffer().toString();
        } catch (Exception exce) {
            return "";
        }
    }

    /**
     * Returns a XML String that has the main information of the current
     * requests, along with the current and max load and the availability
     *
     * @param requestList the current requests
     * @param currentLoad the current number of requests
     * @param maxLoad the maximum limit of concurrent requests
     * @return Returns a XML String that has the main information
     *
     * @author Tiago Ribeiro (1170426)
     */
    public static String writeCurrentRequestAvailabilityXML(List<Request> requestList, long currentLoad, int maxLoad) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("Requests");

            Element currentLoadE = doc.createElement("CurrentLoad");
            currentLoadE.appendChild(doc.createTextNode(String.valueOf(currentLoad)));
            rootElement.appendChild(currentLoadE);

            Element maxLoadE = doc.createElement("MaxLoad");
            maxLoadE.appendChild(doc.createTextNode(String.valueOf(maxLoad)));
            rootElement.appendChild(maxLoadE);

            boolean availability = currentLoad < maxLoad;

            Element availabilityE = doc.createElement("Availability");
            availabilityE.appendChild(doc.createTextNode(String.valueOf(availability)));
            rootElement.appendChild(availabilityE);

            for (Request request : requestList) {
                Element requestElement = doc.createElement("Request");

                Element riskAnalyst = doc.createElement("RiskAnalyst");
                if (request.obtainRiskAnalyst() != null) {
                    riskAnalyst.appendChild(doc.createTextNode(request.obtainRiskAnalyst().email().Email()));
                }
                requestElement.appendChild(riskAnalyst);

                Element state = doc.createElement("State");
                state.appendChild(doc.createTextNode(request.obtainState().name()));
                requestElement.appendChild(state);

                requestElement.setAttribute("id", String.valueOf(request.obtainID()));

                rootElement.appendChild(requestElement);
            }

            doc.appendChild(rootElement);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(doc), new StreamResult(writer));

            return writer.getBuffer().toString();
        } catch (Exception ex) {
            Logger.getLogger(ServiceXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * This method must give back a String after transforming it to xml
     *
     * @param numConsultations
     * @param numRequests
     * @param initTime
     * @return
     * @throws ParserConfigurationException
     * @throws TransformerException This methos was created by Ana Fonseca
     * (1170656)
     */
    public static String writeXMLStatisticReport(Integer numConsultations, Integer numRequests, LocalDateTime initTime) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Element root = doc.createElement("report");
        doc.appendChild(root);

        Element numbConsultations = doc.createElement("numberOfConsultations");
        numbConsultations.setTextContent(numConsultations.toString());
        Element numbRequests = doc.createElement("numberOfRequests");
        numbRequests.setTextContent(numRequests.toString());
        Element time = doc.createElement("time");
        Element execTime = doc.createElement("executionTime");
        execTime.setTextContent(initTime.getHour() + "h:" + initTime.getMinute() + "m:" + initTime.getSecond() + "s");
        Element timePassed = doc.createElement("timePassedSinceExecuting");

        LocalDateTime end = LocalDateTime.now();

        Long betweenSeconds = ChronoUnit.SECONDS.between(initTime, end);

        Long hours = betweenSeconds / 3600;
        Long minutes = (betweenSeconds % 3600) / 60;
        Long seconds = betweenSeconds % 60;

        String resultTimeOfExec = String.format("%02d:%02d:%02d", hours, minutes, seconds);

        timePassed.setTextContent(resultTimeOfExec);
        time.appendChild(execTime);
        time.appendChild(timePassed);

        root.appendChild(numbConsultations);
        root.appendChild(numbRequests);
        root.appendChild(time);

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("StatisticReport.xml"));

        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

        StringWriter writer = new StringWriter();
        tr.transform(new DOMSource(doc), new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    public static String writeRequestsSubmissionMessageXML(int numMax, int counter) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Element rootElement = doc.createElement("message");
        rootElement.appendChild(doc.createTextNode(counter + "/" + numMax + " Requests submitted"));

        doc.appendChild(rootElement);

        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

        StringWriter writer = new StringWriter();
        tr.transform(new DOMSource(doc), new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    /**
     * Returns a XML String that has the main information of the validated
     * requests and the summary
     *
     * @param requestList the validated requests
     * @param sum the summary with the quantity and the average time period
     * @return Returns a XML String that has the main information
     *
     * @author Maria Junqueira (1170842)
     */
    static String ValidatedRequestsXML(List<Request> requestList, String sum) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("Requests");

            Element summaryE = doc.createElement("Summary");
            summaryE.appendChild(doc.createTextNode(String.valueOf(sum)));
            rootElement.appendChild(summaryE);

            for (Request request : requestList) {
                Element requestElement = doc.createElement("Request");

                requestElement.setAttribute("id", String.valueOf(request.obtainID()));

                Element riskAnalyst = doc.createElement("RiskAnalyst");
                if (request.obtainRiskAnalyst() != null) {
                    riskAnalyst.appendChild(doc.createTextNode(request.obtainRiskAnalyst().email().Email()));
                }
                requestElement.appendChild(riskAnalyst);

                Element matrixVersionE = doc.createElement("MatrixVersion");
                matrixVersionE.appendChild(doc.createTextNode(String.valueOf(request.getMatrixVersion())));
                requestElement.appendChild(matrixVersionE);

                Element requestDateE = doc.createElement("RequestDate");
                requestDateE.appendChild(doc.createTextNode(String.valueOf(request.obtainRequestedDate())));
                requestElement.appendChild(requestDateE);

                Element assignedDateE = doc.createElement("AssignedDate");
                assignedDateE.appendChild(doc.createTextNode(String.valueOf(request.obtainAssignedDate())));
                requestElement.appendChild(assignedDateE);

//                requestElement.setAttribute("time since assigned", String.valueOf(request.);
                Map<String, List<Coverage>> map = new LinkedHashMap<>();
                for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                    if (!map.containsKey(icc.obtainInsurance().getAddress().district().district())) {
                        map.put(icc.obtainInsurance().getAddress().district().district(), new ArrayList<Coverage>());
                    }
                }

                for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                    map.get(icc.obtainInsurance().getAddress().district().district()).add(icc.obtainCoverage());
                }
                
                for (String str : map.keySet()) {
                    Element insurances = doc.createElement("Insurances");
                    Element district = doc.createElement("AddressDistrict");
                    district.appendChild(doc.createTextNode(str));
                    
                    Element covs = doc.createElement("Coverages");
                    covs.setAttribute("nCoverages", String.valueOf(map.get(str).size()));
                    
                    for (Coverage cov : map.get(str)) {
                        Element covE = doc.createElement("Coverage");
                        covE.appendChild(doc.createTextNode(cov.obtainDesignation()));
                        
                        covs.appendChild(covE);
                    }
                    
                    insurances.appendChild(district);
                    insurances.appendChild(covs);
                    
                    requestElement.appendChild(insurances);
                }
                
                rootElement.appendChild(requestElement);
            }

            doc.appendChild(rootElement);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(doc), new StreamResult(writer));

            return writer.getBuffer().toString();
        } catch (Exception ex) {
            Logger.getLogger(ServiceXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}
