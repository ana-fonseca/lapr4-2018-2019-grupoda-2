package isep.eapli.riskassessment.controller;

import isep.eapli.riskassessment.utils.ServiceJSON;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import isep.eapli.riskassessment.utils.ServiceXML;
import java.io.IOException;
import java.time.LocalDateTime;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class StatisticReportController {

    public String reportJSON(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) {
       return ServiceJSON.writeJSONStatisticsReport(numConsultations, numSubmittedRequests, from);
    }

    public String reportXML(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) throws ParserConfigurationException, TransformerException {
        return ServiceXML.writeXMLStatisticReport(numConsultations, numSubmittedRequests, from);
    }

    public String reportXHTML(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) throws ParserConfigurationException, TransformerException, SAXException, IOException {
        return ServiceXHTML.writeXHTMLStatisticReport(numConsultations, numSubmittedRequests, from);
    }

}
