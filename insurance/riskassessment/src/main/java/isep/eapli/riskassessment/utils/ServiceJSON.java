package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.*;
import isep.eapli.georef.controller.AdressSearchController;
import isep.eapli.riskassessment.dto.RiskAssessmentComparasionDTO;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author 1171138
 */
public class ServiceJSON {

    /**
     * Method that gets an existing RA case and exports it to a JSON file. The
     * file while contain the case information and the result for each isurance
     * coverage pair
     *
     * @param c case already on DB
     *
     * @return
     * @throws IOException
     */
    public static String writeJSON(Request c) throws IOException {

        //case json object
        JSONObject caseInfo = new JSONObject();

        caseInfo.put("type", c.obtainType());
        caseInfo.put("id", c.obtainID());
        caseInfo.put("state", c.obtainState().name());
        caseInfo.put("version", c.getMatrixVersion().getVersionName());

        JSONArray pairList = new JSONArray();

        int i = 0;
        for (InsuranceCoverageConnection icc : c.getInsuranceCoverageConnectionList()) {
            JSONObject pairObj = new JSONObject();
            JSONObject pairInfo = new JSONObject();
            if (icc.obtainCoverage() != null) {
                pairInfo.put("coverage-designation", icc.obtainCoverage().obtainDesignation());
            }
            pairInfo.put("address-street", icc.obtainInsurance().getAddress().street().toString());
            pairInfo.put("address-city", icc.obtainInsurance().getAddress().getCity().toString());
            pairInfo.put("address-postalCode", icc.obtainInsurance().getAddress().postalCode().toString());
            if (icc.obtainInsurance().getAddress().district() != null) {
                pairInfo.put("address-district", icc.obtainInsurance().getAddress().district().toString());
            }
            if (icc.obtainInsurance().getAddress().country() != null) {
                pairInfo.put("address-country", icc.obtainInsurance().getAddress().country().toString());
            }
            pairObj.put("insuranceCoverageConncetion" + Integer.toString(i + 1), pairInfo);
            pairList.add(i, pairObj);
            i++;
        }
        caseInfo.put("pair", pairList);

        JSONArray resultList = new JSONArray();
        JSONArray expList = new JSONArray();
        int j = 0;
        for (Result r : c.getResultList()) {
            JSONObject resultInfo = new JSONObject();
            JSONObject resultObj = new JSONObject();
            for (j = 0; j < pairList.size(); j++) {
                if (r.obtainIcc().equals(c.getInsuranceCoverageConnectionList().get(j))) {
                    resultInfo.put("isuranceCoverageConnectionNumber", Integer.toString(j + 1));
                    resultInfo.put("index", Integer.toString(r.obtainIndex()));
                    for (Explanation e : r.obtainExplanationList()) {
                        JSONObject expInfo = new JSONObject();
                        JSONObject expObj = new JSONObject();
                        expInfo.put("detail", e.toString());
                        expObj.put("explanation", expInfo);
                        expList.add(expObj);
                    }
                    resultInfo.put("explanationList", expList);
                    resultObj.put("result", resultInfo);
                    resultList.add(resultObj);
                }
            }
        }
        caseInfo.put("resultList", resultList);

        try (FileWriter file = new FileWriter("case" + c.obtainID() + ".json")) {
            file.write(caseInfo.toJSONString());
            file.flush();

        }
        return caseInfo.toJSONString();
    }
    
    public static List<Request> readRequestList(String jsonFile) throws Exception {
        List<Request> requestList = new ArrayList<>();
        JSONParser parser = new JSONParser();
        JSONObject requests = (JSONObject) parser.parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(jsonFile.getBytes()))));
        JSONArray requestsArray = (JSONArray) requests.get("requests");
        for (int i = 0; i < requestsArray.size(); i++) {
            JSONObject request = (JSONObject) requestsArray.get(i);
            requestList.add(parseCaseObject(request));
        }
        return requestList;
    }

    /**
     * Method that creats a RA case from JSON file The file contains the
     * information necessary to creat a case on the DB and then get the result
     *
     * @param givenPath path where the JSON file is
     *
     * @return
     */
    public static Request readJSON(String givenPath) {

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(givenPath)) {

            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONObject caseObject = (JSONObject) obj;

            return parseCaseObject((JSONObject) caseObject);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    public static Request parseCaseObject(JSONObject caseJson) {

        //Get case type
        String type = (String) caseJson.get("type");

        //Get case id
        String id = (String) caseJson.get("id");

        //Get case state
        String state = (String) caseJson.get("state");
        State s = State.valueOf(state);

        //Get case version
        String version = (String) caseJson.get("version");
        if (version.contains("/")) {
            version = version.trim().split("_")[1];
        }
        Version v = new Version(Integer.parseInt(version));

        //Get case pair list, insurance-coverage
        JSONArray pairList = (JSONArray) caseJson.get("pair");
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        int i = 0;
        for (Object pairObj : pairList) {
            JSONObject obj = (JSONObject) pairObj;
            JSONObject objIcc = (JSONObject) obj.get("insuranceCoverageConncetion" + (i + 1));
            Coverage cov = new Coverage(String.valueOf(objIcc.get("coverage-designation")));
            String street = String.valueOf(objIcc.get("address-street"));
            City city = new City(String.valueOf(objIcc.get("address-city")));
            PostalCode postalCodeObj = null;
            try {
                postalCodeObj = new PostalCode(String.valueOf(objIcc.get("address-postalCode")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            District district = null;
            if (objIcc.get("address-district") != null) {
                district = new District(String.valueOf(objIcc.get("address-district")));
            }
            Country country = null;
            if (objIcc.get("address-country") != null) {
                country = new Country(String.valueOf(objIcc.get("address-country")));
            }
            Street streetObj;
            if (street.split(",").length > 1) {
                streetObj = new Street(street.split(",")[0], street.split(",")[1]);
            } else {
                streetObj = new Street(street.split(",")[0]);
            }
            Address ad = null;
            if (district == null || country == null) {
                List<String> completeAddress = new AdressSearchController().completePostalAddress(city.city(), postalCodeObj.toString(), street);
                streetObj = new Street(completeAddress.get(0));
                try {
                    postalCodeObj = new PostalCode(completeAddress.get(1));
                } catch (Exception ex) {
                    Logger.getLogger(ServiceJSON.class.getName()).log(Level.SEVERE, null, ex);
                }
                city = new City(completeAddress.get(2));
                district = new District(completeAddress.get(3));
                country = new Country(completeAddress.get(4));

            }
            ad = new Address(streetObj, city, postalCodeObj, country, district);
            Insurance ins = new Insurance(ad);
            InsuranceCoverageConnection icc = new InsuranceCoverageConnection(ins, cov);

            iccList.add(icc);
            i++;
        }

        //Get case validation
        if (caseJson.get("validation") != null) {
            String validation = (String) caseJson.get("validation");
            if (validation.contains("Risk Analyst")) {
                s = State.PENDING;
            }
        }

        return new Request(iccList, type, s, v);
    }

    /**
     * Method that gets an existing RA case and exports it to a JSON file. The
     * file while contain the case information and the result for each isurance
     * coverage pair
     *
     * Luís Moreira
     *
     * @param racdto
     *
     * @return
     * @throws IOException
     */
    public static String writeJSONRiskAssessmentComparasion(RiskAssessmentComparasionDTO racdto) throws IOException {

        JSONArray pairList = new JSONArray();

        JSONObject pairInfo = new JSONObject();
        JSONArray pairInfoList = new JSONArray();
        int nEle = 0;
        for (String str : racdto.getListSameResult()) {
            JSONObject sameResult = new JSONObject();
            sameResult.put("result", str);
            pairInfoList.add(nEle, sameResult);
            nEle++;
        }
        pairInfo.put("same-results", pairInfoList);

        nEle = 0;
        JSONObject differentInfo = new JSONObject();
        JSONObject differentInfoAux = new JSONObject();
        JSONObject differentInfoAux2 = new JSONObject();
        JSONArray differentResultsArray = new JSONArray();

        for (String resultInfo : racdto.getListDifferentResult().keySet()) {
            String[] rcs = racdto.getListDifferentResult().get(resultInfo).split(",");

            differentInfoAux.put("rc1", rcs[0]);
            differentInfoAux.put("rc2", rcs[1]);
            differentInfo.put("info-result", resultInfo);
            differentInfo.put("rcs", differentInfoAux);

            differentInfoAux2.put("result", differentInfo);
            differentResultsArray.add(nEle, differentInfoAux2);
            nEle++;
        }
        pairInfo.put("different-result", differentResultsArray);

        return pairInfo.toJSONString();
    }

    public static String writeErrorMessage(String error) {
        //Create JSON Object
        JSONObject errorMessage = new JSONObject();

        //Signal as failed
        errorMessage.put("success", false);

        //Set error message
        errorMessage.put("error", error);

        //Convert JSON Object to String
        return errorMessage.toJSONString();
    }

    public static String writeRiskAssessmentRequestResult(Result result) {
        //Create JSON Object
        JSONObject resultMessage = new JSONObject();

        //Signal as failed
        resultMessage.put("success", true);

        //Set error message
        resultMessage.put("data", result);

        //Convert JSON Object to String
        return resultMessage.toJSONString();
    }
    
    public static String writeSubmitRequestsJSON(int numMax, int counter){
        //Create JSON Object
        JSONObject resultMessage = new JSONObject();
        
        resultMessage.put("message", counter + "/" + numMax + " Requests submitted");
        
        return resultMessage.toJSONString();
    }

    /**
     * Returns a JSON String that has the main information of the current
     * requests, along with the current and max load and the availability
     *
     * @param requestList the current requests
     * @param currentLoad the current number of requests
     * @param maxLoad the maximum limit of concurrent requests
     * @return Returns a JSON String that has the main information
     *
     * @author Tiago Ribeiro (1170426)
     */
    public static String writeCurrentRequestAvailabilityJSON(List<Request> requestList, long currentLoad, int maxLoad) {
        try {
            JSONObject requestsInfo = new JSONObject();

            requestsInfo.put("currentLoad", currentLoad);
            requestsInfo.put("maxLoad", maxLoad);
            requestsInfo.put("availability", currentLoad < maxLoad);

            JSONArray requests = new JSONArray();

            for (Request request : requestList) {
                JSONObject requestInfo = new JSONObject();

                requestInfo.put("id", request.obtainID());
                if (request.obtainRiskAnalyst() != null) {
                    requestInfo.put("riskAnalyst", request.obtainRiskAnalyst().email().Email());
                }
                requestInfo.put("state", request.obtainState().name());

                requests.add(requestInfo);
            }

            requestsInfo.put("requests", requests);

            return requestsInfo.toString();
        } catch (Exception ex) {
            Logger.getLogger(ServiceXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * Method that returns JSON String
     *
     * @param numConsultations
     * @param numSubmittedRequests
     * @param from
     * @return JSON input 
     * This method was created by Ana Fonseca (1170656)
     */
    public static String writeJSONStatisticsReport(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) {

        JSONObject obj = new JSONObject();
        Map<String, String> map = new LinkedHashMap<>();

        LocalDateTime end = LocalDateTime.now();

        Long betweenSeconds = ChronoUnit.SECONDS.between(from, end);

        Long hours = betweenSeconds / 3600;
        Long minutes = (betweenSeconds % 3600) / 60;
        Long seconds = betweenSeconds % 60;

        String resultTimeOfExec = String.format("%02d:%02d:%02d", hours, minutes, seconds);

        map.put("Inicio de Execucao: ", from.getHour() + "h:" + from.getMinute() + "m:" + from.getSecond() + "s");
        map.put("Tempo de Execucao: ", resultTimeOfExec);
        map.put("Numero de Consultas: ", String.valueOf(numConsultations));
        map.put("Numero de Pedidos: ", String.valueOf(numSubmittedRequests));

        obj.putAll(map);

        return obj.toString();
    }
}
