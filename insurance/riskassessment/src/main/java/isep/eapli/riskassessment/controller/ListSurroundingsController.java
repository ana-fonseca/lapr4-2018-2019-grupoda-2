package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.SurroundingDB;
import isep.eapli.riskassessment.utils.ListSurroundings;
import java.util.ArrayList;
import java.util.List;

public class ListSurroundingsController {

    private RepositoryFactory repositoryAccess;
    private SurroundingDB surroundingDB;

    private List<NearBySurrounding> surroudings = new ArrayList<>();
    private ListSurroundings ls;

    /**
     * Contructor that will get all Surroudings from the DB and Create a
     * ListSurroundings with that list
     *
     * @author - Luís Moreira
     */
    public ListSurroundingsController() {
        this.repositoryAccess = new RepositoryFactory();
        surroundingDB = repositoryAccess.getSurroundingDB();
        surroudings = surroundingDB.getAll();
        ls = new ListSurroundings(surroudings);
    }

    /**
     * Get the current List of Surroudings
     *
     * @return list of surroudings
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> getList() {
        return ls.getList();
    }

    /**
     * Receives a district and filter the list of Surroundings with the district
     *
     * @param district - filter of the list
     * @return list of near by surroundings of that district
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> filterByDistrict(String district) {
        return ls.filterByDistrict(district);
    }

    /**
     * Filter the List of Surroundings with the the default rectangular area
     *
     * @return list of near by surroundings inside the area
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> filterByDefaultArea() {
        return ls.filterByDefaultArea();
    }

    /**
     * Saves the List of Surroudings in XHTML
     *
     * @author - Luís Moreira
     */
    public void saveListSurroundings() {
        ls.saveListSurroundings();
    }

}
