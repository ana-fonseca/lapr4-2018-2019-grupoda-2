

package isep.eapli.riskassessment.controller;

import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import java.util.List;
import java.util.Optional;


public class GetRequestResultController {
    
    private RepositoryFactory rp;
    private RequestDB rdb;

    public GetRequestResultController(RepositoryFactory rp, RequestDB rdb) {
        this.rp = rp;
        this.rdb = rdb;
    }

    /**
     * method that get the request from the DB and return da result list for that request
     * @param id
     * @return Result List (for that request)
     */
    public List<Result> getRequestResult (long id){
        return rdb.obtainRequestByID(id).getResultList();
    }
}
