package isep.eapli.riskassessment.controller;

import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.utils.ServiceJSON;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import isep.eapli.riskassessment.utils.ServiceXML;
import java.io.FileReader;
import java.util.Properties;

/**
 *
 * @author TiagoRibeiro(1170426
 */
public class CurrentAvailabilityController {

    private RepositoryFactory rp;
    private RequestDB rdb;

    public CurrentAvailabilityController() {
        this.rp = new RepositoryFactory();
        this.rdb = rp.getRequestDB();
    }
    
    public String currentAvailabilityJSON() {
        return ServiceJSON.writeCurrentRequestAvailabilityJSON(rdb.allCurrentRequests(), rdb.countCurrentRequests(), getRequestLimit());
    }
    
    public String currentAvailabilityXML() {
        return ServiceXML.writeCurrentRequestAvailabilityXML(rdb.allCurrentRequests(), rdb.countCurrentRequests(), getRequestLimit());
    }
    
    public String currentAvailabilityXHTML() {
        return ServiceXHTML.getCurrentAvailabilityXHTML(rdb.allCurrentRequests(), rdb.countCurrentRequests(), getRequestLimit());
    }

    private int getRequestLimit() {
        try {
            FileReader in = new FileReader("../requestLimit.properties");
            Properties requestLimitProperties = new Properties();
            requestLimitProperties.load(in);
            int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));

            return requestLimit;
        } catch (Exception e) {
            return 0;
        }
    }

}
