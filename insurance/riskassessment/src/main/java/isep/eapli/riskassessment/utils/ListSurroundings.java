package isep.eapli.riskassessment.utils;

import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.georef.controller.AdressSearchController;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

/**
 * @author Luis Moreira
 */
public class ListSurroundings {

    private List<NearBySurrounding> surroundings = new ArrayList<>();

    /**
     * Contructor that receives a list with surroudings & order that list by
     * surrouding type
     *
     * @param surroudings - list of surroudings
     * @author - Luís Moreira
     */
    public ListSurroundings(List<NearBySurrounding> surroudings) {
        this.surroundings = surroudings;
        orderList();
    }

    /**
     * Get the current List of Surroudings
     *
     * @return list of surroudings
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> getList() {
        return surroundings;
    }

    /**
     * Method that receives a district and returns all surroudings that belong
     * to that district
     *
     * @param district - name of district
     * @return list of surroundings that belong to that district
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> filterByDistrict(String district) {
        if (district != null) {
            List<NearBySurrounding> specificSurr = new ArrayList<>();
            for (NearBySurrounding sur : surroundings) {
                if (sur.getAdd().district() == null) {
                    AdressSearchController asc = new AdressSearchController();
                    List<String> listInfo = asc.completePostalAddress(sur.getAdd().getCity().city(), sur.getAdd().postalCode().postalCode(), sur.getAdd().street().toString());
                    if (listInfo != null && listInfo.get(3).equalsIgnoreCase(district)) { //get the disctrict from the list
                        specificSurr.add(sur);
                    }
                } else if (sur.getAdd().district().district().equalsIgnoreCase(district)) {
                    specificSurr.add(sur);
                }
            }
            surroundings = specificSurr;
            return surroundings;
        }
        return new ArrayList<>();
    }

    /**
     * Return the list of surroudings that belong to the between to points saved
     * in the system
     *
     * @return list of surroudings that belong to the between to points saved in
     * the system
     * @author - Luís Moreira
     */
    public List<NearBySurrounding> filterByDefaultArea() {
        try {
            FileReader in;

            in = new FileReader("..\\coordinates.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            double lat1 = Double.parseDouble(apiMBS.getProperty("lat1").trim());
            double lon1 = Double.parseDouble(apiMBS.getProperty("lon1").trim());
            double lat2 = Double.parseDouble(apiMBS.getProperty("lat2").trim());
            double lon2 = Double.parseDouble(apiMBS.getProperty("lon2").trim());
            if (lat1 > lat2) { //if the lat1 is higher that lat2 it changes to the inverse
                double aux = lat1;
                lat1 = lat2;
                lat2 = aux;
            }
            if (lon1 > lon2) {//if the lon1 is higher that lon2 it changes to the inverse
                double aux = lon1;
                lon1 = lon2;
                lon2 = aux;
            }
            List<NearBySurrounding> specificSurr = new ArrayList<>();
            for (NearBySurrounding sur : surroundings) {
                if (sur.getLocalization() == null) {
                    AdressSearchController asc = new AdressSearchController();
                    String info = asc.addressSearch(sur.getAdd().getCity().city(), sur.getAdd().postalCode().postalCode(), sur.getAdd().street().toString());
                    if (info != null) {
                        double latAux = Double.parseDouble(info.split(",")[0]);
                        double lonAux = Double.parseDouble(info.split(",")[1]);
                        if (latAux >= lat1 && latAux <= lat2 && lonAux >= lon1 && lonAux <= lon2) {
                            specificSurr.add(sur);
                        }
                    }
                } else {

                    double latAux = Double.parseDouble(sur.getLocalization().split(",")[0]);
                    double lonAux = Double.parseDouble(sur.getLocalization().split(",")[1]);
                    if (latAux >= lat1 && latAux <= lat2 && lonAux >= lon1 && lonAux <= lon2) {
                        specificSurr.add(sur);
                    }
                }
            }
            surroundings = specificSurr;
            return surroundings;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * Saves the current List of Surroundings in a xhtml file
     *
     * @author - Luís Moreira
     */
    public void saveListSurroundings() {
        ServiceXHTML.writeListSurroundings(surroundings);
    }

    /**
     * Order the list of surroudings by the surrouding type (e.g. Bombeiros,
     * Hospitais)
     *
     * @author - Luís Moreira
     */
    private void orderList() {
        Collections.sort(surroundings, new Comparator() {
            @Override
            public int compare(Object surroundingOne, Object surroundingTwo) {
                //use instanceof to verify the references are indeed of the type in question
                return ((NearBySurrounding) surroundingOne).getType().obtainDescription()
                        .compareTo(((NearBySurrounding) surroundingTwo).getType().obtainDescription());
            }
        });
    }

}
