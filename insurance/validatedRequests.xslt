<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/Requests">
        <html>
			<body>
				<xsl:apply-templates select="Summary"/><br/>
				<table id="requests">
					<thead>
						<tr>
							<th>ID</th>
							<th>Risk Analyst</th>
              <th>Matrix Version</th>
              <th>Request Date</th>
              <th>Assigned Date</th>
              <th>Address</th>
							<th>Number of Coverages</th>
              <th>Coverages</th>
              <th></th>
						</tr>
					</thead>
					<tbody>
						<xsl:apply-templates select="Request"/>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Summary">
		<b>Summary with quantity and average time period: </b><xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="Request">
		<tr>
			<td><xsl:value-of select="@id"/></td>
      <td><xsl:value-of select="RiskAnalyst"/></td>
			<td><xsl:value-of select="MatrixVersion"/></td>~
      <td><xsl:value-of select="RequestDate"/></td>
      <td><xsl:value-of select="AssignedDate"/></td>
      <xsl:apply-templates select="Insurances"/>
		</tr>
	</xsl:template>

	<xsl:template match="Insurances">
		<td><xsl:value-of select="AddressDistrict"/></td>
		<td><xsl:apply-templates select="Coverages"/></td>
	</xsl:template>

	<xsl:template match="Coverages">
		<td><xsl:value-of select="@nCoverages"/></td>
		<td><xsl:apply-templates select="Coverage"/></td>
	</xsl:template>

	<xsl:template match="Coverage">
		<xsl:value-of select="."/>,
	</xsl:template>

</xsl:stylesheet>
