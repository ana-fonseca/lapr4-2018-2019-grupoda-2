<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="4.0">

<xsl:output method="html"/>

<xsl:template match="/message">
	<html><head><title>Request Submission</title></head><body>
		<h2><b>Message:</b></h2>
		<h3><xsl:value-of select="."/></h3>
	</body></html>
</xsl:template>

</xsl:stylesheet>