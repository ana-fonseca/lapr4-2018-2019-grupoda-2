<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/Requests">
		<html>
			<head>
				<style>
					html * {
						font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
					}
				
					#requests {
						font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
						border-collapse: collapse;
						width: 100%;
					}
					
					#requests td, #requests th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}
					
					#requests th {
						padding-top: 12px;
						padding-bottom: 12px;
						text-align: center;
						background-color: #4CAF50;
						color: white;
					}
				</style>
				<title>Active Requests</title>
			</head>
			<body>
				<xsl:apply-templates select="CurrentLoad"/><br/>
				<xsl:apply-templates select="MaxLoad"/><br/>
				<xsl:variable name="percentage">
					<xsl:value-of select="round(CurrentLoad div(MaxLoad) * 100)"/>
				</xsl:variable>
				<div style="background-color:#E5E5E5;width:700px;height:20px;border-radius:15px;">
					<div align="center">
						<xsl:attribute name="style">
							background-color:
							<xsl:choose>
								<xsl:when test="$percentage &gt; 0 and $percentage &lt; 25">#5BC0DE</xsl:when>
								<xsl:when test="$percentage &gt; 25 and $percentage &lt; 75">#45CB6A</xsl:when>
								<xsl:when test="$percentage &gt; 75 and $percentage &lt; 100">#F7CA00</xsl:when>
								<xsl:when test="$percentage &gt; 99">#D9534F</xsl:when>
							</xsl:choose>
							;height:100%;color:white;font:bold;border-radius:15px;width:
							<xsl:value-of select="($percentage*700) div (100)"/>px
						</xsl:attribute>
						<xsl:value-of select="$percentage"/>%
						<xsl:if test="$percentage &gt; 25"> - <xsl:apply-templates select="Availability"/></xsl:if>
					</div>
				</div>
				<xsl:if test="$percentage &lt; 25"><xsl:apply-templates select="Availability"/></xsl:if>
				<br/><br/>
				<table id="requests">
					<thead>
						<tr>
							<th>ID</th>
							<th>State</th>
							<th>Risk Analyst</th>
						</tr>
					</thead>
					<tbody>
						<xsl:apply-templates select="Request"/>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="CurrentLoad">
		<b>Number of active simultaneous requests: </b><xsl:value-of select="."/>
	</xsl:template>
	
	<xsl:template match="MaxLoad">
		<b>Maximum number of simultaneous requests: </b><xsl:value-of select="."/>
	</xsl:template>
	
	<xsl:template match="Availability">
		<xsl:variable name="availability">
			<xsl:value-of select="."/>
		</xsl:variable>
		<xsl:if test="$availability = 'true'">
			Available!
		</xsl:if>
		<xsl:if test="$availability = 'false'">
			Not available!
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="Request[./RiskAnalyst]">
		<tr>
			<td><xsl:value-of select="@id"/></td>
			<td><xsl:value-of select="State"/></td>
			<td><xsl:value-of select="RiskAnalyst"/></td>
		</tr>
	</xsl:template>
	
	<xsl:template match="Request">
		<tr>
			<td><xsl:value-of select="@id"/></td>
			<td><xsl:value-of select="State"/></td>
		</tr>
	</xsl:template>
	
</xsl:stylesheet>
