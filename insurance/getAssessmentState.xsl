<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
<xsl:template match="/">
  <html>
	<body>
		<h1>Request Information</h1>
    	
		<h2>Request ID: <xsl:value-of select="request/id"/></h2>
		<h3>State:<xsl:value-of select="request/state"/></h3>
		<h4>requestDate:<xsl:value-of select="request/requestDate"/></h4>
		<h5>assignedDate:<xsl:value-of select="request/assignedDate"/></h5>		
		<xsl:for-each select="request/result">
			<result>	
				<h6>Country: <xsl:value-of select="./country"/></h6>
					
				<h7>District: <xsl:value-of select="./district"/></h7>
					
				<h8>Postal Code: <xsl:value-of select="./postalCode"/></h8>
					
				<h9>Street: <xsl:value-of select="./street"/></h9>
				
				<h10>Coverage: <xsl:value-of select="./coverage"/></h10>
				
				<h11>Risk index: <xsl:value-of select="./index"/></h11>
					
			</result>
		</xsl:for-each>
	</body>
   </html>
</xsl:template>
</xsl:stylesheet>