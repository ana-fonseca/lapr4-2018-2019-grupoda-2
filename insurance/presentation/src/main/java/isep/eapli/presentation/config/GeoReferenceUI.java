package isep.eapli.presentation.config;

import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;

public class GeoReferenceUI extends AbstractUI{

    @Override
    protected boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer menuRenderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return menuRenderer.render();
    }
    
    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();
        mainMenu.addItem(new MenuItem(1, "Coordinates from a postal address\n", new CoordinatesFromAddressAction()));
        mainMenu.addItem(new MenuItem(2, "Full postal address from a partial postal address\n", new FullPostalAddressFromPartialAction()));
        mainMenu.addItem(new MenuItem(3, "Surroundings of a type around coordinates\n", new SurroundingsNearByAction()));
        mainMenu.addItem(new MenuItem(4, "Sea elevation from coordinates\n", new SeaElevationFromCoordsAction()));
        mainMenu.addItem(new MenuItem(5, "Travelling Distance and Time between locations.\n", new DistanceCalculatorAction()));
        mainMenu.addItem(new MenuItem(6, "Sea elevation difference between two locations.\n", new DifferenceCalculatorAction()));
        mainMenu.addItem(new MenuItem(7, "Transform the values to the risk matrix.\n", new GeorefInfoToCalculationAction()));
        mainMenu.addItem(new MenuItem(0, "Exit", new ExitWithMessageAction()));
        
        return mainMenu;
    }

    @Override
    public String headline() {
        return "Georefence Service Menu";
    }
    
    
    
}
