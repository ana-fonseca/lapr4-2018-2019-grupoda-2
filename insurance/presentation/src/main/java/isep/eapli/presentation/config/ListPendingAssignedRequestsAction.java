package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.core.domain.RiskAnalyst;

public class ListPendingAssignedRequestsAction implements Action{
    
    private RiskAnalyst riskAnalyst;
    
    public ListPendingAssignedRequestsAction (RiskAnalyst ra) {
        riskAnalyst = ra;
    }
    
    @Override
    public boolean execute() {
        return new ListPendingAssignedRequestsUI(riskAnalyst).doShow();
    }
    
}
