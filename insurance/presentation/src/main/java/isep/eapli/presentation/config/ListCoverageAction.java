package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

class ListCoverageAction implements Action {

    /**
     * Action method that shows the UI of the ListCoverage
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new ListCoverageUI().show();
    }
}
