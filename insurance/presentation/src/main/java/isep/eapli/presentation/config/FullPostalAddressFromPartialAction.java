package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class FullPostalAddressFromPartialAction implements Action {

    @Override
    public boolean execute() {
        return new FullPostalAddressFromPartialUI().doShow();
    }
    
}
