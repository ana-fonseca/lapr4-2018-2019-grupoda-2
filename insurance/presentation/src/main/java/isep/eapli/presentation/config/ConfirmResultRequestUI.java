/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.core.domain.Request;
import java.util.Scanner;

/**
 *
 * @author Luís Silva
 */
public class ConfirmResultRequestUI extends AbstractUI{

    private RequestAnalysisController rac;
    private Request request;
    
    public ConfirmResultRequestUI(Request request) {
        rac = new RequestAnalysisController(request);
        this.request = request;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Deseja confirmar o resultado? [Y/N]");
        Scanner in = new Scanner(System.in);
        String op = in.nextLine();
        switch (op) {
            case "Y":
                rac.confirmResult();
                break;
            case "N":
                return new RequestAnalysisAction(request).execute();
            default:
                break;
        }
        System.out.println("Deseja adicionar uma observação? [Y/N]");
        op = in.nextLine();
        switch (op) {
            case "Y":
                System.out.println("Introduza a Observação");
                rac.addObsToResult(in.nextLine());
                break;
            case "N":
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Confirmar Resultado do Pedido de Avaliação";
    }

}
