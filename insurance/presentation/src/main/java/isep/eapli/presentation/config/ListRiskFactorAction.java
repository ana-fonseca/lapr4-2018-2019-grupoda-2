package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class ListRiskFactorAction implements Action {

    /**
     * Action method that shows the UI of the ListRiskFactor
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new ListRiskFactorUI().show();
    }
}
