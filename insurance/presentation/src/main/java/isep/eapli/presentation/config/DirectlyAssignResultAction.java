/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.core.domain.Request;

/**
 *
 * @author Luís Silva
 */
public class DirectlyAssignResultAction implements Action {

    private Request request;
    
    public DirectlyAssignResultAction(Request request) {
        this.request = request;
    }

    @Override
    public boolean execute() {
        return new DirectlyAssignResultUI(request).doShow();
    }
    
}
