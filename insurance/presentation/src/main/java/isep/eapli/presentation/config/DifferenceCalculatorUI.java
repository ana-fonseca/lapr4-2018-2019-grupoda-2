package isep.eapli.presentation.config;

import com.google.maps.errors.ApiException;
import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.georef.controller.ElevationDifferenceController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maria Junqueira
 */
public class DifferenceCalculatorUI extends AbstractUI {

    @Override
    protected boolean doShow() {
        Scanner in = new Scanner(System.in);
        boolean end = false;
        String latitudeO = "";
        String longitudeO = "";
        while (!end) {
            System.out.print("Please enter an origin latitude: ");
            latitudeO = in.nextLine();
            System.out.print("Please enter an origin longitude: ");
            longitudeO = in.nextLine();

            try {
                Double.parseDouble(latitudeO);
                Double.parseDouble(longitudeO);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid origin coordinates!");
            }
        }
        end = false;
        String latitudeD = "";
        String longitudeD = "";
        while (!end) {
            System.out.print("Please enter a destination latitude: ");
            latitudeD = in.nextLine();
            System.out.print("Please enter a destination longitude: ");
            longitudeD = in.nextLine();

            try {
                Double.parseDouble(latitudeD);
                Double.parseDouble(longitudeD);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid destination coordinates!");
            }
        }
        end = false;

        float difference;
        while (!end) {
            System.out.println("Do you want to use the Google Maps Services, the Microsoft Bing Services or the average of both? [GMS/MBS/AVG]");
            String answer = in.nextLine();
            System.out.println("");
            DecimalFormat df = new DecimalFormat("00.00");
            end = true;
            switch (answer) {
                case "GMS":
                    difference = ElevationDifferenceController.elevationDifferenceGMS(latitudeO, longitudeO, latitudeD, longitudeD);
                    System.out.println("Using GMS: \nDifference Elevation: " + df.format(difference));
                    break;
                case "MBS":
                    difference = ElevationDifferenceController.elevationDifferenceBMS(latitudeO, longitudeO, latitudeD, longitudeD);
                    System.out.println("Using BMS: \nDifference Elevation: " + df.format(difference));
                    break;
                case "AVG":
                    difference = ElevationDifferenceController.elevationDifferenceAVG(latitudeO, longitudeO, latitudeD, longitudeD);
                    System.out.println("Using Average: \nDifference Elevation: " + df.format(difference));
                    break;
                default:
                    end = false;
                    break;
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Distance Calculator";
    }
}
