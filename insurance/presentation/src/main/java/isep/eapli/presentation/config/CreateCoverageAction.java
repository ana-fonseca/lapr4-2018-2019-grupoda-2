package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

class CreateCoverageAction implements Action {

    /**
     * Action method that shows the UI of the Import Base Matrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new CreateCoverageUI().show();
    }
}
