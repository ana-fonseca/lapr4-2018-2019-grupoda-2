package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.riskassessment.controller.RiskAssessmentComparisonController;

import java.util.List;


public class RiskAssessmentComparisonUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private RiskAssessmentComparisonController racc = new RiskAssessmentComparisonController();

    /**
     * Method that shows the UI of the NewMatrixFromExistingMatrix UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        try {
            List<RiskMatrix> riskMatrices = (List<RiskMatrix>) racc.getPublishedRiskMatrices();
            if (riskMatrices.size() == 0) {
                System.out.println("There are no published matrices!");
            } else {
                int count = 1;
                for (RiskMatrix rm : riskMatrices) {
                    System.out.println(count + " - Risk Matrix with Version: " + rm.obtainVersion());
                    count++;
                }
                int inputNumber = Console.readInteger("Choose a matrix from the list by inserting it's number: ");
                RiskMatrix riskMatrix1 = riskMatrices.get(inputNumber - 1);
                inputNumber = Console.readInteger("Choose another matrix from the list by inserting it's number: ");
                RiskMatrix riskMatrix2 = riskMatrices.get(inputNumber - 1);
                if (riskMatrix1.equals(riskMatrix2)) {
                    System.out.println("Different matrices must be chosen!");
                    return false;
                }
                racc.compareRiskAssessment(riskMatrix1.obtainVersion(), riskMatrix2.obtainVersion());
                System.out.println("Comparison done successfully!");
            }
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Invalid number, the comparison was not made!");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "Comparison of Risk Assessment Using Different Versions of the Risk Matrix";
    }
}
