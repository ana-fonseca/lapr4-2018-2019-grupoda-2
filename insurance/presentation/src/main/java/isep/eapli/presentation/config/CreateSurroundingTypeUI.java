/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.CreateSurroundingTypeController;
import isep.eapli.config.controller.ListSurroundingTypeController;

/**
 *
 * @author 1171138
 */
public class CreateSurroundingTypeUI extends AbstractUI{
    
     /**
     * Class object that connects to the Controller
     */
    private CreateSurroundingTypeController cstc = new CreateSurroundingTypeController();

    /**
     * Method that shows the UI of the CreateSurroundigType UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        ListSurroundingTypeController lstc = new ListSurroundingTypeController();
        lstc.getAllSurroundingTypes();
        String description = Console.readLine("Insert the surrounding type description: ");
        if (!cstc.registerSurroundingType(description)) {
            System.out.println("There was an error creating the new surrounding type.");
        } else {
            System.out.println("Surrounding type created successfully!");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "Create Surrounding Type";
    }
}
