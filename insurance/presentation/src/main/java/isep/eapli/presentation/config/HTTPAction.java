package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.http.HttpServerChat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HTTPAction implements Action {

    @Override
    public boolean execute() {
        try {
            HttpServerChat.main(new String[1]);
        } catch (Exception ex) {
            Logger.getLogger(HTTPAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
