package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class ImportBaseMatrixAction implements Action {

    /**
     * Action method that shows the UI of the Import base matrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new ImportBaseMatrixUI().show();
    }
}
