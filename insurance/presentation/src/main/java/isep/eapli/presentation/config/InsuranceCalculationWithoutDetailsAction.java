package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class InsuranceCalculationWithoutDetailsAction implements Action {

    /**
     * Path
     */
    private String[] givenPath;

    /**
     * Public constructor
     * @param givenPath path given
     */
    public InsuranceCalculationWithoutDetailsAction(String[] givenPath){
        this.givenPath = givenPath;
    }

    /**
     * Action method that shows the UI of the Import base matrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new InsuranceCalculationWithoutDetailsUI(givenPath).show();
    }
}
