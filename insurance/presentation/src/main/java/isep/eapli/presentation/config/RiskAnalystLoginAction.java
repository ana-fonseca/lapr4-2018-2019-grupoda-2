/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author becas
 */
public class RiskAnalystLoginAction implements Action {

    @Override
    public boolean execute() {
        return new RiskAnalystLoginUI().doShow();
    }
}
