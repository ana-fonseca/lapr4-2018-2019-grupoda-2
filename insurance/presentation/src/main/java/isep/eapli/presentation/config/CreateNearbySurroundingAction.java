package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class CreateNearbySurroundingAction implements Action {

    @Override
    public boolean execute() {
        return new CreateNearbySurroundingUI().doShow();
    }
    
}
