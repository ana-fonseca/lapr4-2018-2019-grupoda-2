package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.CreateCoverageController;
import isep.eapli.config.controller.ListCoverageController;

class CreateCoverageUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private CreateCoverageController ccc = new CreateCoverageController();

    /**
     * Method that shows the UI of the CreateCoverage UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        String description = Console.readLine("Insert the coverage designation: ");
        if (!ccc.registerCoverage(description)) {
            System.out.println("There was an error creating the new coverage.");
        } else {
            System.out.println("Coverage created successfully!");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "Create Coverage";
    }
}