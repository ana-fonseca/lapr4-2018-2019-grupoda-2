/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author rikar
 */
class ImportDetailedMatrixAction implements Action {
    
    /**
     * Action method that shows the UI of the CreateCoverage
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new ImportDetailedMatrixUI().show();
    }

    /**
     * Indispensable method to execute the program
     *
     * @return null
     */
    public Boolean get() {
        return null;
    }
    
}
