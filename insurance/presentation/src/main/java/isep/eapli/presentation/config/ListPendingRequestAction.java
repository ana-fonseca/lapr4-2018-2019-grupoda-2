package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.core.domain.RiskAnalyst;

public class ListPendingRequestAction implements Action {

    private RiskAnalyst riskAnalyst;
    
    public ListPendingRequestAction (RiskAnalyst ra) {
        riskAnalyst = ra;
    }
     /**
     * Action method that shows the UI of the ListCoveragesNotInRiskMatrix
     * @return boolean returned by show
     */
    @Override
    public boolean execute() { 
        return new ListPendingAssignedRequestsUI(riskAnalyst).doShow();
    }
    
}
