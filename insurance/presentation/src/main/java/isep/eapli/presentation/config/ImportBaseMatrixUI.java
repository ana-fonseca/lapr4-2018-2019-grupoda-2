package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.BaseMatrixController;

public class ImportBaseMatrixUI extends AbstractUI {

    /**
     * Base Matrix Controller attribute.
     */
    private BaseMatrixController bmc = new BaseMatrixController();


    @Override
    protected boolean doShow() {
        if(bmc.importBaseMatrix("importBaseMatrix.csv")){
            System.out.println("File loaded successfuly.");
        }else{
            System.out.println("Couldn't load file.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Import base matrix";
    }
}
