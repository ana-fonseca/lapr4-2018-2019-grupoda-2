package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class NewMatrixFromExistingMatrixAction implements Action {

    /**
     * Action method that shows the UI of the NewMatrixFromExistingMatrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new NewMatrixFromExistingMatrixUI().show();
    }
}