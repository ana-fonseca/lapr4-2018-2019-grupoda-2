package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.AuthenticationController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author becas
 */
public class RiskAnalystLoginUI extends AbstractUI {

    private AuthenticationController ac = new AuthenticationController();

    @Override
    protected boolean doShow() {
        try {
            String email = Console.readLine("Insert the email: ");
            String password = Console.readLine("Insert the password: ");
            if (!ac.RiskAnalystLogin(email, password)) {
                System.out.println("Invalid Email or Password");
                return false;
            } else {
                System.out.println("Login Sucessful");
                RiskAnalystConfigUI rUI = new RiskAnalystConfigUI(email);
                rUI.mainLoop();
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Invalid Password");
            return false;
        }
    }

    @Override
    public String headline() {
        return "Risk Analyst Login";
    }
}
