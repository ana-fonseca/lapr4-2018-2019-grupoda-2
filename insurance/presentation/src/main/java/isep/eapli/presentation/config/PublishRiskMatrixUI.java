package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.PublishRiskMatrixController;
import isep.eapli.core.domain.RiskMatrix;
import java.util.List;
import java.util.Scanner;

public class PublishRiskMatrixUI extends AbstractUI {

    private PublishRiskMatrixController publishRiskMatrixController;

    /**
     * Constructor to initialize the controller
     */
    public PublishRiskMatrixUI(){
        publishRiskMatrixController=new PublishRiskMatrixController();
    }

    /**
     * User interface
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        try {
            List<RiskMatrix> listRiskMatrix = publishRiskMatrixController.showRiskMatrixes();
            if(listRiskMatrix.size() == 0) {
                System.out.println("There are no non published fully completed matrixes to be published !");
            }else {
                int counter = 1;
                for (RiskMatrix riskMatrix : listRiskMatrix) {
                    System.out.println(counter + " - " + riskMatrix.riskMatrixPresentation());
                    counter++;
                }
                int inputNumber = Console.readInteger("Choose the non published matrix to publish, by inserting it's number: ");

                RiskMatrix riskMatrix = listRiskMatrix.get(inputNumber-1);

                try {
                    publishRiskMatrixController.publishRiskMatrix(riskMatrix);
                } catch (IllegalArgumentException e) {
                    System.out.println("Error publication the risk matrix");
                    return false;
                }
                System.out.println("Matrix published successfully!!");
            }
        }catch (IndexOutOfBoundsException e) {
            System.out.println("Number not found, publication not done !");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Publish Risk Matrix";
    }
}
