/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import com.google.maps.errors.ApiException;
import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.*;
import java.util.Scanner;
import isep.eapli.georef.controller.GeorefInfoToCalculationController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luís Silva
 */
public class GeorefInfoToCalculationUI extends AbstractUI {

    private final SurroundingTypeDB rdb;

    public GeorefInfoToCalculationUI() {
        rdb = new RepositoryFactory().getSurroundingTypeDB();
    }

    @Override
    protected boolean doShow() {
        Scanner in = new Scanner(System.in);
        boolean end = false;
        String latitudeO = "";
        String longitudeO = "";
        while (!end) {
            System.out.print("Please enter an origin latitude: ");
            latitudeO = in.nextLine();
            System.out.print("Please enter an origin longitude: ");
            longitudeO = in.nextLine();

            try {
                Double.parseDouble(latitudeO);
                Double.parseDouble(longitudeO);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid origin coordinates!");
            }
        }
        System.out.println("Insert the Surronding type (Hospital/Police)");
        SurroundingType surrounding = rdb.findByType(in.nextLine());
        String distance = null;
        String time = null;
        try {
            distance = new GeorefInfoToCalculationController().getInfoToCalculationDistance(surrounding.obtainDescription(), surrounding.getDistanceValueA(), surrounding.getDistanceValueB(), latitudeO, longitudeO);
        } catch (IOException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApiException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            time = new GeorefInfoToCalculationController().getInfoToCalculationTime(surrounding.obtainDescription(), surrounding.getTimeValueA(), surrounding.getTimeValueB(), latitudeO, longitudeO);
        } catch (IOException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApiException ex) {
            Logger.getLogger(GeorefInfoToCalculationUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(distance);
        System.out.println(time);
        return true;
    }

    @Override
    public String headline() {
        return "Transform the values to the risk matrix";
    }

}
