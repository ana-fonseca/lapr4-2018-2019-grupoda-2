package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author 1171138
 */
public class ListSurroundingTypeAction implements Action{
    
     /**
     * Action method that shows the UI of the ListSurroundingType
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
            return new ListSurroundingTypeUI().show();
    }
    
}
