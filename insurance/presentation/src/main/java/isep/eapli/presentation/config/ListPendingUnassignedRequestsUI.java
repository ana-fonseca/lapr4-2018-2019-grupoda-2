package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RiskAssessmentRequestController;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.RiskAnalyst;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ListPendingUnassignedRequestsUI extends AbstractUI {

    private RiskAnalyst riskAnalyst;
    private static RiskAssessmentRequestController raq = new RiskAssessmentRequestController();
    
    public ListPendingUnassignedRequestsUI (RiskAnalyst riskAnalyst) {
        this.riskAnalyst = riskAnalyst;
    }

    @Override
    public boolean doShow() {
        List<Request> requestList = new ArrayList<>();

        boolean cont = true;

        while (cont) {
            System.out.println("Do you wish to filter through a district? [Y/N]");
            Scanner in = new Scanner(System.in);
            String op = in.nextLine();
            switch (op) {
                case "Y":
                    System.out.println("Please enter a district: ");
                    String district = in.nextLine();
                    requestList = raq.listPendingUnasignedRequests(district);
                    cont = false;
                    break;
                case "N":
                    requestList = raq.listPendingUnassignedRequests();
                    cont = false;
                    break;
                default:
                    break;
            }
        }
        
        cont = requestList.isEmpty() ? false : true;

        System.out.println("");

        for (Request request : requestList) {
            System.out.println("Request ID: " + request.obtainID());
            System.out.println("Matrix Version: " + request.getMatrixVersion());
            System.out.println("Requested Date:" + request.obtainRequestedDate());
            Map<String, List<Coverage>> map = new LinkedHashMap<>();
            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                if (!map.containsKey(icc.obtainInsurance().getAddress().district().district())) {
                    map.put(icc.obtainInsurance().getAddress().district().district(), new ArrayList<Coverage>());
                }
            }

            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                map.get(icc.obtainInsurance().getAddress().district().district()).add(icc.obtainCoverage());
            }

            System.out.println("Insurance-Coverage Connection List:");
            for (String ins : map.keySet()) {
                System.out.println("    Insurance Address District: " + ins);
                System.out.println("    " + map.get(ins).size() + " Coverages");
            }
            System.out.println(request.obtainRiskAnalyst() == null && request.obtainAssignedDate() == null ? "There is no Risk Analyst assigned to this Request" : "Error!");
            System.out.println();
        }
        
        System.out.println( cont == false ? "There are no pending unassigned requests" : "");
        
        while (cont) {
            System.out.println("Do you wish to assign a request to yourself? [Y/N]");
            Scanner in = new Scanner(System.in);
            String op = in.nextLine();
            switch (op) {
                case "Y":
                    System.out.println("Please enter a request ID: ");
                    long id = -1;
                    try {
                        id = Long.parseLong(in.nextLine());
                    } catch (Exception e) {
                        break;
                    }
                    
                    Request chosen = null;
                    
                    for (Request request : requestList) {
                        if (request.obtainID() == id) {
                            chosen = request;
                        }
                    }
                    
                    if (chosen == null) {
                        System.out.println("Please choose a valid request!");
                        break;
                    }
                    
                    raq.assignRiskAnalyst(chosen, riskAnalyst);
                    System.out.println("The chosen request has been assigned to you!");
                    cont = false;
                    break;
                case "N":
                    cont = false;
                    break;
                default:
                    break;
            }
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "List Pending Unassigned Requests";
    }
}
