package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class PublishRiskMatrixAction implements Action {

    /**
     * Action method that shows the UI of the PublishRiskMatrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() { return new PublishRiskMatrixUI().show();
    }
}
