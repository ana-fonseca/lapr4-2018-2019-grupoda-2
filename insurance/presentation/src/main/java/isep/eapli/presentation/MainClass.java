package isep.eapli.presentation;

import isep.eapli.config.bootstrapers.*;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.presentation.config.ConfigurationUI;

public class MainClass {

    /**
     * Main class of the presentation application that is connected to the configuration utility
     * @param args arguments to be used during the execution
     */
    public static void main(String[] args) throws Exception {
        CoverageBootstrapper cb = new CoverageBootstrapper();
        cb.execute();
        SurroundingTypeBootstrapper stb = new SurroundingTypeBootstrapper();
        stb.execute();
        RiskFactorBootstrapper rfb = new RiskFactorBootstrapper();
        rfb.execute();
        RiskMatrixBootstrapper rmb = new RiskMatrixBootstrapper();
        rmb.execute();
        RiskAnalystBootstrapper ramb = new RiskAnalystBootstrapper();
        ramb.execute();
        RequestsBootstrapper rqmb = new RequestsBootstrapper();
        rqmb.execute();
        System.out.println("\n====================");
        System.out.println("I2S Risk Matrix Configuration Utility");
        System.out.println("====================");
        final ConfigurationUI cui = new ConfigurationUI(args);
        cui.mainLoop();
    }
}
