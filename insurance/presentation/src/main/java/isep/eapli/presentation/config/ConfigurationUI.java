package isep.eapli.presentation.config;

import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;

public class ConfigurationUI extends AbstractUI {

    private String[] givenPath;

    public ConfigurationUI(String[] args) {
        super();
        if (args.length >= 1) {
            givenPath = args;
        } else {
            System.out.println("Error loading file path. Using testing configuration.");
        }
    }

    /**
     * doShow method, to show and render the menu
     *
     * @return render result
     */
    protected boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer menuRenderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return menuRenderer.render();
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    public String headline() {
        return "I2S Risk Matrix Configuration Utility";
    }

    /**
     * Method that builds the menu to be used
     *
     * @return constructed menu
     */
    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();
        mainMenu.addItem(new MenuItem(1, "Create Coverage\n", new CreateCoverageAction()));
        mainMenu.addItem(new MenuItem(2, "List Coverage\n", new ListCoverageAction()));
        mainMenu.addItem(new MenuItem(3, "Create Surrounding Type\n", new CreateSurroundingTypeAction()));
        mainMenu.addItem(new MenuItem(4, "List Surrounding Type\n", new ListSurroundingTypeAction()));
        mainMenu.addItem(new MenuItem(5, "Create Risk Factor\n", new CreateRiskFactorAction()));
        mainMenu.addItem(new MenuItem(6, "List Risk Factor\n", new ListRiskFactorAction()));
        mainMenu.addItem(new MenuItem(7, "Import Base Matrix\n", new ImportBaseMatrixAction()));
        mainMenu.addItem(new MenuItem(8, "Characterize Risk Matrix\n", new CharacterizeMatrixAction()));
        mainMenu.addItem(new MenuItem(9, "Import Detailed Risk Matrix\n", new ImportDetailedMatrixAction()));
        mainMenu.addItem(new MenuItem(10, "Publish Risk Matrix\n", new PublishRiskMatrixAction()));
        mainMenu.addItem(new MenuItem(11, "List Risk Factors Not In a Risk Matrix\n", new ListRiskFactorsNotInRiskMatrixAction()));
        mainMenu.addItem(new MenuItem(12, "List Coverages Not In a Risk Matrix\n", new ListCoveragesNotInRiskMatrixAction()));
        mainMenu.addItem(new MenuItem(13, "Export Information About All Coverages And Risk Factors\n", new ExportBaseMatrixAction()));
        mainMenu.addItem(new MenuItem(14, "New Risk Matrix From Existing Detailed/Characterized Risk Matrix\n", new NewMatrixFromExistingMatrixAction()));
        mainMenu.addItem(new MenuItem(15, "Calculate Risk Assessment from Loaded File Without Details\n", new InsuranceCalculationWithoutDetailsAction(givenPath)));
        mainMenu.addItem(new MenuItem(16, "Calculate Risk Assessment from Loaded File With Details\n", new InsuranceCalculationWithDetailsAction(givenPath)));
        mainMenu.addItem(new MenuItem(17, "Compare Two Risk Assessments from Different Risk Matrixes\n", new RiskAssessmentComparisonAction()));
        mainMenu.addItem(new MenuItem(18, "Risk Analyst Registration\n", new RiskAnalystRegistrationAction()));
        mainMenu.addItem(new MenuItem(19, "Risk Analyst Login\n", new RiskAnalystLoginAction()));
        mainMenu.addItem(new MenuItem(20, "Enter the georef Menu\n", new GeoreferenceAction()));
        mainMenu.addItem(new MenuItem(21, "Start the HTTP server\n", new HTTPAction()));
        mainMenu.addItem(new MenuItem(0, "Exit", new ExitWithMessageAction()));
        return mainMenu;
    }
}
