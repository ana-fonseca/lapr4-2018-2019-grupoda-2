package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.NewMatrixFromExistingMatrixController;
import isep.eapli.core.domain.RiskMatrix;

import java.util.List;
import java.util.Scanner;

public class NewMatrixFromExistingMatrixUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private NewMatrixFromExistingMatrixController nmfemc = new NewMatrixFromExistingMatrixController();

    /**
     * Method that shows the UI of the NewMatrixFromExistingMatrix UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        Scanner sc = new Scanner(System.in);
        try {
            List<RiskMatrix> riskMatrices = (List<RiskMatrix>) nmfemc.getAllDetailedCharacterizedRiskMatrices();
            if (riskMatrices.size() == 0) {
                System.out.println("There are no detailed or characterized matrices!");
            } else {
                int counter = 1;
                for (RiskMatrix rm : riskMatrices) {
                    System.out.println(counter + " - Risk Matrix with Version: " + rm.obtainVersion());
                    counter++;
                }
                System.out.println("Choose one matrix from the list by inserting it's number: ");
                int inputNumber = Integer.parseInt(sc.nextLine());
                RiskMatrix riskMatrix = riskMatrices.get(inputNumber - 1);

                try {
                    nmfemc.registerNewMatrixFromExistingMatrix(riskMatrix);
                } catch (IllegalArgumentException e) {
                    System.out.println("There was an error creating the new matrix!");
                    return false;
                }
                System.out.println("New matrix created successfully!");
            }
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Invalid number, the matrix was not created!");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "New Risk Matrix From Existing Detailed/Characterized Risk Matrix";
    }
}
