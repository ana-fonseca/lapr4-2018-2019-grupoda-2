package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.georef.controller.ElevationController;
import java.text.DecimalFormat;
import java.util.Scanner;

public class SeaElevationFromCoordsUI extends AbstractUI {

    @Override
    protected boolean doShow() {
        Scanner in = new Scanner(System.in);
        boolean end = false;
        String latitude = "";
        String longitude = "";
        while (!end) {
            System.out.print("Please enter a latitude: ");
            latitude = in.nextLine();
            System.out.print("Please enter a longitude: ");
            longitude = in.nextLine();

            try {
                Double.parseDouble(latitude);
                Double.parseDouble(longitude);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid coordinates!");
            }
        }

        end = false;

        float elevation = 0f;
        
        while (!end) {
            System.out.println("Do you want to use the Google Maps Services, the Microsoft Bing Services or the average of both? [GMS/MBS/AVG]");
            String answer = in.nextLine();
            System.out.println("");
            DecimalFormat df = new DecimalFormat("00.0000");
            end = true;
            switch (answer) {
                case "GMS":
                    elevation = ElevationController.elevationGMS(latitude, longitude);
                    System.out.println("Elevation of the chosen place using GMS: " + df.format(elevation));
                    break;
                case "MBS":
                    elevation = ElevationController.elevationBMS(latitude, longitude);
                    System.out.println("Elevation of the chosen place using MBS: " + df.format(elevation));
                    break;
                case "AVG":
                    elevation = ElevationController.elevationAVG(latitude, longitude);
                    System.out.println("Elevation of the chosen place using the average of both: " + df.format(elevation));
                    break;
                default:
                    end = false;
                    break;
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Sea Elevation from Coordinates";
    }

}
