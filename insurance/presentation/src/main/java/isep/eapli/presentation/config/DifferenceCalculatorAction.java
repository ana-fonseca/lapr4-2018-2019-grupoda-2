/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author Maria Junqueira
 */
public class DifferenceCalculatorAction implements Action {

   @Override
    public boolean execute() {
        return new DifferenceCalculatorUI().doShow();
    }
    
}
