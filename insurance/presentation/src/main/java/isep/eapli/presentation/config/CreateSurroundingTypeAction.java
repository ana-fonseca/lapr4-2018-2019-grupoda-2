/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author 1171138
 */
public class CreateSurroundingTypeAction implements Action {
    
    
    /**
     * Action method that shows the UI of the Create Surrounding type
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new CreateSurroundingTypeUI().show();
    }
}
