package isep.eapli.presentation.config;

import com.google.maps.errors.ApiException;
import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.georef.controller.TravellingTimeDistanceController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DistanceCalculatorUI extends AbstractUI {

    @Override
    protected boolean doShow() {
        Scanner in = new Scanner(System.in);
        boolean end = false;
        String latitudeO = "";
        String longitudeO = "";
        while (!end) {
            System.out.print("Please enter an origin latitude: ");
            latitudeO = in.nextLine();
            System.out.print("Please enter an origin longitude: ");
            longitudeO = in.nextLine();

            try {
                Double.parseDouble(latitudeO);
                Double.parseDouble(longitudeO);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid origin coordinates!");
            }
        }
        end = false;
        String latitudeD = "";
        String longitudeD = "";
        while (!end) {
            System.out.print("Please enter a destination latitude: ");
            latitudeD = in.nextLine();
            System.out.print("Please enter a destination longitude: ");
            longitudeD = in.nextLine();

            try {
                Double.parseDouble(latitudeD);
                Double.parseDouble(longitudeD);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid destination coordinates!");
            }
        }
        end = false;
        String travelMode = "";
        while (!end) {
            System.out.print("Please enter a travel mode [walking/bicycling/driving]: ");
            travelMode = in.nextLine();
            if (travelMode.equalsIgnoreCase("Walking") || travelMode.equalsIgnoreCase("Bicycling") || travelMode.equalsIgnoreCase("Driving")) {
                end = true;
            } else {
                System.out.println("Please insert a valid travel mode!");
            }
        }
        
        end = false;
        
        float[] distanceTime = new float[2];
        while (!end) {
            try {
                System.out.println("Do you want to use the Google Maps Services, the Microsoft Bing Services or the average of both? [GMS/MBS/AVG]");
                String answer = in.nextLine();
                System.out.println("");
                DecimalFormat df = new DecimalFormat("00.00");
                end = true;
                switch (answer) {
                    case "GMS":
                        distanceTime = TravellingTimeDistanceController.travelCalculationGMS(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
                        System.out.println("Using GMS: \nDistance: " + df.format(distanceTime[0]) + "km\nTime: " + df.format(distanceTime[1]) + "min");
                        break;
                    case "MBS":
                        distanceTime = TravellingTimeDistanceController.travelCalculationBMS(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
                        System.out.println("Using MBS: \nDistance:" + df.format(distanceTime[0]) + "km\nTime: " + df.format(distanceTime[1]) + "min");
                        break;
                    case "AVG":
                        distanceTime = TravellingTimeDistanceController.travelCalculationAVG(latitudeO, longitudeO, latitudeD, longitudeD, travelMode);
                        System.out.println("Using Average: \nDistance:" + df.format(distanceTime[0]) + "km\nTime: " + df.format(distanceTime[1]) + "min");
                        break;
                    default:
                        end = false;
                        break;
                }
            } catch (IOException ex) {
                Logger.getLogger(DistanceCalculatorUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ApiException ex) {
                Logger.getLogger(DistanceCalculatorUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(DistanceCalculatorUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        return false;
    }

    @Override
    public String headline() {
        return "Distance Calculator";
    }

}
