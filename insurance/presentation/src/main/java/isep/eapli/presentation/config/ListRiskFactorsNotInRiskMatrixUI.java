package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.ListRiskFactorsNotInRiskMatrixController;
import isep.eapli.core.domain.RiskFactor;

import java.util.List;

public class ListRiskFactorsNotInRiskMatrixUI extends AbstractUI {

    private ListRiskFactorsNotInRiskMatrixController lrfc;

    @Override
    protected boolean doShow() {
        lrfc = new ListRiskFactorsNotInRiskMatrixController();
        if(lrfc.showRiskMatrices().isEmpty()){
            System.out.println("There are not any published risk matrix in the database!");
            return false;
        }
        for(int i=0; i<lrfc.showRiskMatrices().size(); i++) {
            String str = "" + (i+1) +" - "+lrfc.showRiskMatrices().get(i);
            System.out.println(str);
        }
        int flag=0;
        List<RiskFactor> list = null;
        while(flag==0) {
            int index = Console.readInteger("Input the number of the Risk Matrix to list:");
            try {
                list = lrfc.listRiskFactorNotInRiskMatrix(lrfc.showRiskMatrices().get(index - 1));
                flag = 1;
            } catch (IndexOutOfBoundsException e) {
                flag = 0;
            } catch (AssertionError e){
                System.out.println("The database does not contain any risk matrices");
                return false;
            }
        }
        if(list.isEmpty()){
            System.out.println("Every existent Risk Factor is in the Risk Matrix");
            return false;
        }
        for(RiskFactor rf : list)
            System.out.println(rf.toString());
        return false;
    }

    @Override
    public String headline() {
        return "List Risk Factors not in a Risk Matrix:";
    }
}
