package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.ListSurroundingTypeController;
import isep.eapli.config.controller.NearBySurroundingController;
import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.georef.controller.AdressSearchController;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CreateNearbySurroundingUI extends AbstractUI {

    //Create a Scanner to read input from the console
    private final Scanner in = new Scanner(System.in);

    //Exit flag
    private final String exitFlag = "exit";

    //Controller
    private final NearBySurroundingController controller = new NearBySurroundingController();

    //Create a new model
    private NearBySurrounding modelAux;

    /**
     * Show Method to display the form to Create a new Near
     *
     * @return
     */
    @Override
    protected boolean doShow() {
        //Initialize a new model
        this.modelAux = new NearBySurrounding();

        //Print headling
        this.headline();

        try {
            //AR06 Ask for name
            this.askForName();

            //AR06 Select Type
            this.askForType();

            //AR06 Enter Address
            this.askForAddress();

            //AR06.1 Confirm Address
            this.getGpsLocationForAddress();

            //Confirm GPS Location
            this.askForGpsLocationConfirmation();

            //SAVE
            this.save();

            //Key
            this.pressAnyKeyToContinue();

            //Done, get back to the previous menu by returning false
            return false;

        } catch (UnsupportedOperationException e) {
            //Log
            System.out.println("Operation Not Implemented yet: " + e.getMessage());

            //Key
            this.pressAnyKeyToContinue();

            //Done
            return false;

        } catch (Exception e) {
            //Log
            System.out.println("Create New Surrounding CANCELLED due to " + e.getMessage());

            //Key
            this.pressAnyKeyToContinue();

            //Done
            return false;
        }
    }

    @Override
    public String headline() {
        return "CREATE A NEW NEARBY SURROUNDING";
    }

    /**
     * AR06
     *
     * @return
     * @throws Exception
     */
    private boolean askForName() throws Exception {
        //Exit flag
        String reply = "";

        //Do while
        while (!reply.equalsIgnoreCase(this.exitFlag)) {
            //Print request
            System.out.println("Please enter a Name: ");

            //Read
            reply = in.nextLine();

            //Evaluate
            if (!reply.equalsIgnoreCase("") && !reply.equalsIgnoreCase(this.exitFlag)) {
                //It's a valid name!
                this.modelAux.setDescription(reply);

                //Done
                return true;
            }
        }

        //User decided to exit
        throw new Exception(this.exitFlag);
    }

    /**
     * AR06
     *
     * @return
     * @throws Exception
     */
    private boolean askForType() throws Exception {
        //Exit flag
        String reply = "";

        //Do while
        while (!reply.equalsIgnoreCase(this.exitFlag)) {
            //Print request
            System.out.println("Please Select a Type: \n");

            //Create type controller
            ListSurroundingTypeController surroundingTypeController = new ListSurroundingTypeController();

            //Get all
            ArrayList<SurroundingType> allTypesLocalList = new ArrayList();
            Iterable<SurroundingType> allTypes = surroundingTypeController.getAllSurroundingTypes();

            //Display all types
            int counter = 1;
            for (SurroundingType type : allTypes) {
                //Print item
                System.out.println(counter + ": " + type.obtainDescription());

                //Add to local list
                allTypesLocalList.add(type);

                //Increment Counter
                counter++;
            }

            //Add new option
            System.out.println("0: (Create new Type)");

            //Read
            reply = in.nextLine();

            //Evaluate
            if (reply.equalsIgnoreCase(this.exitFlag)) {
                //User decided to exit
                throw new Exception(this.exitFlag);
            }

            //Selected option
            int selectedOption = -1;

            try {
                //Parse to int
                selectedOption = Integer.parseInt(reply);
            } catch (Exception e) {
                System.out.println("Invalid number. Please try again.");
            }

            //Evaluate if the option is valid
            if (selectedOption > -1 && selectedOption < counter) {
                //Valid selection
                if (selectedOption == 0) {
                    //Create a new one
                    new CreateSurroundingTypeUI().show();

                } else {
                    //Associate
                    this.modelAux.setType(allTypesLocalList.get(selectedOption - 1));

                    //Done
                    return true;
                }
            } else {
                //Invalid Selection
                System.out.println("Invalid Type Selected. Please try again.");
            }
        }

        //Not expected to reach this point
        return false;
    }

    /**
     * AR06
     *
     * @return
     * @throws Exception
     */
    private boolean askForAddress() throws Exception {
        //Exit flag
        String reply = "";

        //Do while
        while (!reply.equalsIgnoreCase(this.exitFlag)) {
            //Print request
            System.out.println("Please enter an Address. You can input an Address with the following patterns: ");
            System.out.println("{Street}, {City}, {Postal Code}, {Country}, {District}");
            System.out.println("{Street}, {City}, {Postal Code}, {Country}");
            System.out.println("{Street}, {City}, {Postal Code}");

            //Read
            reply = in.nextLine();

            //Evaluate
            if (reply.equalsIgnoreCase(this.exitFlag)) {
                //User decided to exit
                throw new Exception(this.exitFlag);
            }

            //Let's try to process
            String[] parts = reply.split(",");

            //Initialize a new Address
            Street street = null;
            City city = null;
            PostalCode postalCode = null;
            Country country = null;
            District district = null;

            //Evaluate number of parts
            if (parts.length < 3) {
                //Invalid
                System.out.println("Invalid Address");

            } else {
                //Its a valid Address
                //District
                if (parts.length > 4) {
                    district = new District(parts[4].trim());
                }

                //Country
                if (parts.length > 3) {
                    country = new Country(parts[3].trim());
                }

                //Street, City, Postal Code
                if (parts.length > 2) {
                    //Street
                    street = new Street(parts[0].trim());

                    //City
                    city = new City(parts[1].trim());

                    //Postal Code
                    try {
                        //Try parse
                        postalCode = new PostalCode(parts[2].trim());
                    } catch (Exception e) {
                        //Log
                        System.out.println("Invalid Postal Code");

                        //Set as null to represent the error
                        postalCode = null;
                    }
                }

                //Build an address
                if (postalCode != null) {
                    AdressSearchController c = new AdressSearchController();
                    
                    List<String> list = c.completePostalAddress(city.city(), postalCode.postalCode(), street.toString());
                    
                    district = new District(list.get(3));
                    country = new Country(list.get(4));
                    //All is ok, assemble
                    this.modelAux.setAdd(new Address(street, city, postalCode, country, district));

                    //Done
                    return true;
                }
            }
        }

        //Not expected to reach this point
        return false;
    }

    /**
     * AR06.1
     *
     * @return
     * @throws Exception
     */
    private void getGpsLocationForAddress() {
        //Log
        System.out.println("Getting GPS Location...");

        //Request location
        String location = this.controller.getAddressSearch(this.modelAux);

        //Save it on the model
        this.modelAux.setLocalization(location);

        //Evaluate
        if (location == null || location.equalsIgnoreCase("")) {
            //Log
            System.out.println("No Location found for the indicated Address");
        } else {
            //log
            System.out.println("GPS Location Found (lat,lon):");
            System.out.println(location);
        }
    }

    /**
     * AR06.1
     *
     * @return
     * @throws Exception
     */
    private boolean askForGpsLocationConfirmation() throws Exception {
        //Exit flag
        String reply = "";

        //Try show photo
        if (this.modelAux.getLocalization() != null && this.modelAux.getLocalization().equalsIgnoreCase("")) {
            //Try to show photo
            this.showAddressPhoto();
        }

        //Do while
        while (!reply.equalsIgnoreCase(this.exitFlag)) {
            //Print request
            System.out.println("Do you confirm the current GPS Location? (Y/N)");

            //Read
            reply = in.nextLine();

            //Evaluate
            if (reply.equalsIgnoreCase(this.exitFlag)) {
                //User decided to exit
                throw new Exception(this.exitFlag);
            } else {
                //Try parse
                if (reply.equalsIgnoreCase("y")) {
                    //User confirmed!
                    return true;

                } else if (reply.equalsIgnoreCase("n")) {
                    //Needs fix
                    this.fixLocation();

                    //Confirm again
                    return askForGpsLocationConfirmation();
                }
            }
        }

        //User decided to exit
        throw new Exception(this.exitFlag);
    }

    /**
     * AR06.1
     *
     * @return
     * @throws Exception
     */
    private boolean fixLocation() throws Exception {
        //Exit flag
        String reply = "";

        //Do while
        while (!reply.equalsIgnoreCase(this.exitFlag)) {
            //Print request
            System.out.println("Please enter a GPS Location. You can input a GPS Location with the following pattern: ");
            System.out.println("+/-{lat},+/-{lon}");

            //Read
            reply = in.nextLine();

            //Evaluate
            if (reply.equalsIgnoreCase(this.exitFlag)) {
                //User decided to exit
                throw new Exception(this.exitFlag);
            }

            //Let's try to process
            String[] parts = reply.split(",");

            //Initialize a new Address
            String lat = "";
            String lon = "";

            //Evaluate number of parts
            if (parts.length < 2) {
                //Invalid
                System.out.println("Invalid GPS Location");

            } else {
                //Trim parts
                parts[0] = parts[0].trim();
                parts[1] = parts[1].trim();

                //Try parse first part
                if (parts[0].length() == 0 || (parts[0].charAt(0) != '+' && parts[0].charAt(0) != '-')) {
                    //Bad symbol
                    lat = "";
                } else {
                    //Valid, try parse float
                    try {
                        //Parse coord to float
                        float coord = Float.parseFloat(parts[0].substring(1, parts[0].length()));

                        //Valid
                        lat = parts[0];

                    } catch (NumberFormatException e) {
                        //Failed
                        lat = "";
                    }
                }

                //Try parse first part
                if (parts[1].length() == 0 || (parts[1].charAt(0) != '+' && parts[1].charAt(0) != '-')) {
                    //Bad symbol
                    lon = "";
                } else {
                    //Valid, try parse float
                    try {
                        //Parse coord to float
                        float coord = Float.parseFloat(parts[1].substring(1, parts[1].length()));

                        //Valid
                        lon = parts[1];

                    } catch (NumberFormatException e) {
                        //Failed
                        lon = "";
                    }
                }

                //Validate
                if (lat.equalsIgnoreCase("") || lon.equalsIgnoreCase("")) {
                    //Invalid
                    System.out.println("Invalid GPS Location. Please try again.");
                } else {
                    //Valid, set
                    this.modelAux.setLocalization(reply.trim());

                    //Can go away
                    return true;
                }
            }
        }

        //Not expected to reach this point
        return false;
    }

    /**
     * AR06.2
     *
     * @return
     * @throws Exception
     */
    private void showAddressPhoto() {
        System.out.println("Showing Photo of the location:" + this.modelAux.getLocalization());
    }

    private void successMessage() {
        //Log
        System.out.println("Nearby Surrounding '" + this.modelAux.getDescription() + "' Created with Success!");
    }

    private void save() {
        //Log
        System.out.println("Saving new Nearby Surrounding...");

        //Save
        this.controller.save(modelAux);

        //Write Success Message
        this.successMessage();
    }

    private void pressAnyKeyToContinue() {
        //Log
        System.out.println("Press any key to continue...");

        //Read
        in.nextLine();
    }

}
