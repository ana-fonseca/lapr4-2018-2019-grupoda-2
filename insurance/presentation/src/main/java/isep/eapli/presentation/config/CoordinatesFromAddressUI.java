package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.georef.controller.AdressSearchController;
import java.util.Scanner;

public class CoordinatesFromAddressUI extends AbstractUI {

    @Override
    protected boolean doShow() {
        System.out.print("Please enter an address line (street): ");
        Scanner in = new Scanner(System.in);
        String street = in.nextLine();
        boolean cont = false;
        String postalCode = "";
        while (!cont) {
            System.out.print("Please enter a postal code (XXXX-XXX): ");
            postalCode = in.nextLine();
            cont = postalCode.matches("[0-9]{4}-[0-9]{3}");
        }
        System.out.print("Please enter a locality/city: ");
        String locality = in.nextLine();

        cont = false;
        boolean useDefault = true;
        
        System.out.println("");

        System.out.println("Do you want to use the Google Maps Services (default) or the Microsoft Bing Services? [GMS/MBS]");
        System.out.println("If entered anything else than MBS, the default will be used.");
        
        useDefault = !in.nextLine().equalsIgnoreCase("BMS");

        if (useDefault) {
            System.out.println("Coordinates of the entered place (GMS): " + new AdressSearchController().addressSearch(locality, postalCode, locality));
        } else {
            System.out.println("Coordinates of the entered place (MBS): " + new AdressSearchController().addressSearchBMS(locality, postalCode, locality));
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Coordinates from partial postal address";
    }

}
