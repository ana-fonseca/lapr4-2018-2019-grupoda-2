package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class CharacterizeMatrixAction implements Action {

    /**
     * Action method that shows the UI of the CharacterizeMatrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new CharacterizeMatrixUI().show();
    }
}
