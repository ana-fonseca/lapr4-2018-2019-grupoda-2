package isep.eapli.presentation.config;

import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;

public class RiskAnalystConfigUI extends AbstractUI {

    private RiskAnalyst currentRA;

    public RiskAnalystConfigUI(String email) {
        currentRA = new RepositoryFactory().getRiskAnalystDB().findByEmail(email);
    }

    @Override
    protected boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer menuRenderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return menuRenderer.render();
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();
        mainMenu.addItem(new MenuItem(1, "List Pending Unassigned Requests\n", new ListPendingUnassignedRequestsAction(currentRA)));
        mainMenu.addItem(new MenuItem(2, "List Pending Requests Assigned To You\n", new ListPendingAssignedRequestsAction(currentRA)));
        mainMenu.addItem(new MenuItem(3, "List Validated Requests Assigned To You\n", new ListValidatedAssignedRequestsAction(currentRA)));
        mainMenu.addItem(new MenuItem(4, "Create a new NearBySurrounding\n", new CreateNearbySurroundingAction()));
        mainMenu.addItem(new MenuItem(5, "List created Surrounding\n", new ListSurroundingsAction()));
        mainMenu.addItem(new MenuItem(0, "Log off", new ExitWithMessageAction()));
        return mainMenu;
    }

    @Override
    public String headline() {
        return "Risk Analyst Menu";
    }

}
