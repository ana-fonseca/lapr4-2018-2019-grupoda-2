package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.BaseMatrixController;

import java.io.IOException;

public class ExportBaseMatrixUI extends AbstractUI {

    /**
     * Base Matrix Controller attribute.
     */
    private BaseMatrixController bmc = new BaseMatrixController();

    @Override
    protected boolean doShow() {
        try {
            if (bmc.exportBaseMatrix("importBaseMatrix.csv")) {
                System.out.println("File exported successfuly.");
            } else {
                System.out.println("Couldn't export file.");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Export base matrix";
    }
}
