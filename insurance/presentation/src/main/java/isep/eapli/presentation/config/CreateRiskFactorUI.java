package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.CreateRiskFactorController;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;

class CreateRiskFactorUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private CreateRiskFactorController crfc = new CreateRiskFactorController();

    /**
     * Method that shows the UI of the Create Risk Factor UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        Iterable<SurroundingType> stList = crfc.getAllSurroundingTypes();
        System.out.println("Surrounding Types in the DB:\n");
        for (SurroundingType st : stList) {
            System.out.println(st);
        }
        System.out.println("");
        String type = Console.readLine("Insert the Risk Factor Surrounding Type: ");
        String metric = Console.readLine("Insert the Risk Factor Metric: ");
        if (!crfc.registerRiskFactor(type, metric)) {
            System.out.println("There was an error creating the new risk factor.");
        } else {
            System.out.println("Risk Factor created successfully!");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "Create Risk Factor";
    }
}