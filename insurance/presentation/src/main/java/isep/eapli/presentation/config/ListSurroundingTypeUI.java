package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.ListSurroundingTypeController;
import isep.eapli.core.domain.SurroundingType;

/**
 *
 * @author 1171138
 */
public class ListSurroundingTypeUI extends AbstractUI {
    /**
     * Class object that connects to the Controller
     */
    private ListSurroundingTypeController lstc = new ListSurroundingTypeController();

    /**
     * Method that shows the UI of the ListSurrouningType UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        System.out.println("===== Surrounding Type List =====");
        int counter = 0;
        for (SurroundingType sType : lstc.getAllSurroundingTypes()) {
            System.out.println(sType);
            counter++;
        }
        if (counter == 0) {
            System.out.println("There are no surrounding type data to show.");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "List Surrounding Types:";
    }
}
