package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.ListRiskFactorController;
import isep.eapli.core.domain.RiskFactor;

public class ListRiskFactorUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private ListRiskFactorController lrfc = new ListRiskFactorController();

    /**
     * Method that shows the UI of the ListRiskFactor UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        System.out.println("===== Risk Factor List =====");
        int counter = 0;
        for (RiskFactor riskFactor : lrfc.getAllRiskFactor()) {
            System.out.println(riskFactor);
            counter++;
        }
        if (counter == 0) {
            System.out.println("There are no RiskFactor data to show.");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "List Risk Factor:";
    }
}
