package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.riskassessment.controller.ListSurroundingsController;
import java.util.List;
import java.util.Scanner;

public class ListSurroundingsUI extends AbstractUI {

    //Create a Scanner to read input from the console
    private final Scanner sc = new Scanner(System.in);

    /**
     * Class object that connects to the Controller
     */
    private ListSurroundingsController lsc = new ListSurroundingsController();

    /**
     * Method that shows the UI of the ListSurrouningType UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        System.out.println("===== Surrounding List =====");

        List<NearBySurrounding> listSurroundings = lsc.getList();

        if (listSurroundings.isEmpty()) {
            System.out.println("No information to display");
        } else {
            System.out.println("Specific distric? (only press enter to ignore)");
            String district = sc.nextLine().trim();
            if (!district.isEmpty()) {
                listSurroundings = lsc.filterByDistrict(district.trim());
            } else {
                System.out.println("Default area search? (y/Y - yes | n/N - no)");
                String defaultAreaSearch = sc.nextLine().trim();
                if (defaultAreaSearch.equalsIgnoreCase("y")) {
                    listSurroundings = lsc.filterByDefaultArea();
                }
            }

            System.out.println("- Print Surroundings -\n");

            for (NearBySurrounding sr : listSurroundings) {
                System.out.println("Description:" + sr.getDescription() + "\nLocalization:" + sr.getLocalization() + "\nType:" + sr.getType().obtainDescription() + "\n");
            }

            System.out.println("Save in XHTML? (y/Y - yes | n/N - no)");
            String saveXHTML = sc.nextLine();
            if (saveXHTML.equalsIgnoreCase("y")) {
                lsc.saveListSurroundings();
            }
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "List Surrounding: ";
    }

}
