package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.CharacterizeMatrixController;
import isep.eapli.config.utils.ExportInformation;

public class CharacterizeMatrixUI extends AbstractUI {

    private CharacterizeMatrixController cmc;

    /**
     * Constructor to initialize the controller
     */
    public CharacterizeMatrixUI(){
        cmc=new CharacterizeMatrixController();
    }

    /**
     * User interface
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        try{
            ExportInformation ex = new ExportInformation();
            ex.exportCharacterizedAndDetailedBaseMatrix("characterizedMatrixWeightBootstrapper.csv");
            ex.exportCharacterizedAndDetailedBaseMatrix("characterizedMatrixNecessityBootstrapper.csv");
            ex.exportCharacterizedAndDetailedBaseMatrix("characterizedMatrixContributionBootstrapper.csv");
        }catch(IllegalArgumentException e){
            System.out.println("Error characterizing the Risk Matrix");
            return false;
        }
        System.out.println("3 files we're exported to be filled and inputted again");
        Console.readLine("Press any key to begin the import");
        String fileNameWeight = "characterizedMatrixWeightBootstrapper.csv";
        String fileNameNecessity = "characterizedMatrixNecessityBootstrapper.csv";
        String fileNameContribution = "characterizedMatrixContributionBootstrapper.csv";
        try{
            cmc.characterizeMatrix(fileNameWeight, fileNameNecessity, fileNameContribution);
        }catch(IllegalArgumentException e){
            System.out.println("Error characterizing the risk matrix");
            return false;
        }
        System.out.println("Matrix characterized successfully!!");
        return false;
    }

    @Override
    public String headline() {
        return "Characterize Risk Matrix";
    }
}
