package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class GeoreferenceAction implements Action {

    public GeoreferenceAction() {
    }

    @Override
    public boolean execute() {
        new GeoReferenceUI().mainLoop();
        return false;
    }
    
}
