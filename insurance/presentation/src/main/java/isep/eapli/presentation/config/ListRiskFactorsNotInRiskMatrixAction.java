package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

import javax.swing.*;

public class ListRiskFactorsNotInRiskMatrixAction implements Action {

    @Override
    public boolean execute() {
        return new ListRiskFactorsNotInRiskMatrixUI().show();
    }
}
