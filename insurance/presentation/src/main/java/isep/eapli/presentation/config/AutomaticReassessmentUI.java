/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.core.domain.Request;
import isep.eapli.riskassessment.controller.InsuranceCalculationController;
import java.util.Scanner;

/**
 *
 * @author Luís Silva
 */
public class AutomaticReassessmentUI extends AbstractUI{

    private Request request;
    private RequestAnalysisController rac;
    private final InsuranceCalculationController calculationController;

    public AutomaticReassessmentUI(Request request) {
        this.request = request;
        rac = new RequestAnalysisController(request);
        calculationController = new InsuranceCalculationController();

    }

    @Override
    protected boolean doShow() {
        boolean verificar = true; //obriga a fazer uma alteração
        while (verificar) {
            Scanner in = new Scanner(System.in);
            String op;
            boolean cont = true;
            while (cont) {
                System.out.println("Deseja adicionar uma cobertura ao Pedido de Avaliação? [Y/N]");
                op = in.nextLine();
                switch (op) {
                    case "Y":
                        System.out.println("Introduza a cobertura a adicionar");
                        if (!rac.addCoverage(in.nextLine())) {
                            System.out.println("Não existe a cobertura introduzida");
                            break;
                        }
                        verificar = false;
                        break;
                    case "N":
                        cont = false;
                        break;
                    default:
                        break;
                }
            }
            cont = true;
            while (cont) {
                System.out.println("Deseja remover uma cobertura ao Pedido de Avaliação? [Y/N]");
                op = in.nextLine();
                switch (op) {
                    case "Y":
                        System.out.println("Introduza a cobertura a remover");
                        rac.removeCoverage(in.nextLine());
                        verificar = false;
                        break;
                    case "N":
                        cont = false;
                        break;
                    default:
                        break;
                }
            }
        }
        calculationController.InsuranceCalculationWithDetails(request);
        rac.AutomaticReassessment();
        return true;
    }

    @Override
    public String headline() {
        return "Solicitar uma reavaliação automática";
    }
    
}
