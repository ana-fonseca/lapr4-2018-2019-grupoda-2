package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.ListCoverageController;
import isep.eapli.core.domain.Coverage;

class ListCoverageUI extends AbstractUI {

    /**
     * Class object that connects to the Controller
     */
    private ListCoverageController lcc = new ListCoverageController();

    /**
     * Method that shows the UI of the ListCoverage UC
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        System.out.println("===== Coverage List =====");
        int counter = 0;
        for (Coverage coverage : lcc.getAllCoverages()) {
            System.out.println(coverage);
            counter++;
        }
        if (counter == 0) {
            System.out.println("There are no Coverage data to show.");
        }
        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    @Override
    public String headline() {
        return "List Coverage";
    }
}