package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class ExportBaseMatrixAction implements Action {

    /**
     * Action method that shows the UI of the Export Base Matrix
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new ExportBaseMatrixUI().doShow();
    }
}
