package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class RiskAssessmentComparisonAction implements Action {

    /**
     * Action method that shows the UI of the RiskAssessmentComparison
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new RiskAssessmentComparisonUI().show();
    }
}
