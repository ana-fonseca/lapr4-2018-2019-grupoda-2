package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.georef.controller.SurroundingsNearByController;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class SurroundingsNearByUI extends AbstractUI {

    @Override
    protected boolean doShow() {
        Scanner in = new Scanner(System.in);
        boolean end = false;
        String latitude = "";
        String longitude = "";
        String type = "";
        while (!end) {
            System.out.print("Please enter a latitude: ");
            latitude = in.nextLine();
            System.out.print("Please enter a longitude: ");
            longitude = in.nextLine();
            System.out.print("Please enter a surrounding type: ");
            type = in.nextLine();

            try {
                Double.parseDouble(latitude);
                Double.parseDouble(longitude);
                end = true;
            } catch (Exception e) {
                System.out.println("Please insert valid coordinates!");
            }
        }

        end = false;

        Map<String, String> info = SurroundingsNearByController.getSurroundings(latitude, longitude, type);
        if (info.isEmpty()) {
            System.out.println("There is no info to display.");
            return false;
        }

        List<String> col = new ArrayList<>(info.values());
        System.out.println("Info of Surroudings");
        Set<String> keys = info.keySet();
        for (int i = 0; i < info.size(); i++) {
            System.out.printf("Name=%s\nCoordinates=%s \n", col.get(i), keys.toArray()[i]);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Surroundings of a type around coordinates";
    }
    
}
