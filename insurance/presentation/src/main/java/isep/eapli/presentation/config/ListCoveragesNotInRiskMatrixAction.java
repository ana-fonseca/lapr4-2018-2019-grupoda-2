package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class ListCoveragesNotInRiskMatrixAction implements Action {

    /**
     * Action method that shows the UI of the ListCoveragesNotInRiskMatrix
     * @return boolean returned by show
     */
    @Override
    public boolean execute() { return new ListCoveragesNotInRiskMatrixUI().show();
    }
}
