/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.config.controller.RiskAssessmentRequestController;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.domain.RiskAnalyst;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Luís Silva
 */
public class RequestAnalysisUI extends AbstractUI {

    private Request request;
    private static RiskAssessmentRequestController raq = new RiskAssessmentRequestController();

    public RequestAnalysisUI(Request request) {
        this.request = request;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Request ID: " + request.obtainID());
        System.out.println("Matrix Version: " + request.getMatrixVersion());
        System.out.println("Requested Date: " + request.obtainRequestedDate());
        System.out.println("Assigned Date: " + request.obtainAssignedDate());
        System.out.println("Time since assignement: " + raq.timePassedSinceAssigned(request));
        Map<String, List<Coverage>> map = new LinkedHashMap<>();
        for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
            if (!map.containsKey(icc.obtainInsurance().getAddress().district().district())) {
                map.put(icc.obtainInsurance().getAddress().district().district(), new ArrayList<Coverage>());
            }
        }

        for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
            map.get(icc.obtainInsurance().getAddress().district().district()).add(icc.obtainCoverage());
        }
        
        float total = 0;

        System.out.println("Insurance-Coverage Connection List:");
        for (String ins : map.keySet()) {
            System.out.println("    Insurance Address District: " + ins);
            System.out.println("        Coverages: ");
            for (Coverage cov : map.get(ins)) {
                System.out.print("          " + cov.obtainDesignation());
                for (Result result : request.getResultList()) {
                    if (result.obtainIcc().obtainCoverage().equals(cov)) {
                        System.out.println(" - Result: " + result.obtainIndex());
                        total += result.obtainIndex();
                        break;
                    }
                }
            }
            System.out.println("    Total result: " + total);
        }
        System.out.println();
        

        final Menu menu = buildMainMenu();
        final MenuRenderer menuRenderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return menuRenderer.render();
    }

    @Override
    public String headline() {
        return "Analisar um pedido de avaliação";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();
        mainMenu.addItem(new MenuItem(1, "Exportar Pedido para XHTML\n", new ExportRequestToXhtmlAction(request)));
        mainMenu.addItem(new MenuItem(2, "Confirmar resultado do Pedido de Avaliação\n", new ConfirmResultRequestAction(request)));
        mainMenu.addItem(new MenuItem(3, "Atribuar diretamente o resultado\n", new DirectlyAssignResultAction(request)));
        mainMenu.addItem(new MenuItem(4, "Pedir uma reavaliação automática\n", new AutomaticReassessmentAction(request)));
        mainMenu.addItem(new MenuItem(0, "Log off", new ExitWithMessageAction()));
        return mainMenu;
    }

}
