package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.config.controller.RiskAssessmentRequestController;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.RiskAnalyst;
import java.util.Scanner;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.riskassessment.utils.FilterRiskAssessmentRequest;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 *
 * @author Maria Junqueira
 */
public class ListValidatedAssignedRequestsUI extends AbstractUI {

    private RiskAnalyst riskAnalyst;
    private static RiskAssessmentRequestController raq = new RiskAssessmentRequestController();
    private static ServiceXHTML xhtml = new ServiceXHTML();
    List<Request> requestList = new ArrayList<>();
    int counter = 0;
    private RequestAnalysisController rac;

    public ListValidatedAssignedRequestsUI(RiskAnalyst riskAnalyst) {
        this.riskAnalyst = riskAnalyst;
    }

    @Override
    protected boolean doShow() {
        boolean cont = true;
        LocalDateTime dates[] = new LocalDateTime[2];
        DateTimeFormatter formatterFull = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        while (cont) {
            System.out.println("Do you wish to filter through a certain time period) [Y/N]");
            Scanner in = new Scanner(System.in);
            String op = in.nextLine();
            switch (op) {
                case "Y":
                    boolean end = false;
                    while (!end) {
                        System.out.println("Please enter a start date (YYYY-MM-DD): ");
                        String startDate = in.nextLine();
                        System.out.println("Please enter a start time (hh:mm or leave blank for none): ");
                        String startTime = in.nextLine();
                        startDate += startTime.isEmpty() ? " 00:00" : " " + startTime;
                        if (startDate != null) {
                            try {
                                LocalDateTime start = LocalDateTime.parse(startDate, formatterFull);
                                dates[0] = start;
                                end = true;
                            } catch (Exception e) {
                                end = false;
                            }
                        }
                    }

                    end = false;

                    while (!end) {
                        System.out.println("Please enter a finish date (YYYY-MM-DD) or enter \"now\" to use the current date: ");
                        String finishDate = in.nextLine();
                        if (finishDate.equalsIgnoreCase("now")) {
                            finishDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                        }
                        System.out.println("Please enter a finish time (hh:mm or leave blank for none) or enter \"now\" to use the current time: ");
                        String finishTime = in.nextLine();
                        if (finishTime.equalsIgnoreCase("now")) {
                            finishTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
                        }
                        finishDate += finishTime.isEmpty() ? " 00:00" : " " + finishTime;
                        if (finishDate != null) {
                            try {
                                LocalDateTime finish = LocalDateTime.parse(finishDate, formatterFull);
                                dates[1] = finish;
                                end = true;
                            } catch (Exception e) {
                                end = false;
                            }
                        }
                    }

                    requestList = FilterRiskAssessmentRequest.filterByDate(raq.validatedRequestList(riskAnalyst), dates[0], dates[1]);
                    cont = false;
                    break;
                case "N":
                    requestList = raq.validatedRequestList(riskAnalyst);
                    cont = false;
                    break;
                default:
                    break;
            }
        }

        cont = !requestList.isEmpty();

        if (!cont) {
            System.out.println("There are no validated requests assigned to you");
            return false;
        }
        System.out.println("Summary:(quantity and average validation time)");

        String sum = raq.averageValidationTime(requestList, counter);
        System.out.println(sum);

        while (cont) {
            System.out.println("Do you want to export the requests and the summary ? [Y/N]");
            Scanner in = new Scanner(System.in);
            String op = in.nextLine();
            cont = false;
            switch (op) {
                case "Y":
                    String requestInfo = xhtml.getValidatedRequestsXHTML(requestList, sum);
                    boolean finish = false;
                    while (!finish) {
                        System.out.println("Introduza o nome do ficheiro a criar para onde pretende exportar a informação");
                        try {
                            rac = new RequestAnalysisController(requestList.get(0));
                            String fileName = in.nextLine();
                            finish = rac.exportToHtml(fileName, requestInfo);
                        } catch (Exception ex) {
                            Logger.getLogger(ExportRequestToXhtmlUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case "N":
                    for (Request request : requestList) {
                        System.out.println("Request ID: " + request.obtainID());
                        System.out.println("Matrix Version: " + request.getMatrixVersion());
                        System.out.println("Requested Date: " + request.obtainRequestedDate());
                        System.out.println("Assigned Date: " + request.obtainAssignedDate());
                        System.out.println("Time since assignement: " + raq.timePassedSinceAssigned(request));
                        Map<String, List<Coverage>> map = new LinkedHashMap<>();
                        for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                            if (!map.containsKey(icc.obtainInsurance().getAddress().district().district())) {
                                map.put(icc.obtainInsurance().getAddress().district().district(), new ArrayList<Coverage>());
                            }
                        }

                        for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                            map.get(icc.obtainInsurance().getAddress().district().district()).add(icc.obtainCoverage());
                        }

                        System.out.println("Insurance-Coverage Connection List:");
                        for (String ins : map.keySet()) {
                            System.out.println("    Insurance Address District: " + ins);
                            System.out.println("    " + map.get(ins).size() + " Coverages");
                        }
                        System.out.println();
                        System.out.println();
                        counter++;
                    }
                    break;
                default:
                    cont = true;
                    break;
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "List Validated Assigned Requests";
    }
}
