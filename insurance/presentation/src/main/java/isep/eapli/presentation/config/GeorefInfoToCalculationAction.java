/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

/**
 *
 * @author Luís Silva
 */
public class GeorefInfoToCalculationAction implements Action {

    public GeorefInfoToCalculationAction() {
    }

    @Override
    public boolean execute() {
        return new GeorefInfoToCalculationUI().doShow(); 
    }
    
}
