package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.riskassessment.controller.InsuranceCalculationController;
import isep.eapli.riskassessment.controller.SeveralInsurancesCalculationController;
import isep.eapli.riskassessment.utils.ServiceJSON;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InsuranceCalculationWithoutDetailsUI extends AbstractUI {

    /**
     * Path to file
     */
    private String[] givenPath;

    /**
     * Constructor with arguments
     *
     * @param givenPath path to file
     */
    protected InsuranceCalculationWithoutDetailsUI(String[] givenPath) {
        this.givenPath = givenPath;
    }

    @Override
    protected boolean doShow() {
        Request c;
        if (givenPath.length == 1) {
            c = ServiceJSON.readJSON(givenPath[0]);
        } else if (givenPath.length > 1){
            List<Request> requests = new ArrayList<>();
            for (String str : givenPath) {
                requests.add(ServiceJSON.readJSON(str));
            }
            SeveralInsurancesCalculationController sicc = new SeveralInsurancesCalculationController();
            sicc.SeveralInsurancesCalculationWithoutDetails(requests);
            RepositoryFactory rf = new RepositoryFactory();
            int last = (int) rf.getRequestDB().size();
            int counter = 0;
            for (Request cas : rf.getRequestDB()) {
                if (counter > (last - requests.size())) {
                    for (Result r : cas.getResultList()) {
                        System.out.println(r.toString());
                    }
                    try {
                        ServiceJSON.writeJSON(cas);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                counter++;
            }
            return false;
        } else {
            System.out.println("No path specified");
            return false;
        }
        InsuranceCalculationController calc = new InsuranceCalculationController();
        calc.InsuranceCalculationWithoutDetails(c);
        RepositoryFactory rf = new RepositoryFactory();
        int last = (int) rf.getRequestDB().size();
        int counter = 0;
        for (Request cas : rf.getRequestDB()) {
            if (counter == last - 1) {
                for (Result r : cas.getResultList()) {
                    System.out.println(r.toString());
                }
                try {
                    ServiceJSON.writeJSON(cas);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            counter++;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Risk Assessment Evaluation";
    }
}
