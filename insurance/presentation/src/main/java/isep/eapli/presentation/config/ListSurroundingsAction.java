package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class ListSurroundingsAction implements Action {

    @Override
    public boolean execute() {
        return new ListSurroundingsUI().doShow();
    }

}
