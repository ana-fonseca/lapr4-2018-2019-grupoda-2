package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.core.domain.RiskAnalyst;

public class ListPendingUnassignedRequestsAction implements Action {
    
    private RiskAnalyst riskAnalyst;
    
    public ListPendingUnassignedRequestsAction (RiskAnalyst ra) {
        riskAnalyst = ra;
    }
    
    @Override
    public boolean execute() {
        return new ListPendingUnassignedRequestsUI(riskAnalyst).doShow();
    }
}
