package isep.eapli.presentation.config;

import eapli.framework.actions.Action;
import isep.eapli.core.domain.RiskAnalyst;

/**
 *
 * @author Maria Junqueira
 */
public class ListValidatedAssignedRequestsAction implements Action{
    
    private RiskAnalyst riskAnalyst;

    public ListValidatedAssignedRequestsAction(RiskAnalyst ra) {
        this.riskAnalyst = ra;
    }
    
    @Override
    public boolean execute(){
        return new ListValidatedAssignedRequestsUI(riskAnalyst).doShow();
    }
    
    
}