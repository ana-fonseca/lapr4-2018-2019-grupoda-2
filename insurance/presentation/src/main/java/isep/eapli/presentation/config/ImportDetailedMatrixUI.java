/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.CreateCoverageController;
import isep.eapli.config.controller.ImportDetailedMatrixController;
import isep.eapli.config.controller.ListCoverageController;
import isep.eapli.config.utils.ExportInformation;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rikar
 */
class ImportDetailedMatrixUI extends AbstractUI {
    /**
     * Class object that connects to the Controller
     */
    private ImportDetailedMatrixController importDetailedMatrixController = new ImportDetailedMatrixController();

    /**
     * Method that shows the UI of the CreateCoverage UC
     *
     * @return false
     */
    protected boolean doShow() {
        try{
            ExportInformation ex = new ExportInformation();
            ex.exportCharacterizedAndDetailedBaseMatrix("importDetailedRiskMatrix.csv");
        }catch(IllegalArgumentException e){
            System.out.println("Error detailing the Risk Matrix");
            return false;
        }

        System.out.println("1 file was exported to be filled and inputted again");
        Console.readLine("Press any key to begin the import");
        String file = "importDetailedRiskMatrix.csv";

        try {
            if (importDetailedMatrixController.importDetailedMatrix(file)) {
                System.out.println("Detailed Matrix imported successfully!");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file.");
            return false;
        }

        return false;
    }

    /**
     * Headline to be used during the UI execution
     *
     * @return headline
     */
    public String headline() {
        return "Import Detailed Matrix";
    }
    
}
