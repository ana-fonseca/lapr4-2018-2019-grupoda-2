package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.riskassessment.controller.InsuranceCalculationController;
import isep.eapli.riskassessment.controller.SeveralInsurancesCalculationController;
import isep.eapli.riskassessment.utils.ServiceJSON;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class InsuranceCalculationWithDetailsUI extends AbstractUI {

    /**
     * Path to file
     */
    private String[] givenPath;

    /**
     * Constructor with arguments
     *
     * @param givenPath path to file
     */
    protected InsuranceCalculationWithDetailsUI(String[] givenPath) {
        this.givenPath = givenPath;
    }

    @Override
    protected boolean doShow() {
        Request c;
        if (givenPath.length == 1) {
            c = ServiceJSON.readJSON(givenPath[0]);
        } else if (givenPath.length > 1){
            List<Request> requests = new ArrayList<>();
            for (String str : givenPath) {
                requests.add(ServiceJSON.readJSON(str));
            }
            SeveralInsurancesCalculationController sicca = new SeveralInsurancesCalculationController();
            sicca.SeveralInsurancesCalculationWithDetails(requests);
            RepositoryFactory raf = new RepositoryFactory();
            int last = (int) raf.getRequestDB().size();
            int count = 0;
            for (Request cas : raf.getRequestDB()) {
                if (count > (last - requests.size())) {
                    for (Result r : cas.getResultList()) {
                        System.out.println(r.toString());
                    }
                    System.out.println();
                }
                count++;
            }
            return false;
        } else {
            System.out.println("No path specified");
            return false;
        }
        InsuranceCalculationController calc = new InsuranceCalculationController();
        calc.InsuranceCalculationWithDetails(c);
        RepositoryFactory rf = new RepositoryFactory();
        int lastoOne = (int) rf.getRequestDB().size();
        int count = 0;
        for (Request cas : rf.getRequestDB()) {
            if (count == lastoOne - 1) {
                for (Result re : cas.getResultList()) {
                    System.out.println(re.toString());
                    for (Explanation s: re.obtainExplanationList()){
                        System.out.println(s);
                    }
                }
                try {
                    ServiceJSON.writeJSON(cas);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            count++;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Risk Assessment Evaluation";
    }
}
