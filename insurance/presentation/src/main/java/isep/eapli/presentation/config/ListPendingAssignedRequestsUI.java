package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RiskAssessmentRequestController;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.RiskAnalyst;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ListPendingAssignedRequestsUI extends AbstractUI {

    private RiskAnalyst riskAnalyst;
    private static RiskAssessmentRequestController raq = new RiskAssessmentRequestController();

    public ListPendingAssignedRequestsUI(RiskAnalyst riskAnalyst) {
        this.riskAnalyst = riskAnalyst;
    }

    /**
     * This method is AR03 UI
     *
     * @return true if everything went correctly
     */
    @Override
    protected boolean doShow() {
        for (Request request : raq.pendingRequestList(riskAnalyst)) {
            System.out.println("Request ID: " + request.obtainID());
            System.out.println("Matrix Version: " + request.getMatrixVersion());
            System.out.println("Requested Date: " + request.obtainRequestedDate());
            System.out.println("Assigned Date: " + request.obtainAssignedDate());
            System.out.println("Time since assignement: " + raq.timePassedSinceAssigned(request));

            Map<String, List<Coverage>> map = new LinkedHashMap<>();
            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                if (!map.containsKey(icc.obtainInsurance().getAddress().district().district())) {
                    map.put(icc.obtainInsurance().getAddress().district().district(), new ArrayList<Coverage>());
                }
            }

            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                map.get(icc.obtainInsurance().getAddress().district().district()).add(icc.obtainCoverage());
            }

            System.out.println("Insurance-Coverage Connection List:");
            for (String ins : map.keySet()) {
                System.out.println("    Insurance Address District: " + ins);
                System.out.println("    " + map.get(ins).size() + " Coverages");
            }
            System.out.println();
        }

        boolean cont = raq.pendingRequestList(riskAnalyst).isEmpty() ? false : true;

        System.out.println(cont == false ? "There are no pending requests assigned to you" : "");

        while (cont) {
            System.out.println("Do you want to start analysing a request? [Y/N]");
            Scanner in = new Scanner(System.in);
            String op = in.nextLine();
            switch (op) {
                case "Y":
                    System.out.println("Please enter a request ID: ");
                    long id = -1;
                    try {
                        id = Long.parseLong(in.nextLine());
                    } catch (Exception e) {
                        break;
                    }

                    Request chosen = null;

                    for (Request request : raq.pendingRequestList(riskAnalyst)) {
                        if (request.obtainID() == id) {
                            chosen = request;
                        }
                    }

                    if (chosen == null) {
                        break;
                    }

                    new RequestAnalysisAction(chosen).execute();
                    cont = false;
                    break;
                case "N":
                    cont = false;
                    break;
                default:
                    break;
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "List Pending Assigned Requests";
    }

}
