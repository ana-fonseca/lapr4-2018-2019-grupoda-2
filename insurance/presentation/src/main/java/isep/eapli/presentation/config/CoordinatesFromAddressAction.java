package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class CoordinatesFromAddressAction implements Action{

    @Override
    public boolean execute() {
        return new CoordinatesFromAddressUI().doShow();
    }
    
}
