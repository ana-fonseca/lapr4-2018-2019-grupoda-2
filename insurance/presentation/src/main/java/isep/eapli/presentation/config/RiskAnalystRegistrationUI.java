/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.AuthenticationController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author becas
 */
public class RiskAnalystRegistrationUI extends AbstractUI {

    private AuthenticationController ac = new AuthenticationController();

    @Override
    protected boolean doShow() {
        try {
            String email = Console.readLine("Insert the email: ");
            String password = Console.readLine("Insert the password: ");
            if (!ac.RiskAnalystRegistration(email, password)) {
                System.out.println("Account already Exists");
                return false;
            } else {
                System.out.println("Registration Successful");
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Invalid password. It must have at least one uppercase letter, one lowercase letter, one number and one symbol (@#$%^&+=)!");
            return false;
        }
    }

    @Override
    public String headline() {
        return "Risk Analyst Registration";
    }
}
