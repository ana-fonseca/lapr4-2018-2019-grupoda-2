package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class DistanceCalculatorAction implements Action {

    @Override
    public boolean execute() {
        return new DistanceCalculatorUI().doShow();
    }
    
}
