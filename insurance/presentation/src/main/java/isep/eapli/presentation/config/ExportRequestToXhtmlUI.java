/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.core.domain.Request;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luís Silva
 */
public class ExportRequestToXhtmlUI extends AbstractUI {
    
    private Request request;
    private static ServiceXHTML xhtml = new ServiceXHTML();
    private RequestAnalysisController rac;
    
    public ExportRequestToXhtmlUI(Request request) {
        this.request = request;
        rac = new RequestAnalysisController(request);
    }

    @Override
    protected boolean doShow() {
        System.out.println("Deseja confirmar o resultado? [Y/N]");
        Scanner in = new Scanner(System.in);
        String op = in.nextLine();
        switch (op) {
            case "Y":
                String requestInfo = xhtml.getContentInXhtml(request);
                System.out.println("Introduza o nome do ficheiro a criar para onde pretende exportar a informação");
        {
            try {
                return rac.exportToHtml(in.nextLine(), requestInfo);
            } catch (IOException ex) {
                Logger.getLogger(ExportRequestToXhtmlUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;
            case "N":
                return new RequestAnalysisAction(request).execute();
            default:
                break;
        }
        return false;
    }

    @Override
    public String headline() {
        return "Exportar Pedido para XHTML";
    }
    
}
