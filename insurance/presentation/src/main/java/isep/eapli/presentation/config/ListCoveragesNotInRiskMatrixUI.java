package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import isep.eapli.config.controller.ListCoveragesNotInRiskMatrixController;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.RiskMatrix;

import java.util.List;
import java.util.Scanner;

public class ListCoveragesNotInRiskMatrixUI extends AbstractUI {

    private ListCoveragesNotInRiskMatrixController listCoveragesNotInRiskMatrixController;

    /**
     * Constructor to initialize the controller
     */
    public ListCoveragesNotInRiskMatrixUI(){
        listCoveragesNotInRiskMatrixController = new ListCoveragesNotInRiskMatrixController();
    }

    /**
     * User interface
     *
     * @return false
     */
    @Override
    protected boolean doShow() {
        Scanner scanner = new Scanner(System.in);
        try {
            List<RiskMatrix> listRiskMatrix = listCoveragesNotInRiskMatrixController.showRiskMatrices();
            if(listRiskMatrix.size() == 0) {
                System.out.println("There are not any published risk matrix in the data base !");
            }else {
                int counter = 0;
                for (RiskMatrix riskMatrix : listRiskMatrix) {
                    System.out.println(counter+1 + ": " + riskMatrix.riskMatrixPresentation());
                    counter++;
                }
                int inputNumber = Console.readInteger("Choose a risk matrix, by inserting it's number: ");
                RiskMatrix riskMatrix = listRiskMatrix.get(inputNumber-1);
                try {
                    System.out.println("List of coverages that are not configured in the risk matrix chosen: \n");
                    List<Coverage> listCoverages = listCoveragesNotInRiskMatrixController.listCoveragesNotInRiskMatrix(riskMatrix);
                    if(listCoverages.isEmpty()){
                        System.out.println("Every existent coverage is in the Risk Matrix");
                        return false;
                    }else {
                        for (Coverage coverage : listCoverages) {
                            System.out.println(coverage);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("Error publication the risk matrix");
                    return false;
                }
            }
        }catch (IndexOutOfBoundsException e) {
            System.out.println("Number not found !");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Coverages not configured in a risk Matrix";
    }
    }