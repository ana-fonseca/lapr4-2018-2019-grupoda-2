/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.presentation.config;

import eapli.framework.presentation.console.AbstractUI;
import isep.eapli.config.controller.RequestAnalysisController;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Luís Silva
 */
public class DirectlyAssignResultUI extends AbstractUI{

    private Request request;
    private RequestAnalysisController rac;
    
    public DirectlyAssignResultUI(Request request) {
        this.request = request;
        rac = new RequestAnalysisController(request);
    }

    @Override
    protected boolean doShow() {
        System.out.println("Deseja atribuir diretamente o resultado? [Y/N]");
        Scanner in = new Scanner(System.in);
        List<Integer> results = new ArrayList<>();
        String op = in.nextLine();
        switch (op) {
            case "Y":
                for (Result result : request.getResultList()) {
                    System.out.println("Adicione o resultado para esta cobertura  "+ result.obtainIcc());
                    results.add(in.nextInt());
                }
                rac.AssignResult(results);
                System.out.println("Adicione uma fundamentação");
                rac.addStatement(in.nextLine());
                break;
            case "N":
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public String headline() {
        return "Atribuar diretamente o resultado";
    }
    
}
