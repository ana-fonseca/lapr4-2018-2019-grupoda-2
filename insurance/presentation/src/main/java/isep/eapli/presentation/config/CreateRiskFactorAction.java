package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

class CreateRiskFactorAction implements Action {

    /**
     * Action method that shows the UI of the CreateRiskFactor
     *
     * @return boolean returned by show
     */
    @Override
    public boolean execute() {
        return new CreateRiskFactorUI().show();
    }
}
