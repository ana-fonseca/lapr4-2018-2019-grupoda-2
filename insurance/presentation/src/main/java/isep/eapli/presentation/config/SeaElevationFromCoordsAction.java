package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class SeaElevationFromCoordsAction implements Action {

    @Override
    public boolean execute() {
        return new SeaElevationFromCoordsUI().doShow();
    }
    
}
