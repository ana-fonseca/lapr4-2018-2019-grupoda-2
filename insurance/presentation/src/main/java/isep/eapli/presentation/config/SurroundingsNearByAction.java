package isep.eapli.presentation.config;

import eapli.framework.actions.Action;

public class SurroundingsNearByAction implements Action {

    @Override
    public boolean execute() {
        return new SurroundingsNearByUI().doShow();
    }
    
}
