package isep.eapli.config.bootstrapers;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoverageBootstrapperTest {

    @Test
    public void createAllInstancesBootstrapper() throws IntegrityViolationException {
        RepositoryFactory rp = new RepositoryFactory();
        CoverageDB cdb = rp.getCoverageDB();
        CoverageBootstrapper cb = new CoverageBootstrapper();
        cb.execute();
        assertTrue(cdb.checkIfAlreadyExists(new Coverage("Storm")));
        assertTrue(cdb.checkIfAlreadyExists(new Coverage("Fire")));
        assertTrue(cdb.checkIfAlreadyExists(new Coverage("Hurricane")));
        assertTrue(cdb.checkIfAlreadyExists(new Coverage("Earthquake")));
        cdb.delete(new Coverage("Storm"));
        cdb.delete(new Coverage("Fire"));
        cdb.delete(new Coverage("Hurricane"));
        cdb.delete(new Coverage("Earthquake"));
    }
}