/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.bootstrapers;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.config.controller.CreateSurroundingTypeController;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1171138
 */
public class SurroundingTypeBootstrapperTest {
    
    public SurroundingTypeBootstrapperTest() {
    }
    
    @Test
    public void testBootstrapper() throws IntegrityViolationException {
        RepositoryFactory rp = new RepositoryFactory();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        SurroundingTypeBootstrapper cb = new SurroundingTypeBootstrapper();
        cb.execute();
        assertTrue(stdb.checkIfAlreadyExists(new SurroundingType("River")));
        assertTrue(stdb.checkIfAlreadyExists(new SurroundingType("Fire Fighter Station")));
        assertTrue(stdb.checkIfAlreadyExists(new SurroundingType("Hospital")));
        assertTrue(stdb.checkIfAlreadyExists(new SurroundingType("Police")));
        stdb.delete(new SurroundingType("River"));
        stdb.delete(new SurroundingType("Fire Fighter Station"));
        stdb.delete(new SurroundingType("Hospital"));
        stdb.delete(new SurroundingType("Police"));
    }
    
}
