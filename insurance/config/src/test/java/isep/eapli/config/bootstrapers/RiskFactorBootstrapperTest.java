package isep.eapli.config.bootstrapers;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.config.controller.CreateSurroundingTypeController;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RiskFactorBootstrapperTest {

    @Test
    public void execute() throws IntegrityViolationException {
        RepositoryFactory rp = new RepositoryFactory();
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        RiskFactorBootstrapper rfb = new RiskFactorBootstrapper();
        CreateSurroundingTypeController cstc = new CreateSurroundingTypeController();
        cstc.registerSurroundingType("River");
        cstc.registerSurroundingType("Fire Fighter Station");
        rfb.execute();
        assertTrue(rfdb.checkIfAlreadyExists(new RiskFactor(new SurroundingType("River"), new Metric("Distance"))));
        assertTrue(rfdb.checkIfAlreadyExists(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance"))));
        assertTrue(rfdb.checkIfAlreadyExists(new RiskFactor(new SurroundingType("River"), new Metric("Time"))));
        assertTrue(rfdb.checkIfAlreadyExists(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time"))));
        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        stdb.delete(new SurroundingType("River"));
        stdb.delete(new SurroundingType("Fire Fighter Station"));
    }
}