package isep.eapli.config.controller;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;
import org.junit.Ignore;

public class CreateRiskFactorControllerTest {

    CreateRiskFactorController crfc = new CreateRiskFactorController();
    CreateSurroundingTypeController cstc = new CreateSurroundingTypeController();

    @Ignore
    public void testAllTheControllerFunctions() throws IntegrityViolationException {
        cstc.registerSurroundingType("Storm");
        cstc.registerSurroundingType("Fire");
        cstc.registerSurroundingType("Hurricane");
        crfc.registerRiskFactor("Storm", "KM");
        crfc.registerRiskFactor("Fire", "KM");
        crfc.registerRiskFactor("Hurricane", "KM");
        LinkedList<SurroundingType> expectedSTList = new LinkedList<>();
        LinkedList<RiskFactor> expectedRFList = new LinkedList<>();
        expectedSTList.add(new SurroundingType("Storm"));
        expectedSTList.add(new SurroundingType("Fire"));
        expectedSTList.add(new SurroundingType("Hurricane"));
        expectedRFList.add(new RiskFactor(new SurroundingType("Storm"), new Metric("KM")));
        expectedRFList.add(new RiskFactor(new SurroundingType("Fire"), new Metric("KM")));
        expectedRFList.add(new RiskFactor(new SurroundingType("Hurricane"), new Metric("KM")));
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        RiskFactorDB rfdb = new RiskFactorDB("DomainDB");
        for (SurroundingType st : stdb.findAll()) {
            if (!expectedSTList.contains(st)) {
                System.out.println(st);
                fail("Not all surrounding types in the DB");
            }
        }
        for (RiskFactor rf : rfdb.findAll()) {
            if (!expectedRFList.contains(rf)) {
                System.out.println(rf);
                fail("Not all risk factors in the DB");
            }
        }
        rfdb.delete(new RiskFactor(new SurroundingType("Storm"), new Metric("KM")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire"), new Metric("KM")));
        rfdb.delete(new RiskFactor(new SurroundingType("Hurricane"), new Metric("KM")));
        stdb.delete(new SurroundingType("Storm"));
        stdb.delete(new SurroundingType("Fire"));
        stdb.delete(new SurroundingType("Hurricane"));
    }

    @Test
    public void noSurroundingType() {
        assertFalse(crfc.registerRiskFactor("failure", "correct"));
    }
}