/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.controller;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.City;
import isep.eapli.core.domain.Country;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.District;
import isep.eapli.core.domain.Insurance;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.PostalCode;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.State;
import isep.eapli.core.domain.Street;
import isep.eapli.core.domain.Version;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Luís Silva
 */
public class RequestAnalysisControllerTest {
    
    public RequestAnalysisControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of confirmResult method, of class RequestAnalysisController.
     */
    @Test
    public void testConfirmResult() throws Exception {
        System.out.println("confirmResult");
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        instance.confirmResult();
    }

    /**
     * Test of addObsToResult method, of class RequestAnalysisController.
     */
    @Test
    public void testAddObsToResult() throws Exception {
        System.out.println("addObsToResult");
        String obs = "";
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        instance.addObsToResult(obs);
    }

    /**
     * Test of AssignResult method, of class RequestAnalysisController.
     */
    @Test
    public void testAssignResult() throws Exception {
        System.out.println("AssignResult");
        List<Integer> index = new ArrayList<>();
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        instance.AssignResult(index);
    }

    /**
     * Test of addStatement method, of class RequestAnalysisController.
     */
    @Test
    public void testAddStatement() throws Exception {
        System.out.println("addStatement");
        String statment = "";
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        instance.addStatement(statment);
    }

    /**
     * Test of AutomaticReassessment method, of class RequestAnalysisController.
     */
    @Test
    public void testAutomaticReassessment() throws Exception {
        System.out.println("AutomaticReassessment");
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        instance.AutomaticReassessment();
    }

    /**
     * Test of addCoverage method, of class RequestAnalysisController.
     */
    @Test
    public void testAddCoverage() throws Exception {
        System.out.println("addCoverage");
        String coverage = "Roubo";
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        boolean expResult = false;
        boolean result = instance.addCoverage(coverage);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeCoverage method, of class RequestAnalysisController.
     */
    @Test
    public void testRemoveCoverage() throws Exception {
        System.out.println("removeCoverage");
        String coverage = "Tempestade";
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        boolean expResult = true;
        boolean result = instance.removeCoverage(coverage);
        assertEquals(expResult, result);
    }

    /**
     * Test of exportToHtml method, of class RequestAnalysisController.
     */
    @Ignore
    public void testExportToHtml() throws Exception {
        System.out.println("exportToHtml");
        String fileName = "teste";
        String requestInfo = "<id>4</id>";
        Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();
        Insurance insu = new Insurance(da);
        Coverage cov = new Coverage("Fogo");
        Coverage cov1 = new Coverage("Tempestade");
        iccList.add(new InsuranceCoverageConnection(insu, cov));
        iccList.add(new InsuranceCoverageConnection(insu, cov1));
        Version v = new Version(30);
        Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
        RequestAnalysisController instance = new RequestAnalysisController(request);
        boolean expResult = false;
        boolean result = instance.exportToHtml(fileName, requestInfo);
        assertEquals(expResult, result);
    }
    
}
