package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ListRiskFactorControllerTest {

    @Test
    public void getAllRiskFactor() throws IntegrityViolationException, ConcurrencyException {
        RepositoryFactory rp = new RepositoryFactory();
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        rfdb.save(new RiskFactor(new SurroundingType("ST1"), new Metric("M1")));
        rfdb.save(new RiskFactor(new SurroundingType("ST2"), new Metric("M2")));
        ListRiskFactorController lrfc = new ListRiskFactorController();
        LinkedList<RiskFactor> expectedList = new LinkedList<>();
        expectedList.add(new RiskFactor(new SurroundingType("ST1"), new Metric("M1")));
        expectedList.add(new RiskFactor(new SurroundingType("ST2"), new Metric("M2")));
        int i = 0;
        for (RiskFactor receivedRiskFactor : lrfc.getAllRiskFactor()) {
            System.out.println(receivedRiskFactor);
            if (!expectedList.contains(receivedRiskFactor)) {
                System.out.println(receivedRiskFactor);
                fail("Not all risk factors were in the list");
            }
            i++;
        }
        assertEquals(expectedList.size(), i);
        i = 0;
        expectedList.clear();
        rfdb.delete(new RiskFactor(new SurroundingType("ST1"), new Metric("M1")));
        rfdb.delete(new RiskFactor(new SurroundingType("ST2"), new Metric("M2")));
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        stdb.delete(new SurroundingType("ST1"));
        stdb.delete(new SurroundingType("ST2"));
        for (RiskFactor ignored : lrfc.getAllRiskFactor()) {
            i++;
        }
        assertEquals(expectedList.size(), i);
    }
}