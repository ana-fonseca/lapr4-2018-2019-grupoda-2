package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;
import org.junit.Ignore;

public class ListCoverageControllerTest {

    public ListCoverageControllerTest(){

    }


    @Ignore
    public void getAllCoverages() throws IntegrityViolationException, ConcurrencyException {
        RepositoryFactory rp = new RepositoryFactory();
        CoverageDB cdb = rp.getCoverageDB();
        cdb.save(new Coverage("CoverageTest1"));
        cdb.save(new Coverage("CoverageTest2"));
        ListCoverageController lcc = new ListCoverageController();
        LinkedList<Coverage> expectedList = new LinkedList<>();
        expectedList.add(new Coverage("CoverageTest1"));
        expectedList.add(new Coverage("CoverageTest2"));
        int i = 0;
        for (Coverage receivedCoverage: lcc.getAllCoverages()){
            if (!expectedList.contains(receivedCoverage)){
                System.out.println(receivedCoverage);
                fail("Not all coverages were in the list");
            }
            i++;
        }
        assertEquals(expectedList.size(), i);
        i = 0;
        expectedList.clear();
        cdb.delete(new Coverage("CoverageTest1"));
        cdb.delete(new Coverage("CoverageTest2"));
        for (Coverage ignored : lcc.getAllCoverages()){
            i++;
        }
        assertEquals(expectedList.size(), i);
    }
}