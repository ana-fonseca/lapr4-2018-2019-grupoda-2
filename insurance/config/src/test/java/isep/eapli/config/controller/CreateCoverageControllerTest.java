package isep.eapli.config.controller;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import org.junit.Ignore;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class CreateCoverageControllerTest {

    private CreateCoverageController ccc;
    private CoverageDB cdb;

    public CreateCoverageControllerTest() {
        ccc = new CreateCoverageController();
        RepositoryFactory rp = new RepositoryFactory();
        cdb = rp.getCoverageDB();
    }

    @Ignore
    public void testAllTheControllerFunctions() throws IntegrityViolationException {
        ccc.registerCoverage("coverage1");
        ccc.registerCoverage("coverage2");
        ccc.registerCoverage("coverage3");
        LinkedList<Coverage> expectedList = new LinkedList<>();
        expectedList.add(new Coverage("coverage1"));
        expectedList.add(new Coverage("coverage2"));
        expectedList.add(new Coverage("coverage3"));
        for (Coverage tmp: cdb.findAll()){
            if (!expectedList.contains(tmp)){
                System.out.println("Failed: "+ tmp);
                fail("Not all coverages in the DB");
            }
        }
        cdb.delete(new Coverage("coverage1"));
        cdb.delete(new Coverage("coverage2"));
        cdb.delete(new Coverage("coverage3"));
    }

    @Ignore
    public void coverageAlreadyExists(){
        assertTrue(ccc.registerCoverage("coverage1"));
        assertFalse(ccc.registerCoverage("coverage1"));
        cdb.delete(new Coverage("coverage1"));
        assertFalse(cdb.checkIfAlreadyExists(new Coverage("coverage1")));
    }

    @Ignore
    public void coverageWithTheSameNameDifferentTypo(){
        assertTrue(ccc.registerCoverage("coverage1"));
        assertFalse(ccc.registerCoverage("cOveraGe1"));
        cdb.delete(new Coverage("coverage1"));
        assertFalse(cdb.checkIfAlreadyExists(new Coverage("coverage1")));
    }

    @Ignore
    public void coverageWithNullName(){
        assertTrue(ccc.registerCoverage("coverage1"));
        assertFalse(ccc.registerCoverage("cOveraGe1"));
        cdb.delete(new Coverage("coverage1"));
        assertFalse(cdb.checkIfAlreadyExists(new Coverage("coverage1")));
    }
}