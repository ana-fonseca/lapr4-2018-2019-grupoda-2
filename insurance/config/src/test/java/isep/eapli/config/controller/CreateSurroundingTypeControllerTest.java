package isep.eapli.config.controller;

import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author 1171138
 */
public class CreateSurroundingTypeControllerTest {
    
    public CreateSurroundingTypeControllerTest() {
    }
    
     @Ignore
    public void testAllTheControllerFunctions() throws IntegrityViolationException {
        CreateSurroundingTypeController stc = new CreateSurroundingTypeController();
        stc.registerSurroundingType("type1");
        stc.registerSurroundingType("type2");
        LinkedList<SurroundingType> expectedList = new LinkedList<>();
        expectedList.add(new SurroundingType("type1"));
        expectedList.add(new SurroundingType("type2"));
        for (SurroundingType tmp: stc.getAllSurroundingTypes()){
            if (!expectedList.contains(tmp)){
                System.out.println(tmp);
                fail("Not all types in the DB");
            }
        }
        SurroundingTypeDB stdb = new SurroundingTypeDB("DomainDB");
        stdb.delete(new SurroundingType("type1"));
        stdb.delete(new SurroundingType("type2"));
    }
    
}
