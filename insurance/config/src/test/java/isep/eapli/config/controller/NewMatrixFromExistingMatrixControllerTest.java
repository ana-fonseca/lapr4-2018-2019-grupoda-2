package isep.eapli.config.controller;

import isep.eapli.config.bootstrapers.CoverageBootstrapper;
import isep.eapli.config.bootstrapers.RiskFactorBootstrapper;
import isep.eapli.config.bootstrapers.RiskMatrixBootstrapper;
import isep.eapli.config.bootstrapers.SurroundingTypeBootstrapper;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class NewMatrixFromExistingMatrixControllerTest {

    @Test
    public void getAllDetailedCharacterizedRiskMatrices() {
        RepositoryFactory rp = new RepositoryFactory();
        RiskMatrixDB rmdb = rp.getRiskMatrixDB();
        for (RiskMatrix rm : rmdb.findAll()) {
            rmdb.remove(rm);
        }
        RiskMatrixBootstrapper rfb = new RiskMatrixBootstrapper();
        new CoverageBootstrapper().execute();
        new SurroundingTypeBootstrapper().execute();
        new RiskFactorBootstrapper().execute();
        rfb.execute();

        NewMatrixFromExistingMatrixController nmfemc = new NewMatrixFromExistingMatrixController();
        List<RiskMatrix> rmList = (List<RiskMatrix>) nmfemc.getAllDetailedCharacterizedRiskMatrices();
        //assertEquals(1, rmList.size());
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        CoverageDB cdb = rp.getCoverageDB();
        for (RiskMatrix rm : rmdb.findAll()) {
            rmdb.remove(rm);
        }

        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        stdb.delete(new SurroundingType("River"));
        stdb.delete(new SurroundingType("Fire Fighter Station"));
        stdb.delete(new SurroundingType("Hospital"));
        stdb.delete(new SurroundingType("Police"));
        cdb.delete(new Coverage("Hurricane"));
        cdb.delete(new Coverage("Earthquake"));
        cdb.delete(new Coverage("Fire"));
        cdb.delete(new Coverage("Storm"));
    }

    @Test
    public void registerNewMatrixFromExistingMatrix() {
        RepositoryFactory rp = new RepositoryFactory();
        RiskMatrixDB rmdb = rp.getRiskMatrixDB();
        for (RiskMatrix rm : rmdb.findAll()) {
            rmdb.remove(rm);
        }
        RiskMatrixBootstrapper rfb = new RiskMatrixBootstrapper();
        new CoverageBootstrapper().execute();
        new SurroundingTypeBootstrapper().execute();
        new RiskFactorBootstrapper().execute();
        rfb.execute();

        NewMatrixFromExistingMatrixController nmfemc = new NewMatrixFromExistingMatrixController();
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        CoverageDB cdb = rp.getCoverageDB();
        List<RiskMatrix> rmList = (List<RiskMatrix>) rmdb.findAll();
        RiskMatrix riskMatrix = rmList.get(0);
        assertTrue(nmfemc.registerNewMatrixFromExistingMatrix(riskMatrix));
        rmList = (List<RiskMatrix>) rmdb.findAll();
//        assertEquals(2, rmList.size());
        for (RiskMatrix rm : rmList) {
            rmdb.remove(rm);
        }

        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        rfdb.delete(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        stdb.delete(new SurroundingType("River"));
        stdb.delete(new SurroundingType("Fire Fighter Station"));
        stdb.delete(new SurroundingType("Hospital"));
        stdb.delete(new SurroundingType("Police"));
        cdb.delete(new Coverage("Hurricane"));
        cdb.delete(new Coverage("Earthquake"));
        cdb.delete(new Coverage("Fire"));
        cdb.delete(new Coverage("Storm"));
    }
}
