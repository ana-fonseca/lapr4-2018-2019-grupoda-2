package isep.eapli.config.utils;

import isep.eapli.config.bootstrapers.RiskMatrixBootstrapper;
import isep.eapli.config.controller.CreateCoverageController;
import isep.eapli.config.controller.CreateRiskFactorController;
import isep.eapli.config.controller.CreateSurroundingTypeController;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ImportInformationTest {

    @Test
    public void importInfo() {
        RepositoryFactory rp = new RepositoryFactory();
        CoverageDB covBD = rp.getCoverageDB();
        RiskFactorDB rfBD = rp.getRiskFactorDB();
        SurroundingTypeDB stBD = rp.getSurroundingTypeDB();

        CreateCoverageController ccc = new CreateCoverageController();
        ccc.registerCoverage("tempestades");
        ccc.registerCoverage("vulcões");
        CreateSurroundingTypeController cstc = new CreateSurroundingTypeController();
        cstc.registerSurroundingType("bombeiros");
        SurroundingType st = new SurroundingType("bombeiros");
        Metric m1 = new Metric("distância");
        Metric m2 = new Metric("tempo");
        RiskFactor rf1 = new RiskFactor(st,m1);
        RiskFactor rf2 = new RiskFactor(st,m2);
        CreateRiskFactorController crfc = new CreateRiskFactorController();
        crfc.registerRiskFactor("bombeiros","distância");
        crfc.registerRiskFactor("bombeiros","tempo");
        Coverage c1 = new Coverage("tempestades");
        Coverage c2 = new Coverage("vulcões");
        CoverageRiskFactorCell crfc1 = new CoverageRiskFactorCell(c1,rf1);
        CoverageRiskFactorCell crfc2 = new CoverageRiskFactorCell(c2,rf2);
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        crfcList.add(crfc1);
        crfcList.add(crfc2);
        RiskMatrix expected = new RiskMatrix(crfcList, "Not Published",1);
        ImportInformation importInformation = new ImportInformation();
        RiskMatrix actual = new RiskMatrix(importInformation.importBaseMatrix("importBaseMatrixTest.csv"),"Not Published",1);
        assertEquals(expected,actual);

        covBD.delete(c1);
        covBD.delete(c2);
        rfBD.delete(rf1);
        rfBD.delete(rf2);
        stBD.delete(st);
    }

    @Test
    public void testImportDetailedRiskMatrix() throws Exception {
        List<List<Object>> dataListLists = new ArrayList<>();
        List<Object> dataList = new ArrayList<>();
        dataList.add(new Coverage("Storm"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        dataList.add(new Scale (1,2,5));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Storm"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        dataList.add(new Scale (1,2,6));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Storm"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        dataList.add(new Scale (1,2,7));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Fire"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        dataList.add(new Scale (1,2,8));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Fire"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        dataList.add(new Scale (1,2,9));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Fire"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        dataList.add(new Scale (1,2,10));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Hurricane"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        dataList.add(new Scale (1,2,11));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Hurricane"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        dataList.add(new Scale (1,2,12));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Hurricane"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        dataList.add(new Scale (1,2,13));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Earthquake"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Distance")));
        dataList.add(new Scale (1,2,14));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Earthquake"));
        dataList.add(new RiskFactor(new SurroundingType("River"), new Metric("Time")));
        dataList.add(new Scale (1,2,15));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Earthquake"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Distance")));
        dataList.add(new Scale (1,2,16));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        dataList.add(new Coverage("Earthquake"));
        dataList.add(new RiskFactor(new SurroundingType("Fire Fighter Station"), new Metric("Time")));
        dataList.add(new Scale (1,2,17));
        dataListLists.add(dataList);
        dataList = new ArrayList<>();
        RiskMatrixBootstrapper rmb = new RiskMatrixBootstrapper();
        System.out.println("importDetailedRiskMatrix");
        String filePath = "importDetailedRiskMatrix.csv";
        ImportInformation instance = new ImportInformation();
        List<List<Object>> answer = instance.readDetailedRiskMatrixFile(filePath);
        assertEquals(answer,  dataListLists);
    }
}