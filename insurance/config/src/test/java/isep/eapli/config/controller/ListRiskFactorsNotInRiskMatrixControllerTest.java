package isep.eapli.config.controller;

import isep.eapli.config.bootstrapers.*;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ListRiskFactorsNotInRiskMatrixControllerTest {
    @After
    public void deleteDB(){
        RepositoryFactory rf = new RepositoryFactory();
        CoverageDB cdb = rf.getCoverageDB();
        SurroundingTypeDB stdb = rf.getSurroundingTypeDB();
        RiskFactorDB rdb = rf.getRiskFactorDB();
        RiskMatrixDB rmdb = rf.getRiskMatrixDB();
        for (RiskMatrix rm: rmdb.findAll()){
            rmdb.remove(rm);
        }
        for (RiskFactor rfa: rdb.findAll()){
            rdb.remove(rfa);
        }
        for (SurroundingType st: stdb.findAll()){
            stdb.remove(st);
        }
        for (Coverage c: cdb.findAll()){
            cdb.remove(c);
        }
    }

    private ListRiskFactorsNotInRiskMatrixController lrfc;

    public ListRiskFactorsNotInRiskMatrixControllerTest(){
        lrfc = new ListRiskFactorsNotInRiskMatrixController();
    }

    @Test
    public void testListRiskFactors(){
        RepositoryFactory rp = new RepositoryFactory();
        RiskMatrixDB rmDB = rp.getRiskMatrixDB();
        CoverageDB cdb = rp.getCoverageDB();
        SurroundingTypeDB stdb = rp.getSurroundingTypeDB();
        RiskFactorDB rfdb = rp.getRiskFactorDB();
        RiskMatrixBootstrapper rfb = new RiskMatrixBootstrapper();
        new CoverageBootstrapper().execute();
        new SurroundingTypeBootstrapper().execute();
        new RiskFactorBootstrapper().execute();
        rfb.execute();
        Metric m1 = new Metric("Distance");
        Metric m2 = new Metric("Time");
        SurroundingType st4 = new SurroundingType("Police");
        SurroundingType st3 = new SurroundingType("Hospital");
        SurroundingType st2 = new SurroundingType("Fire Fighter Station");
        SurroundingType st1 = new SurroundingType("River");
        RiskFactor rf1 = new RiskFactor(st1,m1);
        RiskFactor rf2 = new RiskFactor(st1,m2);
        RiskFactor rf3 = new RiskFactor(st2,m1);
        RiskFactor rf4 = new RiskFactor(st2,m2);
        RiskFactor rf5 = new RiskFactor(st4, m1);
        rf5 = rfdb.save(rf5);
        Coverage c1 = new Coverage("Hurricane");
        Coverage c2 = new Coverage("Earthquake");
        Coverage c3 = new Coverage("Fire");
        Coverage c4 = new Coverage("Storm");
        List<CoverageRiskFactorCell> list = new ArrayList<>();
        list.add(new CoverageRiskFactorCell(c4,rf1));
        list.add(new CoverageRiskFactorCell(c4,rf2));
        list.add(new CoverageRiskFactorCell(c4,rf4));
        list.add(new CoverageRiskFactorCell(c3,rf1));
        list.add(new CoverageRiskFactorCell(c3,rf3));
        list.add(new CoverageRiskFactorCell(c3,rf4));
        list.add(new CoverageRiskFactorCell(c1,rf2));
        list.add(new CoverageRiskFactorCell(c1,rf3));
        list.add(new CoverageRiskFactorCell(c1,rf4));
        list.add(new CoverageRiskFactorCell(c2,rf1));
        list.add(new CoverageRiskFactorCell(c2,rf2));
        list.add(new CoverageRiskFactorCell(c2,rf3));
        list.add(new CoverageRiskFactorCell(c2,rf4));
        RiskMatrix rm = new RiskMatrix(list, "Published",1);
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(0), new Contribution("Positive"), new Weight(9), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(1), new Contribution("Positive"), new Weight(3), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(2), new Contribution("Positive"), new Weight(9), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(3), new Contribution("Positive"), new Weight(4), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(4), new Contribution("Positive"), new Weight(4), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(5), new Contribution("Negative"), new Weight(4), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(6), new Contribution("Negative"), new Weight(6), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(7), new Contribution("Negative"), new Weight(6), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(8), new Contribution("Positive"), new Weight(6), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(9), new Contribution("Negative"), new Weight(2), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(10), new Contribution("Negative"), new Weight(9), new Necessity("Required")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(11), new Contribution("Positive"), new Weight(2), new Necessity("Optional")));
        rm.addNewCharacterizedCell(new CharacterizedCell(list.get(12), new Contribution("Negative"), new Weight(4), new Necessity("Required")));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(0),new Scale(1,2,5)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(1),new Scale(1,2,6)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(2),new Scale(1,2,7)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(3),new Scale(1,2,8)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(4),new Scale(1,2,9)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(5),new Scale(1,2,10)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(6),new Scale(1,2,11)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(7),new Scale(1,2,12)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(8),new Scale(1,2,13)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(9),new Scale(1,2,14)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(10),new Scale(1,2,15)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(11),new Scale(1,2,16)));
        rm.addNewDetailedCell(new DetailedCell(rm.getListCharacterizedCell().get(12),new Scale(1,2,17)));

        rmDB.save(rm);
        List<String> list2 = lrfc.showRiskMatrices();
        List<RiskFactor> list3 = lrfc.listRiskFactorNotInRiskMatrix(list2.get(0));
        //List<RiskFactor> list3 = lrfc.listRiskFactorNotInRiskMatrix(rm.obtainVersion().toString());
        assertEquals(list3.get(0), rf5);
        deleteDB();
        rmDB.delete(rm);
        rfdb.delete(rf1);
        rfdb.delete(rf2);
        rfdb.delete(rf3);
        rfdb.delete(rf4);
        rfdb.delete(rf5);
        stdb.delete(st4);
        stdb.delete(st3);
        stdb.delete(st2);
        stdb.delete(st1);
        cdb.delete(c1);
        cdb.delete(c2);
        cdb.delete(c3);
        cdb.delete(c4);
    }

}