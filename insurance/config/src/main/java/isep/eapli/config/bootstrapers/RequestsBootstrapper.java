package isep.eapli.config.bootstrapers;

import eapli.framework.actions.Action;
import isep.eapli.config.controller.RiskAssessmentRequestController;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;
import isep.eapli.georef.controller.AdressSearchController;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestsBootstrapper implements Action {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private Random rand = new Random();

    private RequestDB requestDB = new RepositoryFactory().getRequestDB();
    private CoverageDB covDB = new RepositoryFactory().getCoverageDB();
    private RiskAnalystDB riskAnalystDB = new RepositoryFactory().getRiskAnalystDB();
    private RiskMatrixDB riskMatrixDB = new RepositoryFactory().getRiskMatrixDB();

    @Override
    public boolean execute() {
        FileReader in = null;
        try {
            in = new FileReader("../requestLimit.properties");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RequestsBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        Properties requestLimitProperties = new Properties();
        try {
            requestLimitProperties.load(in);
        } catch (IOException ex) {
            Logger.getLogger(RequestsBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));
        
        Thread[] thrs = new Thread[10];

        for (int i = 0; i < thrs.length; i++) {
            int finalI = i;
            int finalRequestLimit = requestLimit;
            thrs[i] = new Thread(() -> {
                try {
                    register((finalI == 3 || finalI == 6 ? State.VALIDATED : State.PENDING), finalRequestLimit);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thrs[i].start();
            try {
                thrs[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void register(State state, int requestLimit) throws Exception {    
        long currentRequests = requestDB.countCurrentRequests();
        if (currentRequests >= requestLimit) {
            if (state != State.VALIDATED && state != State.AUTOMATIC) {
                return;
            }
        }
        
        List<InsuranceCoverageConnection> iccList = new ArrayList<>();

        List<Address> addressList = new ArrayList<>();
        addressList.add(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto")));
        addressList.add(new Address(new Street("R. Dom Paio Mendes"), new City("Braga"), new PostalCode("4700-424"), new Country("Portugal")));
        addressList.add(new Address(new Street("Avenida da Boavista"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto")));

        List<Insurance> insList = new ArrayList<>();

        for (Address address : addressList) {
            if (!address.hasDistrict()) {
                List<String> completeAddress = new AdressSearchController().completePostalAddress(address.getCity().city(), address.postalCode().postalCode(), address.street().toString());
                Street street = new Street(completeAddress.get(0));
                PostalCode postalCode = null;
                try {
                    postalCode = new PostalCode(completeAddress.get(1));
                } catch (Exception ex) {
                    Logger.getLogger(RiskAssessmentRequestController.class.getName()).log(Level.SEVERE, null, ex);
                }
                City city = new City(completeAddress.get(2));
                District districtNew = new District(completeAddress.get(3));
                Country country = new Country(completeAddress.get(4));

                address = new Address(street, city, postalCode, country, districtNew);
            }
            insList.add(new Insurance(address));
        }

        for (Coverage cov : covDB.findAll()) {
            if (rand.nextBoolean() || iccList.isEmpty()) {
                iccList.add(new InsuranceCoverageConnection(insList.get(rand.nextInt(3)), cov));
            }
        }
        Version v = null;
        int countAux = rand.nextInt(2);
        for (RiskMatrix matrix : riskMatrixDB.findAll()) {
            if (countAux == 0) {
                v = matrix.obtainVersion();
                break;
            }
            countAux--;
        }
        RiskAnalyst ra = null;
        if (state == State.VALIDATED) {
            ra = riskAnalystDB.findByEmail("ola1@gmail.com");
        } else {
        int count = rand.nextInt(4);
        for (RiskAnalyst riskAnalyst : riskAnalystDB.findAll()) {
            if (count == 0) {
                ra = riskAnalyst;
                break;
            }
            count--;
        }
        }

        Request request = new Request(iccList, "Bootstrapper", state, v);
        
        List<Explanation> explanation = new ArrayList<>();
        for (int i = 0; i < iccList.size(); i++) {
            Result result = new Result(iccList.get(i), explanation, 55);
            request.addResult(result);
        }
    
        if (rand.nextBoolean() || state == State.VALIDATED) {
            request.assignRiskAnalyst(ra);
        }

        if(state == State.VALIDATED){
            request.decide();
        }
        
        requestDB.save(request);
    }
}
