package isep.eapli.config.utils;

import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.CoverageRiskFactorCell;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExportInformation {

    /**
     * Export base matrix information: all the coverages and risk factors that are on the database.
     * @param filename
     * @return true if exported, false if not
     * @throws IOException
     */
    public boolean exportBaseMatrix(String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        RepositoryFactory rp = new RepositoryFactory();

        Iterable<Coverage> covList = rp.getCoverageDB().findAll();
        Iterable<RiskFactor> rfList = rp.getRiskFactorDB().findAll();

        writer.append("Coverage");

        for(RiskFactor rf : rfList){
            writer.append(",");
            writer.append(rf.getSurroundingType().obtainDescription());
            writer.append(" - ");
            writer.append(rf.getMetric().obtainDesignation());
        }

        for(Coverage c : covList){
            writer.append("\n");
            writer.append(c.obtainDesignation());
        }

        writer.flush();
        writer.close();
        File f = new File(filename);
        return f.exists();
    }

    /**
     * Export characterized or detailed matrix base information, to allow the user to fill the missing information.
     * @param fileName name of the file
     * @return true if exported, false if not
     */
    public void exportCharacterizedAndDetailedBaseMatrix(String fileName){
        RepositoryFactory rp = new RepositoryFactory();
        RiskMatrixDB rmdb = rp.getRiskMatrixDB();

        StringBuilder sb = new StringBuilder();
        List<String> listCover = new ArrayList<>();
        List<String> listRF = new ArrayList<>();
        RiskMatrix rm=null;
        for(RiskMatrix tmp : rmdb.findAll())
            rm=tmp;//Objective is to retain the last one
        assert rm != null; //Check if db is empty
        for(CoverageRiskFactorCell crfc : rm.getListCoverageRiskFactorCell()) {
            String[] tmp = crfc.toString().replace("Coverage Risk Factor Cell with Coverage ", "")
                    .replace(" and RiskFactor-> Surrounding type: ", ";").replace("; Metric:", " -").split(";");
            if (!listCover.contains(tmp[0]))
                listCover.add(tmp[0]);
            if (!listRF.contains(tmp[1]))
                listRF.add(tmp[1]);
        }
        sb.append("Coverage,");
        for(int i=0; i<listRF.size(); i++) {
            String str = listRF.get(i);
            sb.append(str);
            if(i<listRF.size()-1)
                sb.append(",");
        }
        sb.append("\n");
        for(int i=0; i<listCover.size(); i++) {
            String str = listCover.get(i);
            sb.append(str).append(",");
            if(i<listCover.size()-1)
                sb.append("\n");
        }
        printStringsToFile(sb, fileName);
    }

    public void printStringsToFile(StringBuilder sb, String fileName){
        try(FileWriter fw = new FileWriter(new File(fileName))){
            fw.write(sb.toString());
        }catch(IOException e){
            throw new IllegalArgumentException();
        }
    }
}
