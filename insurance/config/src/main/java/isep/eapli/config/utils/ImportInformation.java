package isep.eapli.config.utils;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ImportInformation {

    public static List<String> getStringsFromFile(String filename){
        try {
            return Files.lines(java.nio.file.Paths.get(filename)).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Import Base Matrix: every pair coverage/risk factor that is filled with yes will be imported to the matrix.
     * @param ficheiro
     * @return list with CoverageRiskFactorCells
     */
    public List<CoverageRiskFactorCell> importBaseMatrix(String ficheiro) {
        RepositoryFactory rp = new RepositoryFactory();
        CoverageDB covBD = rp.getCoverageDB();
        SurroundingTypeDB stDB = rp.getSurroundingTypeDB();
        RiskFactorDB rfDB = rp.getRiskFactorDB();
        int cont=0;
        List<RiskFactor> rfList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcList = new ArrayList<>();
        List<String> list = getStringsFromFile(ficheiro);
        for (String line : list) {
            cont++;
            String allCampos[] = line.split(",");
            int riskFactorsNum = allCampos.length - 1;
            if (cont > 1) {
                try{
                    Coverage cov = new Coverage(allCampos[0]);
                    for (int i = 0; i < riskFactorsNum; i++) {
                            if (allCampos[i + 1].equalsIgnoreCase("sim")) {
                            CoverageRiskFactorCell crfc = new CoverageRiskFactorCell(cov, rfList.get(i));
                            crfcList.add(crfc);
                        }
                    }
                }catch(NoSuchElementException e){
                    System.out.printf("rip");
                }
            } else {
                for (int i = 1; i <= riskFactorsNum; i++) {
                    try {
                        String riskF[] = allCampos[i].split(" - ");
                        SurroundingType st = stDB.findByType(riskF[0]);
                        Metric m = new Metric(riskF[1]);
                        Optional<RiskFactor> rfa = rfDB.findById(st.toString() + m.toString());
                        RiskFactor rf = rfa.get();
                        rfList.add(rf);
                    } catch (NoSuchElementException e) {
                        //
                    }

                }
            }
        }
        return crfcList;
    }

    /**
     * Method that reads the Detailed Matrix file using it's path.
     * @param filePath file's path in the system
     * @return list with file's data
     * @throws FileNotFoundException exception thrown when file is not found.
     */
    public static List<List<Object>> readDetailedRiskMatrixFile(String filePath) throws FileNotFoundException {
        int counter=0;
        List<RiskFactor> riskFactorList = new ArrayList<>();
        List<List<Object>> informationList = new ArrayList<>();

            Scanner scanner = new Scanner(new File(filePath));
            scanner.useDelimiter(",");
            Scale scale;
            String[] data, riskFactorArray, scaleArray;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                data = line.split(",");
                if(counter==0) { //first line of the file has the risk factors
                    for(counter=1; counter<data.length; counter++) {
                        riskFactorArray = data[counter].split("-");
                        SurroundingType surroundingType = new SurroundingType(riskFactorArray[0]);
                        Metric metric = new Metric(riskFactorArray[1]);
                        RiskFactor riskFactor = new RiskFactor(surroundingType,metric);
                        riskFactorList.add(riskFactor);
                    }
                }else{ //the rest of the lines have the coverage, and the respective scales with the risk factors
                    Coverage cov = new Coverage(data[0]);
                    if(data.length>0) {
                        for (counter = 1; counter < data.length; counter++) {
                            scaleArray = data[counter].split("-");
                            if(scaleArray.length == 3) {
                                scale = new Scale(Integer.parseInt(scaleArray[0]), Integer.parseInt(scaleArray[1]), Integer.parseInt(scaleArray[2]));
                                List<Object> coverageRiskFactorScaleList = new ArrayList<>();
                                coverageRiskFactorScaleList.add(cov);
                                coverageRiskFactorScaleList.add(riskFactorList.get(counter-1));
                                coverageRiskFactorScaleList.add(scale);
                                informationList.add(coverageRiskFactorScaleList);
                            }
                        }
                    }

                }
                counter++;
            }
            scanner.close();

        return informationList;
    }

}
