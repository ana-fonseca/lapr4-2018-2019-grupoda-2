package isep.eapli.config.controller;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.*;

import java.util.ArrayList;
import java.util.List;

public class ListRiskFactorsNotInRiskMatrixController {

    private RiskMatrixDB rmdb;
    private RiskFactorDB rfdb;

    public ListRiskFactorsNotInRiskMatrixController(){
        RepositoryFactory rf = new RepositoryFactory();
        rmdb=rf.getRiskMatrixDB();
        rfdb=rf.getRiskFactorDB();
    }

    public List<String> showRiskMatrices(){
        List<String> list = new ArrayList<>();
        for(RiskMatrix rm : rmdb.findAll()) {
            if(!rm.obtainNonPublishedVersionName().equals("Not Published")){
                list.add(rm.obtainVersion().toString());
            }
        }
        return list;
    }

    public List<RiskFactor> listRiskFactorNotInRiskMatrix(String riskMatrixName){
        RiskMatrix rm=null;
        for(RiskMatrix riskM : rmdb.findAll())
            if(riskM.obtainVersion().toString().equals(riskMatrixName))
                rm=riskM;
        assert rm != null;
        List<RiskFactor> listFinal = new ArrayList<>();
        List<RiskFactor> listInMatrix = new ArrayList<>();
        List<CoverageRiskFactorCell> listCRFC = rm.getListCoverageRiskFactorCell();
        for(CoverageRiskFactorCell crfc : listCRFC)
            if(!listInMatrix.contains(crfc.obtainRiskFactor()))
                listInMatrix.add(crfc.obtainRiskFactor());
        for(RiskFactor rf : rfdb.findAll())
            if(!listInMatrix.contains(rf))
                listFinal.add(rf);
        return listFinal;
    }
}
