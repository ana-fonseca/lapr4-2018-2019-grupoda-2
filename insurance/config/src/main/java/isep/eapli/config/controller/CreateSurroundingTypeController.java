package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;

import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;

/**
 *
 * @author 1171138
 */
public class CreateSurroundingTypeController {
    
     /**
     * SurroundingTypeDB object
     */
      private SurroundingTypeDB stdb; 

    /**
     * Public constructor of the class
     */
    public CreateSurroundingTypeController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        stdb = repositoryAccess.getSurroundingTypeDB();
    }

    /**
     * Method that returns an Iterable collection with all the surrounding Types in the database
     *
     * @return iterable with all the surrounding types
     */
    public Iterable<SurroundingType> getAllSurroundingTypes() {
        return stdb.findAll();
    }

    /**
     * Method that saves a new surrounding type in the database, given a description.
     * If that description is not in the system, the object created is saved. 
     * If the description already exists in the database, the method returns false and it does not save in the database.
     *
     * @param description description to be saved
     * @return result of the saving operation
     */
    public boolean registerSurroundingType(String description) {
        SurroundingType sType = new SurroundingType(description);
        if (stdb.checkIfAlreadyExists(sType)) {
            return false;
        } else {
            try {
                stdb.save(sType);
            } catch (ConcurrencyException | IntegrityViolationException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

   
}
