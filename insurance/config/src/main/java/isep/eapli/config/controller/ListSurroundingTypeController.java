/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.controller;

import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;

/**
 *
 * @author 1171138
 */
public class ListSurroundingTypeController {
    
     /**
     * SurroundingTypeDB object
     */
    private SurroundingTypeDB stdb;

     /**
     * Empty-public constructor of the class
     */
    public ListSurroundingTypeController() {
        RepositoryFactory rp = new RepositoryFactory();
        stdb = rp.getSurroundingTypeDB();
    }
    
      /**
     * Method that returns an Iterable collection with all the surrounding Types in the database
     *
     * @return iterable with all the surrounding types
     */
    public Iterable<SurroundingType> getAllSurroundingTypes() {
        return stdb.findAll();
    }
    
    
    
}
