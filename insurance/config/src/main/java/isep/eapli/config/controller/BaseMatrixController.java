package isep.eapli.config.controller;

import isep.eapli.config.utils.ExportInformation;
import isep.eapli.config.utils.ImportInformation;
import isep.eapli.core.domain.CoverageRiskFactorCell;
import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;
import java.io.IOException;
import java.util.List;

public class BaseMatrixController {

    /**
     * Repository Factory attribute.
     */
    private RepositoryFactory rp;

    /**
     * Repository factory attribute.
     */
    private RiskMatrixDB rmDB;

    /**
     * Constructor.
     */
    public BaseMatrixController() {
        rp = new RepositoryFactory();
        rmDB = new RiskMatrixDB("DomainDB");
    }

    /**
     * Import base matrix.
     *
     * @param filename
     * @return true if imported, false if not
     */
    public boolean importBaseMatrix(String filename) {
        ImportInformation ii = new ImportInformation();
        List<CoverageRiskFactorCell> list = ii.importBaseMatrix(filename);
        int num = rmDB.getNumMatrixes();
        RiskMatrix.startIDat(num);
        RiskMatrix rm = new RiskMatrix(list, "Not Published", num);
        if (!rm.getListCoverageRiskFactorCell().isEmpty()) {
            if (rmDB.checkIfBaseAlreadyExists(rm)) {
                return false;
            }
            rmDB.save(rm);
            return true;
        }
        return false;
    }

    /**
     * Export base matrix.
     *
     * @param filename
     * @return true if exported, false if not
     * @throws IOException
     */
    public boolean exportBaseMatrix(String filename) throws IOException {
        return new ExportInformation().exportBaseMatrix(filename);
    }

}
