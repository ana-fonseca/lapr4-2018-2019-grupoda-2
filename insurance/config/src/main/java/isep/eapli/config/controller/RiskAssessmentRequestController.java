package isep.eapli.config.controller;

import isep.eapli.config.utils.TimeCalculations;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.georef.controller.AdressSearchController;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RiskAssessmentRequestController {

    private RequestDB requestDB;

    public RiskAssessmentRequestController() {
        requestDB = new RepositoryFactory().getRequestDB();
    }

    /**
     * Lists the pending unassigned risk assessment requests
     * 
     * @return the list of the pending unassigned risk assessment requests
     * 
     * Tiago Ribeiro (1170426)
     */
    public List<Request> listPendingUnassignedRequests() {
        return requestDB.findPendingUnassignedRequests();
    }

    /**
     * Lists the pending unassigned risk assessment requests filtering them through a received district
     * 
     * @param district = district to filter them through
     * 
     * @return the list of the filtered risk assessment requests
     * 
     * Tiago Ribeiro (1170426)
     */
    public List<Request> listPendingUnasignedRequests(String district) {
        List<Request> filteredRequestList = new ArrayList<>();
        for (Request request : requestDB.findPendingUnassignedRequests()) {
            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                Address address = icc.obtainInsurance().getAddress();
                if (address.district().district().equalsIgnoreCase(district)) {
                    filteredRequestList.add(request);
                    break;
                }
            }
        }
        return filteredRequestList;
    }

    public boolean assignRiskAnalyst(Request request, RiskAnalyst riskAnalyst) {
        if (request.assignRiskAnalyst(riskAnalyst)) {
            requestDB.save(request);
            return true;
        }
        return false;
    }

    /**
     * Returns List of Pending Assigned Requests to a given Risk Analyst
     *
     * @param riskAnalyst logged in Risk Analyst
     * @return filtered list
     */
    public List<Request> pendingRequestList(RiskAnalyst riskAnalyst) {
        List<Request> filteredList = new ArrayList<>();
        filteredList = requestDB.findPendingAssignedRequests(riskAnalyst);
        return filteredList;
    }

    /**
     * Returns List of Validated Assigned Requests by the Risk Analyst
     * 
     * @param riskAnalyst logged in Risk Analyst
     * @return filtered list
     * 
     * @author Maria Junqueira (1170842)
     */
    public List<Request> validatedRequestList(RiskAnalyst riskAnalyst){
        List<Request> filteredList = new ArrayList<>();
        filteredList = requestDB.findValidatedAssignedRequests(riskAnalyst);
        return filteredList;
    }

    /**
     * This method calculates time passed since being assigned
     * @param request given request
     * @return time
     * This method was created by Ana Fonseca (1170656)
     */
    public String timePassedSinceAssigned(Request request) {
        TimeCalculations tc = new TimeCalculations();
        return tc.timePassedSinceAssigned(request);
    }
    
    /**
     * This method calculated the average time period of all the validated requests
     * @param requestList
     * @param counter
     * @return average (average time period + quantity)
     * 
     * @author Maria Junqueira (1170842)
     */
    public String averageValidationTime(List<Request> requestList, int counter){
        long aux = 0;
        for(Request request : requestList){
            LocalDateTime start = request.obtainAssignedDate();
            LocalDateTime finish = request.obtainDecisionDate();
            long diff = start.until(finish, ChronoUnit.MINUTES);
            aux = aux + diff;
        }
        long ave = Math.round(aux/requestList.size());
        String average = ave/24/60 + ":" + ave/60%24 + ":" + ave%60 + " - Quantity: " + requestList.size();
        return average;
    }
    
}
