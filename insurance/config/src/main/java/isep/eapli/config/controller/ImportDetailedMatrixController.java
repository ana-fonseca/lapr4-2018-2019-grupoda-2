/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.controller;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static isep.eapli.config.utils.ImportInformation.readDetailedRiskMatrixFile;

/**
 * @author rikar
 */
public class ImportDetailedMatrixController {

    /**
     * CoverageDB object
     */
    private RiskMatrixDB riskMatrixDB;

    public ImportDetailedMatrixController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        riskMatrixDB = repositoryAccess.getRiskMatrixDB();
    }

    public boolean importDetailedMatrix(String filePath) throws FileNotFoundException {
        CoverageRiskFactorCell coverageRiskFactorCell;
        List<List<Object>> dataListLists = new ArrayList<>(readDetailedRiskMatrixFile(filePath));
        List<RiskMatrix> listRiskMatrix = new ArrayList<>();
        RiskMatrix riskMatrix = null;
        //0-coverage
        //1-surroundingType
        //2-metric
        //3-scale
        for (RiskMatrix riskM : riskMatrixDB.findAll()) {
            riskMatrix = riskM; //last one is in riskMatrix
        }
        assert riskMatrix != null;
        for (List<Object> dataList : dataListLists) {
            coverageRiskFactorCell = new CoverageRiskFactorCell((Coverage) dataList.get(0), (RiskFactor)dataList.get(1));
            for (CharacterizedCell characterizedCell : riskMatrix.getListCharacterizedCell()) {
                if (characterizedCell.obtainCoverageRisk().equals(coverageRiskFactorCell)) {
                    DetailedCell detailedCell = new DetailedCell(characterizedCell, (Scale) dataList.get(2));
                    riskMatrix.addNewDetailedCell(detailedCell);
                    break;
                }
            }
        }
        riskMatrixDB.save(riskMatrix);
        return true;
    }

}
