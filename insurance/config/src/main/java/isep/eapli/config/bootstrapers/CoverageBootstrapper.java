package isep.eapli.config.bootstrapers;

import eapli.framework.actions.Action;
import isep.eapli.config.controller.CreateCoverageController;

/**
 * Class capable of control multiple instances of the Coverage
 */
public class CoverageBootstrapper implements Action {

    /**
     * Executes all the functions connected to this bootstrapper
     *
     * @return result of the operations
     */
    @Override
    public boolean execute() {
        String[] strs = {"Storm", "Fire", "Hurricane", "Earthquake"};
        Thread[] thrs = new Thread[strs.length];
        for(int i=0; i<thrs.length; i++){
            int finalI = i;
            thrs[i] = new Thread(() -> register(strs[finalI]));
            thrs[i].start();
        }
        for(int i=0; i<thrs.length; i++){
            try {
                thrs[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Method that registers the new coverages given bt the execute command
     *
     * @param description name of the coverage to be created
     */
    private void register(String description) {
        CreateCoverageController ccc = new CreateCoverageController();
        ccc.registerCoverage(description);
    }
}
