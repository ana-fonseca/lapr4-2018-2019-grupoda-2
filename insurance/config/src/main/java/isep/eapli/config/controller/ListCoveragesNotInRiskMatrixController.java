package isep.eapli.config.controller;

import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.CoverageRiskFactorCell;
import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.util.ArrayList;
import java.util.List;

public class ListCoveragesNotInRiskMatrixController {

    private RiskMatrixDB riskMatrixDB;
    private CoverageDB coverageDB;

    public ListCoveragesNotInRiskMatrixController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        riskMatrixDB = repositoryAccess.getRiskMatrixDB();
        coverageDB = repositoryAccess.getCoverageDB();
    }

    public List<RiskMatrix> showRiskMatrices(){
        List<RiskMatrix> listRiskMatrix = new ArrayList<>();
        for(RiskMatrix riskMatrix : riskMatrixDB.findAll()) {
            if(!riskMatrix.obtainNonPublishedVersionName().equals("Not Published")){
                listRiskMatrix.add(riskMatrix);
            }

        }
        return listRiskMatrix;
    }

    public List<Coverage> listCoveragesNotInRiskMatrix(RiskMatrix riskMatrix){
        List<CoverageRiskFactorCell> listCoverageRiskFactorCell = riskMatrix.getListCoverageRiskFactorCell();
        List<Coverage> listCoveragesRiskMatrix = new ArrayList<>();
        List<Coverage> listCoveragesNotInRiskMatrix = new ArrayList<>();
        //The same coverage might be in different cells from the risk matrix, so this for loop avoids repetitions.
        for(CoverageRiskFactorCell coverageRiskFactorCell : listCoverageRiskFactorCell){
            Coverage coverageFromCell = coverageRiskFactorCell.obtainCoverage();
            if(!listCoveragesRiskMatrix.contains(coverageFromCell)){
                listCoveragesRiskMatrix.add(coverageFromCell);
            }
        }
        //Finds the coverages that are in the DB, but not in this risk matrix
        for(Coverage coverage : coverageDB.findAll()){
            if(!listCoveragesRiskMatrix.contains(coverage)){
                listCoveragesNotInRiskMatrix.add(coverage);
            }
        }

        return listCoveragesNotInRiskMatrix;
    }
}
