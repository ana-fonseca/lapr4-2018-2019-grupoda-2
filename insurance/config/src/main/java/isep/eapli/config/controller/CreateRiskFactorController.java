package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;

/**
 * Controller that controls the creation of new Risk Factors in the system
 */
public class CreateRiskFactorController {

    /**
     * RiskFactorDB object
     */
    private RiskFactorDB rfdb;

    /**
     * SurroundingTypeDB object
     */
    private SurroundingTypeDB stdb;

    /**
     * Public constructor of the class
     */
    public CreateRiskFactorController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        rfdb = repositoryAccess.getRiskFactorDB();
        stdb = repositoryAccess.getSurroundingTypeDB();
    }

    /**
     * Method that returns an Iterable collection with all the Surrounding Types in the database
     *
     * @return iterable with all the surrounding types
     */
    public Iterable<SurroundingType> getAllSurroundingTypes() {
        return stdb.findAll();
    }

    /**
     * Method that saves a new risk factor in the database, given a surrounding type and a metric.
     * If the risk factor does not exist in the DB, it is saved and the method returns true. If the risk factor already exists in the DB, it is not saved and the method returns false.
     *
     * @param type   - surrounding type description
     * @param metric - metric description
     * @return
     */
    public boolean registerRiskFactor(String type, String metric) {
        SurroundingType surroundingType = stdb.findByType(type);
        if (surroundingType == null) {
            System.out.println("Surrounding type not in the DB!");
            return false;
        }
        RiskFactor riskFactor = new RiskFactor(surroundingType, new Metric(metric));
        if (rfdb.checkIfAlreadyExists(riskFactor)) {
            return false;
        } else {
            try {
                rfdb.save(riskFactor);
            } catch (ConcurrencyException | IntegrityViolationException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }
}
