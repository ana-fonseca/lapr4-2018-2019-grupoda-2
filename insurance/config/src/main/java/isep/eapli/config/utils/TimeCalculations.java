package isep.eapli.config.utils;

import isep.eapli.core.domain.Request;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TimeCalculations {

    /**
     * This method calculates the time that passed between the assigned date and
     * now
     * https://kodejava.org/how-to-find-the-difference-between-two-localdatetime-objects/
     * This method was created by Ana Fonseca (1170656)
     * @param request Wanted request
     */
    public String timePassedSinceAssigned(Request request) {
        LocalDateTime from = request.obtainAssignedDate();
        LocalDateTime to = LocalDateTime.now();

        LocalDateTime fromTemp = LocalDateTime.from(from);
        long years = fromTemp.until(to, ChronoUnit.YEARS);
        fromTemp = fromTemp.plusYears(years);


        long days = fromTemp.until(to, ChronoUnit.DAYS);
        long months = fromTemp.until(to, ChronoUnit.MONTHS);
        fromTemp = fromTemp.plusMonths(months);
        fromTemp = fromTemp.plusDays(days);

        long hours = fromTemp.until(to, ChronoUnit.HOURS);
        fromTemp = fromTemp.plusHours(hours);

        long minutes = fromTemp.until(to, ChronoUnit.MINUTES);
        fromTemp = fromTemp.plusMinutes(minutes);

        long seconds = fromTemp.until(to, ChronoUnit.SECONDS);
        fromTemp = fromTemp.plusSeconds(seconds);

        return years + " years, " + months + " months, " + days + " days, "
                + hours + " hours, " + minutes + " minutes, " + seconds + " seconds";
    }
    
}
