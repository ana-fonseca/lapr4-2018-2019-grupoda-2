package isep.eapli.config.bootstrapers;

import eapli.framework.actions.Action;
import isep.eapli.config.controller.BaseMatrixController;
import isep.eapli.config.controller.CharacterizeMatrixController;
import isep.eapli.config.controller.ImportDetailedMatrixController;
import java.io.FileNotFoundException;

public class RiskMatrixBootstrapper implements Action {

    /**
     * Bootstrapper of the Risk Matrix
     *
     * @return true
     */
    @Override
    public boolean execute() {
        BaseMatrixController bmc = new BaseMatrixController();

        //matrix 1
        if (!bmc.importBaseMatrix("../testMatrixBootsrapper.csv")) {
            return false;
        }
        CharacterizeMatrixController cmc = new CharacterizeMatrixController();
        cmc.characterizeMatrix("../testCharacterizedMatrixWeightBootstrapper.csv", "../testCharacterizedMatrixNecessityBootstrapper.csv",
                "../testCharacterizedMatrixContributionBootstrapper.csv");
        ImportDetailedMatrixController d = new ImportDetailedMatrixController();
        try {
            d.importDetailedMatrix("../importDetailedRiskMatrix.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // matrix 2
        if (!bmc.importBaseMatrix("../matrix2/testMatrixBootsrapper2.csv")) {
            return false;
        }
        cmc = new CharacterizeMatrixController();
        cmc.characterizeMatrix("../matrix2/testCharacterizedMatrixWeightBootstrapper2.csv", "../matrix2/testCharacterizedMatrixNecessityBootstrapper2.csv",
                "../matrix2/testCharacterizedMatrixContributionBootstrapper2.csv");
        d = new ImportDetailedMatrixController();
        try {
            d.importDetailedMatrix("../matrix2/importDetailedRiskMatrix2.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }
}
