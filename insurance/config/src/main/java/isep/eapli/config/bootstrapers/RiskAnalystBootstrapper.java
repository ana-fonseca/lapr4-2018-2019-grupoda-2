/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.bootstrapers;

import eapli.framework.actions.Action;
import isep.eapli.config.controller.AuthenticationController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author becas
 */
public class RiskAnalystBootstrapper implements Action {
    @Override
    public boolean execute() {
        String[] mails = {"ola1@gmail.com", "ola2@gmail.com", "ola3@gmail.com", "ola4@gmail.com"};
        String[] pass = {"Ola123123#", "oLa123123#", "olA123123#", "olaA12312#"};
        Thread[] thrs = new Thread[mails.length];
        for(int i=0; i<thrs.length; i++){
            int finalI = i;
            thrs[i] = new Thread(() -> register(mails[finalI], pass[finalI]));
            thrs[i].start();
        }
        for(int i=0; i<thrs.length; i++){
            try {
                thrs[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Method that registers the new coverages given bt the execute command
     *
     * @param description name of the coverage to be created
     */
    private void register(String mail,String pass) {
        AuthenticationController ac = new AuthenticationController();
        try {
            ac.RiskAnalystRegistration(mail, pass);
        } catch (Exception ex) {
            Logger.getLogger(RiskAnalystBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
