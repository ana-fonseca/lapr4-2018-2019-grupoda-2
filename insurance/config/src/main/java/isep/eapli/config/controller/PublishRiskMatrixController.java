package isep.eapli.config.controller;

import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.util.ArrayList;
import java.util.List;

public class PublishRiskMatrixController {

    private RiskMatrixDB riskMatrixDB;

    public PublishRiskMatrixController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        riskMatrixDB = repositoryAccess.getRiskMatrixDB();
    }

    public List<RiskMatrix> showRiskMatrixes() {
        List<RiskMatrix> listRiskMatrixEvaluation = new ArrayList<>();
        List<RiskMatrix> listRiskMatrixNonPublished = new ArrayList<>();
        int counter=0;
        boolean riskMatrixCannotBeShown;
        //Outer for loop searches every risk matrix.
        for (RiskMatrix riskM : riskMatrixDB.findAll()) {
            riskMatrixCannotBeShown=false;
            //inner for loop compares every risk matrix
            for (RiskMatrix riskMat : riskMatrixDB.findAll()) {
                /*
                This loop compares matrixes that might already be published. To do that,
                they need to have the same content( equalsWithoutPublication() ),
                need to be published ( !riskMat.obtainNonPublishedVersionName().equals("Not Published") )
                and the published risk matrix needs to have saved the corresponding non publicated version's name.
                If the loop finds risk matrixes that follow these parameters, they are already published and cannot
                be shown to the user.
                 */
                if(riskM.equalsWithoutVersion(riskMat) && !riskMat.obtainNonPublishedVersionName().equals("Not Published") &&
                        riskM.obtainVersion().getVersionName().equals(riskMat.obtainNonPublishedVersionName())) {
                    riskMatrixCannotBeShown = true;
                    break;
                }
            }
            if(!riskMatrixCannotBeShown) {
                listRiskMatrixEvaluation.add(riskM);
            }
        }
        for (RiskMatrix riskMatrixes : listRiskMatrixEvaluation) {
            //From the matrixes that can be shown, they cannot be published and must be complete with all the necessary information.
            if(riskMatrixes.obtainNonPublishedVersionName().equals("Not Published") && !riskMatrixes.getListCoverageRiskFactorCell().isEmpty() && !riskMatrixes.getListCharacterizedCell().isEmpty() && !riskMatrixes.getListDetailedCell().isEmpty()) {
               listRiskMatrixNonPublished.add(riskMatrixes);
            }

        }
        return listRiskMatrixNonPublished;
    }

    public RiskMatrix publishRiskMatrix(RiskMatrix riskMatrix) {
        List<CharacterizedCell> listCharacterizedCell = riskMatrix.getListCharacterizedCell();
        List<DetailedCell> listDetailedCell = riskMatrix.getListDetailedCell();
        List<CoverageRiskFactorCell> listCoverageRiskFactorCell = riskMatrix.getListCoverageRiskFactorCell();
        java.lang.String riskMatrixVersion = riskMatrix.obtainVersion().getVersionName();
        java.lang.String[] division = riskMatrixVersion.split("_");
        //Next risk matrix has the next version number available, corresponding to the risk matrix size
        RiskMatrix newRiskMatrix = new RiskMatrix(listCoverageRiskFactorCell, riskMatrix.obtainVersion().getVersionName(), (int)riskMatrixDB.size());
        for (CharacterizedCell characterizedCell : listCharacterizedCell){
            newRiskMatrix.addNewCharacterizedCell(characterizedCell);
        }
        for (DetailedCell detailedCell : listDetailedCell){
            newRiskMatrix.addNewDetailedCell(detailedCell);
        }
        newRiskMatrix.saveNonPublishedVersionName(riskMatrix.obtainVersion());
        riskMatrixDB.save(newRiskMatrix);
        return newRiskMatrix;
    }


}
