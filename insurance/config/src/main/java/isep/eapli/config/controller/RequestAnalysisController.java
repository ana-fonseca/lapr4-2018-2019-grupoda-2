package isep.eapli.config.controller;

import isep.eapli.core.domain.Coverage;
import isep.eapli.core.domain.InsuranceCoverageConnection;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.core.domain.State;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class RequestAnalysisController {

    private Request request;
    private final RequestDB rdb;
    private final CoverageDB cdb;
    
    public RequestAnalysisController(Request request) {
        rdb = new RepositoryFactory().getRequestDB();
        this.request = request;
        cdb = new RepositoryFactory().getCoverageDB();
    }

    public void confirmResult() {
        request.setState(State.VALIDATED);
        request.changeDecisionDate();
        rdb.update(request);
    }

    public void addObsToResult(String obs) {
        request.introduceObs("Observação: " + obs);
        rdb.update(request);
    }

    public void AssignResult(List<Integer> index) {
        for (int i = 0; i < request.getResultList().size(); i++) {
            request.getResultList().get(i).assignIndex(index.get(i));
        }
        request.setState(State.VALIDATED);
        request.changeDecisionDate();
        rdb.update(request);
    }

    public void addStatement(String statment) {
        request.introduceObs("Fundamentação: " + statment);
        rdb.update(request);
    }

    public void AutomaticReassessment() {
        RiskAnalyst ra= request.obtainRiskAnalyst();
        request.setState(State.PENDING);
        request.assignRiskAnalyst(ra);
        request.changeDecisionDate();
        rdb.update(request);
    }

    public boolean addCoverage(String coverage) {
        Coverage c = cdb.findByDesignation(coverage);    
        if (c != null) {
            InsuranceCoverageConnection icc = new InsuranceCoverageConnection(request.getInsuranceCoverageConnectionList().get(0).obtainInsurance(), c);
            return request.getInsuranceCoverageConnectionList().add(icc);
        }
        return false;
    }

    public boolean removeCoverage(String coverage) {
        Coverage c = cdb.findByDesignation(coverage);
        if (c != null) {
            for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
                if (icc.obtainCoverage().equals(c)) {
                    return request.getInsuranceCoverageConnectionList().remove(icc);
                }
            }
        }
        return false;
    }

    public boolean exportToHtml(String fileName, String requestInfo) throws IOException {
        File file = new File("..//" + fileName + ".html");

        //Create the file
        if (file.createNewFile()) {
            FileWriter writer = new FileWriter(file);
            writer.write(requestInfo);
            writer.close();
            return true;
        } else {
            System.out.println("File already exists.");
            return false;
        }
    }

}
