package isep.eapli.config.controller;

import isep.eapli.core.domain.Coverage;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;

/**
 * Controller of the part of the application that controls the listing of Coverages
 */
public class ListCoverageController {

    /**
     * CoverageDB object
     */
    private CoverageDB cdb;

    /**
     * Empty-public constructor of the class
     */
    public ListCoverageController() {
        RepositoryFactory rp = new RepositoryFactory();
        cdb = rp.getCoverageDB();
    }

    /**
     * Method that returns an Iterator object with all the Coverages in the database
     *
     * @return Iterator with all the Coverages
     */
    public Iterable<Coverage> getAllCoverages() {
        return cdb.findAll();
    }

}
