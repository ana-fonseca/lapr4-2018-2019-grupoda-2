package isep.eapli.config.bootstrapers;


import isep.eapli.config.controller.CreateSurroundingTypeController;
import eapli.framework.actions.Action;
/**
 *
 * @author 1171138
 */
public class SurroundingTypeBootstrapper implements Action{
    
    @Override
    public boolean execute(){
        register("River");
        register("Fire Fighter Station");
        register("Hospital");
        register("Police");
        return true;
    }
    
    private void register(String description){
        CreateSurroundingTypeController cstc = new CreateSurroundingTypeController();
        cstc.registerSurroundingType(description);
    } 
}

