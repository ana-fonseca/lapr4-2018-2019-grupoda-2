package isep.eapli.config.bootstrapers;

import eapli.framework.actions.Action;
import isep.eapli.config.controller.CreateRiskFactorController;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.SurroundingType;

/**
 * Class capable of control multiple instances of the Risk Factor
 */
public class RiskFactorBootstrapper implements Action {

    /**
     * Executes all the functions connected to this bootstrapper
     *
     * @return result of the operations
     */
    @Override
    public boolean execute() {
        register("River", "Distance");
        register("Fire Fighter Station", "Distance");
        register("River", "Time");
        register("Fire Fighter Station", "Time");
        return true;
    }

    /**
     * Method that registers the new risk factor given by the execute command
     *
     * @param surrondingType    - surrounding type description
     * @param metricDescription - metric description
     */
    private void register(String surrondingType, String metricDescription) {
        CreateRiskFactorController crfc = new CreateRiskFactorController();
        crfc.registerRiskFactor(surrondingType, metricDescription);
    }
}
