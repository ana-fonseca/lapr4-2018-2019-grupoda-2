package isep.eapli.config.controller;

import isep.eapli.core.domain.RiskMatrix;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.util.ArrayList;
import java.util.List;

public class NewMatrixFromExistingMatrixController {

    /**
     * RiskMatrixDB object
     */
    private RiskMatrixDB rmdb;

    /**
     * Public constructor of the class
     */
    public NewMatrixFromExistingMatrixController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        rmdb = repositoryAccess.getRiskMatrixDB();
    }

    /**
     * Method that returns an Iterable collection with all the Detailed and Characterized Risk Matrices in the database
     *
     * @return iterable with all the risk matrices
     */
    public Iterable<RiskMatrix> getAllDetailedCharacterizedRiskMatrices() {
        List<RiskMatrix> riskMatrixList = new ArrayList<>();
        for (RiskMatrix rm : rmdb.findAll()) {
            if (rm.getListCharacterizedCell().size() != 0) {
                riskMatrixList.add(rm);
            }
        }
        return riskMatrixList;
    }

    /**
     * Method that saves a new matrix in the database.
     * If the list with coverages and risk factors is not empty, it is saved and the method returns true. If the list is empty, it is not saved and the method returns false.
     *
     * @param riskMatrix - risk matrix to save
     * @return
     */
    public boolean registerNewMatrixFromExistingMatrix(RiskMatrix riskMatrix) {
        if (riskMatrix.getListCoverageRiskFactorCell().size() != 0) {
            int num = rmdb.getNumMatrixes();
            RiskMatrix rm = new RiskMatrix(riskMatrix.getListCoverageRiskFactorCell(), "Not Published", num);
            rmdb.save(rm);
            return true;
        }
        return false;
    }
}
