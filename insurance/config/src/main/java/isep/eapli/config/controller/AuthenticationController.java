/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.config.controller;

import isep.eapli.core.domain.RiskAnalyst;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskAnalystDB;

/**
 *
 * @author DoriaTheGod
 */
public class AuthenticationController {

    private RepositoryFactory rp;
    private RiskAnalystDB raDB;

    public AuthenticationController() {
        rp = new RepositoryFactory();
        raDB = rp.getRiskAnalystDB();
    }

    public boolean RiskAnalystRegistration(String email, String password) throws Exception {
        RiskAnalyst ra = new RiskAnalyst(email, password);
        if (raDB.checkIfAlreadyExists(ra)) {
            return false;
        } else {
            raDB.save(ra);
            return true;
        }
    }
    
    public boolean RiskAnalystLogin(String email, String password) throws Exception{
        RiskAnalyst ra = new RiskAnalyst(email, password);
        if (raDB.checkIfAlreadyExistsFull(ra)) {
            return true;
        }
        return false;
    }
    
    
}
