package isep.eapli.config.utils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CommunicationService {

    private static Properties mailServerProperties;

    /**
     * Public constructor
     */
    public CommunicationService(){
        mailServerProperties = new Properties();
    }

    /**
     * Method that sends an email to an email defined in the properties file. The subject and mail body are received by the method, in HTML format
     * @param subject Subject of the email
     * @param body Body of the email, in HTML format
     * @throws MessagingException all the kinds of mistake in the mail sender controller
     */
    public void sendEmail(String subject, String body) throws MessagingException {
        try (InputStream propertiesStream = CommunicationService.class.getClassLoader().getResourceAsStream("application.properties")) {
            if (propertiesStream != null) {
                mailServerProperties = new Properties();
                mailServerProperties.load(propertiesStream);
            } else {
                throw new FileNotFoundException("Property file missing");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");

        Session getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        MimeMessage generateMailMessage = new MimeMessage(getMailSession);
        generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mailServerProperties.getProperty("email.defaultaddress")));
        generateMailMessage.setSubject(subject);
        generateMailMessage.setContent(body, "text/html");

        Transport transport = getMailSession.getTransport("smtp");
        transport.connect("smtp.gmail.com", mailServerProperties.getProperty("email.address"), mailServerProperties.getProperty("email.password"));
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }
}
