package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.config.utils.CommunicationService;
import isep.eapli.core.domain.Coverage;
import isep.eapli.core.persistenceimplementation.CoverageDB;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;

import javax.mail.MessagingException;

/**
 * Controller of the part of the application that controls the creation of new Coverages in the system
 */
public class CreateCoverageController {

    /**
     * CoverageDB object
     */
    private final CoverageDB cdb;

    /**
     * CommunicationService object
     */
    private CommunicationService cs;

    /**
     * Public constructor of the class
     */
    public CreateCoverageController() {
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        cdb = repositoryAccess.getCoverageDB();
        cs = new CommunicationService();
    }

    /**
     * Method that saves a new coverage in the database, given a description.
     * If that description is not in the system, the object created is saved. If the description already exists in the database, the method returns false and it does not save in the database.
     *
     * @param description description to be saved
     * @return result of the saving operation
     */
    public boolean registerCoverage(String description) {
        Coverage coverage = new Coverage(description);
        if (cdb.checkIfAlreadyExists(coverage)) {
            return false;
        } else {
            try {
                String subject = "I2S | Coverage Creation";
                String emailBody = "Dear user,<br>A new coverage, with the description " + coverage.obtainDesignation() + ", was inserted in the database.<br><br>Best regards,<br>I2S Configuration Service<br><br>(This was sent automatically, do not reply)";
                synchronized (cdb){
                    cdb.save(coverage);
                }
                try{
                    cs.sendEmail(subject, emailBody);
                } catch (MessagingException e) {
                    System.out.println("There was a problem sending the email.");
                }
            } catch (ConcurrencyException | IntegrityViolationException e){
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

}
