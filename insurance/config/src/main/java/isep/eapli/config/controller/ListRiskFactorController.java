package isep.eapli.config.controller;

import isep.eapli.core.domain.RiskFactor;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskFactorDB;

/**
 * Controller that controls the listing of Risk Factors
 */
public class ListRiskFactorController {

    /**
     * RiskFactorDB object
     */
    private RiskFactorDB rfdb;

    /**
     * Empty-public constructor of the class
     */
    public ListRiskFactorController() {
        RepositoryFactory rp = new RepositoryFactory();
        rfdb = rp.getRiskFactorDB();
    }

    /**
     * Method that returns an Iterator object with all the Risk Factors in the database
     *
     * @return Iterator with all the Risk Factors
     */
    public Iterable<RiskFactor> getAllRiskFactor() {
        return rfdb.findAll();
    }

}