package isep.eapli.config.controller;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import isep.eapli.config.utils.ImportInformation;
import isep.eapli.core.domain.*;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RiskMatrixDB;

import java.util.ArrayList;
import java.util.List;

public class CharacterizeMatrixController {

    /**
     * Database containing the Risk Matrices
     */
    private RiskMatrixDB rmdb;

    /**
     * Constructor initializing the database
     */
    public CharacterizeMatrixController(){
        RepositoryFactory rf = new RepositoryFactory();
        rmdb=rf.getRiskMatrixDB();
    }

    /**
     *
     * @param fileNameWeight
     * @param fileNameNecessity
     * @param fileNameContribution
     */
    public void characterizeMatrix(String fileNameWeight, String fileNameNecessity, String fileNameContribution){
        RiskMatrix rm=null;
        for(RiskMatrix tmp : rmdb.findAll())
            rm=tmp;//Objective is to retain the last one
        assert rm != null; //Check if db is empty
        List<RiskFactor> riskFactorList = new ArrayList<>();
        List<CoverageRiskFactorCell> crfcellList = new ArrayList<>();
        List<Weight> weightList = new ArrayList<>();
        List<Necessity> necessityList = new ArrayList<>();
        List<Contribution> contributionList = new ArrayList<>();
        List<String> stringList= ImportInformation.getStringsFromFile(fileNameWeight);
        for(int i=0; i<stringList.size(); i++){
            String line = stringList.get(i);
            String[] currentString=line.trim().split(",");
            if(i==0){
                for(int j=1; j<currentString.length; j++){
                    String[] riskFactorString=currentString[j].trim().split(" - ");
                    riskFactorList.add(new RiskFactor(new SurroundingType(riskFactorString[0]), new Metric(riskFactorString[1])));
                }
            }else{
                for(RiskFactor rf : riskFactorList) {
                    CoverageRiskFactorCell tmp = new CoverageRiskFactorCell(new Coverage(currentString[0]), rf);
                    if(rm.getListCoverageRiskFactorCell().contains(tmp))
                        crfcellList.add(tmp);
                }
            }
        }
        for(int i=0; i<stringList.size(); i++){
            String line = stringList.get(i);
            String[] currentString=line.trim().split(",");
            if(i!=0){
                for(int j=1; j<currentString.length; j++){
                    weightList.add(new Weight(Integer.parseInt(currentString[j])));
                }
            }
        }
        stringList= ImportInformation.getStringsFromFile(fileNameNecessity);
        for(int i=0; i<stringList.size(); i++){
            String line = stringList.get(i);
            String[] currentString=line.trim().split(",");
            if(i!=0){
                for(int j=1; j<currentString.length; j++){
                    necessityList.add(new Necessity(currentString[j]));
                }
            }
        }
        stringList= ImportInformation.getStringsFromFile(fileNameContribution);
        for(int i=0; i<stringList.size(); i++){
            String line = stringList.get(i);
            String[] currentString=line.trim().split(",");
            if(i!=0){
                for(int j=1; j<currentString.length; j++){
                    contributionList.add(new Contribution(currentString[j]));
                }
            }
        }
        if(crfcellList.size() != weightList.size() || crfcellList.size() != contributionList.size() || crfcellList.size() != necessityList.size())
            throw new IllegalArgumentException();
        for(int i=0; i<crfcellList.size(); i++)
            rm.addNewCharacterizedCell(new CharacterizedCell(crfcellList.get(i), contributionList.get(i), weightList.get(i), necessityList.get(i)));
        try {
            rmdb.save(rm);
        } catch (ConcurrencyException | IntegrityViolationException e) {
            throw new IllegalArgumentException();
        }
    }
}
