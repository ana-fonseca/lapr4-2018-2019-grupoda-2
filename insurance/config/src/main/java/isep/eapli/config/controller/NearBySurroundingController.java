package isep.eapli.config.controller;

import isep.eapli.core.domain.Address;
import isep.eapli.core.domain.Metric;
import isep.eapli.core.domain.NearBySurrounding;
import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.SurroundingType;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.SurroundingDB;
import isep.eapli.core.persistenceimplementation.SurroundingTypeDB;
import isep.eapli.georef.GMS.GMSRequest;
import java.util.Map;

public class NearBySurroundingController {

    /**
     * SurroundingTypeDB object
     */
    private SurroundingDB sdb;

    /**
     * Public constructor of the class
     */
    public NearBySurroundingController() {
        //Access Factory
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        
        //Get SurroundingDB
        sdb = repositoryAccess.getSurroundingDB();
    }

    public void save(NearBySurrounding nbs) {
        //save
        sdb.save(nbs);
    }

    public String getAddressSearch(NearBySurrounding nbs) {
        //Create Request
        GMSRequest gms = new GMSRequest();

        //Get Search
        return gms.getAddressSearch(nbs.getAdd().getCity().toString(), nbs.getAdd().postalCode().toString(), nbs.getAdd().street().toString());
    }

}
