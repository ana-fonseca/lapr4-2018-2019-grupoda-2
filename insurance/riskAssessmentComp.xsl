<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<html>
			<body>
				<h1>Same result</h1>
				<table border="1">
					<tr>
						<th>result</th>
					</tr>
					<xsl:for-each select="results/same-results">
						<tr>
							<td>
								<xsl:value-of select="result"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<h1>Different result</h1>
				<table border="1">
					<tr>
						<th>result</th>
					</tr>
					<tr>
						<th>rc1</th>
					</tr>
					<tr>
						<th>rc2</th>
					</tr>
					<xsl:for-each select="results/differente-results">
						<tr>
							<td>
								<xsl:value-of select="result"/>
							</td>
							<td>
								<xsl:value-of select="rc1"/>
							</td>
							<td>
								<xsl:value-of select="rc2"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
