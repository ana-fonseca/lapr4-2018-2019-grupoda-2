/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.georef.JavaLib;

import java.util.Stack;


/**
 *
 * @author Luís Silva
 */
public class EvalVisitor extends ExpressionsBaseListener {

    private final Stack<Integer> stack = new Stack<>();

    public int getResult() {
        return stack.peek();
    }
}
