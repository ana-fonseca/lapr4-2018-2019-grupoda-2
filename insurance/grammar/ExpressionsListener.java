// Generated from Expressions.g4 by ANTLR 4.7.1
package isep.eapli.georef.JavaLib;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExpressionsParser}.
 */
public interface ExpressionsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(ExpressionsParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(ExpressionsParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ExpressionsParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ExpressionsParser.ExprContext ctx);
}