/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.georef.JavaLib;

/**
 *
 * @author Luís Silva
 */
public class GeorefInfoToCalculation {
    
    public String transformValueToMatrixInfo(int valueA, int valueB, float min){
        if (min < valueA) {
            return "Baixa";
        }
        if (min > valueB) {
            return "Alta";
        }else {
            return "Média";
        }
    }
}
