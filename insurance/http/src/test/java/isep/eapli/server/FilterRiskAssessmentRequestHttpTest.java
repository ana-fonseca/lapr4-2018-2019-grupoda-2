/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.server;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luís Silva
 */
public class FilterRiskAssessmentRequestHttpTest {
    
    public FilterRiskAssessmentRequestHttpTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of filter method, of class FilterRiskAssessmentRequestHttp.
     */
    @Test
    public void testFilter() throws Exception {
        System.out.println("filter");
        String response = "{\n"
                + "    \"DataInicio\": \"2019-06-02 14:12\",\n"
                + "    \"DataFim\": \"2019-06-02 15:12\",\n"
                + "    \"pair\": [\n"
                + "        {\n"
                + "            \"Cidade1\":\"Desconhecida\"\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        FilterRiskAssessmentRequestHttp instance = new FilterRiskAssessmentRequestHttp();
        String expResult = "";
        String result = instance.filter(response);
        assertEquals(expResult, result);
    }
    
}
