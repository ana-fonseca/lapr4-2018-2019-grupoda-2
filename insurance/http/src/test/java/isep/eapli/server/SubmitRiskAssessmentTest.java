/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.server;

import isep.eapli.core.domain.Request;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import java.io.FileReader;
import java.util.Properties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Test Install
 */
public class SubmitRiskAssessmentTest {

    public SubmitRiskAssessmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void clearDB() {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        for (Request request : cdb.findAll()) {
            cdb.remove(request);
        }
    }

    /**
     * Test of submitRequest method, of class SubmitRiskAssessment.
     */
    @Test
    public void testSubmitRequestWorking() throws Exception {
        System.out.println("submit");
        String response = "{\n"
                + "    \"state\": \"PROCESSING\",\n"
                + "    \"type\": \"teste1\",\n"
                + "    \"version\": \"2019\\/5\\/12_1\",\n"
                + "    \"resultList\": [],\n"
                + "    \"pair\": [\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion1\": {\n"
                + "				\"address-street\" :	\"Rua Dr. Bernardino, 453\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4100-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "				\"address-district\"	: \"Porto\",\n"
                + "                \"coverage-designation\": \"Storm\"\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion2\": {\n"
                + "            	\"address-street\" :	\"Avenida da Boavista\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4150-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "                \"coverage-designation\": \"Fire\"\n"
                + "            }\n"
                + "        }\n"
                + "    ],\n"
                + "    \"validation\" : \"Risk Analyst\"\n"
                + "}";
        SubmitRiskAssessment instance = new SubmitRiskAssessment();
        String expResult = "The Risk Assessment Request was successful";
        String result = instance.submitRequest(response);
        
        clearDB();
        
        assertEquals(expResult, result);
    }

    @Test
    public void testSubmitRequestSameRequest() throws Exception {
        String response = "{\n"
                + "    \"state\": \"PROCESSING\",\n"
                + "    \"type\": \"teste1\",\n"
                + "    \"version\": \"2019\\/5\\/12_1\",\n"
                + "    \"resultList\": [],\n"
                + "    \"pair\": [\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion1\": {\n"
                + "				\"address-street\" :	\"Rua Dr. Bernardino, 453\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4100-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "				\"address-district\"	: \"Porto\",\n"
                + "                \"coverage-designation\": \"Storm\"\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion2\": {\n"
                + "            	\"address-street\" :	\"Avenida da Boavista\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4150-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "                \"coverage-designation\": \"Fire\"\n"
                + "            }\n"
                + "        }\n"
                + "    ],\n"
                + "    \"validation\" : \"Risk Analyst\"\n"
                + "}";
        SubmitRiskAssessment instance = new SubmitRiskAssessment();
        String expResult = "The Risk Assessment Request was not successful";
        instance.submitRequest(response);
        String result = instance.submitRequest(response);
        
        clearDB();

        assertEquals(expResult, result);
    }

    @Test
    public void testSubmitRequestOverLimit() throws Exception {
        FileReader in = new FileReader("../requestLimit.properties");
        Properties requestLimitProperties = new Properties();
        requestLimitProperties.load(in);
        int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));

        SubmitRiskAssessment instance = new SubmitRiskAssessment();
        
        for (int i = 0; i < 15; i++) {
            String response = "{\n"
                    + "    \"state\": \"PROCESSING\",\n"
                    + "    \"type\": \"teste" + i + "\",\n"
                    + "    \"version\": \"2019\\/5\\/12_1\",\n"
                    + "    \"resultList\": [],\n"
                    + "    \"pair\": [\n"
                    + "        {\n"
                    + "            \"insuranceCoverageConncetion1\": {\n"
                    + "				\"address-street\" :	\"Rua Dr. Bernardino, 453\",\n"
                    + "				\"address-city\"	: \"Porto\",\n"
                    + "				\"address-postalCode\" : \"4100-150\",\n"
                    + "				\"address-country\"	: \"Portugal\",\n"
                    + "				\"address-district\"	: \"Porto\",\n"
                    + "                \"coverage-designation\": \"Storm\"\n"
                    + "            }\n"
                    + "        },\n"
                    + "        {\n"
                    + "            \"insuranceCoverageConncetion2\": {\n"
                    + "            	\"address-street\" :	\"Avenida da Boavista\",\n"
                    + "				\"address-city\"	: \"Porto\",\n"
                    + "				\"address-postalCode\" : \"4150-150\",\n"
                    + "				\"address-country\"	: \"Portugal\",\n"
                    + "                \"coverage-designation\": \"Fire\"\n"
                    + "            }\n"
                    + "        }\n"
                    + "    ],\n"
                    + "    \"validation\" : \"Risk Analyst\"\n"
                    + "}";
            
            instance.submitRequest(response);
        }

        String response = "{\n"
                + "    \"state\": \"PROCESSING\",\n"
                + "    \"type\": \"teste15\",\n"
                + "    \"version\": \"2019\\/5\\/12_1\",\n"
                + "    \"resultList\": [],\n"
                + "    \"pair\": [\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion1\": {\n"
                + "				\"address-street\" :	\"Rua Dr. Bernardino, 453\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4100-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "				\"address-district\"	: \"Porto\",\n"
                + "                \"coverage-designation\": \"Storm\"\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            \"insuranceCoverageConncetion2\": {\n"
                + "            	\"address-street\" :	\"Avenida da Boavista\",\n"
                + "				\"address-city\"	: \"Porto\",\n"
                + "				\"address-postalCode\" : \"4150-150\",\n"
                + "				\"address-country\"	: \"Portugal\",\n"
                + "                \"coverage-designation\": \"Fire\"\n"
                + "            }\n"
                + "        }\n"
                + "    ],\n"
                + "    \"validation\" : \"Risk Analyst\"\n"
                + "}";
        
        String expResult = "The Risk Assessment Request was not successful";
        
        String result = instance.submitRequest(response);
        
        clearDB();

        assertEquals(expResult, result);
    }

}
