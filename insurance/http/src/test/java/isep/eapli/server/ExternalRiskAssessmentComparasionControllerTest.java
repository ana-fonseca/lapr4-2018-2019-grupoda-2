package isep.eapli.server;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ExternalRiskAssessmentComparasionControllerTest {

    /**
     * Test of riskAssessmentComparasion method, of class
     * ExternalRiskAssessmentComparasionController.
     */
    @Test
    public void testRiskAssessmentCompasionNull() {
        String response = null;
        ExternalRiskAssessmentComparasionController instance = new ExternalRiskAssessmentComparasionController();
        String expResult = "";
        String result = instance.riskAssessmentComparasion(response);
        assertEquals(expResult, result);
    }

    /**
     * Test of riskAssessmentComparasion method, of class
     * ExternalRiskAssessmentComparasionController.
     */
    @Test
    public void testRiskAssessmentCompasionEmpty() {
        String response = "";
        ExternalRiskAssessmentComparasionController instance = new ExternalRiskAssessmentComparasionController();
        String expResult = "";
        String result = instance.riskAssessmentComparasion(response);
        assertEquals(expResult, result);
    }

    /**
     * Test of riskAssessmentComparasion method, of class
     * ExternalRiskAssessmentComparasionController.
     */
    @Test
    public void testRiskAssessmentCompasionJSONToJSON() {
        String response = "{\n"
                + "    \"id1\": \"1\",\n"
                + "    \"id2\": \"0\",\n"
                + "    \"local\": \"Porto\"\n"
                + "\n"
                + "}";
        ExternalRiskAssessmentComparasionController instance = new ExternalRiskAssessmentComparasionController();
        String expResult = "json,{\"same-results\":[{\"result\":\"Coverage:Fire\\nCity:Porto\\n\"},{\"result\":\"Coverage:Fire\\nCity:Porto\\n\"},{\"result\":\"Coverage:Fire\\nCity:Porto\\n\"},{\"result\":\"Coverage:Earthquake\\nCity:Porto\\n\"},{\"result\":\"Coverage:Earthquake\\nCity:Porto\\n\"},{\"result\":\"Coverage:Storm\\nCity:Porto\\n\"},{\"result\":\"Coverage:Hurricane\\nCity:Porto\\n\"},{\"result\":\"Coverage:Fire\\nCity:Porto\\n\"},{\"result\":\"Coverage:Earthquake\\nCity:Porto\\n\"},{\"result\":\"Coverage:Storm\\nCity:Porto\\n\"},{\"result\":\"Coverage:Storm\\nCity:Porto\\n\"},{\"result\":\"Coverage:Earthquake\\nCity:Porto\\n\"},{\"result\":\"Coverage:Storm\\nCity:Porto\\n\"}],\"different-result\":[]}";
        String result = instance.riskAssessmentComparasion(response);
        // assertEquals(expResult, result);
    }

}
