package isep.eapli.server;

import isep.eapli.riskassessment.utils.RiskAssessmentComparasionRequest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExternalRiskAssessmentComparasionController {

    /**
     * Controller that receives the request with the information needed to the
     * risk assessment comparasion
     *
     * Luís Moreira
     *
     * @param response
     * @return the info of the comparasion in xml or json
     */
    public String riskAssessmentComparasion(String response) {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(new RiskAssessmentComparasionRequest().riskAssessmentComparasion(response));
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ExternalRiskAssessmentComparasionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

}
