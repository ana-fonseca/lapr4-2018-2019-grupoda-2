package isep.eapli.server;

import isep.eapli.riskassessment.controller.StatisticReportController;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 * This is the controller that manages information between Http To RiskAssessment module
 * @author Ana Isabel Fonseca
 */
public class StatisticReportHttpController {
    
    /**
     * This method returns the information in JSON
     * @param numConsultations number of consultations made 
     * @param numSubmittedRequests number of requests made
     * @param from initial execution time
     * @return JSON information
     * This method was created by Ana Fonseca (1170656)
     */
    public String statisticReportJSON(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(new StatisticReportController().reportJSON(numConsultations, numSubmittedRequests, from));
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(StatisticReportHttpController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

     /**
     * This method returns the information in JSON
     * @param numConsultations number of consultations made 
     * @param numSubmittedRequests number of requests made
     * @param from initial execution time
     * @return JSON information
     * This method was created by Ana Fonseca (1170656)
     */
    public String statisticReportXML(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) throws ParserConfigurationException, TransformerException {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            try {
                builder.append(new StatisticReportController().reportXML(numConsultations, numSubmittedRequests, from));
            } catch (ParserConfigurationException | TransformerException ex) {
                Logger.getLogger(StatisticReportHttpController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ExternalRiskAssessmentComparasionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }
    /**
     * This method returns the information in JSON
     * @param numConsultations number of consultations made 
     * @param numSubmittedRequests number of requests made
     * @param from initial execution time
     * @return JSON information
     * This method was created by Ana Fonseca (1170656)
     */    
    public String statisticReportXHTML(Integer numConsultations, Integer numSubmittedRequests, LocalDateTime from) throws ParserConfigurationException, TransformerException, SAXException, IOException {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            try {
                builder.append(new StatisticReportController().reportXHTML(numConsultations, numSubmittedRequests, from));
            } catch (ParserConfigurationException | TransformerException | SAXException | IOException ex) {
                Logger.getLogger(StatisticReportHttpController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ExternalRiskAssessmentComparasionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }


}
