package isep.eapli.server;

import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.controller.CurrentAvailabilityController;
import java.io.FileReader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author TiagoRibeiro (1170426)
 */
public class CurrentAvailabilityHTTPController {

    public CurrentAvailabilityHTTPController() {
        
    }
    
    public String currentAvailabilityJSON() {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(new CurrentAvailabilityController().currentAvailabilityJSON());
        });
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(CurrentAvailabilityHTTPController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return builder.toString();
    }
    
    public String currentAvailabilityXML() {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(new CurrentAvailabilityController().currentAvailabilityXML());
        });
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(CurrentAvailabilityHTTPController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return builder.toString();
    }
    
    public String currentAvailabilityXHTML() {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(new CurrentAvailabilityController().currentAvailabilityXHTML());
        });
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(CurrentAvailabilityHTTPController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return builder.toString();
    }

}
