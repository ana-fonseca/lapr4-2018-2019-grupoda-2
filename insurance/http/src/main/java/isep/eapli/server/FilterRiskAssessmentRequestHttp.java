/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.server;

import isep.eapli.riskassessment.utils.FilterRiskAssessmentRequest;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.ParseException;

public class FilterRiskAssessmentRequestHttp {
    
    public String filter(String response) throws ParseException, IOException {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            try {
                builder.append(new FilterRiskAssessmentRequest().filter(response));
            } catch (ParseException | IOException ex) {
                Logger.getLogger(FilterRiskAssessmentRequestHttp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(FilterRiskAssessmentRequestHttp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }
}
