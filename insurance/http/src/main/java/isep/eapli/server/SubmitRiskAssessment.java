package isep.eapli.server;

import isep.eapli.core.domain.Request;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import org.json.simple.JSONObject;
import isep.eapli.riskassessment.utils.ServiceJSON;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Maria Junqueira
 */
public class SubmitRiskAssessment {

    private RepositoryFactory rp;
    private RequestDB rdb;
    ServiceJSON service = new ServiceJSON();

    public SubmitRiskAssessment() {
        this.rp = new RepositoryFactory();
        this.rdb = rp.getRequestDB();
    }
    
    public String submitRequest(String response){
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            try {
                builder.append(submit(response));
            } catch (ParseException | IOException ex) {
                Logger.getLogger(SubmitRiskAssessment.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(SubmitRiskAssessment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

    public String submit(String response) throws ParseException, FileNotFoundException, IOException {
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(response);
        JSONObject caseObject = (JSONObject) obj;
        Request r = ServiceJSON.parseCaseObject((JSONObject) caseObject);
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        rdb = repositoryAccess.getRequestDB();

        FileReader in = new FileReader("../requestLimit.properties");
        Properties requestLimitProperties = new Properties();
        requestLimitProperties.load(in);
        int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));

        if (!rdb.checkIfAlreadyExists(r)) {
            if (rdb.countCurrentRequests() < requestLimit) {
                rdb.save(r);
                return ("The Risk Assessment Request was successful");
            }
        }
        
        return "The Risk Assessment Request was not successful";
    }
}
