/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.server;

import isep.eapli.core.domain.Request;
import isep.eapli.riskassessment.controller.SubmitMultipleRequestControllerTT;
import isep.eapli.riskassessment.utils.ServiceJSON;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import isep.eapli.riskassessment.utils.ServiceXML;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

/**
 *
 * @author becas
 */
public class SubmitMultipleRequestsController {

    public String submitMultRequests(String response, String returnType) throws Exception {
        if (response.isEmpty()) {
            return "";
        }
        List<Request> listR = new ArrayList<>();
        response = response.trim();
        if (response.startsWith("<?xml")) {
            listR = ServiceXML.readSubmitRequestsXML(response);
        } else {
            listR = ServiceJSON.readRequestList(response);
        }
        Thread[] thrs = new Thread[listR.size()];
        int[] counter = {0};
        for (int i = 0; i < listR.size(); i++) {
            Request r = listR.get(i);
            thrs[i] = new Thread(() -> {
                try {
                    if (submit(r)) {
                        counter[0]++;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(SubmitMultipleRequestsController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thrs[i].setPriority(Thread.MIN_PRIORITY);
        }
        
        for (Thread thread : thrs) {
            thread.start();
        }
        
        for (Thread thread : thrs) {
            thread.join();
        } 
        
        return returnTypeChooser(returnType, listR.size(), counter[0]);
    }

    public boolean submit(Request r) throws ParseException, IOException, ParserConfigurationException, TransformerException, SAXException, Exception {
        SubmitRiskAssessment sra = new SubmitRiskAssessment();
        SubmitMultipleRequestControllerTT smrc = new SubmitMultipleRequestControllerTT();
        return smrc.submitOneRequest(r);
    }
    
    private String returnTypeChooser(String returnType, int numMax, int submitted) throws Exception{
         switch (returnType) {
            case "xml":
                return ServiceXML.writeRequestsSubmissionMessageXML(numMax, submitted);
            case "json":
                return ServiceJSON.writeSubmitRequestsJSON(numMax, numMax);
            case "xhtml":
                return ServiceXHTML.writeXHTMLSubmitRequests(numMax, submitted);
        }
        return "Success";
    }
}
