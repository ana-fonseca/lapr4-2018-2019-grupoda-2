package isep.eapli.server;

import isep.eapli.core.domain.Request;
import isep.eapli.core.domain.Result;
import isep.eapli.core.persistenceimplementation.RepositoryFactory;
import isep.eapli.core.persistenceimplementation.RequestDB;
import isep.eapli.riskassessment.controller.GetRequestResultController;
import isep.eapli.riskassessment.utils.ServiceJSON;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GetRiskAssessmentRequestResultHttpController {

    //Private
    private RepositoryFactory rp;
    private RequestDB rdb;
    private GetRequestResultController grrc = new GetRequestResultController(rp, rdb);

    //Singleton Constructor
    public GetRiskAssessmentRequestResultHttpController() {
        this.rp = new RepositoryFactory();
        this.rdb = rp.getRequestDB();
        this.grrc = new GetRequestResultController(rp, rdb);
    }

    //Singleton instance
    private static GetRiskAssessmentRequestResultHttpController instance;

    //Singleton Access
    public static GetRiskAssessmentRequestResultHttpController getInstance() {
        //Lazy-load
        if (instance == null) {
            //Init now
            instance = new GetRiskAssessmentRequestResultHttpController();
        }

        //return internal instance
        return instance;
    }
    
    public String getById(String response) {
        StringBuilder builder = new StringBuilder();
        Thread thread = new Thread(() -> {
            builder.append(getJSON(response));
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
        
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(GetRiskAssessmentRequestResultHttpController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

    /**
     * Get a Risk Assessment Request Result by Id
     *
     * @param response
     * @return
     * @throws IOException
     */
    
    
    private String getJSON(String response) {
        try {
            
            //passar a string para jsonm e depois ir buscar o ID
            
            //create info string
             String[] info = null;
             
             //create json object
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(response);
            JSONObject caseObject = (JSONObject) obj;
            info = parseJsonToInfo(caseObject);
            
            
            String id = info[0];
            if (info[1] == null) {
            info[1] = "json";
            }
            
            //if input is not right
            if (id == null || id.isEmpty()) {
                return "";
            }
            
            //TODO: implementar alternativa em XML output 
            
            //Try obtain by Id from database
            List<Result> results = this.grrc.getRequestResult(Long.parseLong(id));
            
            //Get the most recent result (probably the latest one, BUT MAKE SURE!)
            Result mostRecent = results.get(results.size() - 1);

            //Convert object to JSON
            return ServiceJSON.writeRiskAssessmentRequestResult(mostRecent);
        } catch (NoResultException noResultException) {
            //Error ocurred
            return ServiceJSON.writeErrorMessage("Request with Id " + response + " not found.");
        } catch (Exception e) {
            //Log Internal
            System.err.println("Error Ocurred at GetRiskAssessmentRequestResultHttpController.getById for " + response + "\n" + e.getMessage());
            
            //Error ocurred
            return ServiceJSON.writeErrorMessage("Could not return the Request with Id " + response + ". Please contact the System Admin.");
        }
    }
    
    
    private static String[] parseJsonToInfo(JSONObject caseJson) {
        String[] info = new String[2];
        //Get id
        String id = (String) caseJson.get("id");
        if (id != null) {
            info[0] = id;
        }
        return info;
        
    }
}
