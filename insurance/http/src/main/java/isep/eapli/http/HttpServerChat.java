package isep.eapli.http;

import isep.eapli.config.bootstrapers.RequestsBootstrapper;
import java.io.*;
import java.net.*;
import java.time.LocalDateTime;
import java.util.*;

public class HttpServerChat {

    static private LocalDateTime runTime;
    static private final String BASE_FOLDER = "www";
    static private ServerSocket sock;

    public static void main(String args[]) throws Exception {

        setRunTime(LocalDateTime.now());

        args = new String[1];
        args[0] = "30202";

        Socket cliSock = null;

        if (args.length != 1) {
            System.out.println("Local port number required at the command line.");
            System.exit(1);
        }
        try {
            sock = new ServerSocket(Integer.parseInt(args[0]));
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + args[0]);
            System.exit(1);
        }
        System.out.println("Server ready, listening on port number " + args[0]);
        addMsg("HTTP Chat Server is ready ...");
        while (!exit) {
            cliSock = sock.accept();
            HttpChatRequest req = new HttpChatRequest(cliSock, BASE_FOLDER);
            req.start();
            req.join();
        }
    }

    // MESSAGES ARE ACCESSED BY THREADS - LOCKING REQUIRED
    private static int nextMsgNum = 0;
    private static final ArrayList<String> MSG_LIST = new ArrayList<>();

    public static String getMsg(int msgNumber) {
        synchronized (MSG_LIST) {
            while (msgNumber >= nextMsgNum) {
                try {
                    MSG_LIST.wait();
                } // wait for a notification on MSG_LIST's monitor
                // while waiting MSG_LIST's intr lock is released
                catch (InterruptedException ex) {
                    System.out.println("Thread error: interrupted");
                    return null;
                }
            }
            return MSG_LIST.get(msgNumber);
        }
    }

    public static void addMsg(String msg) {
        synchronized (MSG_LIST) {
            MSG_LIST.add(nextMsgNum, msg);
            nextMsgNum++;
            MSG_LIST.notifyAll(); // notify all threads waiting on MSG_LIST's monitor
        }
    }

    public static LocalDateTime RunTime() {
        return runTime;
    }

    public static void setRunTime(LocalDateTime runTime) {
        HttpServerChat.runTime = runTime;
    }

    private static boolean exit = false;
    
    public static void disable() {
        exit = true;
    }

}
