package isep.eapli.http;

import isep.eapli.riskassessment.utils.ServiceJSON;
import isep.eapli.riskassessment.utils.ServiceXHTML;
import isep.eapli.riskassessment.utils.ServiceXML;
import isep.eapli.server.CurrentAvailabilityHTTPController;
import isep.eapli.server.StatisticReportHttpController;
import isep.eapli.server.ExternalRiskAssessmentComparasionController;
import isep.eapli.server.FilterRiskAssessmentRequestHttp;
import isep.eapli.server.GetRiskAssessmentRequestResultHttpController;
import isep.eapli.server.SubmitMultipleRequestsController;
import isep.eapli.server.SubmitRiskAssessment;
import java.io.*;
import java.net.*;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

/**
 *
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpChatRequest extends Thread {

    public static Integer numSubmittedRequests = 0;
    public static Integer numConsultations = 0;
    String baseFolder;
    Socket sock;
    DataInputStream inS;
    DataOutputStream outS;
    StatisticReportHttpController src = new StatisticReportHttpController();
    FilterRiskAssessmentRequestHttp filter = new FilterRiskAssessmentRequestHttp();
    ExternalRiskAssessmentComparasionController eracc = new ExternalRiskAssessmentComparasionController();
    GetRiskAssessmentRequestResultHttpController grarc = GetRiskAssessmentRequestResultHttpController.getInstance();
    CurrentAvailabilityHTTPController cac = new CurrentAvailabilityHTTPController();
    SubmitMultipleRequestsController smrc = new SubmitMultipleRequestsController();
    SubmitRiskAssessment submit = new SubmitRiskAssessment();

    public HttpChatRequest(Socket s, String f) {
        baseFolder = f;
        sock = s;
    }

    @Override
    public void run() {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();
            response.setResponseStatus("200 Ok");

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/currentAvailability") && request.getURI().contains("=")) {
                    numConsultations++;
                    HttpServerChat.addMsg("/currentAvailability");
                    String output = request.getURI().split("=")[1];
                    switch (output) {
                        case "json":
                            response.setContent(cac.currentAvailabilityJSON(), "application/json");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xml":
                            response.setContent(cac.currentAvailabilityXML(), "application/xml");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xhtml":
                            response.setContent(cac.currentAvailabilityXHTML(), "application/xhtml");
                            response.setResponseStatus("200 Ok");
                            break;
                        default:
                            response.setContentFromString(
                                    "<html><body><h1>ERROR: 406 Wrong output parameter</h1></body></html>",
                                    "text/html");
                            response.setResponseStatus("406 Wrong output parameter");
                            break;
                    }
                } else if (request.getURI().startsWith("/messages/")) {
                    numConsultations++;
                    int msgNum;
                    try {
                        msgNum = Integer.parseInt(request.getURI().substring(10));
                    } catch (NumberFormatException ne) {
                        msgNum = -1;
                    }
                    if (msgNum < 0) {
                        response.setContentFromString(
                                "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("405 Method Not Allowed");
                    } else {
                        String msg = HttpServerChat.getMsg(msgNum);
                        // if msgNum doesn't yet exist, the getMsg() method waits
                        // until it does. So the HTTP request was received, but the
                        // HTTP response is sent only when there's a message
                        response.setContentFromString(msg, "text/plain");
                    }
                } else if (request.getURI().startsWith("/report")
                        && request.getURI().contains("=")) {
                    HttpServerChat.addMsg("/report");
                    String output = request.getURI().split("=")[1];
                    switch (output) {
                        case "json":
                            response.setContentFromString(src.statisticReportJSON(numConsultations, numSubmittedRequests, HttpServerChat.RunTime()), "application/json");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xml":
                            response.setContent(src.statisticReportXML(numConsultations, numSubmittedRequests, HttpServerChat.RunTime()), "application/xml");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xhtml":
                            response.setContent(src.statisticReportXHTML(numConsultations, numSubmittedRequests, HttpServerChat.RunTime()), "application/xhtml");
                            response.setResponseStatus("200 Ok");
                            break;
                        default:
                            response.setContentFromString(
                                    "<html><body><h1>ERROR: 406 Wrong output parameter</h1></body></html>",
                                    "text/html");
                            response.setResponseStatus("406 Wrong output parameter");
                            break;
                    }

                } else if (request.getURI().equals("/exit")) {
                    HttpServerChat.addMsg("/exit");
                    response.setContent("Exiting...", "text/plain");
                    response.setResponseStatus("201 Exiting");
                    HttpServerChat.disable();
                    this.interrupt();
                } else { // NOT GET /messages/ , THEN IT MUST BE A FILE
                    String fullname = baseFolder + "/";
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (!response.setContentFromFile(fullname)) {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
            } else { // NOT GET, must be POST
                System.out.println(request.getURI());
                if (request.getMethod().equals("POST")
                        && request.getURI().equals("/submitRiskAssessment")) {
                    HttpServerChat.addMsg(request.getContentAsString());
                    response.setContent(submit.submitRequest(request.getContentAsString()), "application/json");
                    numSubmittedRequests++;
                    response.setResponseStatus("200 Ok");
                } else if (request.getMethod().equals("POST")
                        && request.getURI().equals("/filter")) {
                    HttpServerChat.addMsg(request.getContentAsString());
                    response.setContent(filter.filter(request.getContentAsString()), "application/json");
                    response.setResponseStatus("200 Ok");
                } else if (request.getMethod().equals("POST")
                        && (request.getURI().equals("/riskAssessmentCompasion"))) {
                    HttpServerChat.addMsg(request.getContentAsString());
                    String info = eracc.riskAssessmentComparasion(request.getContentAsString());
                    String typeOutput = info.split(",")[0].trim();
                    System.out.println("info = " + info);
                    numConsultations++;
                    response.setContent(info.substring(typeOutput.length() + 1, info.length()), "application/" + typeOutput);
                    response.setResponseStatus("200 Ok");
                }//br 
                else if (request.getMethod().equals("POST")
                        && (request.getURI().equals("/getRiskAssessmentRequestResult"))) {
                    HttpServerChat.addMsg(request.getContentAsString());
                    String info = grarc.getById(request.getContentAsString());
                    numConsultations++;
                    response.setContent(info, "application/json");
                    response.setResponseStatus("200 Ok");

                } else if (request.getMethod().equals("POST") && request.getURI().startsWith("/submitMult")
                        && request.getURI().contains("=")) {
                    HttpServerChat.addMsg("/submitMult");
                    String output = request.getURI().split("=")[1];
                    switch (output) {
                        case "json":
                            response.setContent(smrc.submitMultRequests(request.getContentAsString(), "json"), "application/json");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xml":
                            response.setContent(smrc.submitMultRequests(request.getContentAsString(), "xml"), "application/xml");
                            response.setResponseStatus("200 Ok");
                            break;
                        case "xhtml":
                            response.setContent(smrc.submitMultRequests(request.getContentAsString(), "xhtml"), "application/xhtml");
                            response.setResponseStatus("200 Ok");
                            break;
                        default:
                            response.setContentFromString(
                                    "<html><body><h1>ERROR: 406 Wrong output parameter</h1></body></html>",
                                    "text/html");
                            response.setResponseStatus("406 Wrong output parameter");
                            break;
                    }
                } else {
                    response.setContentFromString(
                            "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                            "text/html");
                    response.setResponseStatus("405 Method Not Allowed");
                }
            }
            response.send(outS); // SEND THE HTTP RESPONSE
        } catch (IOException ex) {
            System.out.println("Thread I/O error on request/response");
        } catch (ParseException ex) {
            Logger.getLogger(HttpChatRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(HttpChatRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(HttpChatRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(HttpChatRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(HttpChatRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}
