**Maria Junqueira [1170842](../)** - AR05
=======================================


# 1. Requisitos

**AR03** Como Analista de Risco pretendo consultar os Pedidos de Avaliação de Risco por mim já validados.

- AR05.1. OS pedidos devem ser apresentados sempre ordenados dos mais recentes para os mais antigos e deve ser indicado o tempo decorrido desde a atribuição até à sua conclusão.

- AR05.2. Permitir (opcionalmente) que os pedidos sejam filtrados para um dado período de tempo.

- AR05.3. Deve ser apresentado um sumário (e.g quantidade e tempo médio de análise) dos pedidos apresentados.

- AR05.4 Permitir exportar o resultado da consulta (sumário incluído) para um documento XHTML.

**Interpretação do Caso de Uso:** 

# 2. Análise

#### **Diagrama de Sequência de Sistema**

![AR05](AR05_SSD.png)

Neste Caso de Uso, admitimos que o Analista de Risco pede para consultar os Pedidos de Avaliação de Risco por mim já validados. 
Posteriormente o sistema pergunta se o AR quer filtrar a consulta inserindo um período de tempo da validação.
Por fim a lista de Pedidos, filtrada ou não, é exportada para um documento XHTML se o Analista de Risco o desejar.

# 3. Design

## 3.1. Realização da Funcionalidade

#### **Diagrama de Sequência**

![AR05](AR05_SD.png)

## 3.2. Diagrama de Classes

![AR05](AR05_CD.png)

## 3.3. Padrões Aplicados

|**Padrão Controller**|O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o Caso de Uso. Deste modo, verificamos a diminuição da sensibilidade da Domain Layer. Neste caso, em específico, temos o RiskAssessmentController que é utilizado para controlar não só o Caso de Uso AR03, mas também o AR02.
Para além disso, verificamos uma ligação da UI com o RequestAnalysisController, isto acontece porque, apesar de ser o Caso de Uso AR04 a tratar da Análise de Risco, o AR03 deve permitir o redirecionamento do pedido.|
|**Padrão Repository**|Um repositório é essencialmente uma coleção de objetos de domínio em memória, e, com base nisso o padrão Repository permite realizar o isolamento entre a camada de acesso à Base de Dados, a sua camada de apresentação (UI) e camada de negócio (Domain).|
|**Padrão Factory**|Este padrão é muito utilizado em frameworks para definir e manter relacionamentos entre objetos.|
|**Padrão Open/Close Principle**| o princípio do aberto/fechado estabelece que entidades de software (classes, módulos, funções, etc.) devem ser abertas para extensão, mas fechadas para modificação.|

# 4. Implementação

**Na classe RiskAssessmentRequestController:**
	/**
     * Returns List of Validated Assigned Requests by the Risk Analyst
     * 
     * @param riskAnalyst logged in Risk Analyst
     * @return filtered list
     */
    public List<Request> validatedRequestList(RiskAnalyst riskAnalyst){
        List<Request> filteredList = new ArrayList<>();
        filteredList = requestDB.findValidatedAssignedRequests(riskAnalyst);
        return filteredList;
    }

	/**
     * 
     */
    public String averageValidationTime(List<Request> requestList, int counter){
        long aux = 0;
        for(Request request : requestList){
            LocalDateTime start = request.obtainAssignedDate();
            LocalDateTime finish = request.obtainDecisionDate();
            long diff = start.until(finish, ChronoUnit.MINUTES);
            aux = aux + diff;
        }
        long ave = Math.round(aux/requestList.size());
        String average = ave/24/60 + ": " + ave/60%24 + ":" + ave%60 + "Quantity: " + requestList.size();
        return average;
    }

**Na classe ServiceXHTML:**

public boolean validateXML(Document doc, File schemaFile) {
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = schemaFactory.newSchema(schemaFile);

            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(doc));

            return true;
        } catch (SAXException e) {

            e.printStackTrace();
            LOGGER.severe("The XML file is not valid");
        } catch (IOException e) {

            e.printStackTrace();
            LOGGER.severe("File doesn't exist");
        }

        return false;
    }

    /**
     * Gets the results or the state of a case on String formatted in xml style
     *
     * @param Request request
     * @return results/state
     */
    public String getContentInXml(Request request) {
        Document doc;
        String xmlString = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            doc = docBuilder.newDocument();

            if (request.obtainState() != State.VALIDATED) {
                Element root = doc.createElement("request");
                doc.appendChild(root);
                
                Element requestID = doc.createElement("id");
                requestID.setTextContent(String.valueOf(request.obtainID()));
                root.appendChild(requestID);
                
                Element requestState = doc.createElement("state");
                requestState.setTextContent(request.obtainState().toString());
                root.appendChild(requestState);
                
                Element requestDate = doc.createElement("requestDate");
                requestDate.setTextContent(request.obtainRequestedDate().toString());
                root.appendChild(requestDate);
                
                Element assignedDate = doc.createElement("assignedDate");
                assignedDate.setTextContent(request.obtainAssignedDate().toString());
                root.appendChild(assignedDate);
                
                List<Result> resultList = request.getResultList();
                for (Result r : resultList) {
                    Element resultElem = doc.createElement("result");
                    root.appendChild(resultElem);

                    Element country = doc.createElement("country");
                    country.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().country()));
                    resultElem.appendChild(country);

                    Element district = doc.createElement("district");
                    district.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().district()));
                    resultElem.appendChild(district);

                    Element postalCode = doc.createElement("postalCode");
                    postalCode.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().postalCode()));
                    resultElem.appendChild(postalCode);

                    Element street = doc.createElement("street");
                    street.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().street()));
                    resultElem.appendChild(street);

                    Element coverage = doc.createElement("coverage");
                    coverage.setTextContent(r.obtainIcc().obtainCoverage().obtainDesignation());
                    resultElem.appendChild(coverage);

                    Element riskIndex = doc.createElement("index");
                    riskIndex.setTextContent(String.valueOf(r.obtainIndex()));
                    resultElem.appendChild(riskIndex);
                }
            } else {
                Element root = doc.createElement("results");
                doc.appendChild(root);

                List<Result> resultList = request.getResultList();
                for (Result r : resultList) {
                    Element resultElem = doc.createElement("result");
                    root.appendChild(resultElem);

                    Element country = doc.createElement("country");
                    country.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().country()));
                    resultElem.appendChild(country);

                    Element district = doc.createElement("district");
                    district.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().district()));
                    resultElem.appendChild(district);

                    Element postalCode = doc.createElement("postalCode");
                    postalCode.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().postalCode()));
                    resultElem.appendChild(postalCode);

                    Element street = doc.createElement("street");
                    street.setTextContent(String.valueOf(r.obtainIcc().obtainInsurance().getAddress().street()));
                    resultElem.appendChild(street);

                    Element coverage = doc.createElement("coverage");
                    coverage.setTextContent(r.obtainIcc().obtainCoverage().obtainDesignation());
                    resultElem.appendChild(coverage);

                    Element riskIndex = doc.createElement("index");
                    riskIndex.setTextContent(String.valueOf(r.obtainIndex()));
                    resultElem.appendChild(riskIndex);
                }
            }

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                StringWriter writer = new StringWriter();
                tr.transform(new DOMSource(doc), new StreamResult(writer));

                if (request.obtainState() == State.VALIDATED) {
                    if (!validateXML(doc, new File(XSD_OUTPUT_XML_RESULTS))) {
                        return null;
                    }
                } else {
                    if (!validateXML(doc, new File(XSD_OUTPUT_XML_STATE))) {
                        return null;
                    }
                }

                xmlString = writer.getBuffer().toString();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return xmlString;
    }

    /**
     * Gets the results of the state of a case on String formatted in xhtml
     * style
     *
     * @param Request request
     * @return results/state
     */
    public String getContentInXhtml(Request request) {
        String xmlOutput = getContentInXml(request);
        String xhtmlOutput = "";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document doc = null;

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlOutput)));
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        if (doc == null) {
            return "";
        }

        try {
            Transformer tr;
            if (request.obtainState() == State.VALIDATED) {
                tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_FILE_RESULTS));
            } else {
                tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_FILE_STATE));
            }

            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter writer = new StringWriter();
            tr.transform(new DOMSource(doc), new StreamResult(writer));

            xhtmlOutput = writer.getBuffer().toString();

        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return xhtmlOutput;
    }

# 5. Integration/Demonstration

Para este caso as demostrações são feitas ao correr o programa.

# 6. Observações

Apesar de os métodos para o export para XHTML estarem implementados, estão a dar erro. 