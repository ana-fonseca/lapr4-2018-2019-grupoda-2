**Maria Junqueira [1170842](../)** - SE01
=======================================


# 1. Requisitos

**SE01** Como Sistema Externo pretendo submeter um Pedido de Avaliação de Risco para um ou mais locais, sendo indicado para cada local o conjunto de coberturas pretendido.

- SE01.1. O nível mínimo de critério de aceitação é a utilização do HTTP como protocolo de tranferência de dados e JSON como formato de dados de entrada.

- SE01.2. O nível 2 de critério de aceitação é a utilização do HTTP como protocolo de tranferência de dados e, para além do JSON, XML como formato de dados de entrada.

- SE01.3. O nível máximo de critério de aceitação é a utilização do HTTP e do HTTPS como protocolos de tranferência de dados e, para além do JSON, XML como formato de dados de entrada.

**Interpretação do Caso de Uso:** 

De acordo com o enunciado final do Projeto o Sistema Externo quer, utilizando o HTTP como protocolo de transferência, fazer uma submissão de um pedido de avaliação de risco. 
Através do Postman, o sistema externo submete o pedido, que estaria no formato de dados de entrada de XML ou JSON.

Para além disso, o Postman tem que ser capaz de informaro sistema externo se quer que um Analista de Risco avalie o Pedido submetido. Este critério, apesar de opcional, remete para o Caso de Uso AR02, uma vez que o Pedido é redirecionado para uma Lista de Pedidos Pendentes que o Analista de Risco tem acesso.

# 2. Análise

#### **Diagrama de Sequência de Sistema**

![SE01](SE01_SSD.png)

Neste Caso de Uso, admitimos que o Sistema Externo pede para submeter um Pedido de Avaliação de Risco. Depois de inserir o Caso, a/s Localizacão/ões do anterior, a/s Cobertura/s e por fim a Validação do Analista
Se sim, o Pedido de Avaliação de Risco é redirecionado para a Lista de Pedidos Pendentes para um Analista avaliar.

# 3. Design

## 3.1. Realização da Funcionalidade

#### **Diagrama de Sequência**

![SE01](SE01_SD.png)

## 3.2. Diagrama de Classes

![SE01](SE01_CD.png)

## 3.3. Padrões Aplicados

|**Padrão Repository**|Um repositório é essencialmente uma coleção de objetos de domínio em memória, e, com base nisso o padrão Repository permite realizar o isolamento entre a camada de acesso à Base de Dados, a sua camada de apresentação (UI) e camada de negócio (Domain).|

## 3.4. Testes 

**Teste SubmitRequest:** Verificar se o request é submetido.
	
    @Test
    public void testSubmitRequest() throws Exception {
        System.out.println("submit");
        String response = "{\n" +
"    \"state\": \"PROCESSING\",\n" +
"    \"type\": \"teste1\",\n" +
"    \"version\": \"2019\\/5\\/12_1\",\n" +
"    \"resultList\": [],\n" +
"    \"pair\": [\n" +
"        {\n" +
"            \"insuranceCoverageConncetion1\": {\n" +
"				\"address-street\" :	\"Rua Dr. Bernardino, 453\",\n" +
"				\"address-city\"	: \"Porto\",\n" +
"				\"address-postalCode\" : \"4100-150\",\n" +
"				\"address-country\"	: \"Portugal\",\n" +
"				\"address-district\"	: \"Porto\",\n" +
"                \"coverage-designation\": \"Storm\"\n" +
"            }\n" +
"        },\n" +
"        {\n" +
"            \"insuranceCoverageConncetion2\": {\n" +
"            	\"address-street\" :	\"Avenida da Boavista\",\n" +
"				\"address-city\"	: \"Porto\",\n" +
"				\"address-postalCode\" : \"4150-150\",\n" +
"				\"address-country\"	: \"Portugal\",\n" +
"                \"coverage-designation\": \"Fire\"\n" +
"            }\n" +
"        }\n" +
"    ],\n" +
"    \"validation\" : \"Risk Analyst\"\n" +
"}";
        SubmitRiskAssessment instance = new SubmitRiskAssessment();
        String expResult = "The Risk Assessment Request was sucessfull";
        String result = instance.submitRequest(response);
        assertEquals(expResult, result);
    }
    
}


# 4. Implementação

**Métodos Implementados para submissão de uma análise de risco**

public String submitRequest(String response) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        Object obj = jsonParser.parse(response);
        JSONObject caseObject = (JSONObject) obj;
        Request r = ServiceJSON.parseCaseObject((JSONObject) caseObject);
        RepositoryFactory repositoryAccess = new RepositoryFactory();
        rdb = repositoryAccess.getRequestDB();
        if (rdb.checkIfAlreadyExists(r)) {
            rdb.save(r);
        }
        return ("The Risk Assessment Request was sucessfull");
    }

# 5. Integration/Demonstration

Para demonstrar o funcionamento do sistema externos usamos a aplicação Postman que funciona como cliente de HTTP

**Exemplo**

{
    "state": "PROCESSING",
    "type": "teste1",
    "version": "2019\/5\/12_1",
    "resultList": [],
    "pair": [
        {
            "insuranceCoverageConncetion1": {
				"address-street" :	"Rua Dr. Bernardino, 453",
				"address-city"	: "Porto",
				"address-postalCode" : "4100-150",
				"address-country"	: "Portugal",
				"address-district"	: "Porto",
                "coverage-designation": "Storm"
            }
        },
        {
            "insuranceCoverageConncetion2": {
            	"address-street" :	"Avenida da Boavista",
				"address-city"	: "Porto",
				"address-postalCode" : "4150-150",
				"address-country"	: "Portugal",
                "coverage-designation": "Fire"
            }
        }
    ],
    "validation" : "Risk Analyst"
}

# 6. Observações

Neste projeto estão disponível apenas a leitura dos dados introduzidos em certas linguagens(Json), no  futuro poderá ser implementado novas leituras para outras linguagens. 