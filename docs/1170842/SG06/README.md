**Maria Junqueira [1170842](../)** - SG06
=======================================


# 1. Requisitos

**SG06** Como SG pretendo obter a diferença de elevação (em metros) relativamente ao nível médio da água do mar existente entre duas localizações.

- SG06.1. Esta informação deve ser obtida usando o serviço externo MBS.

- SG06.2. Esta informação deve ser obtida usando o serviço externo GMS.

- SG06.3. Esta informação deve ser obtida por combinação (i.e. distância e tempo médio) dos resultados dos serviços externos anteriores. 

- SG06.4. O sistema deve usar o método anterior que estiver configurado para o efeito.

**Interpretação do Caso de Uso:** Neste requesito foi necessario a integração das apis da google e da microsoft para conseguirmos implementar as funcionalidades propostas, o quarto requisito foi escolhida a gms como a predefinida pois após varios testes verificou-se ter os resultados mais proximos da realidade e mais completos

# 2. Análise

![SG06](SG06_SSD.png)

# 3. Design

## 3.1. Realização da Funcionalidade

#### **Diagrama de Sequência**

![SG06](SG06_SD.png)

## 3.2. Diagrama de Classes

![SG06](SG06_CD.png)

## 3.3. Padrões Aplicados

**High Cohesion**:
 O padrão tenta manter os objetos adequadamente focados e compreensíveis.

**Low Coupling**:
 O padrão determina como atribuir responsabilidades eficientemente.

**Solid**
 Interface Segregation Principle

**Controller**
 O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC.

## 3.4. Testes 

**Teste ElevationDifferenceGMSTest:** Calcula a  diferença entre alturas de dois locais

	@Test
    public void ElevationDifferenceGMSTest() {
        String latitude1 = "48.2";
        String longitude1 = "21.3";
        String latitude2 = "49.5";
        String longitude2 = "21.9";
        GMSRequest gms = new GMSRequest();
        float result = gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        DecimalFormat df = new DecimalFormat("00.000");
        assertEquals(df.format(-250.550), df.format(result));
    }

**Teste ElevationDifferenceBMSTest:** Calcula a  diferença entre alturas de dois locais

    @Test
    public void ElevationDifferenceBMSTest() {
        String latitude1 = "48.2";
        String longitude1 = "21.3";
        String latitude2 = "49.5";
        String longitude2 = "21.9";
        BMSRequest bms = new BMSRequest();
        float result = bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        assertEquals(3, result, 0.0001);
	}

# 4. Implementação

	public static float elevationDifferenceGMS(String latitude1, String longitude1, String latitude2, String longitude2){
        GMSRequest gms = new GMSRequest();
        return gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
    }
    
    public static float elevationDifferenceBMS(String latitude1, String longitude1, String latitude2, String longitude2){
        BMSRequest bms = new BMSRequest();
        return bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
    }
    
    public static float elevationDifferenceAVG(String latitude1, String longitude1, String latitude2, String longitude2){
        GMSRequest gms = new GMSRequest();
        float google = gms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        BMSRequest bms = new BMSRequest();
        float bing = bms.elevationDifference(latitude1, longitude1, latitude2, longitude2);
        return (google + bing) / 2;
    }	

	@Override
    public float elevationDifference(String latitude1, String longitude1, String latitude2, String longitude2) {
        float elevation1 = elevationSearch(latitude1, longitude1);
        float elevation2 = elevationSearch(latitude2, longitude2);
        return (elevation1 - elevation2);
    }

# 5. Integration/Demonstration

Esta funcionalidade é feita para um sistema externo, por isso não tem conhecimento das classes do dominio. Dessa forma, esta funcionalidade foi feita a pensar na passagem dos valores de dados simples (neste caso String) em objetos do dominio. O seguimento lógico é o controller receber um pedido dos envolventes e este vai invocar o metodo presente o qual vai usar vários serviço de procura que implementam a interface, que vai fazer a junção dos resultados.

# 6. Observações

O caso de uso foi feito com base nos casos de uso da georeferenciação anteriormente implementados ainda houve uma discusão de grupo para decidir qual seria a api que iria ser a nossa default e através do que foi observado por testes chegou-se ao acordo de ser utilizada a api da google