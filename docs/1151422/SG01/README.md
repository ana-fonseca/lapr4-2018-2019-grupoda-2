**Bruno Rodrigues [1151422](../)** - USDemo1
=======================================


# 1. Requisitos

**SG01**: Como SG pretendo obter a localização GPS de um determinado local a partir do seu endereço postal.

	• SG01.1. Esta informação deve ser obtida usando o serviço externo Microsoft Bing Services (MBS).
	• SG01.2. Esta informação deve ser obtida usando o serviço externo Google Maps Services (GMS).
	• SG01.3. O sistema deve usar o serviço que estiver configurado para o efeito.

Interpretação do caso de uso: De acordo com o enunciado do projeto, este caso de uso consiste em encontrar a posição GPS de um determinado local,
através do código postal recebido. Esta funcionalidade deve ser implementada usando API's externas associadas à microsoft e google (MBS e GMS).
A localização de um determinado local é definido pela sua latitude e longitude.

Dependência de outros requisitos: Não

Correlação com outros requisitios: AR02.2 : Permitir (opcionalmente) que os pedidos sejam filtrados para um dado distrito (e.g. Porto).

# 2. Análise

### Diagrama de Sequência de Sistema
![SG01 Diagrama Análise](SG01_SSD.jpg)

# 3. Design

## 3.1. Realização da Funcionalidade

![SG01 Diagrama de Sequência](SG01_SD.jpg)

## 3.2. Diagrama de Classes

![SG01 Diagrama de Classes](SG01_CD.jpg)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Solid
usos: 
- Single responsibility principle 
- Interface segregation principle


# 5. Integration/Demonstration

Esta funcionalidade é feita através de um sistema externo. O caso de uso começa no controller, que vai chamar o método addressSearch na classe addressSearch.
Esta mesma classe instancia BMS e GMS para que estas classes façam a comunicação com a API.
O resultado (retorno) do UC são coordenadas, latitude e longitude, sendo o tipo de retorno LatLng (classe nativa).

# 6. Observações

Derivado de um atraso, não foi possivel implementar este caso de uso usando a API da google, nem a concretização dos testes unitários.
Se assim for conveniente e decidido em equipa será implementado para a próxima entrega.


