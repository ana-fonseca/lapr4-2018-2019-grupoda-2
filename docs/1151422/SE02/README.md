**Bruno Rodrigues [1151422](../)** - SE02
=======================================


# 1. Requisitos

**SE02** Como SE pretendo submeter um Pedido de Avaliação de Risco para um ou mais locais, sendo indicado para cada local o conjunto de coberturas pretendido. 

##Interpretação do Caso de Uso: 
De acordo com o enunciado , a lógica deste caso de uso consiste na consulta do resultado de uma avaliação de risco previamente persistida na base dados.  

##Dependência de outros requisitos: 
Da comunicação a partir de um chat.
Do método que vai buscar à base dados o Request através do seu ID.

##Correlação com outros requisitos:
Nenhum caso de uso.

##Diagramas/artefactos de análise

![SE02 SSD](SE02_SSD.jpg)


# 3. Design


## 3.1. Realização da Funcionalidade
![SE02 SD](SE02_SD.jpg)

## 3.2. Diagrama de Classes

![SE02 CD](SE02_CD.jpg)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Singleton
O padrão Singleton instancia apenas quando necessário uma determinada class. Aumenta a performance do programa.

## 3.4. Testes 

Testes em desenvolvimento.

# 4. Implementação

##Evidências de que a implementação está em conformidade com o design efetuado

** 1ª evidencia **
![SE02 Evidências1](getById.jpg)

** 2ª evidencia **
![SE02 Evidências2](getRequestResult.jpg)

** 3ª evidencia **
![SE02 Evidências3](obtainRequestById.png)

# 5. Integration/Demonstration

Para esta integração, foi preciso perceber como são estruturados os ficheiros json e xml.
Foram adicionados métodos de escrita e leitura para ficheiros json.

# 6. Observações

Esta funcionalidade está apenas preparada para lidar com ficheiros json. Para melhoramento da mesma, próximo passo passa pela implementação em xml.


