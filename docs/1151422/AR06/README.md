**Bruno Rodrigues [1151422](../)** - AR06
=======================================


# 1. Requisitos

**AR06** Como AR pretendo registar a existência de uma envolvente (e.g. Refinaria da Petrogal) de um determinado tipo (e.g. Local Inflamável) numa determinada localização (e.g. Av. da Liberdade, Leça da Palmeira).

- AR06.1. Com base na informação introduzida, o sistema deve obter a localização GPS do mesmo e solicitar a sua confirmação e/ou retificação.

- AR06.2. Pelo menos uma imagem aérea centrada na envolvente registada deve ser exibida ao AR.

A interpretação feita deste requisito foi no sentido de ...

# 2. Análise

##Interpretação do Caso de Uso: 
De acordo com a lógica do enunciado, é suposto ser registado um novo surrounding, com localização associada. Esta localização deve ser calculada através do serviço google maps implementado com a API fornecida.
O utilizador deve confirmar esta localização e no caso de não estar correta, deve retifica-la.

##Diagramas/artefactos de análise

![AR06 SSD](AR06_SSD.jpg)

# 3. Design



## 3.1. Realização da Funcionalidade

![AR06 SD](AR06_SD.jpg)

## 3.2. Diagrama de Classes



## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Single responsibility principle 

###Repository

## 3.4. Testes 

![Teste](teste.png)


# 4. Implementação



# 5. Integration/Demonstration

Para desenvolvimento deste caso de uso, foi necessária a integração com a funcionalidade da georeferenciação, que dada uma Address, retorna a localização.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



