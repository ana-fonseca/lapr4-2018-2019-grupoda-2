**Aluno Exemplo [1170426](../)** - SG01 + SG02
=======================================


# 1. Requisitos

**SG01:** Como SG pretendo obter a localização GPS de um determinado local a partir do seu endereço
postal.

- SG01.1 - Esta informação deve ser obtida usando o serviço externo Microsoft Bing Services
(MBS).

- SG01.2 - Esta informação deve ser obtida usando o serviço externo Google Maps Services
(GMS).

- SG01.3 - O sistema deve usar o serviço que estiver configurado para o efeito.

*Para este US, foi interpretado que, ao receber um endereço postal, terá que ser retornado a latitude e longitude do local indicado, para tal, pelo que foi observado, os itens obrigatórios de um endereço postal observados são a localidade, a rua em questão e o código postal, por isso, tanto o serviço externo da Microsoft como da Google irá receber estas três informações para analisar e retornar a latitude e longitude.*

**SG02:** Como SG pretendo enriquecer um determinado endereço postal com a informação relativa à
sua localização administrativa como, por exemplo, a localidade, o distrito e o país.

- SG02.1 - Esta informação deve ser obtida usando o serviço externo MBS.

- SG02.2 - Esta informação deve ser obtida usando o serviço externo GMS.

- SG02.3 - O sistema deve usar o serviço que estiver configurado para o efeito.

*Para este US, foi interpretado que, ao receber parte de um endereço postal, terá que ser retornado o endereço postal completo do local indicado, ou seja, a rua, o código postal, a localidade, o distrito e o país (nesta ordem), para tal, pelo que foi observado, os itens obrigatórios de um endereço postal observados são a localidade, a rua em questão e o código postal, por isso, tanto o serviço externo da Microsoft como da Google irá receber estas três informações para analisar e retornar o endereço postal completo.*

# 2. Análise

No caso do terceiro ponto, tanto para o SG01 como para o SG02, como ambos serviços foram configurados para o efeito, foi decidido optar pelo serviço Google Maps Services, pois, após múltiplos testes, foi visto ser o mais fiel à realidade.

##SSD SG01
![Coordinates from Postal Address](SG01_SSD.png)

##SSD SG02
![Complete Postal address from partial Postal Address](SG02_SSD.png)

# 3. Design

## 3.1. Realização da Funcionalidade

###SG01

**Usando o serviço predefinido (GMS):**
![Coordinates from Postal Address](SG01_SD.png)

**Usando o Microsoft Bing Services (BMS):**
![Coordinates from Postal Address](SG01_BMS_SD.png)

###SG02

**Usando o serviço predefinido (GMS):**
![Complete Postal address from partial Postal Address](SG02_SD.png)

**Usando o Microsoft Bing Services (BMS):**
![Complete Postal address from partial Postal Address](SG02_BMS_SD.png)

## 3.2. Diagrama de Classes

###SG01

**Usando o serviço predefinido (GMS):**
![Coordinates from Postal Address](SG01_CD.png)

**Usando o Microsoft Bing Services (BMS):**
![Coordinates from Postal Address](SG01_BMS_CD.png)

###SG02

**Usando o serviço predefinido (GMS):**
![Complete Postal address from partial Postal Address](SG02_CD.png)

**Usando o Microsoft Bing Services (BMS):**
![Complete Postal address from partial Postal Address](SG02_BMS_CD.png)


## 3.3. Padrões Aplicados

*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

## 3.4. Testes 

###SG01
**Teste 1:** Verificar que ao dar os valores corretos, são indicadas as coordenadas certas com o serviço BMS:

	@Test
    public void addressSearchBMSTestComplete() {
        String result = new AdressSearchController().addressSearchBMS("Valbom", "4420-542", "Passeio Quinta do Sol");
        String expected = "41.12784,-8.56206";
        
        assertEquals(expected, result);
    }

**Testes 2 e 3:** Verificar que se falta algum dos valores, o resultado é NULL com o serviço BMS:

	@Test
    public void addressSearchBMSTestCityNull() {
        String result = new AdressSearchController().addressSearchBMS(null, "4420-542", "Passeio Quinta do Sol");
        
        assertNull(result);
    }
    
    @Test
    public void addressSearchBMSTestPostalCodeNull() {
        String result = new AdressSearchController().addressSearchBMS(null, null, "Passeio Quinta do Sol");
        String expected = "41.12784,-8.56206";
        
        assertNull(result);
    }

**Teste 4:** O mesmo que o **Teste 1** mas usando o serviço GMS:

	@Test
    public void addressSearchGMSTestComplete() {
        String result = new AdressSearchController().addressSearch("Valbom", "4420-542", "Passeio Quinta do Sol, 19");
        String expected = "41.12785210,-8.56214360";
        
        assertEquals(expected, result);
    }

**Testes 5 e 6:** O mesmo que os **Testes 2 e 3** mas usando o serviço GMS e verificando que o resultado é uma String vazia:

	@Test
    public void addressSearchGMSTestCityNull() {
        String result = new AdressSearchController().addressSearch(null, "4420-542", "Passeio Quinta do Sol");
        
        assertEquals("", result);
    }
    
    @Test
    public void addressSearchGMSTestPostalCodeNull() {
        String result = new AdressSearchController().addressSearch(null, null, "Passeio Quinta do Sol");
        
        assertEquals("", result);
    }

###SG02

**Teste 1:** Verificar que ao dar os valores corretos, é retornado a lista com o endereço postal completo correto com o serviço BMS:

	@Test
    public void completePostalAddressBMS() {
        List<String> result = new AdressSearchController().completePostalAddressBMS("Valbom", "4420-542", "Passeio Quinta do Sol");
        List<String> expected = new ArrayList<>();
        
        expected.add("Passeio Quinta do Sol");
        expected.add("4420-542");
        expected.add("Valbom");
        expected.add("Porto");
        expected.add("Portugal");
        
        assertEquals(expected, result);
    }

**Teste 2:** O mesmo que o **Teste 1** mas usando o serviço GMS:

	@Test
    public void completePostalAddressGMS() {
        List<String> result = new AdressSearchController().completePostalAddress("Valbom", "4420-542", "Passeio Quinta do Sol, 19");
        List<String> expected = new ArrayList<>();
        
        expected.add("Passeio Quinta do Sol, 19");
        expected.add("4420-542");
        expected.add("Valbom");
        expected.add("Porto");
        expected.add("Portugal");
        
        assertEquals(expected, result);
    }

# 4. Implementação

###SG01 e SG02

**Google Maps Services**

Para estes UC, usando o Google Maps Services como predefinição, é utilizado o API GeocodingApi, que por sua vez usa as classes GeocodingApiRequest e GeocodingResult. A classe GeocodingApiRequest é a classe que permite usar o API para pesquisar pelo Google Maps definindo, neste caso, a localidade, o código postal e o endereço. A classe GeocodingResult é a classe que vai registar e guardar os resultados do "Request" feito anteriormente. Para a classe GeocodingRequest ser utilizado, uma chave de acesso ao GeocodingAPI é necessária, esta chave encontra-se então num ficheiro separado chamado apiPlacesGMS.properties, sob o nome de "geoCodingAPI".
No entanto, para o UC SG01, através do objeto GeocodingResult, vai-se buscar a latitude e longitude do local, mas no caso do SG02, usando o mesmo objeto, vai-se buscar todos os dados do endereço postal em falta.

**Microsoft Bing Services**

No caso do serviço da Microsoft Bing, é usado outro ficheiro que contém uma chave de acesso, o ficheiro chama-se apiPlacesBMS.properties. Usando este serviço, usamos um BMSRequest e um Geocode. Este Geocode é o que cria o link do API (manualmente) usando os dados do endereço postal guardados no GeocodeRequest feito na classe BMSRequest. O Geocode então vai buscar as informações ao GeocodeRequest, cria o link que vai conter as informações do endereço postal e irá devolver um link de uma página XML. Esta página XML vai ser lida e, para o SG01, vai-se buscar a latitude e longitude deste endereço postal, e, no caso do SG02, vai-se buscar as informações em falta no endereço postal. 

# 5. Integration/Demonstration

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



