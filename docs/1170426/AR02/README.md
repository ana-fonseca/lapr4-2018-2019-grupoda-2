**Aluno [1170426](../)** - AR02
=======================================


# 1. Requisitos

**AR02** Como Analista de Risco pretendo consultar os Pedidos de Avaliação de Risco pendentes de validação e que
ainda não estejam atribuídos a nenhum outro Analista de Risco.

- AR02.1 - Os pedidos devem ser apresentados sempre ordenados dos mais antigos para os mais recentes.

- AR02.2 - Permitir (opcionalmente) que os pedidos sejam filtrados para um dado distrito (e.g. Porto).

- AR02.3 - Permitir que o AR requeira para si a análise de um dos Pedidos de Avaliação de Risco pendentes. Como resultado, o Pedido de Avaliação de Risco fica atribuído ao respetivo AR e a data da atribuição deve ser registada.

*Como prioridade deste US vem a listagem dos pedidos de avalição de risco pendentes e que não estejam atribuídos a nenhum outro analista de risco ordenados dos mais antigos aos mais recentes, ou seja, o utilizador AR criado no AR01 terá que ser associado e guardado no pedido de avaliação de risco. O AR também vai ter a possibilidade de filtrar esta listagem para um dado distrito, ou seja, os pedidos de de avaliação de risco terão que ter um distrito/localização associado a eles. Finalmente, como outra possibilidade temos a atribuição de um AR a um pedido de avalição de risco pendente que não tenha sido atríbuido a mais nenhum AR e guardar a data em que esta atribuição foi efetuada, o que quer dizer que o pedido de avalição de risco vai ter que ter uma data de atribuição de AR guardada dentro dele.*

# 2. Análise

##SSD
![AR02: SSD - Consult Pending Risk Assessment Requests Without Assigned Risk Analyst](AR02_SSD.png)

O analista de risco pode escolher simplesmente listar todos os Requests que estão no estado PENDING e não tenham nenhum analista de risco já atribuídos, alternativamente e opcionalmente, o analista de risco pode escolher também filtrar esta lista de Requests por um certo distrito, daí o "alt" e "opt".

# 3. Design

## 3.1. Realização da Funcionalidade

![AR02: SD - Consult Pending Risk Assessment Requests Without Assigned Risk Analyst](AR02_SD.png)

## 3.2. Diagrama de Classes

![AR02: CD - Consult Pending Risk Assessment Requests Without Assigned Risk Analyst](AR02_CD.png)

## 3.3. Padrões Aplicados

**Padrão Controller**

O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. Deste modo, verificamos a diminuição da sensibilidade da Domain Layer. Neste caso, em específico, temos o RiskAssessmentController que é utilizado para controlar não só o UC AR02, mas também o AR03.

**Padrão Repositório:**

Os Requests são encontrados na base de dados através do seu repositório (RequestDB) e, caso seja atribuído um Request a algum analista de risco, este é atualizado e guardado/persistido de novo na base de dados pelo seu repositório.

**Padrão Aggregate Root:**

Sendo a classe Request uma root de um agregado, o padrão Aggregate Root é aplicado pois, para obter qualquer outra informação pertencente ao agregado é necessário obter e passar primeiro pelo Request, por exemplo, não é possível obter o Result sem passar pelo Request.

## 3.4. Testes

**Teste 1:** Verificar que ao adicionar um Risk Analyst, pela primeira vez, este fica "assigned" e que a data fica a atual (classe Request).

	@Test
    public void assignRiskAnalystWorking() throws Exception {
        RiskAnalyst riskAnalyst = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");

        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(1,1);
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request request = new Request(iccList,"a","1",State.PENDING,v);
        assertTrue(request.obtainRequestedDate().format(DateTimeFormatter.BASIC_ISO_DATE).equals(LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE)));

        assertTrue(request.assignRiskAnalyst(riskAnalyst));
        assertTrue(request.obtainAssignedDate().format(DateTimeFormatter.BASIC_ISO_DATE).equals(LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE)));

        assertEquals(riskAnalyst, request.obtainRiskAnalyst());
    }

**Teste 2:** Verificar que após adicionar um Risk Analyst a um Request, não é possível adicionar mais nenhum (classe Request).

	@Test
    public void assignRiskAnalystNotWorking() throws Exception {
        RiskAnalyst riskAnalyst = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        RiskAnalyst riskAnalyst2 = new RiskAnalyst("lapr4da2.1@isep.ipp.pt", "lapr4DA#2");

        List<InsuranceCoverageConnection> iccList = new ArrayList<InsuranceCoverageConnection>();
        Version v = new Version();
        Coverage c = new Coverage("a");
        Address ad = new Address(1,1);
        Insurance i = new Insurance(ad);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(i,c);
        iccList.add(icc);
        Request request = new Request(iccList,"a","1",State.PENDING,v);
        request.assignRiskAnalyst(riskAnalyst);

        assertFalse(request.assignRiskAnalyst(riskAnalyst2));
    }

**Teste 3:** Verificar que ao ir buscar os Requests à base de dados vamos buscar os que estão no estado PENDING e que não têm nenhum Risk Analyst atribuído a eles ordenados do mais antigo ao mais recente (classe RequestDB).

	@Test
    public void findPendingUnassignedRequests() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        rp.getRiskAnalystDB().save(ra1);
        Address a = new Address(12.2, 44.4);
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);
        Version v1 = new Version(1);
        Version v2 = new Version(2);
        Version v3 = new Version(3);
        Request request1 = new Request(insuranceCoverageConnectionList, "Testing", "test1", State.PENDING, v1);
        Request request2 = new Request(insuranceCoverageConnectionList, "Testing", "test2", State.PENDING, v2);
        Request request3 = new Request(insuranceCoverageConnectionList, "Testing", "test3", State.PENDING, v3);
        Request request4 = new Request(insuranceCoverageConnectionList, "Testing", "test3", State.PENDING, new Version(4));
        request2.assignRiskAnalyst(ra1);
        request3.assignRiskAnalyst(ra1);
        request3.setState(State.VALIDATED);

        List<Request> expected = new ArrayList<>();
        expected.add(request1);
        expected.add(request4);

        cdb.save(request1);
        cdb.save(request2);
        cdb.save(request3);
        cdb.save(request4);

        List<Request> result = cdb.findPendingUnassignedRequests();
        assertEquals(expected, result);

        cdb.remove(request1);
        cdb.remove(request2);
        cdb.remove(request3);
        cdb.remove(request4);
    }

# 4. Implementação

Para este requisito foi necessário adicionar um analista de risco (RiskAnalyst) e a data em que o Request foi efetuado (LocalDateTime) como atributos da classe Request para poder filtrar por Requests que não tenham nenhum analista de risco atribuídos ordenados do mais antigo ao mais recente:

 	    @ManyToOne //O mesmo analista de risco pode estar atribuído a mais do que um Request
    	private RiskAnalyst riskAnalyst;
    
    	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    	private LocalDateTime requestedDate;

Para além disso, um método chamado de "findPendingUnassignedRequests()" também foi adicionado à classe RequestDB, classe responsável pela persistência dos Request na base de dados, este método usa o EntityManager para fazer uma Query que permite ir buscar específicamente uma lista de Requests que não tenham nenhum analista de risco atribuído, esteja no estado "PENDING" e ordenada do mais antigo ao mais recente:

		public List<Request> findPendingUnassignedRequests() {
        	EntityManager em = this.entityManager();
        	Query query = em.createNativeQuery("SELECT * FROM Request WHERE State = 'PENDING' AND RISKANALYST_RAID IS NULL ORDER BY REQUESTEDDATE ASC", Request.class);
        	List<Request> requestList = query.getResultList();
        	if (requestList.isEmpty()){
        	    return new ArrayList<>();
        	}
        	return requestList;
    	}

Para filtrar por distritos, será usado o Serviço de Georreferênciação (SG02) para obter informações sobre o endereço postal para então ir buscar os Requests que tenham pelo menos um objeto assegurado pertencente ao distrito pedido. Para tal o método "listPendingUnassignedRequests(String district)" (Overload do método em que não é utilizado filtro) irá buscar todos os Requests usando o método "findPendingUnassignedRequests()" da classe RequestDB (anteriormente mencionado) e irá buscar o endereço postal de cada objeto assegurado, enviando-o para o SG para obter o distrito em questão, caso seja o distrito pedido, este irá ser adicionado à lista filtrada:

		public List<Request> listPendingUnasignedRequests(String district) {
        	List<Request> filteredRequestList = new ArrayList<>();
        	for (Request request : requestDB.findPendingUnassignedRequests()) {
            	for (InsuranceCoverageConnection icc : request.getInsuranceCoverageConnectionList()) {
            	    Address address = icc.obtainInsurance().getAddress();
            	    if (address.district().district().equalsIgnoreCase(district)) {
            	        filteredRequestList.add(request);
            	        break;
            	    }
            	}
       		}
        	return filteredRequestList;
    	}

Para um analista de risco atribuir-se um Request a si mesmo, o atributo "assignedDate" foi adicionado à classe Reqyest:

		@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    	private LocalDateTime assignedDate; 

Tal como o método "assignRiskAnalyst(RiskAnalyst riskAnalyst)", que, verificando se este já foi atribuído a algum analista de risco, atribui este Request ao analista de risco em questão (se não houver nenhum anteriormente atribuído) e torna o valor do atríbuto "assignedDate" o momento em que o analista de risco é atribuído:

		public boolean assignRiskAnalyst(RiskAnalyst riskAnalyst) {
        	if (this.riskAnalyst == null) {
            	this.riskAnalyst = riskAnalyst;
            	this.assignedDate = LocalDateTime.now();
            	return true;
        	}
        	return false;
    	}

# 5. Integration/Demonstration

Para este requisito, para poder filtrar por distritos, será usado o Serviço de Georreferênciação (SG02) para obter informações sobre o endereço postal para então ir buscar os Requests que tenham pelo menos um objeto assegurado pertencente ao distrito pedido. Para tal o método "listPendingUnassignedRequests(String district)" (Overload do método em que não é utilizado filtro) irá buscar todos os Requests usando o método "findPendingUnassignedRequests()" da classe RequestDB (anteriormente mencionado) e irá buscar o endereço postal de cada objeto assegurado, enviando-o para o SG para obter o distrito em questão, caso seja o distrito pedido, este irá ser adicionado à lista filtrada, ou seja, o SG é necessário para usufruir deste UC.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
