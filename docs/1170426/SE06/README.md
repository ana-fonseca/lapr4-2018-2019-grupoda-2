**Aluno Exemplo [1170426](../)** - SE06
=======================================


# 1. Requisitos

**SE06** Como SE pretendo conhecer a disponibilidade atual (sim/não e carga atual) do Serviço de Avaliação de Risco para receber/processar novos pedidos.

- SE06.1 - O processamento destas solicitações deve ocorrer com uma prioridade máxima
relativamente ao processamento das restantes solicitações.

*Para este UC, foi interpretado que, quando o sistema externo for sinalizado devidamente, dependendo do tipo de ficheiro pedido, este irá retornar informações relacionadas com todos os pedidos de avaliação de risco atuais (ou seja, todos os pedidos sob o estado PENDING e PROCESSING), estas informações são: a quantidade de pedidos, o limite de pedidos concorrentes, a disponibilidade do serviço e uma breve informação sobre os pedidos atuais (id, estado e analista de risco, caso tenha um). Para tal, o utilizador não tem que enviar nenhuma informação externa, basta indicar no URI o ouput (e.g. "currentAvailability?output=xml). Em relação à prioridade, entende-se que este mecanismo é capaz de ser executado antes de todos os outros, independemente da quantidade em espera.*

# 2. Análise

No caso deste UC, foram disponibilizados 3 tipos de ficheiros/String como output, JSON, XML e XHTML (transformado por um XSLT através do XML).

![SE06_SSD](SE06_SSD.png)

# 3. Design

## 3.1. Realização da Funcionalidade

![SE06_SD](SE06_SD.png)

## 3.2. Diagrama de Classes

![SE06_CD](SE06_CD.png)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Single responsibility principle 

###Repository

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste JSON 1:** Verificar que o output é o antecipado quando está disponível.

	@Test
    public void testWriteCurrentRequestAvailabilityJSONAvailable() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 10; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String json = ServiceJSON.writeCurrentRequestAvailabilityJSON(requestList, currentLoad, 15);
        
        String expected = "{\"maxLoad\":15,\"availability\":true,\"requests\":[";
        
        int i = 0;
        for (Request request : requestList) {
            if (request.obtainRiskAnalyst() != null) {
                expected += "{\"riskAnalyst\":\"" + request.obtainRiskAnalyst().email().Email() + "\",";
            } else {
                expected += "{";
            }
            i++;
            expected += "\"id\":" + request.obtainID() + ",\"state\":\"" + request.obtainState().name() + "\"}" + (i < requestList.size() ? "," : "]");
        }
        
        expected += ",\"currentLoad\":10}";
        
        clearDB();
        codb.remove(cov);
        
        assertEquals(json, expected);
    }

**Teste JSON 2:** Verificar que o output é o antecipado quando não está disponível.
	@Test
    public void testWriteCurrentRequestAvailabilityJSONNotAvailable() throws Exception {
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 15; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String json = ServiceJSON.writeCurrentRequestAvailabilityJSON(requestList, currentLoad, 15);
        
        String expected = "{\"maxLoad\":15,\"availability\":false,\"requests\":[";
        
        int i = 0;
        for (Request request : requestList) {
            if (request.obtainRiskAnalyst() != null) {
                expected += "{\"riskAnalyst\":\"" + request.obtainRiskAnalyst().email().Email() + "\",";
            } else {
                expected += "{";
            }
            i++;
            expected += "\"id\":" + request.obtainID() + ",\"state\":\"" + request.obtainState().name() + "\"}" + (i < requestList.size() ? "," : "]");
        }
        
        expected += ",\"currentLoad\":15}";
        
        clearDB();
        codb.remove(cov);
        
        assertEquals(json, expected);
    }

**Teste XML 1:** Verificar que o output é o antecipado quando está disponível.

    @Test
    public void testWriteCurrentRequestAvailabilityXMLAvailable() throws Exception {
        clearDB();
        
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 10; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String xml = ServiceXML.writeCurrentRequestAvailabilityXML(requestList, currentLoad, 15);
        
        clearDB();
        codb.remove(cov);
        
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<Requests>\n" +
	"   <CurrentLoad>10</CurrentLoad>\n" +
	"   <MaxLoad>15</MaxLoad>\n" +
	"   <Availability>true</Availability>\n";
        
        for (Request request : requestList) {
            expected += "   <Request id=\"" + request.obtainID() + "\">\n" +
                    (request.obtainRiskAnalyst() == null ? 
	"       <RiskAnalyst/>\n" : "       <RiskAnalyst>" + request.obtainRiskAnalyst().email().Email() + "</RiskAnalyst>\n") +
	"       <State>" + request.obtainState().name() + "</State>\n" +
	"   </Request>\n";
        }
        
        expected += "</Requests>";
        
        assertEquals(expected.replaceAll("\\s", ""), xml.replaceAll("\\s", ""));
    }

**Teste XML 2:** Verificar que o output é o antecipado  não está disponível.

	@Test
    public void testWriteCurrentRequestAvailabilityXMLNotAvailable() throws Exception {
        clearDB();
        
        RepositoryFactory rp = new RepositoryFactory();
        RequestDB cdb = rp.getRequestDB();
        CoverageDB codb = rp.getCoverageDB();
        Coverage cov = new Coverage("Test1");
        cov = codb.save(cov);
        RiskAnalyst ra1 = new RiskAnalyst("lapr4da2@isep.ipp.pt", "lapr4DA#2");
        ra1 = rp.getRiskAnalystDB().save(ra1);
        RiskAnalyst ra2 = new RiskAnalyst("lapr4da21@isep.ipp.pt", "lapr4DA#2");
        ra2 = rp.getRiskAnalystDB().save(ra2);
        Address a = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
        Insurance in = new Insurance(a);
        InsuranceCoverageConnection icc = new InsuranceCoverageConnection(in, cov);
        List<InsuranceCoverageConnection> insuranceCoverageConnectionList = new LinkedList<>();
        insuranceCoverageConnectionList.add(icc);

        for (int i = 0; i < 15; i++) {
            State state = i < 10 / 2 ? State.PROCESSING : State.PENDING;
            Request request1 = new Request(insuranceCoverageConnectionList, String.valueOf(i), state, new Version(i));
            if (state == State.PROCESSING) {
                request1.assignRiskAnalyst(i % 2 == 0 ? ra1 : ra2);
            }
            cdb.save(request1);
        }
        
        List<Request> requestList = cdb.allCurrentRequests();
        long currentLoad = cdb.countCurrentRequests();
        
        String xml = ServiceXML.writeCurrentRequestAvailabilityXML(requestList, currentLoad, 15);
        
        clearDB();
        codb.remove(cov);
        
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<Requests>\n" +
	"   <CurrentLoad>15</CurrentLoad>\n" +
	"   <MaxLoad>15</MaxLoad>\n" +
	"   <Availability>false</Availability>\n";
        
        for (Request request : requestList) {
            expected += "   <Request id=\"" + request.obtainID() + "\">\n" +
                    (request.obtainRiskAnalyst() == null ? 
	"       <RiskAnalyst/>\n" : "       <RiskAnalyst>" + request.obtainRiskAnalyst().email().Email() + "</RiskAnalyst>\n") +
	"       <State>" + request.obtainState().name() + "</State>\n" +
	"   </Request>\n";
        }
        
        expected += "</Requests>";
        
        assertEquals(expected.replaceAll("\\s", ""), xml.replaceAll("\\s", ""));
    }

# 4. Implementação

Para este UC, foi criado um novo ficheiro chamado "requestLimit.properties" que contém o limite de pedidos de avaliação de risco concorrentes, o ficheiro encontra-se sob o seguinte formato: "requestLimit=15" atualmente e é lido da seguinte maneira:

		try {
            FileReader in = new FileReader("..\\requestLimit.properties");
            Properties requestLimitProperties = new Properties();
            requestLimitProperties.load(in);
            int requestLimit = Integer.parseInt(requestLimitProperties.getProperty("requestLimit"));

            return requestLimit;
        } catch (Exception e) {
            return 0;
        }

No caso do ficheiro de XHTML, foi utilizado um XSLT chamado "currentAvailability.xslt" e dentro do método, o ficheiro em XML é obtido e depois transformado em XHTML usando um Transformer da seguinte maneira:

			StreamSource strSource = new StreamSource(XSLT_CURRENT_AVAILABILITY);
            Transformer tr = TransformerFactory.newInstance().newTransformer(strSource);
            
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "html");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            
            StringWriter writer = new StringWriter();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(writer);
            tr.transform(source, result);
Onde, "XSLT\_CURRENT\_AVAILABILITY", é o caminho relativo para o ficheiro XSLT.

Sendo que para este UC é necessário ter uma prioridade máxima, o Controller do módulo RiskAssessment é chamado através de uma thread com prioridade máxima no Controller do móduglo HTTP da seguinte maneira (este modo foi aplicado aos outros métodos, mas como exemplo, será usado o método do JSON) : 

		public String currentAvailabilityJSON() {
        	StringBuilder builder = new StringBuilder();
        	Thread thread = new Thread(() -> {
        	    builder.append(new CurrentAvailabilityController().currentAvailabilityJSON());
        	});
        	thread.setPriority(Thread.MAX_PRIORITY);
        	thread.start();
        
        	try {
        	    thread.join();
        	} catch (InterruptedException ex) {
        	    Logger.getLogger(CurrentAvailabilityHTTPController.class.getName()).log(Level.SEVERE, null, ex);
        	}
        
        	return builder.toString();
    	}
	

# 5. Integration/Demonstration

Devido à prioridade deste UC sobre outros, os outros UC passaram a ser feitos através de threads também, mas estes com prioridade normal, como por exemplo: 

		public String riskAssessmentComparasion(String response) {
        	StringBuilder builder = new StringBuilder();
        	Thread thread = new Thread(() -> {
        	    builder.append(new RiskAssessmentComparasionRequest().riskAssessmentComparasion(response));
        	});
        	thread.setPriority(Thread.NORM_PRIORITY);
        	thread.start();
        	
        	try {
        	    thread.join();
        	} catch (InterruptedException ex) {
        	    Logger.getLogger(ExternalRiskAssessmentComparasionController.class.getName()).log(Level.SEVERE, null, ex);
        	}
        	return builder.toString();
    	}

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



