** Luís Moreira [1170620](../)** SE04
=======================================


# 1. Requisitos

**SE04** Como SE pretendo solicitar uma Comparação de Avaliação de Risco usando duas matrizes de risco distintas para um dado local e respetivo conjunto de coberturas. 

##Interpretação do Caso de Uso: 
De acordo com o enunciado do Projeto a lógica do caso de uso é o sistema externo possibilitar a comparação de avaliação de risco usando duas matrizes de risco distintas que já existão pressistidas na base de dados para um determinado local(cidade), utilizando o HTTP como protocolo de transferência.
 
##Dependência de outros requisitos: 
Da comunicação a partir de um chat.
Da comparação de avaliação de risco.

##Correlação com outros requisitos:
Nenhum caso de uso.

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

##Estudo/análise/comparação com o intuito de tomar as melhores opções de design

Que informação precisa de receber?
	-Dois id's de matrizes diferentes e um local.

Quem vai escrever para diferentes tipos de textos?
	-Varios services

Neste Caso de Uso, admitimos que o Sistema Externo pede para fazer uma comparação de Avaliação de Risco para um determinado local.

##Diagramas/artefactos de análise

![SE04 SSD](SE04_SSD.png)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

## 3.1. Realização da Funcionalidade

![SE04 SD](SE04_SD.png)

## 3.2. Diagrama de Classes

![SE04 CD](SE04_CD.png)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Single responsibility principle 

###Repository

## 3.4. Testes 

**Teste 1:** Verificar se o que é recebido for null trata corretamente

    /**
     * Test of riskAssessmentCompasion method, of class
     * ExternalRiskAssessmentComparasionController.
     */
    @Test
    public void testRiskAssessmentCompasionNull() {
        String response = null;
        ExternalRiskAssessmentComparasionController instance = new ExternalRiskAssessmentComparasionController();
        String expResult = "";
        String result = instance.riskAssessmentCompasion(response);
        assertEquals(expResult, result);
    }

**Teste 2:** Verificar se o que é recebido for vazio trata corretamente

    /**
     * Test of riskAssessmentCompasion method, of class
     * ExternalRiskAssessmentComparasionController.
     */
    @Test
    public void testRiskAssessmentCompasionEmpty() {
        String response = "";
        ExternalRiskAssessmentComparasionController instance = new ExternalRiskAssessmentComparasionController();
        String expResult = "";
        String result = instance.riskAssessmentCompasion(response);
        assertEquals(expResult, result);
    }

**Teste 3:** Verificar que consegue escrever corretamente em json o resultado 

    /**
     * Test of writeJSONRiskAssessmentComparasion method, of class ServiceJSON.
     */
    @Test
    public void testWriteJSONRiskAssessmentComparasion() throws Exception {
        Result resultInst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<>(), 10);
        Result result2Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino2", "4532"), new City("Porto2"), new PostalCode("4100-150"), new Country("Portugal2"), new District("Porto2"))), new Coverage("Test2")), new ArrayList<>(), 20);
        Result result3Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino3", "4532"), new City("Porto3"), new PostalCode("4100-150"), new Country("Portugal3"), new District("Porto3"))), new Coverage("Test3")), new ArrayList<>(), 30);

        List<String> info = new ArrayList<>();
        //Cases with the same result:
        info.add(resultInst.toString());
        info.add(result2Inst.toString());
        //Different results:
        Map<String, String> infoDifferent = new HashMap<>();
        infoDifferent.put(result3Inst.toString(), "35,50");
        RiskAssessmentComparasionDTO expInfo = new RiskAssessmentComparasionDTO(info, infoDifferent);
        String expResult = "{\"same-results\":[{\"result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino, 453\\nCity: Porto\\nPostal Code: 4100-150\\nDistrict: Porto\\nCountry: Portugal. Coverage to Analyse: Coverage related to Test1\\nScore obtained: 10\"},{\"result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino2, 4532\\nCity: Porto2\\nPostal Code: 4100-150\\nDistrict: Porto2\\nCountry: Portugal2. Coverage to Analyse: Coverage related to Test2\\nScore obtained: 20\"}],\"different-result\":[{\"result\":{\"rcs\":{\"rc2\":\"50\",\"rc1\":\"35\"},\"info-result\":\"========= RESULT =========\\nInsurance\\nPostal Address:\\nStreet: Rua Dr. Bernardino3, 4532\\nCity: Porto3\\nPostal Code: 4100-150\\nDistrict: Porto3\\nCountry: Portugal3. Coverage to Analyse: Coverage related to Test3\\nScore obtained: 30\"}}]}";
        String result = ServiceJSON.writeJSONRiskAssessmentComparasion(expInfo);
        assertEquals(expResult, result);
    }

**Teste 4:** Verificar que consegue escrever corretamente em xml o resultado 

	/**
     * Test of writeXMLRiskAssessmentComparasion method, of class ServiceXML.
     */
    @Test
    public void testWriteJSONRiskAssessmentComparasion() throws Exception {
        Result resultInst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"))), new Coverage("Test1")), new ArrayList<>(), 10);
        Result result2Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino2", "4532"), new City("Porto2"), new PostalCode("4100-150"), new Country("Portugal2"), new District("Porto2"))), new Coverage("Test2")), new ArrayList<>(), 20);
        Result result3Inst = new Result(new InsuranceCoverageConnection(new Insurance(new Address(new Street("Rua Dr. Bernardino3", "4532"), new City("Porto3"), new PostalCode("4100-150"), new Country("Portugal3"), new District("Porto3"))), new Coverage("Test3")), new ArrayList<>(), 30);

        List<String> info = new ArrayList<>();
        //Cases with the same result:
        info.add(resultInst.toString());
        info.add(result2Inst.toString());
        //Different results:
        Map<String, String> infoDifferent = new HashMap<>();
        infoDifferent.put(result3Inst.toString(), "35,50");
        RiskAssessmentComparasionDTO expInfo = new RiskAssessmentComparasionDTO(info, infoDifferent);
        String expResult = "<results>\n"
                + " <same-results>\n"
                + "     <result1>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino, 453\n"
                + "City: Porto\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto\n"
                + "Country: Portugal. Coverage to Analyse: Coverage related to Test1\n"
                + "Score obtained: 10</result1>\n"
                + "        <result2>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino2, 4532\n"
                + "City: Porto2\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto2\n"
                + "Country: Portugal2. Coverage to Analyse: Coverage related to Test2\n"
                + "Score obtained: 20</result2>\n"
                + "    </same-results>\n"
                + "    <differente-results>\n"
                + "        <result1>========= RESULT =========\n"
                + "Insurance\n"
                + "Postal Address:\n"
                + "Street: Rua Dr. Bernardino3, 4532\n"
                + "City: Porto3\n"
                + "Postal Code: 4100-150\n"
                + "District: Porto3\n"
                + "Country: Portugal3. Coverage to Analyse: Coverage related to Test3\n"
                + "Score obtained: 30<rc1>35</rc1>\n"
                + "            <rc2>50</rc2>\n"
                + "        </result1>\n"
                + "    </differente-results>\n"
                + "</results>";
        String result = ServiceXML.writeXMLRiskAssessmentComparasion(expInfo);
        result = result.replaceAll("\\s", "");
        expResult = expResult.replaceAll("\\s", "");

        assertTrue(result.equalsIgnoreCase(expResult));
        assertEquals(expResult, result);
    }

# 4. Implementação

##Evidências de que a implementação está em conformidade com o design efetuado

** 1ª evidencia **
![SE04 Evidências1](SE04_ligacao1.1.1_SD.png)

** 2ª evidencia **
![SE04 Evidências2](SE04_ligacao1.1.1.3Ate1.1.1.9_SD.png)

** 3ª evidencia **
![SE04 Evidências3](SE04_ligacao1.1.1.10Ate1.1.1.12_SD.png)

# 5. Integração/Demonstração
*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

Para que fosse possivel esta integração, foi preciso perceber como são estruturados os ficheiros json e xml, tanto para ler o pedido, como para devolver a resposta. Foi preciso perceber que informação é importante mostrar quando ocorre o pedido.

Como já havia o Service para Json onde foram adicionados os metódos de leitura e escrita neste tipo de ficheiros e foi criado o Service para XML.
Para o caso de ir procurar as matrizes de risco à base de dados, foi preciso adicionar um id que fosse possivel escolher mais facilmente.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

Num projeto futuro, seria mais correto começar por analisar os requesitos em vez começar logo a por em prática. Levando a melhorias, como por exemplo o caso dos Services do Json e XML, estes deviam implementar uma interface de leitura e outra interface para escrita.