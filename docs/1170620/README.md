** Luís Moreira [1170620](../)** 
===============================


### Índice das Funcionalidade Desenvolvidas ###


| Semana | Funcionalidade     |
|--------|--------------------|
| **1**  | [SG03](SG03) |
| **2**  | [SE04](SE04) |
| **3**  | [AR07](AR07) |