** Luís Moreira [1170620](../)** AR07
=======================================


# 1. Requisitos

**AR07** Como AR pretendo obter uma listagem das envolventes registadas no sistema. 

 - AR07.1. A listagem deve estar organizada por tipo de envolvente (e.g. Bombeiros, Hospitais). 
 - AR07.2. Permitir filtrar a listagem para esta incluir apenas envolventes de um determinado distrito. 
 - AR07.3. Permitir filtrar a listagem para esta incluir apenas envolventes localizadas numa área geográfica retangular arbitrariamente definida pelo AR (i.e. através de um conjunto de coordenadas GPS). 
 - AR07.4. Permitir exportar o resultado da listagem para um documento XHTML. 

##Interpretação do Caso de Uso: 
De acordo com o enunciado do Projeto a lógica do caso de uso é haver um pedido para a listagem dos envolventes registados no sistema, havendo uma filtragem por tipo de envolvente e podendo haver para apenas um determinado distrito
e dentro de uma area retangular. Sendo que este resultado possa ser exportado para um ficheiro XHTML.
 
##Dependência/Correlação de outros requisitos: 
Precisa que o registo de envolventes seja funcional.
Caso de uso AR06.

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

##Estudo/análise/comparação com o intuito de tomar as melhores opções de design

Como será tratado as restrições serem opcional?
	-É perguntado ao utilizador.

Que area será usado como default para a área geográfica retangular?
	-Área definida dentro de um ficheiro de configuração.

Quem terá a responsabilidade de guardar em um documento XHTML.
	-ServiceXHTML.


##Diagramas/artefactos de análise

![AR07 SSD](AR07_SSD.png)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

## 3.1. Realização da Funcionalidade

![AR07 SD](AR07_SD.png)

## 3.2. Diagrama de Classes

![AR07 CD](AR07_CD.png)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Single responsibility principle 

###Repository

## 3.4. Testes 

**Teste 1** Verificar que o output é o antecipado quando está disponível no método filterByDistrict.

    @Test
    public void testFilterByDistrict() {
        String districtTest = "Porto";
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));

        //result
        List<NearBySurrounding> result = listSurr.filterByDistrict(districtTest);
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }

**Teste 2** Verificar que o output é corretamente tratado quando é enviado null.

    @Test
    public void testFilterByDistrictNull() {
        String districtTest = "Porto";
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        lnbs.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        //result
        List<NearBySurrounding> result = listSurr.filterByDistrict(null);
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }
	
**Teste 3** Verificar que o método filterByDefaultArea trata de filtrar corretamente.
	
    @Test
    public void testFilterByDefaultArea() {
        String description = "SurroundingTest";
        SurroundingType surroundingType = new SurroundingType("SurroundingTypeTest");
        SurroundingType surroundingType2 = new SurroundingType("SurroundingTypeTest");
        Map<Metric, String> metricStringMap = new HashMap<>();
        metricStringMap.put(new Metric("MetricTest1"), "low");
        metricStringMap.put(new Metric("MetricTest2"), "medium");
        metricStringMap.put(new Metric("MetricTest3"), "high");
        Address complete = new Address(streetComplete, city, postalCode, country, district);
        Address complete2 = new Address(streetComplete, city, postalCode, country, district2);
        Address complete3 = new Address(street2, city2, postalCode2, country, district3);

        NearBySurrounding nbs1 = new NearBySurrounding(description, surroundingType, metricStringMap, complete);
        nbs1.setLocalization("41.124480926269115,-8.443161629638666"); //dentro da localização predefinida
        NearBySurrounding nbs2 = new NearBySurrounding(description, surroundingType, metricStringMap, complete2);
        nbs2.setLocalization("41.53597325445661,-8.433498243408166");//dentro da localização predefinida
        NearBySurrounding nbs3 = new NearBySurrounding(description, surroundingType2, metricStringMap, complete3);
        nbs3.setLocalization("37.1536076569702,-8.066788338623041");//fora da localização predefinida

        List<NearBySurrounding> lnbs = new ArrayList<>();
        lnbs.add(nbs1);
        lnbs.add(nbs2);
        lnbs.add(nbs3);

        ListSurroundings listSurr = new ListSurroundings(lnbs);
        //expected
        List<NearBySurrounding> expResult = new ArrayList<>();
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete));
        expResult.add(new NearBySurrounding(description, surroundingType, metricStringMap, complete2));

        //result
        List<NearBySurrounding> result = listSurr.filterByDefaultArea();
        assertTrue(expResult.containsAll(result));
        assertTrue(result.containsAll(expResult));
    }	

# 4. Implementação

Para este UC, foi criado um novo ficheiro chamado "coordinates.properties" que contém duas coordenadas, sendo que estas irão servir para os cantos oposto de uma área geográfica retangular.

	  FileReader in;

            in = new FileReader("..\\coordinates.properties");
            Properties apiMBS = new Properties();
            apiMBS.load(in);
            double lat1 = Double.parseDouble(apiMBS.getProperty("lat1").trim());
            double lon1 = Double.parseDouble(apiMBS.getProperty("lon1").trim());
            double lat2 = Double.parseDouble(apiMBS.getProperty("lat2").trim());
            double lon2 = Double.parseDouble(apiMBS.getProperty("lon2").trim());
            if (lat1 > lat2) { //if the lat1 is higher that lat2 it changes to the inverse
                double aux = lat1;
                lat1 = lat2;
                lat2 = aux;
            }
            if (lon1 > lon2) {//if the lon1 is higher that lon2 it changes to the inverse
                double aux = lon1;
                lon1 = lon2;
                lon2 = aux;
            }
			

No caso do utilizador querer exportar para um ficheiro de XHTML, foi utilizado um XSLT chamado "nearBySurroudings.xslt" e dentro do método, a informação em XML é obtido e depois transformado em XHTML usando um Transformer da seguinte forma:

			String xmlOutput = ServiceXML.writeXMLNearBySurrouding(lnbs);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            Document doc;

            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlOutput)));

            if (doc != null) {

                Transformer tr = TransformerFactory.newInstance().newTransformer(new StreamSource(XSLT_NEAR_BY_SURROUNDINGS));

                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "html");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                FileWriter writer = new FileWriter("../ListNearBySurroundings.html");
                tr.transform(new DOMSource(doc), new StreamResult(writer));
            }
			
Como é pedido para ordenar pelo Tipo de Envolvente, é utilizado um sort da Classe Collection.

		Collections.sort(surroundings, new Comparator() {
            @Override
            public int compare(Object surroundingOne, Object surroundingTwo) {
                //use instanceof to verify the references are indeed of the type in question
                return ((NearBySurrounding) surroundingOne).getType().obtainDescription()
                        .compareTo(((NearBySurrounding) surroundingTwo).getType().obtainDescription());
            }
        });

##Evidências de que a implementação está em conformidade com o design efetuado

###Evidências Controller###

![AR07_Evidencias_Controller_SD](AR07_Evidencias_Controller_SD.png)

###Evidências Exemplo List Surrounding###

![AR07_Evidencias_ExListSurroundings_SD.png](AR07_Evidencias_ExListSurroundings_SD.png)

# 5. Integração/Demonstração
*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

	Para o desenvolvimento deste caso de uso foi preciso ter atenção à base de dados, para acrescentar juntamente com o caso de uso AR06 um repositório para os Surroundings.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

	Num desenvolvimento futuro seria feito um estudo com mais promonor á informação que realmente é precisa ser guardada no ficheiro xhtml. Desenvolver um estudo para que áre é realmente importante estar intrinseca na área geográfica retangular.
	Poderia também desenvolver diversos métodos de exportação.