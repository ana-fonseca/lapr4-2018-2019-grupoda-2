** Luís Moreira [1170620](../)** SG03
=======================================


# 1. Requisitos

**SG03** Como SG pretendo obter quais são as envolventes (designação e localização) de um determinado tipo (e.g. Hospital) que existem na proximidade de um determinado local. 

- SG03.1. Esta informação deve ser obtida usando o serviço externo Microsoft Bing Services(MBS).
- SG03.2. Esta informação deve ser obtida usando o serviço externo Google Maps Services (GMS).
- SG03.3. Esta informação deve ser obtida por combinação (i.e. união, sem elementos duplicados) dos resultados dos serviços externos anteriores.
- SG03.4. O sistema deve usar o método anterior que estiver configurado para o efeito

##Interpretação do Caso de Uso: 
De acordo com o enunciado do Projeto a lógica do caso de uso é com um "determinado local"(localização) e um tipo de envolvente (e.g. Hospital)
 usar o serviço externo MBS e o GMS para receber os envolventes(designação e localização) do tipo escolhido.
 Sendo entendido por localização coordenadas no formato latitude, longitude e designação o nome do envolvente(e.g. Hospital São João).
 
##Dependência de outros requisitos: 
Não existe

##Correlação com outros requisitos:
Relacionado com a avaliação de risco da matriz.
 

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

##Estudo/análise/comparação com o intuito de tomar as melhores opções de design

Como é que o RABL(Risk Assessment Business Logic) vai conhecer a informação que é devolvida pelo Sistema Externo de Georeferenciação?
	- Usar DTO pronto para receber um valor primitivo ou String e transformar em objetos do dominío.

Caso haja o MBS e GMS qual deve ser usado?
	- Isso dependerá da implementação, por isso as classes do sistema externo não vão ser chamadas diretamente, mas vai passar por uma classe intermedia que irá decidir qual é utilizado por defeito.
	- Nesta resolução foi escolhido usar ambos os sistemas e juntar os resultados(sem repetições)
	
Como vai haver mais do que um serviço de procura como se garante bom acopulamento?
	- Utilizar uma interface

Como é que as envolventes  são identificadas?
	- Pela localização (latitude e longitude)


##Diagramas/artefactos de análise

![SG03 Diagrama Análise](SG03_SSD.png)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

## 3.1. Realização da Funcionalidade

![SG03 Diagrama Funcionalidade](SG03_SD.png)

## 3.2. Diagrama de Classes

![SG03 Diagrama Classes](SG03_CD.png)

## 3.3. Padrões Aplicados

###High Cohesion
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

###Low Coupling
O padrão determina como atribuir responsabilidades para apoiar.

###Controller
O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC. 

###Solid
usos: 
- Single responsibility principle 
- Interface segregation principle


## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível fazer uma procura por surroudings com latitude ou longitude ou surroudingType em null.

	@Test
    public void testGetSurroundingsNullParam() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "hospital";
        Map<String, String> result;
        Map<String, String> expResult = new HashMap<>();

        //latitude null
        result = SurroundingsNearBy.getSurroundings(null, longitude, surroundingType);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));

        //longitude null
        result = SurroundingsNearBy.getSurroundings(latitude, null, surroundingType);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));

        //surroundingType null
        result = SurroundingsNearBy.getSurroundings(latitude, longitude, null);
        //compare key set
        assertTrue(expResult.keySet().containsAll(result.keySet()));
        assertTrue(result.keySet().containsAll(expResult.keySet()));
        //compare values
        assertTrue(result.values().containsAll(expResult.values()));
    }
	
**Test 2:** Verificar que não recebe nenhum envolvente se enviar um tipo de envolvente não existente nos serviços de procura

	@Test
    public void testGetSurroundingsNotValidSurroudingType() {
        String latitude = "47.668979";
        String longitude = "-122.387562";
        String surroundingType = "DontExit";
        Map<String, String> result = SurroundingsNearBy.getSurroundings(null, longitude, surroundingType);;
        Map<String, String> expResult = new HashMap<>();
    }

# 4. Implementação

##Evidências de que a implementação está em conformidade com o design efetuado

Codigo a mostrar que está de acordo com a ligação 1.1 do diagrama de sequencia.
![SG03 Ligação 1.1 do Diagrama Sequência](SG03_ligacao1.1_SD.png)


Codigo a mostrar que está de acordo com as ligações 1.1.1 até 1.1.4 do diagrama de sequencia.
![SG03 Ligaçções 1.1.1 até 1.1.4 do Diagrama Sequência](SG03_ligacoes1.1.1Ate1.1.4_SD.png)

##Outros ficheiros (e.g. de configuração) relevantes

Como é preciso usar valores default e como se quer que seja possivel alterar em run time, foi escolhido usar dois ficheiro de propriedades.
Cada um deles com a chave da api associada a cada um (Microsoft Bing Services  e Google Maps Services), assim como a distancia que se escolheu como máxima em cada um dos casos.
A razão de as distâncias não serem guardadas na mesma variavel, é devido aos sistemas terem nivel de precisão e quantidade de informação diferentes.
Os ficheiros encontram se na root do projeto de georeferenciação e têm o nome "apiPlacesGMS.properties" & "apiPlacesMBS.properties".

# 5. Integração/Demonstração

Como esta funcionalidade é feita para um sistema externo, esta não tem conhecimento das classes do dominio. Por isso, esta funcionalidade foi feita a pensar na facil 
passagem dos valores de dados simples (neste caso String) em objetos do dominio.
O seguimento lógico é o controller receber um pedido dos envolventes e este vai invocar o metodo presente no SurroudingsNearBy em que este vai usar os vários serviço de procura que implementam a interface SearchService e que este vai fazer a junção dos resultados.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
Na realização deste trabalho, foi preciso uma busca intensiva sobre a documentação sobre a API da Microsoft Bing Services sobre georeferenciação e está tem pouco apoio e é de difícil alterção de código, o que leva ao alto acopulamento. Por outro lado,
a API da Google Maps Services sobre georeferenciação é abastada de documentação e apoio online e de fácil uso, estando já preparada para mudanças, usando melhores práticas de codigo.
A nível de informação que se consegue obter com estes sistemas, o sistema da Bing não funciona em muitos países o que leva a que não seja uma implementação boa, ao contrario da informação presente nos serviços da Google
em que existe a uma escala muito maior com atualizações muito mais frequentes.

Vendo quer a prespetiva de implementação, quer a de informação presente nos dois sistemas, num projeto futuro usaria a API do Google.


