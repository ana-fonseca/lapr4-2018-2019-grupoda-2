**Luís Silva [1160892](../)** - SG-07
=======================================


# 1. Requisitos

**SG07** Como SG pretendo disponibilizar de forma integrada/consolidada toda a informação
georreferenciada necessária ao cálculo do índice de risco de um determinado Pedido de Avaliação de
Risco.

- SG07.1. A transformação dos valores numéricos (e.g. distância, tempo, quantidade)
fornecidos pelos serviços externos para a escala discreta da matriz de risco (“baixa”, “média”,
“alta”) é realizada através de um par de valores (A e B) pré-definidos (configurados) por tipo
de envolvente. Exemplo: um valor numérico inferior a A implica classificar como “baixa”, por
outro lado se o valor for superior a B implica classificar como “alta”, caso contrário classifica-se como “média”.

- SG07.2. A transformação anterior é realizada através de uma expressão especificada de
acordo com uma gramática a desenvolver. A gramática deve permitir expressar regras
semelhantes às aplicadas anteriormente (i.e. baseadas em 2 valores).

- SG07.3. Evoluir a gramática para permitir expressar regras mais complexas (e.g. explorar
informação de contexto) e para uma escala com mais de 3 valores discretos.

A interpretação feita deste requisito foi no sentido de usar o sistema externo de georreferenciação na ajuda ao cálculo do índice de risco.

# 2. Análise

![SG07 Diagrama Análise](SG07-SSD.png)

##### Main Actor

+ Sistema Externo(SG)

##### Interested parts and their interests

+ Sistema Georreferenciação : pretende ajudar no calculo do índice de risco;


# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

![SG07 Diagrama Sequência](SG07-SD.png)

## 3.2. Diagrama de Classes

![SG07 Diagrama de Classes](SG07-CD.png)

## 3.3. Padrões Aplicados

Padrão Repository
Padrão Controller

## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test
	public void testTransformValueToMatrixInfo() {
		System.out.println("transformValueToMatrixInfo");
		int valueA = 0;
		int valueB = 5;
		float min = 3.0F;
		GeorefInfoToCalculation instance = new GeorefInfoToCalculation();
		String expResult = "Média";
		String result = instance.transformValueToMatrixInfo(valueA, valueB, min);
		assertEquals(expResult, result);
		valueA = 4;
		expResult = "Baixa";
		result = instance.transformValueToMatrixInfo(valueA, valueB, min);
		assertEquals(expResult, result);
		min = 7;
		expResult = "Alta";
		result = instance.transformValueToMatrixInfo(valueA, valueB, 	min);
		assertEquals(expResult, result);
}

# 4. Implementação

	public String getInfoToCalculationDistance(String surronding, int valueA, int valueB, String latitudeO, String longitudeO) throws IOException, FileNotFoundException, ApiException, InterruptedException{
			DecimalFormat df = new DecimalFormat("00.00");
			Map<String,String> map = SurroundingsNearByController.getSurroundings(surronding, latitudeO, longitudeO);
			float min = Float.MAX_VALUE;
			for (String value : map.values()) {
					String[] coordenadas = value.split(",");
					float [] distanceTime = TravellingTimeDistanceController.travelCalculationGMS(latitudeO, longitudeO, coordenadas[0], coordenadas[1], null);
					if (distanceTime[0] < min) {
							min = distanceTime[0];
					}
			}
			String result = new GeorefInfoToCalculation().transformValueToMatrixInfo(valueA, valueB, min);
			return "Distance: " + df.format(min) + "km\n Result to scale: " + result;
	}

	public String getInfoToCalculationTime(String surronding, int valueA, int valueB, String latitudeO, String longitudeO) throws IOException, FileNotFoundException, ApiException, InterruptedException {
			DecimalFormat df = new DecimalFormat("00.00");
			Map<String, String> map = SurroundingsNearBy.getSurroundings(surronding, latitudeO, longitudeO);
			float min = Float.MAX_VALUE;
			for (String value : map.values()) {
					String[] coordenadas = value.split(",");
					float[] distanceTime = TravellingTimeDistanceController.travelCalculationGMS(latitudeO, longitudeO, coordenadas[0], coordenadas[1], null);
					if (distanceTime[1] < min) {
							min = distanceTime[1];
					}
			}
			String result = new GeorefInfoToCalculation().transformValueToMatrixInfo(valueA, valueB, min);
			return "Time: " + df.format(min) + "min\n Result to scale: " + result;
	}
	public String transformValueToMatrixInfo(int valueA, int valueB, float min){
		if (min < valueA) {
				return "Baixa";
		}
		if (min > valueB) {
				return "Alta";
		}else {
				return "Média";
		}
}

# 5. Integration/Demonstration

Apenas é possível correr a gramática usando o antlr na linha de comandos fora do projeto.

# 6. Observações

Não foi possível implementar em java forma de correr a gramática.
