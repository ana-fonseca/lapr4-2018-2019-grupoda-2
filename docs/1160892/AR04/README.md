**Luís Silva [1160892](../)** - AR04
=======================================


# 1. Requisitos

**AR04** Como AR pretendo analisar um Pedido de Avaliação de Risco pendente de validação que esteja
a mim atribuído

- AR04.1. Os detalhes do Pedido de Avaliação de Risco devem ser apresentados.

- AR04.2. Permitir exportar a informação do Pedido de Avaliação de Risco em causa para um
documento XHTML permitindo aquando da visualização do documento colapsar
(esconder/mostrar) partes da informação (e.g. as justificações).

- AR04.4. Permitir que o AR confirme o(s) resultado(s) do Pedido de Avaliação de Risco,
podendo este acrescentar algum comentário/observação (opcional). O pedido deve ser dado
como concluído.

- AR04.5. Permitir que o AR atribua diretamente resultado(s) ao Pedido de Avaliação de Risco.
Neste caso, é obrigatório introduzir uma fundamentação. O pedido deve ser dado como
concluído.

- AR04.6. Permitir que o AR solicite uma reavaliação automática do Pedido de Avaliação de
Risco. Neste caso, é necessário que o AR altere ou remova alguma da informação que suporta
o(s) resultado(s) atual e/ou introduza novos dados (e.g. a existência de um quartel de
bombeiros) que devem ser tidos em consideração. Após reavaliação automática ter ocorrido,
o Pedido de Avaliação de Risco deve voltar a ser analisado pelo mesmo AR.

A interpretação feita deste requisito foi no sentido de ...

# 2. Análise

### Diagrama de Sequência de Sistema
![AR04 Diagrama Análise](AR04-SSD.png)

##### Main Actor

+ Analista de Risco(AR)

##### Interested parts and their interests

+ Analista de Risco : pretende tratar de um Pedido de Avaliação de Risco pendente de validação que esteja
a ele atribuído.

##### Pre conditions

+ Existência de Pedidos de Avaliação de Risco atribuídos ao analista.

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

![AR04 Diagrama Sequência](AR04-SD.png)

## 3.2. Diagrama de Classes

![AR04 Diagrama de Classes](AR04-CD.png)

## 3.3. Padrões Aplicados

* Padrão Controller
* Padrão Repositório
* Padrão Open/Close Principle

## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Exemplo do teste feito para confirmar resultado.

	@Test
	public void testConfirmResult() throws Exception {
			System.out.println("confirmResult");
			Address da = new Address(new Street("Rua Dr. Bernardino", "453"), new City("Porto"), new PostalCode("4100-150"), new Country("Portugal"), new District("Porto"));
			List<InsuranceCoverageConnection> iccList = new ArrayList<>();
			Insurance insu = new Insurance(da);
			Coverage cov = new Coverage("Fogo");
			Coverage cov1 = new Coverage("Tempestade");
			iccList.add(new InsuranceCoverageConnection(insu, cov));
			iccList.add(new InsuranceCoverageConnection(insu, cov1));
			Version v = new Version(30);
			Request request = new Request(iccList, "teste", State.AUTOMATIC, v);
			RequestAnalysisController instance = new RequestAnalysisController(request);
			instance.confirmResult();
	}

# 4. Implementação

	public void confirmResult() {
			request.setState(State.VALIDATED);
			request.changeDecisionDate();
			rdb.update(request);
	}

	public void AssignResult(List<Integer> index) {
        for (int i = 0; i < request.getResultList().size(); i++) {
            request.getResultList().get(i).assignIndex(index.get(i));
        }
        request.setState(State.VALIDATED);
        request.changeDecisionDate();
        rdb.update(request);
    }

		public boolean exportToHtml(String fileName, String requestInfo) throws IOException {
        File file = new File("..//" + fileName + ".html");

        //Create the file
        if (file.createNewFile()) {
            FileWriter writer = new FileWriter(file);
            writer.write(requestInfo);
            writer.close();
            return true;
        } else {
            System.out.println("File already exists.");
            return false;
    }

		public void AutomaticReassessment() {
        RiskAnalyst ra= request.obtainRiskAnalyst();
        request.setState(State.PENDING);
        request.assignRiskAnalyst(ra);
        request.changeDecisionDate();
        rdb.update(request);
    }

# 5. Integration/Demonstration

Para demonstrar o export do Pedido de Avaliação é necessário garantir que o ficheiro para o qual pretende exportar a informação não exista.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
