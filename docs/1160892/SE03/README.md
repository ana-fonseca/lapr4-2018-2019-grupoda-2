**Luís Silva [1160892](../)** - SE03
=======================================


# 1. Requisitos

**SG03** Como sistema externo pretendo obter o resultado dos Pedidos de Avaliação de Risco já concluídos que
satisfazem um determinado conjunto de critérios de filtragem.

- SE03.1. Suportar filtro para um determinado período de tempo.

- SE03.2. Suportar filtro para pedidos relativos a locais pertencentes a uma ou mais cidades.

- SE03.3. Suportar a combinação dos filtros anteriores.

A interpretação do caso de uso: O objetivo deste caso de uso é permitir uma análise dos dados mais intuitiva, para isso é necessário implementar alguns filtros, como apresentar a informação para apenas um determinado período de tempo e/ou para cidades específicas. Este caso de uso fica assim dependente da existência de resultados de pedidos de avaliação de risco já concluídos.  

# 2. Análise

### Diagrama de Sequência de Sistema
![SE03 Diagrama Análise](SE03-SSD.png)

##### Main Actor

+ Sistema Externo(SE)

##### Interested parts and their interests

+ Sistema Externo : pretende obter o resultado dos Pedidos de Avaliação de Risco já concluídos que
satisfazem um determinado conjunto de critérios de filtragem.

##### Pre conditions

+ Existência de Pedidos de Avaliação de Risco já concluídos.


# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

*Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade.*

![SE03 Diagrama Sequência](SE03-SD.png)

## 3.2. Diagrama de Classes

*Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade.*

![SE03 Diagrama de Classes](SE03-CD.png)

## 3.3. Padrões Aplicados

Padrão Repository

## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar a correta filtragem em relação ao tempo determinado período de tempo.

	@Test
	public void testFilterByDate() {
			System.out.println("filterByDate");
			InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("incendio"));
			InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("tempestade"));
			InsuranceCoverageConnection icc3 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("furacao"));
			InsuranceCoverageConnection icc4 = new InsuranceCoverageConnection(new Insurance(new Address(null, null, null)), new Coverage("inundacoes"));
			List<InsuranceCoverageConnection> iccList = new ArrayList<>();
			iccList.add(icc1);
			iccList.add(icc2);
			iccList.add(icc3);
			iccList.add(icc4);
			Request r =  new Request(iccList, "teste", State.PENDING, new Version());
			List<Request> rList = new ArrayList<>();
			rList.add(r);

			LocalDateTime inicio = LocalDateTime.MIN;
			LocalDateTime fim = LocalDateTime.MIN.plusHours(5);
			List<Request> expResult = new ArrayList<>();
			List<Request> result = FilterRiskAssessmentRequest.filterByDate(rList, inicio, fim);
			assertEquals(expResult, result);

			inicio = LocalDateTime.MIN;
			fim = LocalDateTime.MAX;
			expResult = new ArrayList<>();
			expResult.add(r);
			result = FilterRiskAssessmentRequest.filterByDate(rList, inicio, fim);
			assertEquals(expResult, result);
	}

**Teste 2:** Verificar a correta filtragem em relação ás cidades pedidas.

	@Test
	public void testFilterByLocation() {
			System.out.println("filterByLocation");
			InsuranceCoverageConnection icc1 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Porto"), null)), new Coverage("incendio"));
			InsuranceCoverageConnection icc2 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Aveiro"), null)), new Coverage("tempestade"));
			InsuranceCoverageConnection icc3 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Lisboa"), null)), new Coverage("furacao"));
			InsuranceCoverageConnection icc4 = new InsuranceCoverageConnection(new Insurance(new Address(null, new City("Coimbra"), null)), new Coverage("inundacoes"));
			List<InsuranceCoverageConnection> iccList1 = new ArrayList<>();
			List<InsuranceCoverageConnection> iccList2 = new ArrayList<>();
			iccList1.add(icc1);
			iccList1.add(icc2);
			iccList2.add(icc3);
			iccList2.add(icc4);
			Request r =  new Request(iccList1, "teste", State.PENDING, new Version());
			Request r1 =  new Request(iccList2, "teste", State.PENDING, new Version());
			List<Request> rList = new ArrayList<>();
			rList.add(r);
			rList.add(r1);

			List<City> locations = new ArrayList<>();
			List<Request> expResult = new ArrayList<>();
			List<Request> result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
			assertEquals(expResult, result);

			locations.add(new City("Porto"));
			expResult.add(r);
			result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
			assertEquals(expResult, result);

			locations.add(new City("Coimbra"));
			expResult.add(r1);
			result = FilterRiskAssessmentRequest.filterByLocation(rList, locations);
			assertEquals(expResult, result);
	}
# 4. Implementação

**Métodos Implementados para filtragem dos dados**

	public static List<Request> filterByDate(List<Request> r, LocalDateTime inicio, LocalDateTime fim){
			List<Request> requestFiltered = new ArrayList<>();
			for (Request request : r) {
					if (request.obtainRequestedDate().isAfter(inicio) && request.obtainRequestedDate().isBefore(fim)) {
							requestFiltered.add(request);
					}
			}
			return requestFiltered;
	}

	public static List<Request> filterByLocation(List<Request> r, List<City> locations){
			List<Request> requestFiltered = new ArrayList<>();
			for (int i = 0; i < r.size(); i++) {
					for (int j = 0; j < r.get(i).getInsuranceCoverageConnectionList().size(); j++) {
							if (locations.contains(r.get(i).getInsuranceCoverageConnectionList().get(j).obtainInsurance().getAddress().getCity())) {
									requestFiltered.add(r.get(i));
							}
					}
			}
			return requestFiltered;
	}

# 5. Integration/Demonstration

Para demonstrar o funcionamento do sistema externos usamos a aplicação Postman que funciona como cliente de HTTP, colocando os critérios de filtragem pretendidos no body.

**Exemplo**

	{
    "DataInicio": "2000-01-23 14:12",
    "DataFim": "2010-12-11 13:15",
    "pair": [
        	{
            	"Cidade1": "Porto"
        	},
        	{
            	"Cidade2": "Lisboa"
        	}
    	]
	}

# 6. Observações

Neste projeto estão disponível apenas a leitura dos dados introduzidos em certas linguagens(Json), no  futuro poderá ser implementado novas leituras para outras linguagens. Poderá também ser necessário implementar métodos que satisfaçam novos critérios de filtragem.
