**Ana Fonseca [1170656](../)** - SE07
=======================================


# 1. Requisitos


**SE07** Como Sistema Externo pretendo pbter um relatório estatístico da atividade realizada pelo Serviço de Avaliação de Risco desde a última vez que foi colocado em execução.

**Interpretação do Caso de Uso:** Por não estar especificado, após uma discussão de Grupo, decidimos colocar no projeto as seguintes informações: Hora do início de execução e o tempo passado desde executar o Serviço Externo, quantas consultas foram feitas e quantos pedidos foram processados. Este Caso de Uso depende da comunicação a partir de um chat e da comparação de avaliação de risco.

# 2. Análise

**System Sequence Diagram**

![SE07_SSD](SE07_SSD.png)


# 3. Design


## 3.1. Realização da Funcionalidade

Este caso de Uso (no Postman) é iniciado utilizando um "GET" e o id "/report" e lido no HttpChatRequest, de seguida, e de acordo com o tipo de output pretendido chamamos o Service, sendo as alternativas: ServiceJSON, ServiceXML e ServiceXHTML. Depois de entrar numa destas classes, e passando as informações importantes por parâmetro, transformamos estes atributos, de forma organizada, no tipo de output pretendido. E devolvemos ao utilizador ao dar setTextContent deste retorno.

## 3.2. Diagrama de Classes

![SE07_SD](SE07_SD.png)

## 3.3. Padrões Aplicados

**High Cohesion**
O padrão tenta manter os objetos adequadamente focado, gerenciado e compreensível.

**Low Coupling**
O padrão determina como atribuir responsabilidades para apoiar.

## 3.4. Testes

**XML:** O xml é verificado com o XSD feito para o caso. As verificações são de estrutura, estando, neste caso, report com o número dos casos e um complexType time com dois elementos simples de tempo. Estes são recebidos como Strings e por isso, verificamos a estrutura desta informação.

![testeXML](testeXML.png)

# 4. Implementação

**Receber Informação do Tipo de Output no HttpChatRequest**

![implementacao](implementacao.png)

# 5. Integration/Demonstration

Este caso de Uso precisava apenas do HttpChatRequest e Server para conseguir comunicar com o postman, sendo que este Caso de Uso não recebe qualquer tipo de informação via input de ficheiros (JSON, XML, XHTML), logo apenas precisa de informações de Classe que são obtidas: mal o programa corre (horas de execução), cálculo (tempo que passou desde que foi executado) e de classe (sempre que é feita uma consulta ou um pedido de processamento de pedido).

# 6. Observações

Num projeto futuro, seria mais correto começar por analisar os requisitos em vez começar logo a por em prática. Podendo melhorar no sentido dos Services (Json e XML) porque estes deviam implementar uma interface de leitura e outra interface para escrita.




