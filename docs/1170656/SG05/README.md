**Ana Fonseca [1170656](../)** - SG05
=======================================


# 1. Requisitos

**SG05** Como SG pretendo obter a distância (em quilómetros) e tempo de viagem (em minutos) entre duas localizações tendo em consideração a forma de deslocação (e.g. de automóvel, a pé).

- SG05.1. Esta informação deve ser obtida usando o serviço externo MBS.

- SG05.2. Esta informação deve ser obtida usando o serviço externo GMS.

- SG05.3. Esta informação deve ser obtida por combinação (i.e. distância e tempo médio) dos resultados dos serviços externos anteriores. 

- SG05.4. O sistema deve usar o método anterior que estiver configurado para o efeito.

**Interpretação do Caso de Uso**: De acordo com o enunciado, pretendemos um método que, utilizando MBS e GMS consiga o tempo e distância entre dois locais tendo em consideração o mode de viagem (carro, a pé ou de bicicleta). Decidimos que o modo default de viagem seria o de carro porque correspondia com as APIs.
Com a API da Google, os resultados têm conta o tráfego automóvel no momento por isso os resultados, para os mesmos valores, podem variar.

# 2. Análise

![SG05_SSD](SG05_SSD.png)

# 3. Design

## 3.1. Realização da Funcionalidade

![SG05_SD](SG05_SD.png)


## 3.3. Padrões Aplicados

**High Cohesion**:
 O padrão tenta manter os objetos adequadamente focados e compreensíveis.

**Low Coupling**:
 O padrão determina como atribuir responsabilidades eficientemente.

**Solid**
 Interface Segregation Principle

**Controller**
 O padrão Controller atribui as responsabilidades de receber e lidar com um evento do sistema para uma classe que representa o UC.

## 3.4. Testes 

Para afetos de demonstração do funcionamento do Caso de Uso, porque o resultado tem em consideração variáveis inconstantes como o tempo e o trânsito não é possível efetuar testes, para compensar este facto iremos demonstrar utilizando variáveis gerais conhecidas pela maioria utilizando a interface gráfica.

# 4. Implementação

![implementacao](implementacao1.PNG)

Método do Controller que chama o método da Google:

![gms](gms.PNG)

Método do Controller que chama o método da Microsoft:

![bms](bms.PNG)

Método do Controller que chama a average das duas APIs:

![avg](avg.PNG)


# 5. Integration/Demonstration

Esta funcionalidade é feita para um sistema externo, por isso não tem conhecimento das classes do dominio. Dessa forma, esta funcionalidade foi feita a pensar na passagem dos valores de dados simples (neste caso String) em objetos do dominio. O seguimento lógico é o controller receber um pedido dos envolventes e este vai invocar o metodo presente o qual vai usar vários serviço de procura que implementam a interface, que vai fazer a junção dos resultados.

# 6. Observações

Como explicado, o Caso de Uso é quase que impossível de testar, porque os resultados, por dependerem de valores como trânsito e hora do dia, são incostantes. Dessa forma, para testar, criámos uma interface onde poderão testar com valores conhecidos, como as coordenadas do ISEP.


