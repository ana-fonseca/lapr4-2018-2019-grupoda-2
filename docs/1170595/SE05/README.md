﻿**Álvaro Dória [1170595](../)** - SE05
=======================================


# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*

*Exemplo*

**SE05** Como SE pretendo submeter de uma só vez um conjunto de Pedidos de Avaliação de Risco que
devem ser tratados de forma conjunta.

*Criterio de aceitação*
O processamento destas solicitações deve ocorrer com uma prioridade máxima
relativamente ao processamento das restantes solicitações. 

Para este UC, foi interpretado que iria ser feita a reutilização dos metodos criados no SE01 de forma a se poder submeter em vez de um so pedido varios ao mesmo tempo, como todos os metodos necessarios para o nivel 2 não estava criados foram criados na realização deste UC, este UC envia respostas em JSON, XML e XHTML e tambem lê ficheiros JSON e XML.

# 2. Análise

![SE05_SSD](SSD_21.png)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

![SE05_SD](SD_21.png)

## 3.2. Diagrama de Classes

![SE05_CD](CD_21.png)

## 3.3. Padrões Aplicados

Padrão controller
High Cohesion 
Low Coupling

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação
A implementação deste UC foi feito como descrito no diagram de sequencias para verificar-se isso mesmo encontra-se o codigo abaixo

 public String submitMultRequests(String response, String returnType) throws Exception {
        if (response.isEmpty()) {
            return "";
        }
        List<Request> listR = new ArrayList<>();
        response = response.trim();
        if (response.startsWith("<?xml")) {
            listR = ServiceXML.readSubmitRequestsXML(response);
        } else {
            listR = ServiceJSON.readRequestList(response);
        }
        Thread[] thrs = new Thread[listR.size()];
        int[] counter = {0};
        for (int i = 0; i < listR.size(); i++) {
            Request r = listR.get(i);
            thrs[i] = new Thread(() -> {
                try {
                    if (submit(r)) {
                        counter[0]++;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(SubmitMultipleRequestsController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thrs[i].setPriority(Thread.MIN_PRIORITY);
        }
        
        for (Thread thread : thrs) {
            thread.start();
        }
        
        for (Thread thread : thrs) {
            thread.join();
        } 
        
        return returnTypeChooser(returnType, listR.size(), counter[0]);
    }

    private boolean submit(Request r) throws ParseException, IOException, ParserConfigurationException, TransformerException, SAXException, Exception {
        SubmitRiskAssessment sra = new SubmitRiskAssessment();
        SubmitMultipleRequestControllerTT smrc = new SubmitMultipleRequestControllerTT();
        return smrc.submitOneRequest(r);
    }
    
    private String returnTypeChooser(String returnType, int numMax, int submitted) throws Exception{
         switch (returnType) {
            case "xml":
                return ServiceXML.writeRequestsSubmissionMessageXML(numMax, submitted);
            case "json":
                return ServiceJSON.writeSubmitRequestsJSON(numMax, numMax);
            case "xhtml":
                return ServiceXHTML.writeXHTMLSubmitRequests(numMax, submitted);
        }
        return "Success";
    }


# 5. Integration/Demonstration
No nosso projeto nos temos o package presentation que é responsável por fazer a ligação com as restantes partes do projeto, sempre que um caso de uso é terminado na presentation criam-se os metodos necessarios para que acha comunicação entre este e os de mais.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



