﻿**Alvaro Doria [1170595](../)** - SG04
=======================================


# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*


**SG04** Como SG pretendo obter a elevação (em metros) relativamente ao nível médio da água do mar
de um determinado local.

- SG04.1 Esta informação deve ser obtida usando o serviço externo MBS

- SG02.2 Esta informação deve ser obtida usando o serviço externo GMS

- SG02.3 Esta informação deve ser obtida por combinação (i.e. valor médio) dos resultados dos
serviços externos anteriores.

- SG02.4 O sistema deve usar o método anterior que estiver configurado para o efeito.

Neste requesito foi necessario a integração das apis da google e da microsoft para conseguirmos implementar as funcionalidades propostas, o quarto requisito foi escolhida a gms como a predefinida pois após varios testes verificou-se ter os resultados mais proximos da realidade e mais completos

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

![Get Elavation relative to sea](SSD_6.png)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

![Get Elavation relative to sea](SD_6.png)

## 3.2. Diagrama de Classes

![Get Elavation relative to sea](CD_6.png)

## 3.3. Padrões Aplicados

*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

@Test
    public void ElevationRequestGMSTest() {
        String latitude = "48.2";
        String longitude = "21.3";
        GMSRequest gms = new GMSRequest();
        float result = gms.elevationSearch(latitude, longitude);
        DecimalFormat df = new DecimalFormat("00.000");
        assertEquals(df.format(203.364), df.format(result));
    }

    @Test
    public void ElevationRequestBMSTest() {
        String latitude = "48.2";
        String longitude = "21.3";
        BMSRequest bms = new BMSRequest();
        float result = bms.elevationSearch(latitude, longitude);
        assertEquals(40, result, 0.0001);
    }

# 4. Implementação

Neste caso de uso foi acrescentado as classes BMSRequest e GMSRequest os metodos necessarios para a implementação da elevação com base o nivel medio das aguas do mar adicionado com o calculo da media entre as duas aplicações para que consigamos ver um intermedio entre ambos os resultados


# 5. Integration/Demonstration

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

O caso de uso foi feito com base nos casos de uso da georeferenciação anteriormente implementados ainda houve uma discusão de grupo para decidir qual seria a api que iria ser a nossa default e através do que foi observado por testes chegou-se ao acordo de ser utilizada a api da google



