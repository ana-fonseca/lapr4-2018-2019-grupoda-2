﻿# Projeto de LAPR4 2018-19 do GrupoDA-2#


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **[1170595](/docs/1170595/)** | Álvaro Dória |
| **[1170656](/docs/1170656/)** | 	Ana Fonseca |
| **[1151422](/docs/1151422/)** | Bruno Rodrigues |
| **[1160892](/docs/1160892/)** | Luís Silva |
| **[1170620](/docs/1170620/)** | Luís Moreira |
| **[1170842](/docs/1170842/)** | 	Maria Junqueira |
| **[1170426](/docs/1170426/)** | 	Tiago Ribeiro |


# 2. Distribuição de Funcionalidades ###

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

| Aluno Nr.	| Semana 1 | Semana 2 | Semana 3 |
|------------|----------|----------|----------|
| [**1170595**](/docs/1170595/)| [AR01](/docs/1170595/AR01)| [SG04](/docs/1170595/SG04)| [USDemo3](/docs/1170595/USDemo3) |
| [**1170656**](/docs/1170656/)| [AR03](/docs/1170656/AR03)| [SG05](/docs/1170656/SG05)| [SE07](/docs/1170656/SE07) |
| [**1151422**](/docs/1151422/)| [SG01](/docs/1151422/SG01)| [SE02](/docs/1151422/SE02)| [USDemo3](/docs/1151422/USDemo3) |
| [**1160892**](/docs/1160892/)| [SE03](/docs/1160892/SE03)| [AR04](/docs/1160892/AR04)| [SG07](/docs/1160892/SG07) |
| [**1170620**](/docs/1170620/)| [SG03](/docs/1170620/SG03)| [SE04](/docs/1170620/SE04) | [AR07](/docs/1170620/AR07) |
| [**1170842**](/docs/1170842/)| [SE01](/docs/1170842/SE01)| [AR05](/docs/1170842/AR05)| [USDemo3](/docs/1170842/USDemo3) |
| [**1170426**](/docs/1170426/)| [AR02](/docs/1170426/AR02)| [SG01+SG02](/docs/1170426/SG01+SG02)| [SE06](/docs/1170426/SE06) |



